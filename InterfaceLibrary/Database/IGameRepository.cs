﻿using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface IGameRepository
    {
        void RegisterGamePlayer(int gameId, int playerId, int color);
        void SaveGamePlayer(IGamePlayer pl);
        void GameBet(int roomId, int gameId, IPlayerInfo player, decimal winAmount, int operationType);
        void EndGame(int gameId, int winnerId, int opponent_id, decimal winAmount, int win_type);
        void CancelGame(int gameId);
        int CreateGame(int roomId);
        int NewRound(int tableId);

        bool RegisterRoundPlayer(int roundId, int playerId);
        void SaveRoundPlayer(IRoundPlayer pl);
        void PlayerConnectionChanged(int roomId, int gameId, int playerId, bool connected);
        bool SaveGameState(List<IMove> undoList, IRoundPlayer pl, IDice dice);
        void EndRound(int roundId, int winnerId, int winType);
        decimal GameFinishCoins(IPlayerInfo info, int gameId, decimal betAmount, int operation, int roomId);

    }
}
