﻿using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface IPlayerRepository
    {
        int GetPlayerId(Guid token, bool expired);
        void RankIncrease(IPlayerInfo player);
        decimal GetPlayerBalance(int playerId);
        bool UpdateCreateRoomProperty();


        List<IMerchant> GetAllMerchants();
        void UpdateMerchant(IMerchant merchant);
        void GetCantCreateRoomMessage();
        void GetStatusPoints(IStatuses lobby);
        IPlayerInfo GetPlayerInfo(int playerId);
        void ChangeAvatar(int playerId, int avatarId);
        void ChangeStoneColor(int playerId, int stoneColor);
    }
}
