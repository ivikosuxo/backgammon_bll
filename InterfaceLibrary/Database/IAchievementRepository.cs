﻿using InterfaceLibrary.Achievements;
using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface IAchievementRepository
    {
        List<IAchievement> UpdateAchievementsList();
        void GetAchievementTranslations(IAchievement achievement);
        IAchievement GetAchievementById(int achievement_id);
        void DeactivateAchievement(int achievement_id);
        List<IPlayerAchievement> UpdatePlayerAchievementsList(int playerId);
        bool PlayerAchievementIncrease(IPlayerInfo info, int achievementId, int count, int maxCount);
        void PlayerAchievementClaim(int playerId, int achievementId);
    }
}
