﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
  public  interface IHistoryRepository
    {
         JArray GetMoves(int roundId);
         JObject GetRoundInfo(int roundId);
         JArray GetAllGamesInfo(int playerId);
         JArray GetGamePlayers(int gameId, int plyaerId);
         JArray GetRounds(int gameId);
         JObject GetGameInfo(int game_Id);
       
    }
}
