﻿using InterfaceLibrary.Game;
using InterfaceLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface IRoomRepository
    {
        int CreateRoom(ICreateRoomModel model); 
        void BlockBalance(IPlayerInfo player, int roomId, decimal maxbet, decimal minBet);
        decimal UnblockAmount(int playerId, int roomId);
        void RoomDeactivate(int roomId);
        void SaveChatMessage(int roomId, int playerId, int gameId, string message);
        bool ProblemReport(int roomId, int gameId, int roundId, string token, string problemText);
    }
}
