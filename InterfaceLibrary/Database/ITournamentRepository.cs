﻿using InterfaceLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface ITournamentRepository
    {
        ITournament GetTournamentById(int tournamentId);
        List<decimal> GetTournamentPrizePool(int tournamentId);
        void GetTournamentNames(ITournament tournament);
        void RegisterTournamentPlayer(ITournament tournament, int playerId);

        void BlockTournamentFee(ITournament tournament, int playerId);
        List<int> GetRegisteredPlayers(ITournament tournament);
        List<ITournamentPlayer> GetRegisteredTournamentPlayers(List<int> playersId, int tourId);
        void UnBlockTournamentFee(ITournament tournament, int playerId);
        void TournamentTransaction(int tournamentId, int playerId, decimal amount, int operationType, long mainTransactionId);
        void UpdateTournamentStatus(int tournamentId, int status);
        void GivePlayerWinAmount(ITournament tournament);
        void UnblockAmountForAllPlayers(ITournament tournament);
        void TournamentBalanceReset(ITournament tournament);
        void UnblockAmountForSomePlayers(List<int> players, ITournament tournament);
        void RemovePlayerFromTournament(int playerId, int tournamentId);
        int CreateTournamentRoom(ITournament tournament, int speed, int type);
        void CloseTournament(int tournamentId);
        void OpenTournament(int tournamentId);
        List<int> GetTournnamentList();
    }
}
