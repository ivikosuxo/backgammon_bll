﻿using InterfaceLibrary.Game;
using InterfaceLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Database
{
    public interface IShopRepository
    {
        IShopProduct GetShopProduct(int productId);
        List<ICategoryTranslate> GetCategories();
        List<IShopProduct> GetShopProducts();
        void GetProductNames(IShopProduct product);
        void GetCategoryNames(ICategoryTranslate product);
        void GetOrderProductNames(IOrderedProduct product);
        List<IOrderedProduct> GetOrderedProducts();
        List<IOrderedProduct> GetOrderedProducts(int playerId);
        void BuyProduct(IPlayerInfo playerId, IShopProduct product, int count);
    }
}
