﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Interfaces
{
    public interface IMessageBroker
    {
        void SendMessage(string sessionId, string data);
        void BroadCastAll(string data);
        void SendMessageToSomeUsers(List<string> users,string data);
    }
}
