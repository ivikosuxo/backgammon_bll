﻿using InterfaceLibrary.Achievements;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Models;
using log4net;

namespace InterfaceLibrary.Factory
{
    public interface IFactory
    {
        ILog Logger { get; set; }
        ITournament GetTournamentById(int id);
        IGameException GetExceptionById(int id);
        IAchievement GetAchievementById(AchievementType type);

        
    }
}
