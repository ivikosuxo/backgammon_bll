﻿using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Factory
{
    public interface ILobby : IStatuses
    {


        bool AddUser(IPlayer user, string session, JObject jdata);
        void SendMessageAll(string command, JObject data);
        void ServerPlayersCountChange();
        void UpdateCoins(int playerId, decimal coinCount);
        void UpdateBalance(int playerId);
        void SendMessage(int userId, string command, JObject data);
        void UpdateBalance(int playerId, decimal balance);
        void UpdateStatuses();
        JObject GetRoomInfo();
        void CountChange();
    }
}
