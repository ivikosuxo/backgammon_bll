﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Enums
{
    public enum TournamentState
    {
        Waiting= 1, OnGoing, Finished, Declined
    }
}
