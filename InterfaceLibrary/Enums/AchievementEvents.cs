﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Enums
{
   public enum AchievementEvents
    {
        Roll = 1, MoveSubmit, EndRound,EndGame,TournamentFinish, TournamentWin,TournamentTopPlace
    }
}
