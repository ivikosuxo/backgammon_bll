﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Enums
{
   public enum AchievementType
    {
        Roll=1,WinRound, LoseRound, WinGame, LoseGame,TournamentWin,TournamentPlay,Mars,DoubleMars, RoyalDeffence, StoneKiller, PlayGame, KillStone, TournamentTopPlace
    }
}
