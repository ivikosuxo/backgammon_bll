﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
    public interface IMove
    {
         bool Roll
        {
            get; set;
        } 
         int StartPosition { get; set; }  
         int EndPosition { get; set; }  
         int OpKill { get; set; }  
         int MyKill { get; set; }  

         int OpDiscard { get; set; } 
         int MyDiscard { get; set; } 
         bool Killer { get; set; }  
         int OpponentState { get; set; }  
         int State { get; set; } 
         int Value { get; set; }  
         int Action { get; set; } 
         bool Winner { get; set; }  
         string BoardState { get; set; } 
         int FreezeCount { get; set; } 
         bool IsRoyalDefense { get; set; }  
         bool Blocked { get; set; }  

         bool FirstRoll { get; set; } 

         int RaiseX
        {
            get; set;
        }

        JObject ToJson();
    }
}
