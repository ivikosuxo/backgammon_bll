﻿ 
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Classes.GameClass
{
    public class TournamentPlayer : ITournamentPlayer
    {
        private static Random Random { get; set; } = new Random();

        public TournamentPlayer( int player_id,string username,int tourId) {
            Username = username;
            PlayerId = player_id;
            TournamentId = tourId;
            RandomIndex = Random.Next(5000);
        }

        public int Index { get; set; } = 0;
        public int PlayerId { get; set; }
        public bool IsActive { get; set; } = true;
        public string Username { get; set; }
        public int TournamentId { get; set; }
        public int TournamentLevel { get; set; } = 0;
        public decimal TournamentFee { get; set; } = 0m;
        public int CoinFee { get; set; } = 0;
        public int RandomIndex { get; set; } = 0;

        public decimal Prize { get; set; } = 0;

        public JObject ToJson() {

            var data = new JObject();
            data["id"] =PlayerId;
            data["username"] = Username;
            data["prize"] = Prize; 

            return data;
        }
    }
}
