﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
   public  interface IPlayer
    {
        IPlayerInfo Info { get; set; }
        long LastActiveDate { get; set; }
        decimal GetCurrentBalance();
        int GetPlayerId();
        void Ping();
        JObject ToJson();
    }
}
