﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
    public interface IRoom
    {
        int RoomId { get; set; }
        int TournamentId { get; set; }

        bool IsReconnect(int playerId);
        bool IsGameState(bool tournamentGames);
        string GetRoomName();
        JObject GetOpponentInfo(int playerId);
        void AddRoomPlayer(IPlayerInfo info, string connection, int roomId);
        void RaisePress(int playerId, bool accept);
        void RaiseAccept(int playerId, JObject data);
        int GetGameId();
        void PlayAgain(int playerId);
        void EndGame(int winnerId);
        bool Reconnect(IPlayerInfo pl, string session);
        bool IsSameNetwork(int merchantId);
        void AddUserWaitingList(IPlayerInfo user, string session, JObject jdata);
        void DisconnectWaiter(int playerId);
        void AcceptOpponent(string session, JObject jdata);
        void DeclineOpponent(string session, JObject jdata);
        void AddUser(IPlayerInfo user, string session, JObject jdata);
 
        void Undo(int playerId, JObject data);
        void Submit(int playerId, JObject data);
        void GiveUp(int playerId, JObject data);
        void Play(int playerId, JObject data);
        void PlaySeveralMoves(int playerId, JObject data);
        void Roll(int playerId);
        void StartGame();
        void SendMessage(int userId, string command, JObject message);
        void SendMessageAll(string command, JObject message);
        void CountChange();
        bool RemoveUser(int userId);
        void Disconnect(int playerId);
        void LogPlayerSatusChange(int playerId, bool connected);
        void Destroy();
        JObject GetRoomInfo();
    }
}
