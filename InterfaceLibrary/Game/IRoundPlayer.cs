﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
    public interface IRoundPlayer
    {
        int RoundId { get; set; }
        int PlayerId { get; set; }
        int RoundPoint { get; set; }
    }
}
