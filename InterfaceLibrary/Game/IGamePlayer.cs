﻿using InterfaceLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
   public interface IGamePlayer
    {
        int GameId { get; set; }
        int PlayerId { get; set; }
        int GamePoint { get; set; }
        bool IsConnected { get; set; }
        bool IsWinner { get; set; }
        string ConnectionIP { get; set; }
        DeviceType Device { get; set; }
    }
}
