﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
   public interface IDice
    {
        int First { get; set; }
        int Second { get; set; }
        bool IsDouble { get; set; }
    }
}
