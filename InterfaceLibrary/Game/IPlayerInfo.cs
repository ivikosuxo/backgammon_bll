﻿using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
    public interface IPlayerInfo
    {
        int PlayerId { get; set; }
        string Username { get; set; }
        decimal Balance { get; set; }
        bool IsBlocked { get; set; }
        decimal Coin { get; set; }
        int AvatarId { get; set; }
        int Rank { get; set; }
        bool ChatBan { get; set; }
        int MerchantId { get; set; }
        bool IsNetwork { get; set; }

        int GameCount { get; set; }

        bool IsVerified { get; set; }

        int StoneColor { get; set; }
        List<IPlayerAchievement> Achievements { get; set; }
        List<IOrderedProduct> OrderedProducts { get; set; }
       // decimal GetCurrentBalance();
        IPlayerAchievement GetAchievement(int achievementId);
        JObject GetInfo();
        JArray GetAchievementsToJson();
    }
}
