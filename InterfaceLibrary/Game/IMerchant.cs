﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
   public interface IMerchant
    {
        int MerchantId { get; set; }
        string MerchantName { get; set; }
        decimal RakePercent { get; set; }
        int CurrencyId { get; set; }
        bool IsNetwork { get; set; }
   

    }
}
