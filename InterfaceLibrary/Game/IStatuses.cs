﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Game
{
    public interface IStatuses
    {
        List<int> StatusPoints { get; set; }
        List<int> StatusGameCounts { get; set; }
    }
}
