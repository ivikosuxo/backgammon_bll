﻿using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Achievements
{
    public interface IAchievement
    {
        int AchievementId { get; set; }
        int BlockGroup { get; set; }
        string AchievementTitle { get; set; }
        string AchievementTitleRus { get; set; }
        string AchievementTitleGeo { get; set; }

        string AchievementDescription { get; set; }
        string AchievementDescriptionGeo { get; set; }
        string AchievementDescriptionRus { get; set; }
        int MinRank { get; set; }

        int Count { get; set; }
        int CoinCount { get; set; }
        bool IsActive { get; set; }

        bool CalculateAchievement(IPlayerInfo info, JObject data);
        void Execute(IPlayerInfo info, int roomId, JObject data);
        int IncreasePlayerAchievement(IPlayerAchievement achievement, JObject data);
        IPlayerAchievement CreatePlayerAchievement(IPlayerInfo info);
        void Openachievement(int playerId, int roomId);
        void SetDices(int dice1, int dice2);

        bool IsValid(int rank, AchievementEvents startEvent);
        JObject ToJson();
    }
}
