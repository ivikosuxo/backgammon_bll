﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary
{
    public interface IPlayerAchievement
    {
        int AchievementId { get; set; }
        int Count { get; set; } 
        int FullCount { get; set; }
        bool Complete { get; set; } 
        bool IsActive { get; set; }
        bool Claimed { get; set; }
        JObject ToJson();
    }
}
