﻿using InterfaceLibrary.Database;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;

namespace InterfaceLibrary.Manager
{
   public interface IShopManager
    {


        IShopRepository ShopDBManager { get; set; }
        void Initialize();
        bool GetShopProduct(int productId);
        bool DeactivateShopProduct(int productId);
        IShopProduct GetProductById(int productId);
        void BuyProduct(string session, int playerId, int count, int productId);
        JObject ToJson();
       
    }
}
