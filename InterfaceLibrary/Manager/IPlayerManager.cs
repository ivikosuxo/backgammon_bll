﻿using InterfaceLibrary.Database;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Manager
{
    public interface IPlayerManager
    {


        int PlayersCount();
        IPlayerRepository PlayerRepository { get; set; }
        void Login(string session, JObject json);
        List<int> GetActivePlayers();
        IPlayer GetPlayerById(int playerId);
        void ClearPlayers();
        bool BlockPlayer(int playerId);
        bool UnblockPlayer(int playerId);
        bool PlayerExists(int playerId);
        IPlayerInfo GetPlayerInfoById(int playerId);
        JObject GetPlayerOrderProducts(int playerId);
        bool RemovePlayer(int playerId);
        decimal GetPlayerBalance(int playerId);
        IPlayer PlayerLogin(int playerId);
        void PlayerJoin(IPlayerInfo pl);
        IPlayer PlayerLogin(string tkn);
        int GetPlayerIDByToken(string tkn);
        int GetConnectedPlayerIdByToken(string tokenString);
        void ChangeAvatar(int playerId, int avatarId);
        void ChangeStone(int playerId, int stoneColor);

        bool IsSameNetwork(int firstMerchantId, int secondMerchantId, bool network);
        void initialize();
    }
}
