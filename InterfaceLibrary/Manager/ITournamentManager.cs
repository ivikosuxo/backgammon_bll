﻿using InterfaceLibrary.Database;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Manager
{
   public interface ITournamentManager
    {
        List<ITournament> ActiveTournaments { get; set; }
        ITournamentRepository TournamentDbInstance { get; set; }
        void Initialize();
        void CleanTournament();
        void OrderTournaments();
        JObject GetTournamentsInfo(int playerId);
        bool ChangeTournamentName(int tournamentId);
        bool ActivateTournament(int tournamentId);
        ITournament GetTournamentById(int tournamentId);
        bool CancelTournament(int tournamentId, int errorId);
        bool DeleteTournament(int tournamentId);
    }
}
