﻿using InterfaceLibrary.Database;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Game;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Manager
{
    public interface IRoomManager
    {
        Dictionary<string, IRoom> Rooms { get; set; }
        ILobby LobbyRoom { get; set; }

        IRoomRepository RoomDbManager { get; set; }
        void RemoveRoom(int roomId);
        void AddRoom(int roomId, IRoom room);
        void Initialize();
        JArray GetRoomsInfo();
        JObject CreateRoom(ICreateRoomModel data);
        void SaveChatMessage(int roomId, int playerId, int gameId, string message);

        IRoom GetRoomById(int roomId);
    }
}
