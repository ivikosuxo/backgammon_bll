﻿using InterfaceLibrary.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Manager
{
    public interface IHistoryManager
    {
        IHistoryRepository HistoryDbInstance  {get;set;}
        JObject GetRoundInfo(int roundId);
        JObject GetGameInfo(string token, int gameId);
    }
}
