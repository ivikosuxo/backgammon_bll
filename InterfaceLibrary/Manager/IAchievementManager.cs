﻿using InterfaceLibrary.Achievements;
using InterfaceLibrary.Database;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Manager
{
    public interface IAchievementManager
    {
        List<IAchievement> Achievements { get; set; } 
        IAchievementRepository AchievementRepositoryInstance { get; set; }

        void Initialize();
        void UpdateAchievements();
        void CalculateAchievement(int playerId, int roomId, AchievementEvents startEvent, JObject data);
        bool ActivateAchievement(int achievementId);
        bool DeactivateAchievement(int achievementId);
        bool PlayerAchievementIncrease(IPlayerInfo info, int achievementId, int count, int maxCount);
        JArray AchievementsToJson(int playerRank);
    }
}
