﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
    public interface IShopProduct
    {
        int Id { get; set; }
        int Count
        {
            get; set;
        }

        int StockCount
        {
            get; set;
        }

        int CoinPrice
        {
            get; set;
        }

        string Name
        {
            get; set;
        }

        string NameGeo
        {
            get; set;
        }

        string NameRus
        {
            get; set;
        }
        string Description
        {
            get; set;
        }
        string DescriptionGeo
        {
            get; set;
        }
        string DescriptionRus
        {
            get; set;
        }
        int Category { get; set; }
        string ImgUrl
        {
            get; set;
        }
        int MinRank
        {
            get; set;
        }

        bool IsActive
        {
            get; set;
        }
        bool IsDeleted
        {
            get; set;
        }

        JObject ToJson();
       }
}
