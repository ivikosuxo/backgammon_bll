﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
    public interface ICategoryTranslate
    {
          int Id { get; set; }
          string Name { get; set; }
          string NameGeo { get; set; }
          string NameRus { get; set; }

        JObject ToJson();
    }
}
