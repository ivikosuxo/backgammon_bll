﻿using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using System.Threading.Tasks;
using System.Timers;

namespace InterfaceLibrary.Models
{
    public interface ITournament
    {
        int MerchantId { get; set; }
        bool IsNetwork { get; set; }
        string TournamentName
        {
            get; set;
        }
        int TournamentId
        {
            get; set;
        }

        string TournamentNameRus
        {
            get; set;
        }
        string TournamentNameGeo
        {
            get; set;
        }

        int FinalPoint { get; set; }
        int MaxCount { get; set; }
        int MinimalCount { get; set; }
        decimal TournamentFee { get; set; }
        decimal TournamentPrize { get; set; }
        int TournamentDepth { get; set; }
        bool IsActive
        {
            get; set;
        }

        DateTime FinishTime
        {
            get; set;
        }
        DateTime StartDate
        {
            get; set;
        }

        Timer TournamentTimer
        {
            get; set;
        }

        Timer TournamentStartTimer { get; set; }

        List<decimal> PrizeList { get; set; }

        TournamentType TournamentType { get; set; }
        TournamentState State { get; set; }
         

          List<ITournamentPlayer> TopPlayers { get; set; } 

          List<List<ITournamentPlayer>> TournamentLevelPlayers { get; set; }  

          List<ITournamentPlayer> TournamentPlayers { get; set; }


        void Activate();
        void SetGameType(int type);
        void DeleteTournament();
        void Finish(int winnerId);
        void CancelTournament(int reason);
        void TournamentStatusChange();
        void FirstMatchMacking();
        void MatchMacking();
        void RemoveTournament();
        void AddPlayerToTournament(IPlayerInfo info);
         JObject GetFlow();
        void TournamentGameWin(int winnner);

        void RemovePlayerFromTournament(int plId, bool manual);
        JObject ToJson(int id);

    }
}
