﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
   public interface IGameException
    {
          bool Fatal { get; set; }  
          int ErrorId { get; set; } 
          void Throw();
    }
}
