﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
    public interface ITournamentPlayer
    {
        int Index { get; set; }
        int PlayerId { get; set; }
        bool IsActive { get; set; }
        string Username { get; set; }
        int TournamentId { get; set; }
        int TournamentLevel { get; set; }
        decimal TournamentFee { get; set; }
        int CoinFee { get; set; }
        int RandomIndex { get; set; }

        decimal Prize { get; set; }

        JObject ToJson();

    }
}