﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
    public interface IOrderedProduct
    {
          int Id
        {
            get; set;
        }
          int PlayerId
        {
            get; set;
        }
          int ProductId
        {
            get; set;
        }

          string Name { get; set; }
          string NameGeo { get; set; }
          string NameRus { get; set; }

          int Count
        {
            get; set;
        }

          decimal Price
        {
            get; set;
        }
          string ImgUrl
        {
            get; set;
        }
          long TransactionId
        {
            get; set;
        }
        int OrderStatusValue
        {
            get; set;
        }

          string Comment
        {
            get; set;
        }
          DateTime OrderTime
        {
            get; set;
        }

        JObject ToJson();

    }
}
