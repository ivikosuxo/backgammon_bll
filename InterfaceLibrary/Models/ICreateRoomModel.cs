﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLibrary.Models
{
  public  interface ICreateRoomModel
    {
         string Roomname
        {
            get; set;
        }

         string Token
        {
            get; set;
        }
         int RaiseCount
        {
            get; set;
        }
         int MaxPoint
        {
            get; set;
        }
         decimal MinBet
        {
            get; set;
        }

         decimal MaxBet
        {
            get; set;
        }

         int Speed
        {
            get; set;
        }
         int RuleType
        {
            get; set;
        }
         bool JacobRule
        {
            get; set;
        }
         bool CrawfordRule
        {
            get; set;
        }
         bool DoubleRule
        {
            get; set;
        }

         bool WithDouble { get; set; } 
         bool BeaverRule
        {
            get; set;
        }
         bool IsProtected
        {
            get; set;
        }
         string Password
        {
            get; set;
        }
         bool ChatEnabled
        {
            get; set;
        }

         int CreatorId
        {
            get; set;
        }

         double Percent
        {
            get; set;
        }

        void Validate(); 
    }
}
