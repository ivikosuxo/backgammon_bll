﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Config
{
  public  class GameSpeed
    {
        public const int Fast = 1;
        public const int Normal = 2;
        public const int Slow = 3;
    }
}
