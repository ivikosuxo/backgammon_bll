﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Config
{
   public static class GlobalConfig
    {
        public static int WinCoinCount = 150;
        public static int LoseCoinCount = 100;
        public static int Gameport = 443;
        public static int CantCreateRoomMessageId = 114;
        public static double RakePercent = 4;
        public static List<int> StatusPoints = new List<int>();
        public static decimal AchievementMinBet = 0.50m;
    }
}
