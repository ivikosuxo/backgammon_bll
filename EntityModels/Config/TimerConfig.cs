﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Config
{
  public static   class TimerConfig
    {
        //turn timer
        public const int DeltaSeconds = 2;
        public const int TurnSeconds = 10;
        public const int TurnDeltaSeconds = 5;
        public const int TimerChangeDelay =2;
        public const int RaiseTimerSize = 15;

        //reserver timer
        public const int ReserveSeconds = 60;
        public const int ReserveDeltaSeconds = 30;

        // next game timer
        public const int NextGameSeconds = 7;
        public const int LifeTimeSeconds = 30;

        //tournament timer 
        public const int TournamentWaitTime = 15;
        public const int MatchmackingDelay = 15;
        public const int StartTournamentDelay = 3;

        public const int LobbyPlayerClearTimeMinutes = 1;
        public const int LobbyTournamentClearTimeMinutes = 1;
    }
}
