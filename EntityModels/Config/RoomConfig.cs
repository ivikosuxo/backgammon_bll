﻿namespace EntityModels.Config
{
   public class RoomConfig
    {
        
        public int RoomId { get; set;}
        public string Roomname {get; set; }
        public int CreatorId { get; set; }
        public int Speed { get; set; }
        public int Type { get; set; }
        public int MaxPoint { get; set; }
        public int RaiseCount { get; set; } = 1;
        public bool WithDouble { get; set; } = true;
        public decimal MinBet { get; set; }

        public decimal MaxBet { get; set; }

        public int MerchantId { get; set; }
        public bool IsNetwork { get; set; }
        public int PlayersCount { get; set; } = 2;

        public bool IsProtected { get; set; }
        public string Password { get; set; } 

        public bool DoubleRule { get; set; }
        public bool BeaverRule { get; set; }
        public bool CrawfordRule { get; set; }
        public bool JacobRule { get; set; }

        public bool ChatEnabled { get; set; }
     
        public int MatchType()
        {

            if ( MaxBet > MinBet)
            {
                return 2;
            }
            if (this.MaxPoint > 1)
            {
                return 1;
            }

            return 0;
        }

           
    }
}
