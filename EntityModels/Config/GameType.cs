﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Config
{
    public class GameType
    {
         public const int European= 1;
         public const int Georgian = 2;
         public const int Long = 3;
         public const int Hyper = 4;
         public const int Khachapuri = 5;
         public const int Eureka = 6;
         public const int Blitz = 7;
    }
}
