﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.StateCalculator
{
  public  class PlayModel
    {
        public PlayModel(bool killDisc,int playCount)
        {
            KillDiscard = killDisc;
            PlayCount = playCount;

        }
        public int PlayCount
        {
            get;set;
        }
        public bool KillDiscard
        {
            get;set;
        }
    }
}
