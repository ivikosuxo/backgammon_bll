﻿
using InterfaceLibrary;
using InterfaceLibrary.Game;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Config
{
    public class PlayerInfo : IPlayerInfo
    {
        private object locker = new object();
        public int PlayerId { get; set; }
        public string Username { get; set; }
        public decimal Balance { get; set; }
        public bool IsBlocked { get; set; }
        public decimal Coin { get; set; } = 0;
        public int AvatarId { get; set; }
        public int Rank { get; set; } = 0;
        public bool ChatBan { get; set; } = false;
        public int MerchantId { get; set; } = 0;
        public bool IsNetwork { get; set; } = false;

        public int GameCount { get; set; } = 0;

        public bool IsVerified { get; set; } = false;

        public int StoneColor { get; set; } = 1;
        public List<IPlayerAchievement> Achievements { get; set; } = new List<IPlayerAchievement>();
        public List<IOrderedProduct> OrderedProducts
        {
            get; set;
        } = new List<IOrderedProduct>();

    

        public IPlayerAchievement GetAchievement(int achievementId)
        {
            lock (locker)
            {
               return Achievements.FirstOrDefault(e => e.AchievementId == achievementId);
            }          
          
        }

        public JObject GetInfo()
        {
            var info = new JObject()
            {
                ["blocked"] = IsBlocked,
                ["username"] = Username,
                ["player_id"] = PlayerId,
                ["avatar"] = AvatarId,
                ["balance"] = Balance,
                ["rank"] = Rank,
                ["coin"] = Coin,
                ["stone_color"] = StoneColor,
                ["game_count"] = GameCount
            };
            return info;

        }

        public JArray GetAchievementsToJson()
        {
            var arr = new JArray();
            Achievements.ForEach(e => arr.Add(e.ToJson()));
 
            return arr;
           
        }
    }
}
