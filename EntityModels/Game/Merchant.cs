﻿using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Merchanter
{
    public class Merchant :IMerchant
    {
        public int MerchantId { get; set; }
        public string MerchantName { get; set; }
        public bool IsNetwork { get; set; }

        public int CurrencyId { get; set; }
        public decimal RakePercent { get; set; }
    }
}
