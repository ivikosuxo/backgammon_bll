﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class RoomIsFullException :GameException
    {
        public RoomIsFullException()
        {
            ErrorId = ErrorCodes.RoomIsFull;
            Fatal = true;
        }
        public override string Message => "Room is full";
    }
}
