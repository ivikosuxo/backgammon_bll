﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class NetworkIsNotSupportedException :GameException
    {
        public NetworkIsNotSupportedException() {
            ErrorId = ErrorCodes.NetworkIsNotSupported;
            Fatal = true;

        }
        public override string Message => "Network is not supported";
    }
}
