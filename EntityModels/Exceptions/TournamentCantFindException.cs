﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
  public  class TournamentCantFindException :GameException
    {
        public TournamentCantFindException()
        {
            ErrorId = ErrorCodes.TournamentAlreadyContains;
            Fatal = false;
        }
        public override string Message => "Current tournament doesn't exists.";
    }
}
