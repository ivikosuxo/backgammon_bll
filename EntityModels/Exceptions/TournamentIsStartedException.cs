﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class TournamentIsStartedException :GameException
    {
        public TournamentIsStartedException()
        {
            ErrorId = ErrorCodes.TournamentIsStarted;
            Fatal = false;
        }
        public override string Message => "Tournament is already started";
    }
}
