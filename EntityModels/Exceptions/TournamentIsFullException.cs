﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
  public  class TournamentIsFullException :GameException
    {
        public TournamentIsFullException()
        {
            ErrorId = ErrorCodes.TournamentIsFull;
            Fatal = false;
        }
        public override string Message => "Tournament is full";
    }
}
