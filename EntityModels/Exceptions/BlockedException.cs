﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
  public  class BlockedException :GameException
    {
        public BlockedException()
        {
            ErrorId = ErrorCodes.AccountIsBlocked;

        }
        public override string Message => "Account is Blocked";
    }
}
