﻿using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class GameException : Exception , IGameException
    {
        public bool Fatal { get; set; } = true;
        public int ErrorId { get; set; } = -1;

        public virtual JObject ToJson()
        {
            var data = new JObject()
            {
                ["id"] = ErrorId,
                ["message"]=Message,
                ["fatal"]=Fatal,
                ["error"]=true

            };
            return data;
        }

        public override string Message => "Something went wrong";

        public void Throw() {
            throw this;
        }
    }
}
