﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class NotEnoughGameCountException :GameException
    {
        public NotEnoughGameCountException()
        {
            ErrorId = ErrorCodes.NotEnoughGameCount;
            Fatal = false;
        }

        public override string Message => "Not Enough Game Count";
    }
}
