﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class MaxRankException : GameException
    {

        public MaxRankException()
        {
            ErrorId = ErrorCodes.MaxRank;
            Fatal = false;
        }
        public override string Message => "You are already on Max Rank";
    }
}
