﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class NotEnoughBalanceException :GameException
    {

        public NotEnoughBalanceException()
        {
            ErrorId = ErrorCodes.NotEnoughMoney;
            Fatal = true;
        }
        public override string Message => "Not enough balance";
    }
}
