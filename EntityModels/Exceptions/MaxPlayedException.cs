﻿ 
using EntityModels.StateCalculator;
using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class MaxPlayedException :GameException
    {
        public PlayModel Model;
        public int MoveValue = 0;
        public MaxPlayedException(PlayModel val)
        {
            Model = val;
            ErrorId = ErrorCodes.InvalidMove;
            Fatal = false;
        }
        public override string Message => "Player Can Move Possible Moves";
    }
}
