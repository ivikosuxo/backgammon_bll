﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class DifferentMerchantException : GameException 
    {
        public DifferentMerchantException()
        {
            Fatal = false;
            ErrorId = ErrorCodes.DifferentMerchant;
        }

        public override string Message => "Different Merchant";
    }
}
