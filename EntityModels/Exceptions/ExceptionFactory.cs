﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public static class ExceptionFactory
    {
        public static GameException GetExceptionById(int errorId)
        {
            switch (errorId)
            {
                case ErrorCodes.CantAuthenticate:
                    return new AuthenticateException();

                case ErrorCodes.AccountIsBlocked:
                    return new BlockedException();

                case ErrorCodes.InvalidMove:
                    return new InvalidMoveException();

                case ErrorCodes.NotCorrectPassword:
                    return new IncorrectPasswordException();

                case ErrorCodes.ShortPassword:
                    return new PasswordSizeException();

                case ErrorCodes.PasswordSizeError:
                    return new PasswordSizeException();

                case ErrorCodes.NotEnoughMoney:
                    return new NotEnoughBalanceException(); 

                case ErrorCodes.NotEnoughPlayers:
                    return new NotEnoughPlayerException();

                case ErrorCodes.RoomIsNotActive:
                    return new RoomNotExistsException(); 

                case ErrorCodes.TournamentAlreadyContains:
                    return new TournirAlreadyContainsPlayerException();

                case ErrorCodes.NotValidBet:
                    return new NotValidBetException(); 

                case ErrorCodes.TournamentIsFull:
                    return new TournamentIsFullException();

                case ErrorCodes.TournamentIsStarted:
                    return new TournamentIsStartedException();

                case ErrorCodes.TournamenCantFind:
                    return new TournamentCantFindException();

                case ErrorCodes.MaxRank:
                    return new MaxRankException();

                case ErrorCodes.ToManyRoomCreated:
                    return new ToManyRoomCreatedException();

                case ErrorCodes.NotEnoughCoins:
                    return new NotEnoghtCoinException();

                case ErrorCodes.NotEnoughItems:
                    return new NotEnoughItemsException();

                case ErrorCodes.RoomIsFull:
                    return new RoomIsFullException();

                case ErrorCodes.LobbySingleInstanse:
                    return new LobbySingleInstanceException();

                case ErrorCodes.ProductIsSold:

                    return new ProductIsSoldException();

                case ErrorCodes.SomethingWentWrong: 
                    return new ProblemDetectedException();

                case ErrorCodes.NotEnoughGameCount:
                    return new NotEnoughGameCountException();

                default:
                    return new GameException();
            }

        }
    }
}
