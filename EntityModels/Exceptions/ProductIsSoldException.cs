﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public  class ProductIsSoldException :GameException
    {
        public ProductIsSoldException() 
        {
            ErrorId = ErrorCodes.ProductIsSold;

        }
        public override string Message => "Product is Sold";
    }
}
