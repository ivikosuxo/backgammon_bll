﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class PasswordSizeException :GameException
    {
        public PasswordSizeException()
        {
            ErrorId = ErrorCodes.PasswordSizeError;
            Fatal = false;
        }
        public override string Message => "Password size must be in 4-16 range";
    }
}
