﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class RoomNotExistsException :GameException 
    {
        public RoomNotExistsException()
        {
            ErrorId = ErrorCodes.RoomIsNotActive;
            Fatal = true;
        }
        public override string Message => "Room doesn't exists";
    }
}
