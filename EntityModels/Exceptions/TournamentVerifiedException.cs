﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class TournamentVerifiedException : GameException
    {
        public TournamentVerifiedException()
        {
            ErrorId = ErrorCodes.UserMustVerify;
            Fatal = false;
        }
        public override string Message => "you must verify your account";
    }
}
