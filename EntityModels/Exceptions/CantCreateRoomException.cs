﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using EntityModels.Entities;

namespace EntityModels.Exceptions
{
    public class CantCreateRoomException   :GameException
    {
        public CantCreateRoomException()
        {
            ErrorId = ErrorCodes.CantCreateRoom;
            Fatal = false;
            
        }

        public override JObject ToJson()
        {
            var data = new JObject()
            {
                ["id"] = ErrorId,
                ["message"] = BlockCreateRoomModel.CreateRoomMessage,
                ["fatal"] = Fatal 
            };
            return data;
        }
        public override string Message => "Temporary you can't create room";
    }
}
