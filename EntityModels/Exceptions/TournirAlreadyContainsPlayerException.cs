﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class TournirAlreadyContainsPlayerException :GameException
    {
        public TournirAlreadyContainsPlayerException()
        {
            ErrorId = ErrorCodes.TournamentAlreadyContains;
            Fatal = false;
        }
        public override string Message => "Tournament is already contains player";
    }
}
