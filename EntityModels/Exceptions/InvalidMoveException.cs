﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class InvalidMoveException :GameException
    {

        public InvalidMoveException()
        {
            ErrorId = ErrorCodes.InvalidMove;
            Fatal = false;
        }
        public override string Message => "Invalid move";
    }
}
