﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class SameIPRestrinction   :GameException
    {
        public SameIPRestrinction()
        {
            ErrorId = ErrorCodes.SameIPRestriction;
            Fatal = true;
        }
        public override string Message => "Same IP Restriction";
    }
}
