﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public  class NotEnoughItemsException   : GameException
    {
        public NotEnoughItemsException()
        {
            ErrorId = ErrorCodes.NotEnoughItems;
            Fatal = false;
        }
        public override string Message => "Not enough Items.";
    }
}
