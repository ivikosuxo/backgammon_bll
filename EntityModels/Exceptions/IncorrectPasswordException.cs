﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class IncorrectPasswordException :GameException
    {
        public IncorrectPasswordException()
        {
            ErrorId = ErrorCodes.NotCorrectPassword;
            Fatal = false;
        }
        public override string Message => "Incorrect password";
    }
}
