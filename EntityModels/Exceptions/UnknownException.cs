﻿using EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public  class UnknownException :GameException
    {
        public UnknownException()
        {
            ErrorId = ErrorCodes.CantAuthenticate;
            Fatal = true;
        }
        public override string Message => "Unknown Error";
    }
}
