﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public  class CantRaiseException  :GameException
    {
        public CantRaiseException()
        {
            ErrorId = ErrorCodes.CantRaise;
            Fatal = false;
        }
        public override string Message => "Can't Raise";
    }
}
