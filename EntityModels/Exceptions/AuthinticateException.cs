﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public   class AuthenticateException : GameException
    {

        public AuthenticateException()
        {
            ErrorId = ErrorCodes.CantAuthenticate;
            Fatal = true;
        }
        public override string Message => "Can't Authenticate";
    }
}
