﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
  public  class ProblemDetectedException :GameException
    {
        public ProblemDetectedException()
        {
            ErrorId = ErrorCodes.UnknownError;
            Fatal = false;
        }
        public override string Message => "To Many room created";
    }
}
