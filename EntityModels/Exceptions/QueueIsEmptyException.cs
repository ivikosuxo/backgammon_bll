﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class QueueIsEmptyException : GameException
    {
        public QueueIsEmptyException()
        {
            ErrorId = ErrorCodes.NotEnoughCoins;
            Fatal = false;
        } 
         
        public override string Message => "Can't get queue number, from random org";
    }
}
