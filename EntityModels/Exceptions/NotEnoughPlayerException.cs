﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public  class NotEnoughPlayerException :GameException
    {
        public NotEnoughPlayerException()
        {
            ErrorId = ErrorCodes.NotEnoughPlayers;
            Fatal = false;
        }
        public override string Message => "Not enough players.";
    }
}
