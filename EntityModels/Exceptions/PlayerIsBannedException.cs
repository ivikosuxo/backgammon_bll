﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public class PlayerIsBannedException :GameException
    {
        public PlayerIsBannedException()
        {
            ErrorId = ErrorCodes.ChatIsBanned;
            Fatal = false;
        }

        public override string Message => "chat_banned";
    }
}
