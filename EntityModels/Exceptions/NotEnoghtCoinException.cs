﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
    public class NotEnoghtCoinException : GameException
    {

        public NotEnoghtCoinException()
        {
            ErrorId = ErrorCodes.NotEnoughCoins;
            Fatal = false;
        }
        public override string Message => "Not enough coin";
    }
}
