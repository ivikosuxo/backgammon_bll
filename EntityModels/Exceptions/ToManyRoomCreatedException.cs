﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.Types;

namespace EntityModels.Exceptions
{
   public class ToManyRoomCreatedException :GameException
    {
        public ToManyRoomCreatedException()
        {
            ErrorId = ErrorCodes.ToManyRoomCreated;
            Fatal = false;
        }
        public override string Message => "To Many room created";
    }
}
