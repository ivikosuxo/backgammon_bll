﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
  public    class NotValidBetException    :GameException
    {
        public NotValidBetException()
        {
            ErrorId = ErrorCodes.NotValidBet;
            Fatal = false;
        }
        public override string Message => "Not valid Bet";
    }
}
