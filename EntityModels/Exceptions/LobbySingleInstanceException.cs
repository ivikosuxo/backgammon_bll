﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Exceptions
{
   public  class LobbySingleInstanceException :GameException
    {
        public LobbySingleInstanceException()
        {

            Fatal = true;
            ErrorId = ErrorCodes.LobbySingleInstanse;
        }
        public override string Message => "You have new active session";
    }
}
