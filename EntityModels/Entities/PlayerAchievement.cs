﻿using InterfaceLibrary;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Achivments
{
    public class PlayerAchievement : IPlayerAchievement
    {
        public int AchievementId { get; set; }
        public int Count { get; set; } = 0;
        public int FullCount { get; set; } = 0;
        public bool Complete { get; set; } = false;
        public bool IsActive { get; set; } = true;
        public bool Claimed { get; set; } = false;
        public PlayerAchievement() {
             
        }
        public PlayerAchievement(int achievementId,int count,int fullCount,bool complete , bool active) {
            AchievementId = achievementId;
            Count = count;
            FullCount = fullCount;
            Complete = complete;
            IsActive = active;
        }

        public JObject ToJson()
        {
            var data = new JObject()
            {
                ["id"]=AchievementId,            
                ["count"]=Count,
                ["full_count"]=FullCount,
                ["complete"] = Complete,
                ["is_active"] = IsActive,
               
            };
            return data;
        }


    }
}
