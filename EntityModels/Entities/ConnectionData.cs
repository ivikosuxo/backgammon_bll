﻿ 
using EntityModels.Types;
using InterfaceLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Entities
{
    public class ConnectionData
    {
        public int PlayerId { get; set; }
        public int RoomId { get; set; } = -1; 
        public ConnectionType Type { get; set; } = ConnectionType.Void;
        public int TournamentId { get; set; } = 0;
        
    }
}
