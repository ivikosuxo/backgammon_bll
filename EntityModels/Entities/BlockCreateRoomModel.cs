﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Entities
{
   public class BlockCreateRoomModel
    {
        public static bool CanCreateRoom { get; set; }
        public static string CreateRoomMessage { get; set; }
        public static string CreateRoomMessageGeo { get; set; }
        public static string CreateRoomMessageRus { get; set; }
        public static int RoomLimitPerUser { get; set; } 

        public static JObject ToJson() {
            var jResult = new JObject();
            jResult["can_create"] = CanCreateRoom;
            jResult["description"] = CreateRoomMessage;
            jResult["description_geo"] = CreateRoomMessageGeo;
            jResult["description_rus"] = CreateRoomMessageRus;
            return jResult;
        }
    }
}
