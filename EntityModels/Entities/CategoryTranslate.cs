﻿using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Entities
{
    public class CategoryTranslate :ICategoryTranslate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameGeo { get; set; }
        public string NameRus { get; set; }

        public JObject ToJson()
        {
            var jobject = new JObject();
            jobject["id"] = Id;
            jobject["name"] = Name;
            jobject["name_geo"] = NameGeo;
            jobject["name_rus"] = NameRus;

            return jobject;

        }
    }
}
