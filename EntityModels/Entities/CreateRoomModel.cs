﻿using  EntityModels.Config;
using EntityModels.Exceptions;
using InterfaceLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Entities
{
    public class CreateRoomModel : ICreateRoomModel
    {
        public string Roomname
        {
            get; set;
        }

        public string Token
        {
            get; set;
        }
        public int RaiseCount
        {
            get; set;
        }
        public int MaxPoint
        {
            get; set;
        }
        public decimal MinBet
        {
            get; set;
        }

        public decimal MaxBet
        {
            get; set;
        }

        public int Speed
        {
            get; set;
        }
        public int RuleType
        {
            get; set;
        }
        public bool JacobRule
        {
            get; set;
        }
        public bool CrawfordRule
        {
            get; set;
        }
        public bool DoubleRule
        {
            get; set;
        }

        public bool WithDouble { get; set; } = true;
        public bool BeaverRule
        {
            get; set;
        }
        public bool IsProtected
        {
            get; set;
        }
        public string Password
        {
            get; set;
        }
        public bool ChatEnabled
        {
            get; set;
        }

        public int CreatorId
        {
            get; set;
        }

        public double Percent
        {
            get; set;
        } = GlobalConfig.RakePercent;

        public void Validate()
        {
            MinBet = Math.Abs(decimal.Round( MinBet,2,MidpointRounding.AwayFromZero));
            if(MinBet < 0.05m)
            {
                MinBet = 0.05m;
            }
            if (MinBet < 0.5m)
            {
                MaxPoint = 1;
                RaiseCount = 1;
            }

            MaxBet = MinBet * RaiseCount;

            MaxPoint = Math.Abs(MaxPoint);
            ValidateRaise();
            ValidatePoints();
            ValidateRules();

            IsProtected = Password.Length != 0;

            if(IsProtected && (Password.Length < 4 || Password.Length > 16))
            {
                throw new PasswordSizeException();
            }
            if(!IsProtected)
            {
                Password = "";
            }        
        }

        private void ValidateRaise()
        {
            if(Speed < 1 || Speed > 3)
            {
                Speed = 1;
            }
            if(RaiseCount < 1 || RaiseCount > 64)
            {
                RaiseCount = 1;
            }
            if(RaiseCount > 1 && MaxPoint > 1)
            {
                MaxPoint = 1;
            }

        }

        private void ValidatePoints()
        {
           
            if(RuleType == GameType.European && MaxPoint > 64)
            {
                MaxPoint = 64;
            }

            if(RuleType == GameType.Georgian && MaxPoint > 6)
            {
                MaxPoint = 6;
            }

            if(RuleType == GameType.Georgian && MaxPoint > 6)
            {
                MaxPoint = 6;
            }

        }

        private void ValidateRules()
        {
            if (RuleType==GameType.Georgian || RuleType == GameType.Long)
            {
                DoubleRule = true;
            }
            if(MaxPoint == 1 && RaiseCount == 1)
            {
                DoubleRule = false;
            }
            if(!DoubleRule)
            {
                JacobRule = false;
                CrawfordRule = false;
                BeaverRule = false;
            }
            if(RaiseCount < 4 &&MaxPoint<4)
            {
                BeaverRule = false;
            }
            if(MaxPoint > 1)
            {
                JacobRule = false;
            }
            
            if(MaxPoint == 1)
            {
                CrawfordRule = false;
            }

            if (RuleType!= GameType.European && RuleType != GameType.Hyper)
            {
                BeaverRule = false;
                CrawfordRule = false;
                JacobRule = false;
            }

        }
    }
}
