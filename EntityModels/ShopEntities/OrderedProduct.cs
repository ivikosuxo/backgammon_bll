﻿using InterfaceLibrary.Enums;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
 

namespace BLL.Classes.Entities
{
    public class OrderedProduct  :IOrderedProduct
    {
        public int Id
        {
            get; set;
        }
        public int PlayerId
        {
            get; set;
        }
        public int ProductId
        {
            get; set;
        }

        public string Name { get; set; }
        public string NameGeo { get; set; }
        public string NameRus { get; set; }

        public int Count
        {
            get; set;
        }

        public decimal Price
        {
            get; set;
        }
        public string ImgUrl
        {
            get; set;
        }
        public long TransactionId
        {
            get; set;
        }
        public int OrderStatusValue
        {
            get { return OrderStatusValue; }
            set { OrderStatus = (OrderStatus)value; }
        }

        public OrderStatus OrderStatus
        {
            get; set;
        }

        public string Comment
        {
            get; set;
        }
        public DateTime OrderTime
        {
            get; set;
        }

        public JObject ToJson()
        {
            var orderdate = OrderTime.ToString("dd-MM-yy HH:mm");
            var data = new JObject()
            {
                ["id"] = Id,
                ["player_id"] = PlayerId,
                ["name"]= Name,
                ["name_geo"]=NameGeo,
                ["name_rus"]=NameRus,
                ["transaction_id"]=TransactionId,
                ["order_status"]=OrderStatus.ToString(),
                ["order_status_value"] =(int) OrderStatus,
                ["comment"]=Comment     ,
                ["time"] = orderdate,
                ["price"] = Price,
                ["product_id"] = ProductId,
                ["img_url"] = ImgUrl,
                ["count"] = Count
            };
            return data;
        }
    }
}
