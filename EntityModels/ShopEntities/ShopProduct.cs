﻿using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.Entities
{
    public class ShopProduct : IShopProduct
    {
        public int Id
        {
            get; set;
        }
        public int Count
        {
            get; set;
        }

        public int StockCount
        {
            get; set;
        }

        public int CoinPrice
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string NameGeo
        {
            get; set;
        }

        public string NameRus
        {
            get; set;
        }
        public string Description
        {
            get; set;
        }
        public string DescriptionGeo
        {
            get; set;
        }
        public string DescriptionRus
        {
            get; set;
        }
        public int Category { get; set; }
        public string ImgUrl
        {
            get; set;
        }
        public int MinRank
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }
        public bool IsDeleted
        {
            get; set;
        }

        public JObject ToJson()
        {
            var data = new JObject()
            {
                ["id"] = Id,
                ["name"] = Name,
                ["name_geo"] = NameGeo,
                ["name_rus"] = NameRus,
                ["description"] = Description,
                ["description_geo"] = DescriptionGeo,
                ["description_rus"] = DescriptionRus,
                ["coin_price"] = CoinPrice,
                ["count"] = Count,
                ["category"] = Category,
                ["stock_count"] = StockCount,
                ["img_url"] = ImgUrl,
            };

            return data;
        }

    }
}
