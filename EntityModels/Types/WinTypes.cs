﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Types
{
  public  class WinTypes
    {
        public const int Normal  = 1;
        public const int TimeOut = 2;
        public const int GiveUpRound = 3;
        public const int GiveUpGame = 4;
        public const int CancelGame = 5;
        public const int RaiseTimeOut = 5;
        public const int RaiseDecline = 6;
        public const int Canceled = 7;
    }
}
