﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Types
{
   public class RoundPlayerState
    {
        public const int MoveState = 0;
        public const int KilledState = 1;
        public const int DiscardState = 2;
    }
}
