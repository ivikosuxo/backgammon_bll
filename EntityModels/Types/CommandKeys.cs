﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Types
{
    public class CommandKeys
    {
        //game play
        public const string give_up = "give_up";
        public const string play = "play";
        public const string move = "move";
        public const string change = "change";
        public const string undo = "undo";
        public const string roll = "roll";
        public const string pick = "pick";
        public const string submit = "submit";
        public const string submit_you = "submit_you";
        public const string submit_enemy = "submit_enemy";
        public const string next_game = "next_game";
        public const string play_en = "play_en";
        public const string play_you = "play_you";
        public const string play_en_several = "play_en_several";
        public const string play_several = "play_several";
        public const string creator_join = "creator_join";
        public const string chat = "chat";
        public const string incorrect_move = "incorrect_move";
        public const string invalid_move = "invalid_move";
        public const string message = "message";
        public const string turn_timer = "turn_timer";
        public const string reserve_timer = "reserve_timer";
        public const string end_game = "end_game";
        public const string play_again = "play_again";

        public const string problem_report = "problem_report";
      
        public const string disconnect_opponent = "disconnect_opponent";
        public const string start_bg = "start_bg"; 

        public const string waiting_add = "waiting_add";
        public const string waiting_remove = "waiting_remove";
        public const string waiting_decline = "waiting_decline";
        public const string waiting_accept = "waiting_accept";
        public const string waiter = "waiter";

        public const string achievement_info = "achievement_info";        // game room 
        public const string can_join = "can_join";
        public const string pre_join_room = "pre_join_room";
        public const string join_room = "join_room";
        public const string connection_type = "connection_type";
        public const string game_initialize = "game_initialize";
        public const string opponent_info = "opponent_info";
        public const string reconnect = "reconnect";
        //system
        public const string error = "error";
        public const string id = "id";
        public const string tournament_id = "tournament_id";
        public const string user_id = "user_id";
        public const string room_id = "room_id";
        public const string command = "command";
        public const string connect = "connect";
        public const string login = "login";
        public const string spectate = "spectate";
        public const string can_create_room = "can_create_room";
        public const string can_create_room_message = "can_create_room_message";
        public const string incorrect_password = "incorrect_password";

        //raise
        public const string raise = "raise"; 
        public const string raise_answer = "raise_answer";
        public const string raise_you = "raise_you";
        public const string cant_raise = "cant_raise";

        //Lobby
        public const string avatar_change = "avatar_change";
        public const string ping = "ping";
        public const string ping_latency = "ping_latency";
        public const string create_room = "create_room";
        public const string room_info = "room_info";
        public const string join_lobby = "join_lobby";
        public const string balance_check = "balance_check";
        public const string lobby_info = "lobby_info";
        public const string count_change = "count_change";
        public const string lobby_data = "lobby_data";
        public const string remove_room = "remove_room";
        public const string round_data = "round_data";
        public const string game_history = "game_history";
        public const string game_lobby_history = "game_lobby_history";
        public const string update_balance = "update_balance";
        public const string update_coin_count = "update_coin_count";
        public const string active_connections = "active_connections";
        public const string sit_on_room = "sit_on_room";
        public const string rank_increase = "rank_increase";
        public const string rank_increase_error = "rank_increase_error";
        public const string change_stone = "change_stone";
        //tournament
        public const string tournament_add = "tournament_add";
        public const string tournament_cancel = "tournament_cancel";
        public const string tournament_remove = "tournament_remove";
        public const string tournament_finish = "tournament_finish";
        public const string tournament_extra_player = "tournament_extra_player";
        public const string tournaments_info = "tournaments_info";
        public const string tournaments_register = "tournaments_register";
        public const string tournaments_unregister = "tournaments_unregister";
        public const string tournaments_players_count = "tournaments_players_count";
        public const string tournament_room_create = "tournament_room_create";
        public const string tournament_status_change = "tournament_status_change";
        public const string tournament_join = "tournament_join";
        public const string tournament_pre_join = "tournament_pre_join"; 
        public const string tournament_total_info = "tournament_total_info";


        //  achievement
        public const string achievement_add = "achievement_add";
        public const string achievement_remove = "achievement_remove";
        public const string achievement_unlock = "achievement_unlock";

        //shop
        public const string buy_product = "buy_product";
        public const string shop_info = "shop_info";
        public const string order_products = "order_products";
        public const string remove_product = "remove_product";
        public const string add_product = "add_product";


    }
}
