﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Types
{
    public   class StaticStrings
    {
        public const  string Id = "id";
        public const  string Fatal = "fatal";
        public const  string StartGame = "start_game";
        public const string room_id = "room_id";
        public const string user_id = "user_id";
    }
}
