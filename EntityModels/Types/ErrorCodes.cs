﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModels.Types
{
   public static class ErrorCodes
    {

        public const int UnknownError = 0;
        public const int LobbySingleInstanse = 1;
        public const int NotEnoughMoney = 2;
        public const int OpponentDisagree = 3;
        public const int CantAuthenticate = 4;
        public const int AccountIsBlocked = 5;
       
        public const int YouDisagree = 6;
        public const int InvalidMove = 7;

        public const int NotValidBet = 8;
        public const int NotEnoughCoins = 9;
        public const int MaxRank =10;
        public const int OpponentDeclined = 11;
        public const int ChatIsBanned = 12;
        public const int NotEnoughGameCount = 13;

        public const int CantCreateRoom = 50;
        public const int RoomIsFull = 400;
        public const int RoomIsNotActive = 500;
        public const int Reconnect = 550;

        public const int NotCorrectPassword = 600;
        public const int ShortPassword = 601;

        public const int PasswordSizeError = 602;
        public const int SpectateIsNotEnabled = 650;
        public const int CantRaise = 680;

        public const int ToManyRoomCreated = 690;

        public const int UnexpectedToken = 700;    
        public const int TimeOut = 900;
        public const int SomethingWentWrong = 1000;

        public const int DifferentMerchant = 1050;
        public const int NetworkIsNotSupported = 1051;

        //tournament errors 
        public const int TournamentIsFull = 2000;
        public const int TournamentIsStarted = 2001;
        public const int NotEnoughPlayers = 2002;
        public const int TournamentExtraPlayer = 2003;
        public const int TournamentAlreadyContains = 2004;
        public const int TournamenCantFind = 2005;
        public const int TournamentCanceled = 2006;
        public const int SameIPRestriction = 2007;
        public const int UserMustVerify = 2008;
        // shop 
        public const int CantFindProduct = 2500;
        public const int ProductIsSold = 2501;
        public const int ProductIsNotAvailable = 2502;
        public const int NotEnoughItems = 2503;
    }
}
