﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Translations;

namespace Helpers
{
    public class TranslationHelpers
    {
        public static Dictionary<string, string> GetTranslations(CultureInfo cultureInfo)
        {
            var resourceManager = Translation.ResourceManager; 
            var resourceSet = resourceManager.GetResourceSet(cultureInfo, true, true);

            return resourceSet.OfType<DictionaryEntry>()
                .ToDictionary(r => r.Key.ToString(),
                    r => r.Value.ToString());
        }

        public static string GetTranslation<T>(T str)
        {
            var cultureInfo = Thread.CurrentThread.CurrentCulture;
            var resourceManager = Translation.ResourceManager;

            var resourceSet = resourceManager.GetResourceSet(cultureInfo, true, true);

            var translationEntry = resourceSet.OfType<DictionaryEntry>()
                .Select(i => (DictionaryEntry?)i)
                .FirstOrDefault(i => string.Equals(i.GetValueOrDefault().Key.ToString(), str.ToString(), StringComparison.InvariantCultureIgnoreCase));

            var result = translationEntry == null ? str.ToString() : translationEntry.GetValueOrDefault().Value?.ToString() ?? str.ToString();

            return result;
        }
    }
}
