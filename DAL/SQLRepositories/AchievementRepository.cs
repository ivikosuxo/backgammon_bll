﻿
using BLL.Achivments;
using InterfaceLibrary;
using InterfaceLibrary.Achievements;
using InterfaceLibrary.Database;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Database
{
    public class AchievementRepository :IAchievementRepository
    {
        private IFactory Factory { get; set; }
        public AchievementRepository(IFactory factory)
        {
            Factory = factory;

        }
        public   List<IAchievement> UpdateAchievementsList()
        {

            var result = new List<IAchievement>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from achievements where [active] =1 order by min_rank asc",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var type = int.Parse(reader["achievement_type"].ToString());
                    var achievement = Factory.GetAchievementById((AchievementType)type);
                    achievement.AchievementId = int.Parse(reader["id"].ToString());
                  
                    achievement.Count = int.Parse(reader["count"].ToString());
                    achievement.MinRank = int.Parse(reader["min_rank"].ToString());

                    achievement.IsActive = bool.Parse(reader["active"].ToString());
                
                   
                    achievement.CoinCount = int.Parse(reader["coin_count"].ToString());

                    if (type == (int)AchievementType.Roll)
                    {
                        var dice1 = int.Parse(reader["dice_1"].ToString());
                        var dice2 = int.Parse(reader["dice_2"].ToString());
                        achievement.SetDices(dice1, dice2);
                    }
                    GetAchievementTranslations(achievement); 
                    result.Add(achievement);
                }

            }
            return result;
        }

        public   void GetAchievementTranslations(IAchievement achievement)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from AchievementTranslations where    achievementid = " + achievement.AchievementId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var lang = reader["lang"].ToString();
                    var description = reader["description"].ToString();
                    if (lang.Equals("ka-GE"))
                    {
                        achievement.AchievementTitleGeo = name;
                        achievement.AchievementDescriptionGeo = description;
                    }
                    else if (lang.Equals("ru-RU"))
                    {
                        achievement.AchievementTitleRus = name;
                        achievement.AchievementDescriptionRus = description;
                    }
                    else if (lang.Equals("en-US"))
                    {
                        achievement.AchievementTitle = name;
                        achievement.AchievementDescription= description; 
                    }
                }

            }

        }

        public   IAchievement GetAchievementById(int achievement_id)
        {
            IAchievement achievement =null;
        
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from achievements where id = "+ achievement_id,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var type = int.Parse(reader["achievement_type"].ToString());
                    achievement = Factory.GetAchievementById((AchievementType)type);
                   
                 
                    achievement.AchievementId = int.Parse(reader["id"].ToString());
 
                    achievement.Count = int.Parse(reader["count"].ToString());
                    achievement.MinRank = int.Parse(reader["min_rank"].ToString());

                    achievement.IsActive = bool.Parse(reader["active"].ToString());
                    achievement.MinRank = int.Parse(reader["min_rank"].ToString());
                  
                    achievement.CoinCount = int.Parse(reader["coin_count"].ToString());

                    if (type == (int)AchievementType.Roll)
                    {
                        var dice1 = int.Parse(reader["dice_1"].ToString());
                        var dice2 = int.Parse(reader["dice_2"].ToString());
                        achievement.SetDices(dice1, dice2);
                    }
                    GetAchievementTranslations(achievement);
                    return achievement;
                }
               
            }
            throw new Exception();
        }

        public   void DeactivateAchievement(int achievement_id)
        {
        
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "update Achievements set active = 0 where id = " + achievement_id,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                cmd.ExecuteNonQuery();                 
            }
  
        }

        public   List<IPlayerAchievement> UpdatePlayerAchievementsList(int playerId)
        {

            var result = new List<IPlayerAchievement>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from player_achievements where player_id = " + playerId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var playerAchievement = new PlayerAchievement()
                    {
                        AchievementId = int.Parse(reader["achievement_id"].ToString()),
                        Complete = bool.Parse(reader["completed"].ToString()),
                        Count = int.Parse(reader["open_count"].ToString()),
                        FullCount = int.Parse(reader["max_count"].ToString()),
                    };
                    result.Add(playerAchievement);
                }
            }
            return result;
        }

        public   bool PlayerAchievementIncrease(IPlayerInfo info, int achievementId,int count,int maxCount)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "sp_achievement_increase",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var completeParam = new SqlParameter()
                {
                    ParameterName = "@complete",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Bit
                };
                cmd.Parameters.AddWithValue("@achievement_id", achievementId);
                cmd.Parameters.AddWithValue("@player_id", info.PlayerId);
                cmd.Parameters.AddWithValue("@count", count);
                cmd.Parameters.AddWithValue("@max_count", maxCount);
                cmd.Parameters.Add(completeParam);
                connection.Open();

                cmd.ExecuteNonQuery();


                return (bool)completeParam.Value;
            }
        }

 
        public   void PlayerAchievementClaim(int playerId, int achievementId)
        {

        }
    }
}
