﻿using System;
using System.Collections.Generic; 
using EntityModels.Config;
using System.Data.SqlClient;
using System.Data; 
using EntityModels.Entities;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Game;
using InterfaceLibrary.Database;
using EntityModels.Merchanter;

namespace DAL.Database
{
    public class PlayerRepository : IPlayerRepository
    {
         public IFactory Factory { get; set; }

         public PlayerRepository(IFactory factory)
        {
            Factory = factory;
        }

        public   int GetPlayerId(Guid token,bool expired)
        {
            var player_id = 0;
            var is_active = true;

            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select player_id , is_active from player_sessions where  session_id = '" + token.ToString() + "'",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    is_active = bool.Parse(reader["is_active"].ToString());
                    if (expired || is_active)
                    {
                        player_id = int.Parse(reader["player_id"].ToString());
                    } 
                  
                } 
            }

            return player_id;
        }


        public List<IMerchant> GetAllMerchants()
        {
            var result = new List<IMerchant>();

            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from merchants",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var merchant = new Merchant();
                    merchant.MerchantId = int.Parse(reader["merchant_id"].ToString());
                    merchant.RakePercent = decimal.Parse(reader["rake_percent"].ToString());
                    merchant.CurrencyId = int.Parse(reader["currency_id"].ToString());
                    merchant.MerchantName = reader["merchant_id"].ToString();
                    merchant.IsNetwork = bool.Parse(reader["is_network"].ToString());
                }

            }


            return result;
        }

        public void UpdateMerchant(IMerchant merchant)
        {



            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from merchants where merchant_id = " + merchant.MerchantId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    merchant.MerchantId = int.Parse(reader["merchant_id"].ToString());
                    merchant.RakePercent = decimal.Parse(reader["rake_percent"].ToString());
                    merchant.CurrencyId = int.Parse(reader["currency_id"].ToString());
                    merchant.MerchantName = reader["merchant_id"].ToString();
                    merchant.IsNetwork = bool.Parse(reader["is_network"].ToString());
                }

            }


        }

        public   void RankIncrease(IPlayerInfo player)
        {

            //DB lock transaction 
            var errorcode = 0;
            var balance = 0m;
            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_rank_increase",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var errorCodeParam = new SqlParameter()
                {
                    ParameterName = "@error_code",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var balanceParam = new SqlParameter()
                {
                    ParameterName = "@coin_balance",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Money
                };


                cmd.Parameters.AddWithValue("@player_id", player.PlayerId); 
                cmd.Parameters.Add(errorCodeParam);
                cmd.Parameters.Add(balanceParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                errorcode = (int)errorCodeParam.Value;
                balance = (decimal)balanceParam.Value;
            }
            if (errorcode > 0)
            {
                var exception =  Factory.GetExceptionById(errorcode);
                exception.Throw();
            }
            else
            {
                player.GameCount = 0;
                player.Rank++;
            }

            player.Coin = balance;


        }


    
        public   decimal GetPlayerBalance(int playerId)
        {
            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "SELECT    current_balance as balance FROM BaseDB.dbo.players    where  player_id = " + playerId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                var balance = 0m;
                while (reader.Read())
                {
                    balance = decimal.Parse(reader["balance"].ToString());

                }

                return balance;

            }

        }

        public   bool UpdateCreateRoomProperty()
        {
            try
            { 

                using (var connection = DbManager.GetBackgammonConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "SELECT top 1 * FROM [game_config] AS [gc]",
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                         BlockCreateRoomModel.CanCreateRoom = bool.Parse(reader["can_create_room"].ToString());
                        BlockCreateRoomModel.RoomLimitPerUser = int.Parse(reader["room_limit_per_user"].ToString());
                        GlobalConfig.RakePercent = double.Parse(reader["rake_percent"].ToString());
                    }
                }

                GetCantCreateRoomMessage();
                return true;
            }
            catch (Exception ex)
            {
                Factory.Logger.Error($"UpdateCreateRoomProperty: ", ex);
                return false;
            }
        }

        public   void GetCantCreateRoomMessage() {

            try
            { 
                using (var connection = DbManager.GetBaseDbConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "SELECT * FROM [translationValues] AS [TV] where translationId = "+GlobalConfig.CantCreateRoomMessageId,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var lang= reader["lang"].ToString() ;
                        var message = reader["value"].ToString();

                        if (lang.Equals("ka-GE"))
                        {
                            BlockCreateRoomModel.CreateRoomMessageGeo = message;

                        }
                        else if (lang.Equals("ru-RU"))
                        {
                            BlockCreateRoomModel.CreateRoomMessageRus = message;

                        }
                        else if (lang.Equals("en-US"))
                        {
                            BlockCreateRoomModel.CreateRoomMessage = message;

                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                Factory.Logger.Error($"GetCantCreateRoomMessage: ", ex); 
            }
             
        }

         
        public   void GetStatusPoints(IStatuses lobby)
        {
            var listPoints = new List<int>(); 
            var listGames = new List<int>();
            try
            { 

                using (var connection = DbManager.GetBaseDbConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "SELECT  * FROM [PlayerStatuses] AS [statuses] order by id asc",
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listPoints.Add((int) decimal.Parse(reader["fee"].ToString()));
                        listGames.Add( int.Parse(reader["game_count"].ToString())); 
                    }
                }
            }
            catch (Exception ex) {
                Factory.Logger.Error($"GetStatusPoints: ", ex); 
            }
            lobby.StatusPoints = listPoints;
            lobby.StatusGameCounts = listGames;
        }
        public   IPlayerInfo GetPlayerInfo(int playerId)
        {

            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "SELECT rank as rank, coin as coin, is_verified as is_verified,  game_count as game_count , stone_color as stone_color ,  avatar_id , player_id as player_id, user_name as user_name, blocked_in_game  ,current_balance as balance FROM BaseDB.dbo.players    where  player_id = " + playerId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                var info = new PlayerInfo();
                while (reader.Read())
                {
                    info.PlayerId = int.Parse(reader["player_id"].ToString());
                    info.Username = reader["user_name"].ToString();
                    info.IsVerified = bool.Parse(reader["is_verified"].ToString());
                    info.GameCount = int.Parse(reader["game_count"].ToString());
                    info.IsBlocked = bool.Parse(reader["blocked_in_game"].ToString());
                    info.AvatarId = int.Parse(reader["avatar_id"].ToString());
                    info.Balance = decimal.Parse(reader["balance"].ToString());
                    info.Rank = int.Parse(reader["rank"].ToString());
                    info.Coin = Decimal.Parse(reader["coin"].ToString());
                    info.StoneColor = int.Parse(reader["stone_color"].ToString());
                }

             

                return info;

            }

        }
        public   void ChangeAvatar(int playerId, int avatarId)
        {
            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "update BaseDB.dbo.players set avatar_id= " + avatarId + " where  player_id = " + playerId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteNonQuery();
            }
        }

        public   void ChangeStoneColor(int playerId, int stoneColor)
        {
            using (var connection = DbManager.GetBaseDbConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "update BaseDB.dbo.players set stone_color= " + stoneColor + " where  player_id = " + playerId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteNonQuery();
            }
        }
    }


}
