﻿using System.Configuration;
using System.Data.SqlClient;

namespace DAL.Database
{
    public class DbManager
    {
        private static DbManager _managerInstance;
        private static readonly string ConnectionStringBaseDb = ConfigurationManager.ConnectionStrings["DefaultConnectionBaseDB"].ToString();
        private static readonly string ConnectionStringBackgammon = ConfigurationManager.ConnectionStrings["DefaultConnectionBackgammon"].ToString();

        public static DbManager GetDbInstance()
        {
            _managerInstance = _managerInstance ?? new DbManager();
            return _managerInstance;
        }        

        public static SqlConnection GetBaseDbConnection()
        {

            return new SqlConnection(ConnectionStringBaseDb);
        }

        public static SqlConnection GetBackgammonConnection()
        {

            return new SqlConnection(ConnectionStringBackgammon);
        }
        
    }
}
