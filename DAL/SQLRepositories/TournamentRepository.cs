﻿ 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Database;
using InterfaceLibrary.Models;
using EntityModels.Classes.GameClass;

namespace DAL.Database
{
    public class TournamentRepository :ITournamentRepository
    {
         
        private IFactory FactoryInstance { get; set;}

        public TournamentRepository(IFactory factory)
        {

            FactoryInstance = factory;

        }


        public  ITournament GetTournamentById(int tournamentId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from tournaments where tournament_status =1 and  id = " + tournamentId + "",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    var type = int.Parse(reader["tournir_type"].ToString());
                    var tournament = FactoryInstance.GetTournamentById(type);
                    tournament.TournamentType = (TournamentType)type;
                    tournament.TournamentId = int.Parse(reader["id"].ToString());

                    tournament.TournamentFee = decimal.Parse(reader["bet_amount"].ToString());
                    tournament.TournamentPrize = decimal.Parse(reader["prize"].ToString());
                    tournament.MaxCount = int.Parse(reader["players_max_count"].ToString());
                    tournament.State = TournamentState.Waiting;
                    tournament.SetGameType(int.Parse(reader["game_type"].ToString()));
                    tournament.IsActive = true;
                    tournament.MinimalCount = int.Parse(reader["min_count"].ToString());
                    tournament.FinalPoint = int.Parse(reader["final_point"].ToString());
                    if (type == (int)TournamentType.FreeRoll)
                    {
                        tournament.StartDate = DateTime.Parse(reader["start_date"].ToString());
                    }
                    tournament.PrizeList = GetTournamentPrizePool(tournament.TournamentId);
                    GetTournamentNames(tournament);
                    tournament.Activate();
                    return tournament;
                }

            }

            throw new Exception();

        }

        public  List<decimal> GetTournamentPrizePool(int tournamentId)
        {
            var result = new List<decimal>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from tournament_prize_pool where    tournamentId = " + tournamentId + " order by [index] asc",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var res = decimal.Parse(reader["percent"].ToString()) / 100;
                    result.Add(res);
                }

                if (result.Count == 0)
                {
                    throw new Exception();
                }
            }

            var prizePool = new List<decimal>();
            var divider = 2;
            for (var i = 0; i < result.Count; i++)
            {
                if (i < 2)
                {
                    prizePool.Add(result[i]);
                }
                else
                {
                    var prizeData = result[i] / divider;
                    for (int j = 0; j < divider; j++)
                    {
                        prizePool.Add(prizeData);
                    }
                    divider *= 2;
                }
            }

            return prizePool;

        }

        public  void GetTournamentNames(ITournament tournament)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from TournamentTranslations where    tournamentid = " + tournament.TournamentId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var lang = reader["lang"].ToString();
                    if (lang.Equals("ka-GE"))
                    {
                        tournament.TournamentNameGeo = name;
                    }
                    else if (lang.Equals("ru-RU"))
                    {
                        tournament.TournamentNameRus = name;
                    }
                    else if (lang.Equals("en-US"))
                    {
                        tournament.TournamentName = name;
                    }
                }

            }

        }

        public  void RegisterTournamentPlayer(ITournament tournament, int playerId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "sp_tournament_player_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                cmd.Parameters.AddWithValue("@player_id", playerId);
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
        public  void BlockTournamentFee(ITournament tournament, int playerId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_tournament_block",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var errorParam = new SqlParameter()
                {
                    ParameterName = "@error_code",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                var balanceParam = new SqlParameter()
                {
                    ParameterName = "@player_balance",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Money
                };
                var mainTransactionParam = new SqlParameter()
                {
                    ParameterName = "@main_transaction_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };
                cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                cmd.Parameters.AddWithValue("@player_id", playerId);
                cmd.Parameters.AddWithValue("@amount", tournament.TournamentFee);
                cmd.Parameters.Add(errorParam);
                cmd.Parameters.Add(balanceParam);
                cmd.Parameters.Add(mainTransactionParam);
                connection.Open();

                cmd.ExecuteNonQuery();
                var error = int.Parse(errorParam.Value.ToString());
                var balance = decimal.Parse(balanceParam.Value.ToString());
                var mainTransactionId = long.Parse(mainTransactionParam.Value.ToString());
                if (error == 0)
                { 
                    TournamentTransaction(tournament.TournamentId, playerId, -1 * tournament.TournamentFee, 6, mainTransactionId);
                }
                else
                {
                    var exception = FactoryInstance.GetExceptionById(error);
                    exception.Fatal = false;
                    exception.Throw();
                }

            }



        }

        public  List<int> GetRegisteredPlayers(ITournament tournament)
        {
            var result = new List<int>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from tournament_player where  tournament_Id = " + tournament.TournamentId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };

                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var res = int.Parse(reader["player_id"].ToString());
                    result.Add(res);
                }
            }

            return result;

        }

        public  List<ITournamentPlayer> GetRegisteredTournamentPlayers(List<int> playersId, int tourId)
        {
            var result = new List<ITournamentPlayer>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                connection.Open();
                for (int i = 0; i < playersId.Count; i++)
                {
                    using (var cmd = new SqlCommand()
                    {
                        CommandText = "select * from Basedb.dbo.players where   player_id = " + playersId[i],
                        CommandType = CommandType.Text,
                        Connection = connection
                    })
                    {
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var id = int.Parse(reader["player_id"].ToString());
                            var username = reader["user_name"].ToString();

                            var tournamentPlayer = new TournamentPlayer(id, username, tourId);
                            result.Add(tournamentPlayer);
                        }
                        reader.Close();
                    }
                }
            }

            return result;

        }
        public  void UnBlockTournamentFee(ITournament tournament, int playerId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_tournament_unblock",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                cmd.Parameters.AddWithValue("@player_id", playerId);

                connection.Open();

                cmd.ExecuteNonQuery();

            }
      
        }

        public  void TournamentTransaction(int tournamentId, int playerId, decimal amount, int operationType, long mainTransactionId)
        {

            try
            {
                if (amount != 0)
                {


                    using (var connection = DbManager.GetBackgammonConnection())
                    {

                        var cmd = new SqlCommand()
                        {
                            CommandText = "sp_tournament_transaction",
                            CommandType = CommandType.StoredProcedure,
                            Connection = connection
                        };

                        cmd.Parameters.AddWithValue("@tournament_id", tournamentId);
                        cmd.Parameters.AddWithValue("@playerID", playerId);
                        cmd.Parameters.AddWithValue("@amount", amount);
                        cmd.Parameters.AddWithValue("@opType", operationType);
                        cmd.Parameters.AddWithValue("@mainTransactionId", mainTransactionId);

                        connection.Open();

                        cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                FactoryInstance.Logger.Error($"TournamentTransaction: tournament_id={tournamentId} , player_id={playerId}", ex);

            }
        }

        public  void UpdateTournamentStatus(int tournamentId, int status)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "update tournaments set tournament_status =@status where id=@tournament_id",
                    CommandType = CommandType.Text,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournamentId);
                cmd.Parameters.AddWithValue("@status", status);

                connection.Open();

                cmd.ExecuteNonQuery();

            }

        }

        public  void GivePlayerWinAmount(ITournament tournament)
        {
            tournament.TopPlayers.Clear();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                connection.Open();
                var players = tournament.TournamentPlayers.OrderByDescending(e => e.TournamentLevel).ToList();
                for (var i = 0; i < tournament.PrizeList.Count; i++)
                {
                    if (i >= players.Count)
                    {
                        return;
                    }
                    tournament.TopPlayers.Add(players[i]);
                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_tournament_winner",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };

                    players[i].Prize = tournament.PrizeList[i] * tournament.TournamentPrize;
                    cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                    cmd.Parameters.AddWithValue("@player_id", players[i].PlayerId);
                    cmd.Parameters.AddWithValue("@win_amount", players[i].Prize); 

                    cmd.ExecuteNonQuery();

           
                }
            }

        }



        public  void UnblockAmountForAllPlayers(ITournament tournament)
        {
            try
            { 
                using (var connection = DbManager.GetBackgammonConnection())
                {
                    connection.Open();
                    foreach (var player in tournament.TournamentPlayers)
                    {
                        var cmd = new SqlCommand()
                        {
                            CommandText = "sp_tournament_unblock",
                            CommandType = CommandType.StoredProcedure,
                            Connection = connection
                        };

                        cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                        cmd.Parameters.AddWithValue("@player_id", player.PlayerId);
                        cmd.ExecuteNonQuery();
                
                    }

                }
            }
            catch (Exception ex)
            {
                FactoryInstance.Logger.Error($"UnblockAmountForAllPlayers  tournament_id={tournament.TournamentId}",ex);
            }
        }
        public  void TournamentBalanceReset(ITournament tournament)
        {
            using (var connection = DbManager.GetBackgammonConnection())
            {
                connection.Open();

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_tournament_balance_reset",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);

                cmd.ExecuteNonQuery();

            }
        }
        public  void UnblockAmountForSomePlayers(List<int> players, ITournament tournament)
        {
            using (var connection = DbManager.GetBackgammonConnection())
            {
                connection.Open();
                foreach (var playerId in players)
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_tournament_unblock",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };

                    cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                    cmd.Parameters.AddWithValue("@player_id", playerId);
                    cmd.ExecuteNonQuery();

                }

            }
        }

        public  void RemovePlayerFromTournament(int playerId, int tournamentId)
        {
            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "delete from tournament_player where player_id = @player_id and tournament_id= @tournament_id",
                    CommandType = CommandType.Text,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournamentId);
                cmd.Parameters.AddWithValue("@player_id", playerId);

                connection.Open();

                cmd.ExecuteNonQuery();

            }

        }

 

      

        public  int CreateTournamentRoom(ITournament tournament,int speed , int type)
        {
            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_tournament_room",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };
                var roomIdParam = new SqlParameter()
                {
                    ParameterName = "@room_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };
                cmd.Parameters.AddWithValue("@tournament_id", tournament.TournamentId);
                cmd.Parameters.AddWithValue("@tournament_name", tournament.TournamentName);
                cmd.Parameters.AddWithValue("@game_type", type);
                cmd.Parameters.AddWithValue("@game_speed", speed);
                cmd.Parameters.Add(roomIdParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                return (int)roomIdParam.Value;
            }

            throw new Exception();

        }

        public   void CloseTournament(int tournamentId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "update tournaments set end_date = getDate()  where id=@tournament_id",
                    CommandType = CommandType.Text,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@tournament_id", tournamentId);


                connection.Open();

                cmd.ExecuteNonQuery();

            }
        }
        public   void OpenTournament(int tournamentId)
        {
            try
            {


                using (var connection = DbManager.GetBackgammonConnection())
                {

                    var cmd = new SqlCommand()
                    {
                        CommandText = "update tournaments set start_date = getDate()  where id=@tournament_id",
                        CommandType = CommandType.Text,
                        Connection = connection
                    };

                    cmd.Parameters.AddWithValue("@tournament_id", tournamentId);


                    connection.Open();

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                FactoryInstance.Logger.Error($"OpenTournament: tournament_id={tournamentId} ", ex);
            }
        }
        public   List<int> GetTournnamentList()
        {

            var result = new List<int>();
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from tournaments where   tournament_status  = 1",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var id = int.Parse(reader["id"].ToString());
                    var type = int.Parse(reader["tournir_type"].ToString());

                    if (type == 1)
                    {
                        var date = DateTime.Parse(reader["start_date"].ToString());
                        var difference = (date - DateTime.Now).TotalMilliseconds;
                        if (difference > 0)
                        {
                            result.Add(id);
                        }

                    }
                    else
                    {
                        result.Add(id);
                    }

                }

            }

            return result; 

        }


    }
}
