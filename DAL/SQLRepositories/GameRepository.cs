﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InterfaceLibrary.Game;
using DAL.Database;
using InterfaceLibrary.Database;
using InterfaceLibrary.Factory;

namespace DAL.Database
{
    public class GameRepository :IGameRepository
    {
        private IFactory BaseFactory { get; set; }
        public GameRepository(IFactory factory) {

            BaseFactory = factory;
        }
        #region Game

        public  void RegisterGamePlayer(int gameId, int playerId,int color)
        {

            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_player_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@game_id", gameId);
                cmd.Parameters.AddWithValue("@player_id", playerId);
                cmd.Parameters.AddWithValue("@color", color);
                connection.Open();

                cmd.ExecuteNonQuery();

            }

        }

        public  void SaveGamePlayer(IGamePlayer pl)
        {
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_player_update",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@game_id", pl.GameId);
                cmd.Parameters.AddWithValue("@player_id", pl.PlayerId);
                cmd.Parameters.AddWithValue("@point", pl.GamePoint);
                cmd.Parameters.AddWithValue("@connected", pl.IsConnected);
                cmd.Parameters.AddWithValue("@is_winner", pl.IsWinner);
                 cmd.Parameters.AddWithValue("@ip", pl.ConnectionIP);
                 cmd.Parameters.AddWithValue("@device", (int)pl.Device);
                connection.Open();

                cmd.ExecuteNonQuery();

            }



        }

        public  void GameBet(int roomId, int gameId, IPlayerInfo player, decimal winAmount,int operationType)
        {

            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_transaction",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@room_id", roomId);
                cmd.Parameters.AddWithValue("@game_id", gameId);
                cmd.Parameters.AddWithValue("@player_id", player.PlayerId);
                cmd.Parameters.AddWithValue("@amount", winAmount);
                cmd.Parameters.AddWithValue("@op_type", 1);
                cmd.Parameters.AddWithValue("@main_transaction_id", 0);
                connection.Open();
                cmd.ExecuteNonQuery();

            }
          

        }

        public  void EndGame(int gameId, int winnerId, int opponent_id, decimal winAmount, int win_type)
        {

            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_close",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@game_id", gameId);
                cmd.Parameters.AddWithValue("@player_id", winnerId);
                cmd.Parameters.AddWithValue("@opponent_id", opponent_id);
                cmd.Parameters.AddWithValue("@win_amount", winAmount);
                cmd.Parameters.AddWithValue("@win_type", win_type);
                connection.Open();
                cmd.ExecuteNonQuery();

            }

        }

        public  void CancelGame(int gameId)
        {

            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_cancel",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@game_id", gameId);

                connection.Open();
                cmd.ExecuteNonQuery();

            }

        }

        public  int CreateGame(int roomId)
        {
            var result = 0;
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var gameParam = new SqlParameter()
                {
                    ParameterName = "@game_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                cmd.Parameters.AddWithValue("@room_id", roomId);
                cmd.Parameters.Add(gameParam);

                connection.Open();
                cmd.ExecuteNonQuery();

                result = (int)gameParam.Value;

            }


            return result;
        }

        #endregion


        #region Round
        public  int NewRound(int tableId)
        {
            var result = 0;
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_round_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var gameParam = new SqlParameter()
                {
                    ParameterName = "@round_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                cmd.Parameters.AddWithValue("@game_id", tableId);
                cmd.Parameters.Add(gameParam);

                connection.Open();
                cmd.ExecuteNonQuery();

                result = (int)gameParam.Value;

            }


            return result;

        }




        public  bool RegisterRoundPlayer(int roundId, int playerId)
        {
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_round_player_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@round_id", roundId);
                cmd.Parameters.AddWithValue("@player_id", playerId);

                connection.Open();

                cmd.ExecuteNonQuery();

            }

            return true;
        }

        public  void SaveRoundPlayer(IRoundPlayer pl)
        {
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_round_player_update",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@round_id", pl.RoundId);
                cmd.Parameters.AddWithValue("@player_id", pl.PlayerId);
                cmd.Parameters.AddWithValue("@point", pl.RoundPoint);

                connection.Open();

                cmd.ExecuteNonQuery();

            }

        }

        public  void PlayerConnectionChanged(int roomId, int gameId, int playerId, bool connected)
        {
            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_disconnect_player",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@room_id", roomId);
                cmd.Parameters.AddWithValue("@game_id", gameId);
                cmd.Parameters.AddWithValue("@player_id", playerId);
                cmd.Parameters.AddWithValue("@is_connected", connected);

                connection.Open();

                cmd.ExecuteNonQuery();

            }


        }

        public  bool SaveGameState(List<IMove> undoList ,IRoundPlayer pl,IDice dice)
        {
            try
            {

                using(var connection = DbManager.GetBackgammonConnection())
                {
                    connection.Open();
                    for(int i = 0; i < undoList.Count; i++)
                    {
 
                        var move = undoList[i];

                        var cmd = new SqlCommand()
                        {
                            CommandText = "sp_round_turn_create1",
                            CommandType = CommandType.StoredProcedure,
                            Connection = connection
                        };

                    cmd.Parameters.AddWithValue("@round_id", pl.RoundId);
                    cmd.Parameters.AddWithValue("@player_id", pl.PlayerId);
                    cmd.Parameters.AddWithValue("@game_state", move.BoardState);
                    cmd.Parameters.AddWithValue("@dice1", dice.First);
                    cmd.Parameters.AddWithValue("@dice2", dice.Second);
                    cmd.Parameters.AddWithValue("@is_winner",move.Winner);
                    cmd.Parameters.AddWithValue("@action",move.Action );
                    cmd.Parameters.AddWithValue("@killer", move.Killer);
                    cmd.Parameters.AddWithValue("@start_position", move.StartPosition);
                    cmd.Parameters.AddWithValue("@move_index", move.Value);
                    cmd.Parameters.AddWithValue("@raisex", move.RaiseX);
                    cmd.Parameters.AddWithValue("@roll", move.Roll);
                    cmd.Parameters.AddWithValue("@my_kill", move.MyKill);
                    cmd.Parameters.AddWithValue("@op_kill", move.OpKill);
                    cmd.Parameters.AddWithValue("@my_discard", move.MyDiscard);
                    cmd.Parameters.AddWithValue("@op_discard", move.OpDiscard);
                    cmd.Parameters.AddWithValue("@first_roll", move.FirstRoll);
                    cmd.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception ex)
            {
                BaseFactory.Logger.Error($"SaveGameState: round_id={pl.RoundId} , player_id={pl.PlayerId} ", ex);

            }
            return true;
        }

        public  void EndRound(int roundId, int winnerId, int winType)
        {

            using(var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_round_close",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                cmd.Parameters.AddWithValue("@round_id", roundId);
                cmd.Parameters.AddWithValue("@winner_id", winnerId);
                cmd.Parameters.AddWithValue("@win_type", winType);
                connection.Open();
                cmd.ExecuteNonQuery();

            }


        }



        #endregion


        public  decimal GameFinishCoins(IPlayerInfo playerInfo, int gameId, decimal betAmount, int operation,int roomId)
        {
            var result = 0m;
            var totalCoins = 0m;
            try
            { 

              /*  if(betAmount < GlobalConfig.AchievementMinBet)
                {
                    return result;
                } */

                using(var connection = DbManager.GetBackgammonConnection())
                {

                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_game_finish",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };

                    var coinParam = new SqlParameter()
                    {
                        ParameterName = "@coin_count",
                        Direction = ParameterDirection.Output,
                        SqlDbType = SqlDbType.Money
                    };

                    var coinTotalParam = new SqlParameter()
                    {
                        ParameterName = "@total_coin_count",
                        Direction = ParameterDirection.Output,
                        SqlDbType = SqlDbType.Money
                    };
                    cmd.Parameters.AddWithValue("@game_id", gameId);
                    cmd.Parameters.AddWithValue("@player_id", playerInfo.PlayerId);
                    cmd.Parameters.AddWithValue("@money", betAmount);
                    cmd.Parameters.AddWithValue("@operation_type", operation);
                    cmd.Parameters.Add(coinParam);
                    cmd.Parameters.Add(coinTotalParam);

                    connection.Open();
                    cmd.ExecuteNonQuery();

                    result = (decimal)coinParam.Value;
                    totalCoins = (decimal)coinTotalParam.Value;
                }
            }
            catch(Exception ex)
            {
                //log message
                BaseFactory.Logger.Error($"GameFinishCoins: game_id={gameId} , player_id={playerInfo.PlayerId} ", ex);
            }

            playerInfo.Coin = totalCoins;


            return result;
        }
    }
}
