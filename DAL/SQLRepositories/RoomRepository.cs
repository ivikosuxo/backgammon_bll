﻿ 
using System; 
using System.Data;
using System.Data.SqlClient; 
using InterfaceLibrary.Factory;
using InterfaceLibrary.Game; 
using InterfaceLibrary.Database;
using InterfaceLibrary.Models;

namespace DAL.Database
{
    public   class RoomRepository   : IRoomRepository
    {
        private IFactory Factory { get; set;}

        public RoomRepository(IFactory factory)
        {

            Factory = factory;
        }
        public   int CreateRoom(ICreateRoomModel model)
        { 
            var roomId = 0;
            var errorcode = 0;

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_room_create",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var errorCodeParam = new SqlParameter()
                {
                    ParameterName = "@error_code",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var roomIdParam = new SqlParameter()
                {
                    ParameterName = "@room_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };


                cmd.Parameters.AddWithValue("@roomname", model.Roomname);
                cmd.Parameters.AddWithValue("@creator", model.CreatorId);
                cmd.Parameters.AddWithValue("@speed", model.Speed);
                cmd.Parameters.AddWithValue("@min_bet", model.MinBet);
                cmd.Parameters.AddWithValue("@max_bet", model.MaxBet);
                cmd.Parameters.AddWithValue("@percent", model.Percent);
                cmd.Parameters.AddWithValue("@points", model.MaxPoint);
                cmd.Parameters.AddWithValue("@game_type", model.RuleType);
                cmd.Parameters.AddWithValue("@is_protect", model.IsProtected);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@enable_chat", model.ChatEnabled);

                //rules
                cmd.Parameters.AddWithValue("@beaver", model.BeaverRule);
                cmd.Parameters.AddWithValue("@davi", model.DoubleRule);
                cmd.Parameters.AddWithValue("@crawford", model.CrawfordRule);
                cmd.Parameters.AddWithValue("@jacob", model.JacobRule);

                cmd.Parameters.AddWithValue("@with_double", model.WithDouble);

                cmd.Parameters.Add(errorCodeParam);
                cmd.Parameters.Add(roomIdParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                errorcode = (int)errorCodeParam.Value;
                roomId = (int)roomIdParam.Value;
            }

            if (errorcode > 0)
            {
                var exception=  Factory.GetExceptionById(errorcode);
                exception.Throw();
            }


            return roomId;
        }
         
        public   void BlockBalance(IPlayerInfo player,  int roomId,decimal minBet, decimal maxbet )
        {

            //DB lock transaction
            if (minBet == 0)
            {
                return;
            }
            var errorcode = 0;
            var balance = 0m;
            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_room_block",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var errorCodeParam = new SqlParameter()
                {
                    ParameterName = "@error_code",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var balanceParam = new SqlParameter()
                {
                    ParameterName = "@player_balance",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Money
                };


                cmd.Parameters.AddWithValue("@room_id", roomId);
                cmd.Parameters.AddWithValue("@user_id", player.PlayerId);
                cmd.Parameters.AddWithValue("@amount", maxbet);
                cmd.Parameters.Add(errorCodeParam);
                cmd.Parameters.Add(balanceParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                errorcode = (int)errorCodeParam.Value;
                balance = (decimal)balanceParam.Value;
            }
            if (errorcode > 0)
            {
                var exception = Factory.GetExceptionById(errorcode);
                exception.Throw();
            }

        
        }

        public decimal UnblockAmount(int playerId, int roomId)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {

                var cmd = new SqlCommand()
                {
                    CommandText = "sp_room_unblock",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };


                var balanceParam = new SqlParameter()
                {
                    ParameterName = "@player_balance",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Money
                };


                cmd.Parameters.AddWithValue("@room_id", roomId);
                cmd.Parameters.AddWithValue("@user_id", playerId);
                cmd.Parameters.Add(balanceParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                var balance = (decimal)balanceParam.Value;

                return balance;
            }

        }

        public   void RoomDeactivate(int roomId)
        {
            try
            {
                using (var connection = DbManager.GetBackgammonConnection())
                {

                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_room_close",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };
                    cmd.Parameters.AddWithValue("@room_id", roomId);

                    connection.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                Factory.Logger.Error($"RoomDeactivate: room_id={roomId} ", ex);
            }

        }

        public   bool ProblemReport(int roomId, int gameId, int roundId, string token, string problemText)
        {
            var success = true;
            try
            {


                using (var connection = DbManager.GetBaseDbConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_reportproblem",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };


                    cmd.Parameters.AddWithValue("@sessionId", token);
                    cmd.Parameters.AddWithValue("@text", problemText);
                    cmd.Parameters.AddWithValue("@imagePath", "path");
                    cmd.Parameters.AddWithValue("@roomId", roomId);
                    cmd.Parameters.AddWithValue("@gameId", gameId);
                    cmd.Parameters.AddWithValue("@roundId", gameId);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }


        public   void SaveChatMessage(int roomId, int playerId,   int gameId, string message)
        {  
                using (var connection = DbManager.GetBaseDbConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_save_chat_message",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    }; 

                    cmd.Parameters.AddWithValue("@room_id", roomId);
                    cmd.Parameters.AddWithValue("@player_id", playerId); 
                    cmd.Parameters.AddWithValue("@game_id", gameId);
                    cmd.Parameters.AddWithValue("@message", message);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                } 
        }

    }
}
