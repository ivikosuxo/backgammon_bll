﻿using  EntityModels.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Data.SqlClient; 
using InterfaceLibrary.Database;
 

namespace DAL.Database
{
    public class HistoryRepository : IHistoryRepository
    {
        public   JArray GetMoves(int roundId) 
        {
            var result = new JArray();

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from round_turn where round_id = " + roundId + " order by round_turn_id asc",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var config = new JObject();
                    
                    config["player_id"] = int.Parse(reader["player_id"].ToString());
                    config["board_state"] = reader["board_state"].ToString();
                    config["dice_1"] = int.Parse(reader["dice1"].ToString());
                    config["dice_2"] = int.Parse(reader["dice2"].ToString());
                    config["is_winner"] = bool.Parse(reader["is_winner"].ToString());
                    config["roll"] = bool.Parse(reader["roll"].ToString());
                    config["raise"] = int.Parse(reader["raisex"].ToString());
                    config["action"] = int.Parse(reader["action"].ToString());
                    config["killer"] = bool.Parse(reader["killer"].ToString());
                    config["move_index"] = int.Parse(reader["move_index"].ToString());
                    config["my_discard"] = int.Parse(reader["my_discard"].ToString());
                    config["op_discard"] = int.Parse(reader["op_discard"].ToString());
                    config["op_kill"] = int.Parse(reader["op_kill"].ToString());
                    config["my_kill"] = int.Parse(reader["my_kill"].ToString());
                     var firstRoll = false;
                   bool.TryParse(reader["first_roll"]?.ToString(),out firstRoll);
                    config["first_roll"] = firstRoll;
                     
                    
                    
                result.Add(config);
                } 
            } 
            return result;
        }

        public   JObject GetRoundInfo(int roundId)
        {

    
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from game_round  where round_id = " + roundId ,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var config = new JObject()
                    {
                        ["winner_id"] = int.Parse(reader["round_winner_id"].ToString()),
                        ["end_type"] = reader["end_round_type"].ToString(),
                        ["completed"] = bool.Parse(reader["completed"].ToString()),
                        ["round_id"] = int.Parse(reader["round_id"].ToString()),
                        ["game_id"] = int.Parse(reader["game_id"].ToString()),                       
                    };
                     
                    return config;
                }
            }
            throw new GameException();
        }
        public   JArray GetRounds(int gameId)
        {

            var result = new JArray();

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select round_id ,round_winner_id from game_round where round_winner_id >0 and game_id = " + gameId+" order by round_id asc",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var config = new JObject()
                    {
                        ["round_id"] = int.Parse(reader["round_id"].ToString()),
                        ["winner_id"] = int.Parse(reader["round_winner_id"].ToString()),
                    };
                    result.Add(config);
                   
                }
            }
            return result;
        }

 

        public   int GetPlayerId(int gameId, int plId) {
            var result = 0;
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select  player_id from game_players where game_id = " + gameId + " and player_id = " + plId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

               
                while (reader.Read())
                {
                    var playerId = int.Parse(reader["player_id"].ToString());


                    if (playerId == plId)
                    {
                        return playerId;
                    } else if (result==0) {
                        result = playerId;
                    }

                }
            }

            return result;
        }
 
        public   JArray GetGamePlayers(int gameId,int plyaerId)
        {
            var arr = new JArray();
            var defPlayer = plyaerId;
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from game_players where game_id = " + gameId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int currentPlayer = int.Parse(reader["player_id"].ToString());  
                    if (defPlayer==0)
                    {
                        defPlayer = currentPlayer;
                    }

                    var plId = int.Parse(reader["player_id"].ToString());
                    var config = new JObject();
                    config["score"] = int.Parse(reader["score"].ToString());
                    config["color"] = int.Parse(reader["color"].ToString());
                    config["player_id"] = currentPlayer;
                    config["me"] = defPlayer == currentPlayer;
                    arr.Add(config);
                }

                return arr;
            }
            throw new GameException();
        }
        public   JObject GetGameInfo(int game_Id)
        {



            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from game where game_id = " + game_Id,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var completed = bool.Parse(reader["game_status"].ToString());

                    var config = new JObject();

                        config["game_speed"] = int.Parse(reader["game_speed"].ToString());
                        config["winning_score"] = reader["winning_score"].ToString();
                        config["total_bet"] =  double.Parse(reader["total_bet"].ToString());
                        config["winner_id"] = int.Parse(reader["winner_id"].ToString());
                        config["completed"] = completed;
                        config["game_type"] = int.Parse(reader["game_type"].ToString());

                    if (completed)
                     {
                        config["win_type"] = int.Parse(reader["win_type"].ToString());
                    }
                    return config;
                }
            }
            throw new GameException();
        }


        public   JArray GetAllGamesInfo(int playerId)
        { 
            var result = new JArray();

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "sp_game_all_history",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };


                cmd.Parameters.AddWithValue("@player_id", playerId);
                
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                   

                    var config = new JObject();

                    config["game_id"] = int.Parse(reader["game_id"].ToString());
                    config["game_speed"] = int.Parse(reader["game_speed"].ToString());       
                    config["total_bet"] =  double.Parse(reader["total_bet"].ToString());
                    config["op_name"] = reader["user_name"].ToString();                     
                    config["my_score"] = int.Parse(reader["my_score"].ToString());
                    config["op_score"] = int.Parse(reader["op_score"].ToString());
                    config["winner"] = bool.Parse(reader["winner"].ToString());
                    config["game_type"] = int.Parse(reader["game_type"].ToString());
                    config["time"] =  DateTime.Parse(reader["time"].ToString()) ;
                    config["rake"] = decimal.Parse( reader["rake"].ToString());
                    config["win_type"] = int.Parse(reader["win_type"].ToString());
                    config["type"] = int.Parse(reader["game_type"].ToString());
                    result.Add(config);
                }
            }


            return result;
        }

    }
}
