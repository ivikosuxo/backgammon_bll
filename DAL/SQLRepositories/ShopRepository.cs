﻿using BLL.Classes.Entities;
 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.Entities;
using InterfaceLibrary.Database;
using InterfaceLibrary.Models;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Factory;
using DAL.Database;
using InterfaceLibrary.Game;

namespace DAL.Database
{
    public   class ShopRepository :IShopRepository
    {

        private IFactory SystemFactory { get; set; }
        public ShopRepository(IFactory factory)
        {

            SystemFactory = factory;
        }
        public   IShopProduct  GetShopProduct(int productId)
        { 
            IShopProduct result=null;
            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_products where id = "+ productId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result = new ShopProduct();
                    result.Id = int.Parse(reader["id"].ToString());
                    result.Count = int.Parse(reader["count"].ToString());
                    result.CoinPrice = int.Parse(reader["coin_price"].ToString());
                    result.Category = int.Parse(reader["shop_product_type_id"].ToString());
                    result.StockCount = int.Parse(reader["stock_count"].ToString());
                    result.MinRank = int.Parse(reader["min_rank"].ToString());
                    result.ImgUrl = reader["img_url"].ToString();
                    result.IsActive = bool.Parse(reader["is_active"].ToString());
                    result.IsDeleted = bool.Parse(reader["is_deleted"].ToString());
                    GetProductNames(result); 
                }

            }
            return result;
        }

         
        public   List<ICategoryTranslate>  GetCategories()
        {

            var result = new List<ICategoryTranslate>();
            using(var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_product_types where isdeleted = 0",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    var product = new CategoryTranslate(); 
                    product.Id = int.Parse(reader["id"].ToString());   
                    GetCategoryNames(product);
                    result.Add(product);
                } 
            }
            return result;
        }

         
        public   List<IShopProduct> GetShopProducts()
        {

            var result = new List<IShopProduct>();
            using(var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_products where is_active =1 and is_deleted = 0",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    var product = new ShopProduct(); 
                    product.Id = int.Parse(reader["id"].ToString());  
                    product.Count = int.Parse(reader["count"].ToString());
                    product.Category = int.Parse(reader["shop_product_type_id"].ToString());
                    product.CoinPrice = int.Parse(reader["coin_price"].ToString());
                    product.StockCount = int.Parse(reader["stock_count"].ToString());
                    product.MinRank = int.Parse(reader["min_rank"].ToString());
                    product.ImgUrl = reader["img_url"].ToString();
                    product.IsActive = bool.Parse(reader["is_active"].ToString());
                    product.IsDeleted = bool.Parse(reader["is_deleted"].ToString());
                    GetProductNames(product);
                    result.Add(product);
                }

            }
            return result;
        }

        public   void GetProductNames(IShopProduct product)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_products_translations where    shopProductId = " + product.Id,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var lang = reader["lang"].ToString();
                    var description = reader["description"].ToString();
                    if (lang.Equals("ka-GE"))
                    {
                        product.NameGeo = name;
                        product.DescriptionGeo = description;
                    }
                    else if (lang.Equals("ru-RU"))
                    {
                        product.NameRus = name;
                        product.DescriptionRus = description;
                    }
                    else if (lang.Equals("en-US"))
                    {
                        product.Name = name;
                        product.Description = description;
                    }
                }

            }

        }

        public   void GetCategoryNames(ICategoryTranslate product)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_product_type_translations where    ShopProductTypeId = " + product.Id,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var lang = reader["lang"].ToString();
                
                    if (lang.Equals("ka-GE"))
                    {
                        product.NameGeo = name;
                        
                    }
                    else if (lang.Equals("ru-RU"))
                    {
                        product.NameRus = name;
                 
                    }
                    else if (lang.Equals("en-US"))
                    {
                        product.Name = name; 
                    }
                }

            }

        }

        public   void GetOrderProductNames(IOrderedProduct product)
        {

            using (var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from shop_products_translations where    shopProductId = " + product.ProductId,
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var lang = reader["lang"].ToString();
                   
                    if (lang.Equals("ka-GE"))
                    {
                        product.NameGeo = name;
                       
                    }
                    else if (lang.Equals("ru-RU"))
                    {
                        product.NameRus = name;
                        
                    }
                    else if (lang.Equals("en-US"))
                    {
                        product.Name = name;
                        
                    }
                }

            }

        }


        public   List<IOrderedProduct> GetOrderedProducts()
        {

            var result = new List<IOrderedProduct>();
            using(var connection = DbManager.GetBackgammonConnection())
            {
                var cmd = new SqlCommand()
                {
                    CommandText = "select * from ordered_products",
                    CommandType = CommandType.Text,
                    Connection = connection
                };
                connection.Open();
                var reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    var product = new OrderedProduct();

                    product.Id = int.Parse(reader["id"].ToString());
                    product.PlayerId = int.Parse(reader["player_id"].ToString());
                    product.Comment = reader["comment"].ToString();
                    product.OrderTime = DateTime.Parse(reader["create_time"].ToString());
                    product.OrderStatus = (OrderStatus)int.Parse(reader["order_status"].ToString());
                    product.TransactionId = long.Parse(reader["transaction_id"].ToString());
                    product.Count =   int.Parse(reader["count"].ToString());
                    product.Price = decimal.Parse(reader["price"].ToString());
                    product.ImgUrl = reader["img_url"].ToString();
                    product.ProductId = int.Parse(reader["product_id"].ToString());

                    GetOrderProductNames(product);
                    result.Add(product);
                }

            }
            return result;
        }

        public   List<IOrderedProduct> GetOrderedProducts(int playerId)
        { 
            var result = new List<IOrderedProduct>();
            try
            {
                using (var connection = DbManager.GetBackgammonConnection())
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "select * from ordered_products where player_id = " + playerId,
                        CommandType = CommandType.Text,
                        Connection = connection
                    };
                    connection.Open();
                    var reader = cmd.ExecuteReader(); 
                    while (reader.Read())
                    {
                        var product = new OrderedProduct(); 
                        product.Id = int.Parse(reader["id"].ToString());
                        product.PlayerId = int.Parse(reader["player_id"].ToString());
                        product.Comment = reader["comment"].ToString();
                        product.OrderStatus = (OrderStatus)int.Parse(reader["order_status"].ToString());
                        product.TransactionId = long.Parse(reader["transaction_id"].ToString());
                        product.OrderTime = DateTime.Parse(reader["create_time"].ToString());
                        product.Count = int.Parse(reader["count"].ToString());
                        product.Price = decimal.Parse(reader["price"].ToString());
                        product.ImgUrl = reader["img_url"].ToString();
                        product.ProductId = int.Parse(reader["product_id"].ToString());
                        GetOrderProductNames(product);
                        result.Add(product);
                    } 
                }
            }
            catch (Exception ex)
            {
                SystemFactory.Logger.Error($"GetOrderedProducts: player_id={playerId}", ex);
            }
            return result;
        }

        public   void BuyProduct(IPlayerInfo player, IShopProduct product, int count)
        { 
            using(var connection = DbManager.GetBackgammonConnection())
            {


                var cmd = new SqlCommand()
                {
                    CommandText = "sp_shop_buy",
                    CommandType = CommandType.StoredProcedure,
                    Connection = connection
                };

                var errorCodeParam = new SqlParameter()
                {
                    ParameterName = "@error_code",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var coinParam = new SqlParameter()
                {
                    ParameterName = "@coin_balance",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Money
                };

                var orderProductId = new SqlParameter()
                {
                    ParameterName = "@ordered_product_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.Int
                };

                var transactionParam = new SqlParameter()
                {
                    ParameterName = "@transaction_id",
                    Direction = ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };



                cmd.Parameters.AddWithValue("@product_id", product.Id);
                cmd.Parameters.AddWithValue("@player_id", player.PlayerId);
                cmd.Parameters.AddWithValue("@count", count);


                cmd.Parameters.Add(errorCodeParam);
                cmd.Parameters.Add(coinParam);
                cmd.Parameters.Add(orderProductId);
                cmd.Parameters.Add(transactionParam);
                connection.Open();
                cmd.ExecuteNonQuery();

                var errorcode = (int)errorCodeParam.Value;
           
              

                if(errorcode > 0)
                {
                    var exception = SystemFactory.GetExceptionById(errorcode);
                    exception.Throw();
                }
                else
                {
                    var coin = (decimal)coinParam.Value;
                    var id = (int)orderProductId.Value;
                    var trId = (long)transactionParam.Value;
                    var prod = new OrderedProduct()
                    {
                        Id = id,
                        Comment = "",
                        OrderStatus = OrderStatus.Ordered,
                        OrderTime = DateTime.Now,
                        PlayerId = player.PlayerId, 
                        Count= count,
                        ImgUrl = product.ImgUrl,
                        ProductId = product.Id,
                        Price = product.CoinPrice*count,
                        TransactionId = trId,
                        Name =  product.Name , 
                        NameGeo =  product.NameGeo ,  //nameGeo
                        NameRus =  product.NameRus   //nameRus
                    };


                    player.Coin = coin;
                    player.OrderedProducts.Add(prod);

            
                }
            }
        }
    }
}
