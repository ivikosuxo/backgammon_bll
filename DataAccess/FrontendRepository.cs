﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class FrontendRepository
    {
        private static readonly string ConnectionStringBaseDb = ConfigurationManager.ConnectionStrings["DefaultConnectionBaseDB"].ToString();
        private static readonly string ConnectionStringBackgammon = ConfigurationManager.ConnectionStrings["DefaultConnectionBackgammon"].ToString();

        public static bool ProblemReport(int roomId, int gameId, int roundId, string token, string problemText)
        {
            var success = true;
            try
            {

                using (var connection = new SqlConnection(ConnectionStringBaseDb))
                {
                    var cmd = new SqlCommand()
                    {
                        CommandText = "sp_reportproblem",
                        CommandType = CommandType.StoredProcedure,
                        Connection = connection
                    };

                    cmd.Parameters.AddWithValue("@sessionId", token);
                    cmd.Parameters.AddWithValue("@text", problemText);
                    cmd.Parameters.AddWithValue("@imagePath", "path");
                    cmd.Parameters.AddWithValue("@roomId", roomId);
                    cmd.Parameters.AddWithValue("@gameId", gameId);
                    cmd.Parameters.AddWithValue("@roundId", gameId);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

    }
}
