﻿using BLL.Classes.Entities;
using BLL.Database;
using BLL.Exceptions;
using BLL.Managers;
using BLL.Types;
using Ganss.XSS;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BLL_API.Controllers
{
    public class GameController : Controller
    {
        [HttpPost]
        public string CreateRoom()
        {
            //var model = JsonConvert.DeserializeObject<CreateRoomModel>(jsonData);

            var result = new JObject();
            try
            {
                var req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                var json = new StreamReader(req).ReadToEnd();


                var input = JObject.Parse(json);

                var maxBet = input["max_bet"].Value<int>();
                var raiseCount = maxBet;
                var model = new CreateRoomModel();

                model.MinBet = input["min_bet"].Value<decimal>();
                model.MaxPoint = input["max_point"].Value<int>();
                model.RaiseCount = raiseCount;
                model.BeaverRule = input["beaver"].Value<bool>();
                model.CrawfordRule = input["crawford"].Value<bool>();
                model.DoubleRule = input["double"].Value<bool>();
                model.JacobRule = input["jacob"].Value<bool>();
                model.RuleType = input["type"].Value<int>();
                model.Password = input["password"].Value<string>().Replace(" ", "");

                model.Speed = input["speed"].Value<int>();
                model.Token = input["token"].Value<string>().Replace(" ", "");
                model.ChatEnabled = input["chat_enabled"].Value<bool>();


                result = RoomManager.CreateRoom(model);
            }
            catch (GameException ex)
            {
                result["id"] = ex.ErrorId;
                result["room_id"] = 0;
            }

            catch (Exception)
            {
                result["id"] = ErrorCodes.UnexpectedToken;
                result["room_id"] = 0;
            }


            return result.ToString();
        }

        // GET: Game
        [HttpPost]
        public string ProblemDetected()
        {
            var result = new JObject();
            try
            {

                var req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                var json = new StreamReader(req).ReadToEnd();
                var input = JObject.Parse(json);

                var token = input["token"].Value<string>();
                var problem = input["text"].Value<string>();
                var length = problem.Length;
                length = length > 512 ? 512 : length;
                problem = problem.Substring(0, length);
                var roomId = input["room_id"].Value<int>();
                var gameId = input["game_id"].Value<int>();
                var roundId = input["round_id"].Value<int>();

                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedCssClasses?.Clear();
                sanitizer.AllowedAtRules?.Clear();
                sanitizer.AllowedAttributes?.Clear();
                sanitizer.AllowedCssProperties?.Clear();
                sanitizer.AllowedSchemes?.Clear();
                sanitizer.AllowedTags?.Clear();
                var problemSanitized = sanitizer.Sanitize(problem);
                result["success"] = RoomRepository.ProblemReport(roomId, gameId, roundId, token, problemSanitized);
            }
            catch (Exception)
            {
                result["success"] = false;
            }

            return result.ToString();
        }

       
    }
}