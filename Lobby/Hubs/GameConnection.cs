﻿using BLL.Managers;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Lobby.Hubs
{
    public class GameConnection :PersistentConnection
    {
        protected override bool AuthorizeRequest(IRequest request)
        {

            return base.AuthorizeRequest(request);
        }
        protected override Task OnConnected(IRequest request, string sessionId)
        {
            var token = request.QueryString["token"];
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            socketManager.OnConnect(sessionId, token);
            return base.OnConnected(request, sessionId);
        }

        protected override Task OnReceived(IRequest request, string sessionId, string data)
        {
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            socketManager.OnMessage(sessionId, data);
           
            return null; //Connection.Broadcast(data);

        }
        protected override Task OnDisconnected(IRequest request, string sessionId, bool stopCalled)
        {
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            socketManager.OnDisconnect(sessionId);
            return base.OnDisconnected(request, sessionId, stopCalled);
        }
        protected override Task OnReconnected(IRequest request, string connectionId)
        {
            return base.OnReconnected(request, connectionId);
        }

    }
}