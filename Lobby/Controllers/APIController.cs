﻿
using BLL.Managers;
using EntityModels.Entities;
using  EntityModels.Types;
using Lobby.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Lobby.Controllers
{
    public class APIController : Controller
    {
        // GET: API
        private string _guid = "49746fb2-b4e2-4c9a-9bcc-006a09a53666";

        //achievement calls
        [HttpPost]
        public JsonResult ActivateAchievement(int achievementId, string hash)
        {

            if (validate(achievementId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"
                });
            }
            var achievementManager = BaseApplication.Current.AchievementManagerInstance;
            var result = achievementManager.ActivateAchievement(achievementId);
            return Json(new
            {
                success = result
            });
        }
        [HttpPost]
        public JsonResult DeactivateAchievement(int achievementId, string hash)
        {
            if (validate(achievementId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            var achievementManager = BaseApplication.Current.AchievementManagerInstance;
            var result = achievementManager.DeactivateAchievement(achievementId);

            return Json(new
            {
                success = result
            });
        }

        //tournament calls
        [HttpPost]
        public JsonResult ActivateTournament(int tournamentId, string hash)
        {

            if (validate(tournamentId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }

            var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
            var result = tournamentManager.ActivateTournament(tournamentId);
            tournamentManager.OrderTournaments();

            return Json(new
            {
                success = result
            });
        }

        public JsonResult ChangeTournamentName(int tournamentId, string hash)
        { 
            if (validate(tournamentId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash" 
                });
            }
            var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
            var result = tournamentManager.ChangeTournamentName(tournamentId);

            return Json(new
            {
                success = result
            });
        }


        [HttpPost]
        public JsonResult DeactivateTournament(int tournamentId, string hash)
        {
            if (validate(tournamentId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"
                });
            }
            var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
            var result = tournamentManager.CancelTournament(tournamentId, ErrorCodes.TournamentCanceled);

            return Json(new
            {
                success = result
            });
        }


        [HttpPost]
        public JsonResult TournamentManualWin(int tournamentId,int playerId, string hash)
        {
            ApplicationManager.Logger.Error($"HttpPost TournamentManualWin: tournament_id={tournamentId}, player_id={playerId}");
            if (validate(tournamentId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"
                });
            }
            var result = true;

            try
            {
                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
                var tournament = tournamentManager.GetTournamentById(tournamentId);
                tournament.TournamentGameWin(playerId);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($" HttpPost TournamentManualWin exception: tournament_id={tournamentId}, player_id={playerId}" ,ex);
                result = false;
            }

         

            return Json(new
            {
                success = result
            });
        }



        [HttpPost]
        public JsonResult DeleteTournament(int tournamentId, string hash)
        {
            if (validate(tournamentId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"
                });
            }
            var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
            var result = tournamentManager.DeleteTournament(tournamentId);

            return Json(new
            {
                success = result
            });
        }

        //shop api calls


        [HttpPost]
        public JsonResult ActivateShopProduct(int productId, string hash)
        {

            if (validate(productId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            var shopManager = BaseApplication.Current.BaseShopManagerInstance;
            var result = shopManager.GetShopProduct(productId);

            return Json(new
            {
                success = result
            });
        }



        [HttpPost]
        public JsonResult DeactivateShopProduct(int productId, string hash)
        {

            if (validate(productId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            var shopManager = BaseApplication.Current.BaseShopManagerInstance;
            var result = shopManager.DeactivateShopProduct(productId);
           

            return Json(new
            {
                success = result
            });
        }





        [HttpPost]
        public JsonResult GetGameInfo(string randomStr, string hash)
        {

            if (validate(string.Empty, hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            return Json(new
            {
                players = playerManager.PlayersCount(),
                rooms = roomManager.Rooms.Count,
                games = roomManager.Rooms.Values.Count(e => e.IsGameState(false)),
                tournamentGames = roomManager.Rooms.Values.Count(e => e.IsGameState(true))
            });
        }


        [HttpPost]
        public JsonResult GetActivePlayers( string hash)
        {

            if (validate(string.Empty, hash, _guid))
            {

                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                }) ;
            }
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var result = new OnlinePlayersResult(playerManager.GetActivePlayers()); 
            return Json(result);  
        }


        [HttpPost]
        public JsonResult ChangeStatusCreateRoom(string date, bool canCreate, string message, string hash)
        {
            if (validate(date, hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            BlockCreateRoomModel.CanCreateRoom = canCreate;
            BaseApplication.Current.PlayerManagerInstance.PlayerRepository.UpdateCreateRoomProperty();

            var data = ApplicationManager.GetBlockedRoomMessage();
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessageAll(CommandKeys.can_create_room, data);

            return Json(new
            {
                success = true
            });
        }

        [HttpPost]
        public JsonResult BlockPlayer(int playerId, string hash)
        {
            if (validate(playerId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"

                });
            }
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var result = playerManager.BlockPlayer(playerId);

            return Json(new
            {
                success = result
            });
        }
        [HttpPost]
        public JsonResult UnblockPlayer(int playerId, string hash)
        {
            if (validate(playerId.ToString(), hash, _guid))
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "incorrect hash"
                });
            }

            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var result = playerManager.UnblockPlayer(playerId); 
            return Json(new
            {
                success = result
            });
        }




        private string hash(string localHash, string key)
        { 
            var xkey = Encoding.UTF8.GetBytes(key);
            var hmacSha256 = new HMACSHA256(xkey);
            var data = Encoding.UTF8.GetBytes(localHash);
            var b = hmacSha256.ComputeHash(data);
            var sBuilder = new StringBuilder(); 
            foreach (var t in b)
            {
                sBuilder.Append(t.ToString("x2"));
            } 
            return sBuilder.ToString(); 
        }

        [HttpPost]
        public JsonResult UpdateGameSettings()
        { 
            var result = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.UpdateCreateRoomProperty(); 
            var data = ApplicationManager.GetBlockedRoomMessage();

            var roomManager = BaseApplication.Current.RoomManagerInstance;

            roomManager.LobbyRoom.SendMessageAll(CommandKeys.can_create_room, data);
            roomManager.LobbyRoom.UpdateStatuses();
            return Json(new
            {
                success = result
            });
        }

        private bool validate(string localHash, string param, string key)
        {
            return !hash(localHash, key).Equals(param);
        }
    }
}