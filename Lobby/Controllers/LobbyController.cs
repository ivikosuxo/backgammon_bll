﻿using System.Web.Mvc;
using System;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using Helpers;
using Ganss.XSS;

using BLL.Classes.Entities;
using BLL.Managers;
using  EntityModels.Exceptions;
using  EntityModels.Types;

using System.Configuration;
using Lobby.Models;
using EntityModels.Entities;

namespace Lobby.Controllers
{
    public class LobbyController : Controller
    {
        public static int counter = 0;
        private object ErroCodes { get; set; }
        private string CreateRoomUrl { get; set; } = "http://Socket.nardi.ge/Game/CreateRoom";

        private string _signalRUrl;
        private string _createRoomUrl;

        public LobbyController()
        {
            _signalRUrl = ConfigurationManager.AppSettings["SignalRUrl"];
            _createRoomUrl = ConfigurationManager.AppSettings["CreateRoomUrl"];
        }

        public ActionResult Lobby(string token)
        {
            var viewModel = new LobbyViewModel
            {
                SignalRUrl = _signalRUrl,
                CreateRoomUrl = _createRoomUrl
            };

            return View(viewModel);
        }

        public ActionResult MobileLobby(string token)
        {
            var viewModel = new MobileLobbyViewModel
            {
                SignalRUrl = _signalRUrl,
                CreateRoomUrl = _createRoomUrl
            };

            return View(viewModel);
        }

        public ActionResult Backgammon(string token, string room)
        {
            var viewModel = new BackgammonViewModel
            {
                SignalRUrl = _signalRUrl
            };

            return View(viewModel);
        }

        public ActionResult History(string token, string game_id)
        {
            var viewModel = new HistoryViewModel
            {
                SignalRUrl = _signalRUrl
            };

            return View(viewModel);
        }

        public ActionResult Tournament(string token, string room)
        {
            var viewModel = new TournamentViewModel
            {
                SignalRUrl = _signalRUrl
            };

            return View(viewModel);
        }

        [HttpPost]
        public string ProblemDetected()
        {
            var result = new JObject();
            try
            {
                var req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                var json = new StreamReader(req).ReadToEnd();
                var input = JObject.Parse(json);

                var token = input["token"].Value<string>();
                var problem = input["text"].Value<string>();
                var length = problem.Length;
                length = length > 512 ? 512 : length;
                problem = problem.Substring(0, length);
                var roomId = input["room_id"].Value<int>();
                var gameId = input["game_id"].Value<int>();
                var roundId = input["round_id"].Value<int>();

                if (length==0)
                {
                    throw new Exception();
                }

                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedCssClasses?.Clear();
                sanitizer.AllowedAtRules?.Clear();
                sanitizer.AllowedAttributes?.Clear();
                sanitizer.AllowedCssProperties?.Clear();
                sanitizer.AllowedSchemes?.Clear();
                sanitizer.AllowedTags?.Clear();

                var problemSanitized = sanitizer.Sanitize(problem);


                var roomDbManager = BaseApplication.Current.RoomManagerInstance.RoomDbManager;
                result["success"] = roomDbManager.ProblemReport(roomId, gameId, roundId, token, problemSanitized);

            }
            catch (Exception)
            {
                result["success"] = false;
            }

            return result.ToString();
        }

        [HttpPost]
        public string CreateRoom()
        {
            //var model = JsonConvert.DeserializeObject<CreateRoomModel>(jsonData);

            var result = new JObject();
            try
            {
                var req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                var json = new StreamReader(req).ReadToEnd();


                var input = JObject.Parse(json);
                var maxBet = input["max_bet"].Value<int>();
                var raiseCount = maxBet;
                var model = new CreateRoomModel();

                model.MinBet = input["min_bet"].Value<decimal>();
                model.MaxPoint = input["max_point"].Value<int>();
                model.RaiseCount = raiseCount;
                model.BeaverRule = input["beaver"].Value<bool>();
                model.CrawfordRule = input["crawford"].Value<bool>();
                model.DoubleRule = input["double"].Value<bool>();
                model.JacobRule = input["jacob"].Value<bool>();
                model.RuleType = input["type"].Value<int>();
                model.Password = input["password"].Value<string>().Replace(" ", "");

                model.Speed = input["speed"].Value<int>();
                model.Token = input["token"].Value<string>().Replace(" ", "");
                model.ChatEnabled = input["chat_enabled"].Value<bool>();
                model.WithDouble = input["with_double"].Value<bool>();

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                result = roomManager.CreateRoom(model);

            }
            catch (GameException ex)
            {
                result["id"] = ex.ErrorId;
                result["room_id"] = 0;
            }

            catch (Exception)
            {
                result["id"] = ErrorCodes.UnexpectedToken;
                result["room_id"] = 0;
            }


            return result.ToString();
        }
        public ActionResult Translations()
        {
            var cultureInfo = Thread.CurrentThread.CurrentCulture;

            var data = new
            {
                ka_GE = GenerateJSData("ka-GE"),
                ru_RU = GenerateJSData("ru-RU"),
                en_US = GenerateJSData("en-US"),
            };

            return new JavaScriptResult { Script = $"const AppTranslations = Object.freeze({JsonConvert.SerializeObject(data)})" };
        }

        private Dictionary<string, string> GenerateJSData(string cultureName)
        {
            var data = TranslationHelpers.GetTranslations(new CultureInfo(cultureName));
            data = data.ToDictionary(pair => pair.Key.ToLower(), pair => pair.Value);
            return data;
        }
    }
}