﻿using Owin;
using Microsoft.Owin;
using BLL.Managers;
using Lobby.Models;
using Lobby.Hubs;
using System.Web.Routing;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(Lobby.Startup))]
namespace Lobby
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR<GameConnection>("/Game");

            BaseApplication.Initialize();
            BaseApplication.Current.ServerStart(new SignalrMessageBroker());
        }
    }
}
