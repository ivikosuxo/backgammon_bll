﻿namespace Lobby.Models
{
    public class LobbyViewModel
    {
        public string SignalRUrl { get; set; }
        public string CreateRoomUrl { get; set; }
    }
}