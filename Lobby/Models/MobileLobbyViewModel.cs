﻿namespace Lobby.Models
{
    public class MobileLobbyViewModel
    {
        public string SignalRUrl { get; set; }
        public string CreateRoomUrl { get; set; }
    }
}