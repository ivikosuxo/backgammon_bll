﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lobby.Models
{
    public class OnlinePlayersResult
    {
        public List<int> players { get; set; }

        public OnlinePlayersResult(List<int> onlinePlayers) {
            players = onlinePlayers; 
        }
       
    }
}