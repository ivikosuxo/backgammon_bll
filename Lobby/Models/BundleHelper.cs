﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Lobby.Models
{

    public static class BundleHelpers
    {
        public static void SetDefaultOrdering(this BundleCollection bundles)
        {
            var defaultOrderer = new BundleDefaultOrderer();
            bundles.GetRegisteredBundles().ToList().ForEach(i => i.Orderer = defaultOrderer);
        }
    }

    public class BundleDefaultOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}