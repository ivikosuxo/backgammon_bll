﻿using BLL.Interfaces;
using InterfaceLibrary.Interfaces;
using Lobby.Hubs;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lobby.Models
{
    public class SignalrMessageBroker : IMessageBroker
    {
        public void BroadCastAll(string data)
        {

            var connectionInstance = GlobalHost.ConnectionManager.GetConnectionContext<GameConnection>();
            connectionInstance.Connection.Broadcast(data);
           
        }

        public void SendMessage(string sessionId, string data)
        {
            var connectionInstance = GlobalHost.ConnectionManager.GetConnectionContext<GameConnection>();
            connectionInstance.Connection.Send(sessionId, data);
        }

        public void SendMessageToSomeUsers(List<string> users,string data )
        {
            var connectionInstance = GlobalHost.ConnectionManager.GetConnectionContext<GameConnection>();
            connectionInstance.Connection.Send(users, data);
        }
    }
}