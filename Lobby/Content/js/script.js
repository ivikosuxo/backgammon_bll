$(document).ready(function(e){
	

	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	
	
	$('.sm-menu').hide();
	$(".sm-show-menu").click(function() {
		$('.sm-menu').slideToggle(200);
	});
	
	$(".btclick1").click(function () {
		if($(this).hasClass('active')) { } else {
			$(".btclick1").removeClass('active');
			$(".btclick2").removeClass('active');
			$(".btclick3").removeClass('active');
			$(this).addClass('active');
			$(".btheme1").addClass('active');
			$(".btheme2").removeClass('active');
			$(".btheme3").removeClass('active');
		}
	});
	$(".btclick2").click(function () {
		if($(this).hasClass('active')) { } else {
			$(".btclick1").removeClass('active');
			$(".btclick2").removeClass('active');
			$(".btclick3").removeClass('active');
			$(this).addClass('active');
			$(".btheme1").removeClass('active');
			$(".btheme2").addClass('active');
			$(".btheme3").removeClass('active');
		}
	});
	$(".btclick3").click(function () {
		if($(this).hasClass('active')) { } else {
			$(".btclick1").removeClass('active');
			$(".btclick2").removeClass('active');
			$(".btclick3").removeClass('active');
			$(this).addClass('active');
			$(".btheme1").removeClass('active');
			$(".btheme2").removeClass('active');
			$(".btheme3").addClass('active');
		}
	});
	
	
	$('.faqdiv_open').hide();
	$("#first-faq").show();
	
	$(".faq-click").click(function () {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).parent().find(".faqdiv_open").stop().slideUp(200);
		} else {
			$(".faq-click").removeClass('active');
			$(this).addClass('active');
			$(".faqdiv_open").stop().slideUp(200);
			$(this).parent().find(".faqdiv_open").stop().slideDown(200);
		}
	});
	
	
	$(".gendlabel").click(function () {
	    if ($(this).hasClass('active'))
	    {}
		else {
	        $(".gendlabel").removeClass('active');
	        $(".gendlabel > input").attr('checked', false).val('');

	        $(this).addClass('active');
	        $(this).find('input').attr('checked', true).val('1');
		}
	});


	
	
	$(".prtabs").click(function () {
		if($(this).hasClass('active')) { }
		else if($(this).hasClass('proftab1')) {
			$(".prtabs").removeClass('active');
			$(this).addClass('active');
			$(".profile-view1").fadeIn(100);
			$(".profile-view2").hide();
			$(".profile-view3").hide();
		} else if($(this).hasClass('proftab2')) {
			$(".prtabs").removeClass('active');
			$(this).addClass('active');
			$(".profile-view2").fadeIn(100);
			$(".profile-view1").hide();
			$(".profile-view3").hide();
		} else {
			$(".prtabs").removeClass('active');
			$(this).addClass('active');
			$(".profile-view3").fadeIn(100);
			$(".profile-view2").hide();
			$(".profile-view1").hide();
		}
	});
	

	$("#login-password").keyup(function (e) {
	    if (e.keyCode == 13) {
	        $('#login-btn').click();
	    }
	});

	$("#password-recovery-username-inp").keyup(function (e) {
	    if (e.keyCode == 13) {
	        $('#password-recovery-accept-btn').click();
	    }
	});
	
	$("#password-recovery-password-repeat-inp").keyup(function (e) {
	    if (e.keyCode == 13) {
	        $('#password-recovery-accept-update-btn').click();
	    }
	});
    	
	$("#registration-security-code-inp").keyup(function (e) {
	    if (e.keyCode == 13) {
	        $('#confirm-registration-btn').click();
	    }
	});

});


 