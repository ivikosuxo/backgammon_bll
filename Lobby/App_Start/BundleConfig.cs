﻿using Lobby.Models;
using System.Web;
using System.Web.Optimization;

namespace Lobby
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        { 
            // lobby bundle
            bundles.Add(new ScriptBundle("~/bundles/lobby").Include(
                
                    "~/Scripts/lobby/globalFunctions.js",
                    "~/Scripts/config/globalConfig.js", 
                    "~/Scripts/jquery.js",
                     "~/Scripts/jquery.signalR-2.2.1.js",
                    "~/Scripts/lobby/enscroll.js",
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/lobby/achievement.js", 
                    "~/Scripts/lobby/tournament.js",
                    "~/Scripts/plugins/translate.js",
                    "~/Scripts/lobby/localization.js",
                    "~/Scripts/lobby/lobbySocket.js",
                    "~/Scripts/lobby/achievement.js",
                    "~/Scripts/lobby/gamehistory.js",
                    "~/Scripts/lobby/lobby.js",
                    "~/Scripts/lobby/shop.js",
                    "~/Scripts/config/translate.js",
                    "~/Scripts/plugins/Desktop.js" 
                 ));

            // mobile bundle
            bundles.Add(new ScriptBundle("~/bundles/mobilelobby").Include(  
                    "~/Scripts/plugins/jquery.js",
                    "~/Scripts/jquery.signalR-2.2.1.js",
                    "~/Scripts/bootstrap.min.js", 
                    "~/Scripts/lobby/globalFunctions.js", 
                    "~/Scripts/config/globalConfig.js", 
                    "~/Scripts/config/translate.js",  
                    "~/Scripts/lobby/localization.js", 
                    "~/Scripts/lobby/lobbySocket.js", 
                    "~/Scripts/mobile/mobileAchievement.js", 
                    "~/Scripts/lobby/lobby.js",  
                    "~/Scripts/lobby/shop.js",  
                    "~/Scripts/plugins/mobileplugin.js",  
                    "~/Scripts/mobile/mobilescript.js", 
                    "~/Scripts/mobile/mobileFrontEnd.js", 
                    "~/Scripts/mobile/mobile.js"
                 ));

            // game main bundle
            bundles.Add(new ScriptBundle("~/bundles/game").Include(
                    "~/Scripts/jquery.js",
                    "~/Scripts/jquery.signalR-2.2.1.js",
                    "~/Scripts/plugins/createjs.js",
                    "~/Scripts/plugins/diceAnimator.js",
                    "~/Scripts/plugins/singleDice.js",
                    "~/Scripts/config/resources.js",
                    "~/Scripts/config/translate.js",
                    "~/Scripts/lobby/globalFunctions.js",
                    "~/Scripts/config/globalConfig.js",
                    "~/Scripts/popups/problemReport.js",
                    "~/Scripts/popups/ChatPopup.js",
                    "~/Scripts/popups/AddWaiter.js", 
                    "~/Scripts/backgammon/loadingBar.js",
                    "~/Scripts/backgammon/draw.js",
                    "~/Scripts/backgammon/Stone.js",
                    "~/Scripts/backgammon/player.js",
                    "~/Scripts/backgammon/RightPanel.js",
                    "~/Scripts/backgammon/ChipsRow.js",
                    "~/Scripts/popups/rotatescreen.js", 
                    "~/Scripts/backgammon/Backgammon.js",
                    "~/Scripts/backgammon/KilledStones.js",
                    "~/Scripts/backgammon/Board.js",
                    "~/Scripts/backgammon/Hint.js", 
                    "~/Scripts/backgammon/discard.js",
                    "~/Scripts/backgammon/dice.js",
                    "~/Scripts/backgammon/playerdice.js",
                    "~/Scripts/backgammon/Roll.js",
                    "~/Scripts/backgammon/georgianlogic.js",
                     "~/Scripts/backgammon/neweurekalogic.js",
                     "~/Scripts/backgammon/eurekalogic.js",
                    "~/Scripts/backgammon/hyperlogic.js",
                    "~/Scripts/backgammon/longlogic.js",
                     "~/Scripts/backgammon/khachapurilogic.js",
                    "~/Scripts/backgammon/europeanlogic.js",
                    "~/Scripts/backgammon/discardPanel.js",  
                    "~/Scripts/backgammon/gameEngine.js", 
                    "~/Scripts/popups/waitpopup.js", 
                    "~/Scripts/popups/raisepopup.js",  
                    "~/Scripts/popups/giveuppopup.js",
                    "~/Scripts/popups/settingspopup.js",
                    "~/Scripts/popups/button.js",
                    "~/Scripts/popups/passwordpopup.js",
                    "~/Scripts/plugins/timer.js",
                    "~/Scripts/popups/achievementpopup.js", 
                    "~/Scripts/popups/delaypopup.js" 
                 ));
            // game extra bundle
            bundles.Add(new ScriptBundle("~/bundles/backgammon").Include(
                    "~/Scripts/backgammon/bgSocket.js",
                    "~/Scripts/popups/losepopup.js",
                    "~/Scripts/popups/winpopup.js"
            ));
            // tournament extra bundle
            bundles.Add(new ScriptBundle("~/bundles/tournament").Include(
                "~/Scripts/popups/tournamentlosepopup.js",
                "~/Scripts/popups/tournamentwinpopup.js",
                "~/Scripts/backgammon/tournamentsocket.js" 
           ));
            //history bundle
            bundles.Add(new ScriptBundle("~/bundles/history").Include(
                    "~/Scripts/jquery.js",
                    "~/Scripts/jquery.signalR-2.2.1.js",
                    "~/Scripts/plugins/createjs.js",
                    "~/Scripts/plugins/diceAnimator.js",
                    "~/Scripts/plugins/singleDice.js", 
                    "~/Scripts/config/resources.js",
                    "~/Scripts/config/translate.js",
                    "~/Scripts/lobby/globalFunctions.js",
                    "~/Scripts/config/globalConfig.js",
                    "~/Scripts/backgammon/loadingBar.js",
                    "~/Scripts/backgammon/draw.js",
                    "~/Scripts/backgammon/Stone.js",
                    "~/Scripts/backgammon/player.js",
                    "~/Scripts/backgammon/RightPanel.js",
                    "~/Scripts/backgammon/ChipsRow.js", 
                    "~/Scripts/backgammon/KilledStones.js",
                    "~/Scripts/backgammon/Board.js",
                    "~/Scripts/backgammon/Hint.js",
                    "~/Scripts/backgammon/Roll.js",
                    "~/Scripts/history/history.js",
                    "~/Scripts/backgammon/discard.js",
                    "~/Scripts/backgammon/dice.js",
                    "~/Scripts/backgammon/playerdice.js",
                    "~/Scripts/backgammon/georgianlogic.js",
                     "~/Scripts/backgammon/neweurekalogic.js",
                     "~/Scripts/backgammon/eurekalogic.js",
                    "~/Scripts/backgammon/hyperlogic.js",
                    "~/Scripts/backgammon/discardPanel.js",
                    "~/Scripts/history/historyengine.js",
                    "~/Scripts/history/historysocket.js",  
                    "~/Scripts/backgammon/georgianlogic.js",
                    "~/Scripts/backgammon/longlogic.js",
                    "~/Scripts/backgammon/khachapurilogic.js",
                    "~/Scripts/backgammon/europeanlogic.js", 
                    "~/Scripts/popups/rotatescreen.js",
                    "~/Scripts/popups/problemReport.js",
                    "~/Scripts/popups/ChatPopup.js",
                    "~/Scripts/popups/waitpopup.js",
                    "~/Scripts/popups/raisepopup.js",
                    "~/Scripts/popups/giveuppopup.js",
                    "~/Scripts/popups/settingspopup.js",
                    "~/Scripts/popups/button.js",
                    "~/Scripts/popups/passwordpopup.js",
                    "~/Scripts/popups/historywinpopup.js",
                    "~/Scripts/popups/historylosepopup.js",
                    "~/Scripts/popups/achievementpopup.js",
                    "~/Scripts/plugins/timer.js",
                    "~/Scripts/history/historyslider.js",
                    "~/Scripts/history/historyplayer.js",
                    "~/Scripts/history/historybuttonspanel.js",
                    "~/Scripts/history/roundselector.js",
                    "~/Scripts/popups/delaypopup.js"
                ));  

           //lobby style bundle
             bundles.Add(new StyleBundle("~/style/lobby").Include( 
                        "~/Content/css/bootstrap.min.css",
                        "~/Content/css/font-awesome.min.css",
                        "~/Content/css/lobby.css" 
                ));
            //mobile lobby css
            bundles.Add(new StyleBundle("~/style/mobilelobby").Include(
                 "~/Content/css/bootstrap.min.css",
                 "~/Content/css/font-awesome.min.css",
                 "~/Content/css/moblobby.css"
         ));
            // main game css
           bundles.Add(new StyleBundle("~/style/game").Include(
                    "~/Content/fonts/font.css",
                    "~/Content/css/font-awesome.min.css",
                    "~/Content/css/backgammoncss.css"
             ));

            //orderding all bundles
            bundles.SetDefaultOrdering();

          /*  foreach (var bundle in bundles)
            {
                bundle.Transforms.Clear();
            } */
        }
    }
}
