﻿
function createRoom(evt) {


    var spd;
    switch (evt.speed) {
        case 1:
            spd = "fast";
            break;
        case 2:
            spd = "normal";
            break;

        case 3:
            spd = "slow";
            break;

    }

    var tr = $("<tr></tr>");
    tr.addClass("selnardi");
    tr.attr('data-playerCount', evt.player_count);
    tr.attr("ID", evt.room_id);
    tr.data("with_doubles", evt.with_doubles);

    var td = $("<td></td>");
    td.addClass("tdsize-1");

    var td1 = $("<span></span>");
    var color = "greenColor";
    if (evt.player_count == 2) {
        color = "redColor";
    }

    td1.addClass("statuses " + color);

    var td2 = $("<i></i>");
    td2.addClass("fa fa-circle");
    td1.append(td2);
    td.append(td1);
    td.append(evt.room_id);
    tr.append(td);


    var td = $("<td></td>");
    td.addClass("table-name");
    td.text(evt.room_name);
    tr.append(td);  

    td = $("<td></td>");
    td.addClass("tdsize-2 text-center");




    var jacob = evt.jacob;
    var davi = evt.double;
    var crawford = evt.crawford;
    var beaver = evt.beaver;

    var jcb = $("<span></span>");
    jcb.addClass("rules1");
    jcb.text("J");


    var bvr = $("<span></span>");
    bvr.addClass("rules1");
    bvr.text("B");

    var dv = $("<span></span>");
    dv.addClass("rules1");
    dv.text("D");

    var crw = $("<span></span>");
    crw.addClass("rules1");
    crw.text("C");

    if (jacob) {
        jcb.addClass("ruleact");

    };

    if (beaver) {

        bvr.addClass("ruleact");

    };


    if (davi) {

        dv.addClass("ruleact");


    };
    if (crawford) {
        crw.addClass("ruleact");
    };

    td.append(jcb);
    td.append(bvr);
    td.append(dv);
    td.append(crw);

    tr.append(td);



    td = $("<td></td>");
    td.addClass("tdsize-3 gametype");

    tr.data("type", evt.type);


    var type = "long_type";
 

    type = GetGameType(evt.type);
    var typeSpan = $("<span></span>");
    typeSpan.addClass("translator");


    if (evt.type > 2) {
        typeSpan.addClass("text-underline");
    }

    

    typeSpan.data("translate", type);
    typeSpan.append(GetTranslatedValue(type)); 
    td.append(typeSpan);
    tr.append(td);


    td = $("<td></td>");
    td.addClass("speedtype tdsize-4");

    td.data("SPEED", evt.speed);
    tr.data("speed", evt.speed)

    td.data("value", evt.speed);
    td.addClass("translator");
    td.data("translate", spd);
    td.text(GetTranslatedValue(spd));

    tr.append(td);

    //withdoubles
    var doubleTd = $("<td></td>");
    doubleTd.addClass("tdsize-6");
    var doublesSpan = $("<span></span>");
    doublesSpan.addClass("whiteColor translator");

    var doublesText = "yes";
    if (!evt.with_doubles) {
        doublesText = "no";
    }
    
    doublesSpan.data("translate", doublesText);
    doublesSpan.text(GetTranslatedValue(doublesText));
    doubleTd.append(doublesSpan);

    tr.append(doubleTd);
    // amount
    td = $("<td></td>");
    td.data("amount", evt.min_bet);

    tr.data("amount", evt.min_bet);

    td.addClass("title-4 tdsize-5 gamebet");
    var bet_text = evt.min_bet;
    if (evt.min_bet < evt.max_bet) {
        bet_text = evt.min_bet + "-" + evt.max_bet;
    }

    td.html(bet_text);
    tr.append(td);


    td = $("<td></td>");
    td.addClass("points  tdsize-6");
    td.addClass("points");
    var font = $("<font></font>");
    font.addClass("translator");
    font.data("translate", "till_pre");
    font.text(GetTranslatedValue("till_pre"));
    td.append(font);
    td.append(evt.points);



    var font1 = $("<font></font>");
    font1.addClass("translator");
    font1.data("translate", "till_post");
    font1.text(GetTranslatedValue("till_post"))
    td.append(font1)
    tr.append(td);
    tr.data("point", evt.points);

    td = $("<td></td>");
    td.addClass("tdsize-7");

    var z = $('<i class="fa fa-unlock"></i>');

    if (evt.protected) {
        z = $('<i class="fa fa-lock"></i>');
    }

    td.append(z);
    tr.append(td);


    td = $("<td></td>");
    td.attr("ID", "pls-count-" + evt.room_id);


    tr.data("players_count", evt.player_count);

    td.addClass("tdsize-8 text-right");
    var playerCount = parseInt(evt.player_count);
    var btn = $('<button class="play-tourn btn btn-success redirect redirect1 translator"></button>');
    btn.data("translate", "play");
    btn.text(GetTranslatedValue("play"));

    btn.data("value", evt.room_id);
    tr.data("prior" ,1);
    if (playerCount === 2) {
        btn.removeClass("btn-success");
        btn.removeClass("btn-primary");
        btn.addClass("btn-danger");
        btn.data("translate", "ongoing");
        btn.text(GetTranslatedValue("ongoing"));
        tr.data("prior", 2);
        td1.removeClass("greenColor");
        td1.addClass("redColor");
        btn.prop("disabled", true);
    }

    td.append(btn);
    tr.append(td);

    evt.visible = evt.visible || player_id == evt.creator_id;
    if (evt.visible) {
        if (player_id == evt.creator_id) {
            btn.removeClass("btn-success");
            btn.removeClass("btn-danger");
            btn.addClass("btn-primary");
            tr.data("creator", 1);
            btn.data("translate", "return_type");
            btn.text(GetTranslatedValue("return_type"));
            tr.data("prior", 0);
            btn.prop("disabled", false);
            td1.removeClass("greenColor");
            td1.removeClass("redColor");
            td1.addClass("yellowColor");
        }
        tr.show();
    } else {
        tr.hide();
    }

  

    $("#room-container").append(tr);

}

function CloseWindow() { 
    window.close(); 
}


function OpenPopup(url,name) { 

    var width = 1050;
    var height = 670;
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        height = 710;
    } 

    popups["pop" + activeRoomID] = window.open(url, "game_table" + name, "width=" + width + ", height=" + height);

   
}
function OpenHistoryUrl(url, gameId) {

    var width = 1050;
    var height = 670;
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        height = 710;
    } 

    window.open(url, "game" + gameId, "width=" + width + ", height=" + height);
}



function addShopHistoryItems(items) {

    $("#shop-history").empty();
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        addShopHistoryItem(item, i)
    }

}

function addShopHistoryItem(item, index) {
    var mainTr = $("<tr></tr>");
    mainTr.attr("id", "shop_history_" + item.id);


    //index
    var indexTd = $("<td></td>");
    indexTd.text(index + 1);


    //image
    var imageTd = $("<td></td>");
    var imager = $('<img/>');
    imager.attr('src', 'https://nardi.ge/images/' + item.img_url);

    imageTd.append(imager);

    //name status td
    var nameTd = $("<td></td>");
    var div1 = $("<div></div>");

    div1.addClass("translate_title");
    div1.data("name0", item.name_geo);
    div1.data("name1", item.name);
    div1.data("name2", item.name_rus);
    div1.text(GetNameByLang(item.name_geo, item.name, item.name_rus));


    var small = $("<small></small>")
    small.addClass("translator");
    small.data("translate", item.order_status.toLowerCase());

 
    if (item.order_status_value == 1) {
        small.addClass("greenColor");
    }
    if (item.order_status_value == 2) {
        small.addClass("yellowColor");
    }
    if (item.order_status_value == 3) {
        small.addClass("redColor");
    }

    small.text(GetTranslatedValue(item.order_status.toLowerCase()));


    nameTd.append(div1);
    nameTd.append(small);
    //price td
    var priceTd = $("<td></td>");
    priceTd.text(item.price);
    //price td
    var countTd = $("<td></td>");
    countTd.text(item.count);
    //price td
    var dateTd = $("<td></td>");
    dateTd.text(item.time);
    ///

    mainTr.append(indexTd);
    mainTr.append(imageTd);
    mainTr.append(nameTd);
    mainTr.append(priceTd);
    mainTr.append(countTd);
    mainTr.append(dateTd);

    $("#shop-history").append(mainTr);
}
