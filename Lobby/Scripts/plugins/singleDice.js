﻿(function (library, img, cjs, ss) {

    var p; // shortcut to reference prototypes
    library.webFontTxtFilters = {};

    // libraryrary properties:
    library.properties = {
        width: 550,
        height: 400,
        fps: 30,
        color: "#FFFFFF",
        webfonts: {},
        manifest: []
    };


    library.ssMetadata = [
        { name: "single_roll", frames: [[348, 210, 49, 49], [427, 308, 49, 49], [412, 410, 49, 49], [412, 461, 49, 49], [427, 359, 49, 49], [103, 230, 49, 49], [348, 282, 136, 24], [0, 163, 159, 65], [166, 0, 174, 127], [0, 0, 164, 161], [342, 0, 150, 136], [166, 129, 135, 124], [0, 230, 101, 97], [254, 421, 78, 83], [171, 423, 81, 86], [0, 423, 84, 87], [92, 329, 85, 92], [0, 329, 90, 90], [86, 423, 83, 86], [179, 255, 81, 83], [179, 340, 83, 79], [262, 255, 84, 78], [264, 335, 81, 78], [334, 415, 76, 74], [427, 138, 77, 70], [347, 335, 78, 70], [427, 210, 76, 70], [348, 138, 77, 70]] }
       ];

    library.webfontAvailable = function (family) {
        library.properties.webfonts[family] = true;
        var txtFilters = library.webFontTxtFilters && library.webFontTxtFilters[family] || [];
        for (var f = 0; f < txtFilters.length; ++f) {
            txtFilters[f].updateCache();
        }
    };
    // symbols:



    (library._1_0000_1 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(0);
    }).prototype = p = new cjs.Sprite();



    (library._1_0001_2 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(1);
    }).prototype = p = new cjs.Sprite();



    (library._1_0002_3 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(2);
    }).prototype = p = new cjs.Sprite();



    (library._1_0003_4 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(3);
    }).prototype = p = new cjs.Sprite();



    (library._1_0004_5 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(4);
    }).prototype = p = new cjs.Sprite();



    (library._1_0005_6 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(5);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00000 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(6);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00001 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(7);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00002 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(8);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00003 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(9);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00004 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(10);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00005 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(11);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00006 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(12);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00007 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(13);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00008 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(14);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00009 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(15);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00010 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(16);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00011 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(17);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00012 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(18);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00013 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(19);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00014 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(20);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00015 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(21);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00016 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(22);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00017 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(23);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00018 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(24);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00019 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(25);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00020 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(26);
    }).prototype = p = new cjs.Sprite();



    (library.leyer_00021 = function () {
        this.spriteSheet = sss["single_roll"];
        this.gotoAndStop(27);
    }).prototype = p = new cjs.Sprite();



    (library.dots = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer 1
        this.instance = new library._1_0000_1();

        this.instance_1 = new library._1_0001_2();

        this.instance_2 = new library._1_0002_3();

        this.instance_3 = new library._1_0003_4();

        this.instance_4 = new library._1_0004_5();

        this.instance_5 = new library._1_0005_6();

        this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.instance }] }).to({ state: [{ t: this.instance_1 }] }, 1).to({ state: [{ t: this.instance_2 }] }, 1).to({ state: [{ t: this.instance_3 }] }, 1).to({ state: [{ t: this.instance_4 }] }, 1).to({ state: [{ t: this.instance_5 }] }, 1).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 0, 49, 49);


    (library.WhiteDiceFLA = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer 3
        this.dot = new library.dots();
        this.dot.setTransform(124.5, 60.1, 1, 1, -22, 0, 0, 24.1, 25.1);
        this.dot.alpha = 0.738;
        this.dot._off = true;

        this.timeline.addTween(cjs.Tween.get(this.dot).wait(18).to({ _off: false }, 0).wait(1).to({ regX: 24, rotation: -12, x: 126.5, y: 52.4, alpha: 0.828 }, 0).wait(1).to({ regX: 24.1, rotation: -2, x: 128.6, y: 44.5, alpha: 0.93 }, 0).wait(1).to({ regX: 23.9, rotation: 0, x: 129.4, y: 40.7, alpha: 1 }, 0).wait(1).to({ y: 40.1 }, 0).wait(1).to({ y: 39.5 }, 0).wait(1).to({ y: 38.9 }, 0).wait(1).to({ y: 38.2 }, 0).wait(1));

        // leyer.psd
        this.instance = new library.leyer_00000();
        this.instance.setTransform(0, 627);

        this.instance_1 = new library.leyer_00001();
        this.instance_1.setTransform(0, 586);

        this.instance_2 = new library.leyer_00002();
        this.instance_2.setTransform(0, 524);

        this.instance_3 = new library.leyer_00003();
        this.instance_3.setTransform(0, 445);

        this.instance_4 = new library.leyer_00004();
        this.instance_4.setTransform(2, 376);

        this.instance_5 = new library.leyer_00005();
        this.instance_5.setTransform(11, 342);

        this.instance_6 = new library.leyer_00006();
        this.instance_6.setTransform(31, 273);

        this.instance_7 = new library.leyer_00007();
        this.instance_7.setTransform(45, 211);

        this.instance_8 = new library.leyer_00008();
        this.instance_8.setTransform(43, 172);

        this.instance_9 = new library.leyer_00009();
        this.instance_9.setTransform(48, 134);

        this.instance_10 = new library.leyer_00010();
        this.instance_10.setTransform(48, 118);

        this.instance_11 = new library.leyer_00011();
        this.instance_11.setTransform(52, 98);

        this.instance_12 = new library.leyer_00012();
        this.instance_12.setTransform(63, 75);

        this.instance_13 = new library.leyer_00013();
        this.instance_13.setTransform(65, 63);

        this.instance_14 = new library.leyer_00014();
        this.instance_14.setTransform(66, 53);

        this.instance_15 = new library.leyer_00015();
        this.instance_15.setTransform(65, 44);

        this.instance_16 = new library.leyer_00016();
        this.instance_16.setTransform(68, 34);

        this.instance_17 = new library.leyer_00017();
        this.instance_17.setTransform(74, 28);

        this.instance_18 = new library.leyer_00018();
        this.instance_18.setTransform(74, 17);

        this.instance_19 = new library.leyer_00019();
        this.instance_19.setTransform(75, 10);

        this.instance_20 = new library.leyer_00020();
        this.instance_20.setTransform(79, 3);

        this.instance_21 = new library.leyer_00021();
        this.instance_21.setTransform(80, 0);
        this.instance_21._off = true;

        this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.instance }] }).to({ state: [{ t: this.instance_1 }] }, 1).to({ state: [{ t: this.instance_2 }] }, 1).to({ state: [{ t: this.instance_3 }] }, 1).to({ state: [{ t: this.instance_4 }] }, 1).to({ state: [{ t: this.instance_5 }] }, 1).to({ state: [{ t: this.instance_6 }] }, 1).to({ state: [{ t: this.instance_7 }] }, 1).to({ state: [{ t: this.instance_8 }] }, 1).to({ state: [{ t: this.instance_9 }] }, 1).to({ state: [{ t: this.instance_10 }] }, 1).to({ state: [{ t: this.instance_11 }] }, 1).to({ state: [{ t: this.instance_12 }] }, 1).to({ state: [{ t: this.instance_13 }] }, 1).to({ state: [{ t: this.instance_14 }] }, 1).to({ state: [{ t: this.instance_15 }] }, 1).to({ state: [{ t: this.instance_16 }] }, 1).to({ state: [{ t: this.instance_17 }] }, 1).to({ state: [{ t: this.instance_18 }] }, 1).to({ state: [{ t: this.instance_19 }] }, 1).to({ state: [{ t: this.instance_20 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(21).to({ _off: false }, 0).wait(1).to({ y: -0.6 }, 0).wait(1).to({ y: -1.2 }, 0).wait(1).to({ y: -1.8 }, 0).wait(1).to({ y: -2.5 }, 0).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 627, 136, 24);


    // stage content:



    (library.single_dice = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // tavari.psd
        this.instance = new library.WhiteDiceFLA();
        this.instance.setTransform(123.5, 662, 1, 1, 0, 0, 0, 123.5, 662);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(275, 827, 136, 24);

})(library = library || {}, images = images || {}, createjs = createjs || {}, ss = ss || {});
var library, images, createjs, ss;