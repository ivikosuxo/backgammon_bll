 var waitOponnent_tr=  ["გთხოვთ დაელოდოთ მოწინააღმდეგეს..","Please wait for opponent"];
var betAmount_tr = ["ფსონი","Bet Amount"];
var pointsToWin_tr =["თამაში: ","Points to win: "];
var pointsToWinEnd_tr =[" - მდე",""];
var raise_tr = ["გაზრდა","Raise"];
var submit_tr = ["დასტური","Submit"];
var undo_tr = ["გაუქმება","Undo"];
var roll_tr =["გაგორება","Roll"];
var points_tr =["ქულები","Points"];
var changeSide_tr =["კარის შეცვლა","Change Side"];
var autoRoll_tr =["ავტო გაგორება","Auto Roll"];
var autoSubmit_tr = ["ავტო დადასტურება", "Auto Submit"];

//მისიები

var completeMission_tr = ["თქვენ შეასრულეთ მისია", "You have completed Mission"];
var coin_tr = ["ქულა", "Coin"];

//
var insertPassword_tr = ["შეიყვანეთ პაროლი", "Insert Password"];
var insert_tr = ["შეყვანა", "Submit"]
var close_tr=["დახურვა","Close"]
//დანებება
var giveup_tr=["დანებება","GIVE UP"];
var giveupText_tr=["დანებების შემთხევაში თქვენ წააგებთ","If you give up, you will lose this round"]
var giveupText1_tr=["რაუნდს ან თამაშს.","or this game."];
var giveupText2_tr=["",""];

var giveupRound_tr=["რაუნდის","Round"];
var giveupGame_tr=["თამაშის","Game"];

var game_tr = ["თამაში","Game"];

var opp_wants_to_play1 = ["მოწინააღმდეგეს სურს თამაში","Opponent wants to play"];
var opp_wants_to_play2 = ["თანახმა ხართ?","Do you agree?"];


var cancel_tr= ["დარწმუნებული ხართ რომ გსურთ გაუქმება","Are you sure you want cancel?"]


var raiseText_tr = ["მოწინააღმდეგეს სურს ფსონის გაზრდა","Opponent wants to raise"];
var raiseText1_tr = ["თქვენ შეგიძლიათ უარყოთ და წააგოთ ეს რაუნდი.","You can discard it and lose this round."];


var oponnetLeave_tr =["ოპონენტმა დატოვა მაგიდა" , "opponent has left the room"];    

var youWin_tr =["თქვენ გაიმარჯვეთ","YOU WIN"];
var youLose_tr=["თქვენ დამარცხდით","YOU LOSE"];

var playAgain_tr=["თამაში","Play Again"];
var playAgainText_tr=["კიდევ გსურთ თამაში იგივე წესებით?", "Do you want to play again"];
var playAgainText1_tr=["", " with same rules?"];

//errormesage

var incorrectToken_tr = ["ვერ ხერხდება ავტორიზაცია", "Can't authenticate"];
var blockedException_tr = ["თქვენი პროფილი დაბლოკილია", "Your account is blocked"];

var incorrectMove_tr=["არასწორი სვლა","Incorrect Move"];
var activeSession_tr = ["თქვენ გაქვთ ახალი აქტიური სესია", "You have a new active session"];
var maxRank_tr = ["თქვენ ხართ მაქსიმალურ რანკზე", "You are on max rank"];
var notEnoughCoin_tr = ["არასაკმარისი ქოინი", "Not enough coin"];
var notEnoughPlayers_tr = ["არასაკმარისი მოთამაშე", "Not enough players"];
var incorrectBet_tr = ["არასწორი ბეთი", "Incorrect Bet"];
var insufficientFunds_tr=["არასაკმარისი ბალანსი","Insufficient funds"];
var fullRoom_tr=["კონკრეტული ოთახი დაკავებულია","Current Room is full"];
var roomIsNotActive_tr=["ოთახი არ არსებობს","Current room is not active"]; 
var  incPassword_tr=["არასწორი პაროლი","Inccorect password"];
var shortPass_tr = ["პაროლის უნდა შეიცავდეს 4-16 სიმბოლოს","Password size must be in 4-16 range"];
var leavedTable_tr=["ოპონენტმა დატოვა მაგიდა","opponent leaved table"];
var  youDisagree_tr=["თქვენ არ დაეთანხმეთ","You disagree"];
var  connectionError_tr = ["სერვერთან კავშირი გაწყდა.", "You have disconnected from the server"];
var tournamentCantFind_tr = ["ტურნირი არ მოიძებნა", "Tournament doesn't exists."]; 
var tournamentIsFull_tr = ["ტურნირი შევსებულია", "Tournament is full"];
var tournamentIsStarted_tr = ["ტურნირი დაწყებულია", "Tournament is already started"];
var tournamentAlreadyRegistered_tr = ["თქვენ უკვე დარეგისტრირებული ხართ ტურნირზე", "You are already registered"];
var tournamentCanceled_tr = ["ტურნირი გაუქმდა", "Tournament Canceled"];
var tournamentExtra_tr = ["თქვენ დატოვეთ ტურნირი","You leaved tournament"];
var timeOutException_tr = ["დრო ამოიწურა", "Time Out"];
var cantCreateRoom_tr=["ვერ ხერხდება ოთახის შექმნა, მოგვიანებით სცადეთ","Can't create room , try again later"];

var unexpectedToken_tr=["დაფიქსირდა შეცდომა","Unexpected token"];
var undefinedError_tr=["დაფიქსირდა შეცდომა","Something went wrong"];
var problem_tr = ["პრობლემის შეტყობინება", "Something went wrong"];

var productIsSold_tr = ["პროდუქტის მარაგი ამოწურულია", "Product is already sold"];


var tournamentLose_tr = ["თქვენ გამოეთიშეთ ტურნირს!","You left Tournament"];
var tournamentWin_tr = ["თქვენ გახვედით პირველ ადგილზე!", "You Win Tournament"];
var tournamentNextLevel_tr = ["თქვენ გადახვედით შემდეგ ეტაპზე!", "You promoted to next level"];
// chat 


var chat_tr = ["CHAT","ჩათი"];