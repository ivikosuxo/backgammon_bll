

(function (lib, img, cjs, ss) {

    var p;
    // shortcut to reference prototypes
    lib.webFontTxtFilters = {};

    // library properties:
    lib.properties = {
        width: 550,
        height: 400,
        fps: 50,
        color: "#FFFFFF",
        webfonts: {},
        manifest: []
    };


    lib.ssMetadata = [
        { name: "dice_roll", frames: [[958, 51, 49, 49], [958, 102, 49, 49], [947, 153, 49, 49], [896, 183, 49, 49], [896, 132, 49, 49], [958, 0, 49, 49], [0, 164, 247, 68], [253, 0, 269, 129], [0, 234, 150, 108], [297, 262, 141, 108], [138, 352, 141, 106], [870, 257, 142, 110], [572, 256, 142, 111], [749, 132, 145, 123], [427, 141, 143, 119], [572, 141, 145, 113], [152, 238, 143, 112], [0, 344, 136, 111], [0, 0, 251, 162], [440, 369, 133, 110], [575, 369, 131, 111], [524, 0, 223, 139], [749, 0, 207, 130], [253, 131, 172, 105], [281, 372, 142, 91], [867, 369, 148, 94], [716, 362, 149, 100], [716, 257, 152, 103]] }
    ];

    lib.webfontAvailable = function (family) {
        lib.properties.webfonts[family] = true;
        var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
        for (var f = 0; f < txtFilters.length; ++f) {
            txtFilters[f].updateCache();
        }
    };
    // symbols:



    (lib._1_0000_1 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(0);
    }).prototype = p = new cjs.Sprite();



    (lib._1_0001_2 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(1);
    }).prototype = p = new cjs.Sprite();



    (lib._1_0002_3 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(2);
    }).prototype = p = new cjs.Sprite();



    (lib._1_0003_4 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(3);
    }).prototype = p = new cjs.Sprite();



    (lib._1_0004_5 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(4);
    }).prototype = p = new cjs.Sprite();



    (lib._1_0005_6 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(5);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer0 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(6);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer1 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(7);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer10 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(8);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer11 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(9);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer12 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(10);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer13 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(11);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer14 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(12);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer15 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(13);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer16 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(14);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer17 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(15);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer18 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(16);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer19 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(17);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer2 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(18);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer20 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(19);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer21 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(20);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer3 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(21);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer4 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(22);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer5 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(23);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer6 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(24);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer7 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(25);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer8 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(26);
    }).prototype = p = new cjs.Sprite();



    (lib.Layer9 = function () {
        this.spriteSheet = ss["dice_roll"];
        this.gotoAndStop(27);
    }).prototype = p = new cjs.Sprite();



    (lib.dots = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer 1
        this.instance = new lib._1_0000_1();

        this.instance_1 = new lib._1_0001_2();

        this.instance_2 = new lib._1_0002_3();

        this.instance_3 = new lib._1_0003_4();

        this.instance_4 = new lib._1_0004_5();

        this.instance_5 = new lib._1_0005_6();

        this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.instance }] }).to({ state: [{ t: this.instance_1 }] }, 1).to({ state: [{ t: this.instance_2 }] }, 1).to({ state: [{ t: this.instance_3 }] }, 1).to({ state: [{ t: this.instance_4 }] }, 1).to({ state: [{ t: this.instance_5 }] }, 1).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 0, 49, 49);


    (lib.WhiteDiceFLA = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer 3
        this.dots1 = new lib.dots();
        this.dots1.setTransform(142.6, 103, 1, 1, -22, 0, 0, 23.9, 25.1);
        this.dots1.alpha = 0.699;
        this.dots1._off = true;

        this.timeline.addTween(cjs.Tween.get(this.dots1).wait(17).to({ _off: false }, 0).wait(1).to({ rotation: -14, x: 144, y: 95.7, alpha: 0.77 }, 0).wait(1).to({ rotation: -7, x: 146.7, y: 87.3, alpha: 0.852 }, 0).wait(1).to({ rotation: -2, x: 147.3, y: 84, alpha: 0.922 }, 0).wait(1).to({ rotation: 0, x: 148.5, y: 81.7, alpha: 1 }, 0).wait(1).to({ y: 81.1 }, 0).wait(1).to({ x: 148.6, y: 80.5 }, 0).wait(1).to({ y: 79.9 }, 0).wait(1).to({ x: 148.7, y: 79.2 }, 0).wait(1));

        // Layer 2
        this.dots2 = new lib.dots();
        this.dots2.setTransform(212.3, 60.6, 0.997, 0.997, 22, 0, 0, 25.4, 24.9);
        this.dots2.alpha = 0.691;
        this.dots2._off = true;

        this.timeline.addTween(cjs.Tween.get(this.dots2).wait(17).to({ _off: false }, 0).wait(1).to({ rotation: 25, x: 211.9, y: 52.6, alpha: 0.762 }, 0).wait(1).to({ rotation: 28, x: 208.2, y: 45.6, alpha: 0.84 }, 0).wait(1).to({ scaleX: 1, scaleY: 1, rotation: 28.8, x: 207.5, y: 42.9, alpha: 0.922 }, 0).wait(1).to({ rotation: 29.8, x: 206.8, y: 40.1, alpha: 1 }, 0).wait(1).to({ y: 39.5 }, 0).wait(1).to({ x: 206.7, y: 38.8 }, 0).wait(1).to({ y: 38.1 }, 0).wait(1).to({ x: 206.6, y: 37.4 }, 0).wait(1));

        // tavari.psd
        this.instance = new lib.Layer0();
        this.instance.setTransform(0, 628);

        this.instance_1 = new lib.Layer1();
        this.instance_1.setTransform(0, 567);

        this.instance_2 = new lib.Layer2();
        this.instance_2.setTransform(4, 488);

        this.instance_3 = new lib.Layer3();
        this.instance_3.setTransform(20, 418);

        this.instance_4 = new lib.Layer4();
        this.instance_4.setTransform(29, 380);

        this.instance_5 = new lib.Layer5();
        this.instance_5.setTransform(50, 314);

        this.instance_6 = new lib.Layer6();
        this.instance_6.setTransform(64, 247);

        this.instance_7 = new lib.Layer7();
        this.instance_7.setTransform(62, 207);

        this.instance_8 = new lib.Layer8();
        this.instance_8.setTransform(66, 174);

        this.instance_9 = new lib.Layer9();
        this.instance_9.setTransform(66, 161);

        this.instance_10 = new lib.Layer10();
        this.instance_10.setTransform(70, 129);

        this.instance_11 = new lib.Layer11();
        this.instance_11.setTransform(81, 103);

        this.instance_12 = new lib.Layer12();
        this.instance_12.setTransform(84, 85);

        this.instance_13 = new lib.Layer13();
        this.instance_13.setTransform(83, 67);

        this.instance_14 = new lib.Layer14();
        this.instance_14.setTransform(83, 55);

        this.instance_15 = new lib.Layer15();
        this.instance_15.setTransform(86, 34);

        this.instance_16 = new lib.Layer16();
        this.instance_16.setTransform(92, 27);

        this.instance_17 = new lib.Layer17();
        this.instance_17.setTransform(92, 18);

        this.instance_18 = new lib.Layer18();
        this.instance_18.setTransform(93, 12);

        this.instance_19 = new lib.Layer19();
        this.instance_19.setTransform(96, 6);

        this.instance_20 = new lib.Layer20();
        this.instance_20.setTransform(98, 4);

        this.instance_21 = new lib.Layer21();
        this.instance_21.setTransform(99, 0);
        this.instance_21._off = true;

        this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.instance }] }).to({ state: [{ t: this.instance_1 }] }, 1).to({ state: [{ t: this.instance_2 }] }, 1).to({ state: [{ t: this.instance_3 }] }, 1).to({ state: [{ t: this.instance_4 }] }, 1).to({ state: [{ t: this.instance_5 }] }, 1).to({ state: [{ t: this.instance_6 }] }, 1).to({ state: [{ t: this.instance_7 }] }, 1).to({ state: [{ t: this.instance_8 }] }, 1).to({ state: [{ t: this.instance_9 }] }, 1).to({ state: [{ t: this.instance_10 }] }, 1).to({ state: [{ t: this.instance_11 }] }, 1).to({ state: [{ t: this.instance_12 }] }, 1).to({ state: [{ t: this.instance_13 }] }, 1).to({ state: [{ t: this.instance_14 }] }, 1).to({ state: [{ t: this.instance_15 }] }, 1).to({ state: [{ t: this.instance_16 }] }, 1).to({ state: [{ t: this.instance_17 }] }, 1).to({ state: [{ t: this.instance_18 }] }, 1).to({ state: [{ t: this.instance_19 }] }, 1).to({ state: [{ t: this.instance_20 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).to({ state: [{ t: this.instance_21 }] }, 1).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(21).to({ _off: false }, 0).wait(1).to({ y: -0.6 }, 0).wait(1).to({ y: -1.2 }, 0).wait(1).to({ y: -1.8 }, 0).wait(1).to({ y: -2.5 }, 0).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 628, 247, 68);


    // stage content:



    (lib.diceroll_new = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // tavari.psd
        this.instance = new lib.WhiteDiceFLA();
        this.instance.setTransform(123.5, 662, 1, 1, 0, 0, 0, 123.5, 662);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(275, 828, 247, 68);

})(lib = lib || {}, images = images || {}, createjs = createjs || {}, ss = ss || {});
var lib, images, createjs, ss;