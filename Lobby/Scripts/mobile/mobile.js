﻿function AddTournaments(event) {
   // registerTournamentEvents();
    //shopEvents();

    for (var i = 0; i < event.tournaments.length; i++) {
        addTournament(event.tournaments[i]);
    }
  
}




function addTournament(event) {

    var id = event.id
    var status = event.state;
    var bet = event.fee + " <font class='GEL'>b</font>";
    var prize = event.prize; 
    var tournamentName = event.name;
    var playersCount = event.players_count + " / " + event.max_count; 
    var tournament_type = event.tournament_type; 
    var alreadySit = event.player_sit;


    var regKey = "registration";
    var buttonStyle = "tour-register btn  btn-primary register";
    if (alreadySit) {

        regKey = "cancel";
        buttonStyle = "tour-register btn  btn-danger unregister";
    }
    //create main tr
    var maintr = $("<tr></tr>");
    maintr.attr("id", "tour-" + id);

    var td1 = $("<td></td>");
     
    var nameSpan = $("<span></span>");
    nameSpan.addClass("whiteColor translate_title");
    nameSpan.data("name0", event.name_ge);
    nameSpan.data("name2", event.name_ru);
    nameSpan.data("name1", event.name);
    nameSpan.text(GetNameByLang(event.name_ge, event.name, event.name_ru));

    //small1
    var small1 = $("<small></small>");
    var span2 = $("<span></span>");
    span2.addClass("translator");
    span2.data("translate", "buy_in");
    span2.text(GetTranslatedValue("buy_in"));
     

    var span3 = $("<span></span>");
    span3.addClass("whiteColor");
    span3.html(" "+bet);

    small1.append(span2);
    small1.append(span3);
      //small2
    var small2 = $("<small></small>");
    var span4 = $("<span></span>");
    span4.addClass("translator");
    span4.data("translate","members");
    span4.text(GetTranslatedValue("members"));

    var span5 = $("<span></span>");
    span5.addClass("whiteColor players-count"); 
    span5.text(" "+ playersCount);

    small2.append(span4);
    small2.append(span5);

    td1.append(nameSpan);
    td1.append(small1);
    td1.append(small2);
    // var td2 

    var td2 = $("<td></td>");
    td2.addClass("text-center yellowColor");

    var span6 = $("<span></span>"); 
    span6.text(prize);
    var span7 = $("<span></span>");
    span7.html("<font class='GEL'>b</font>");
 
    td2.append(span6);
    td2.append(span7);


    //td3
    var td3 = $("<td></td>");
    td3.addClass("register_td");


    
 
 

    if (status >= 2) {
        var div1 = $("<div></div>");
        var spaner = $("<span><i class='fa fa-circle'></i></span>");
        spaner.addClass("statuses");
        var stateKey = "";
        var spaner1 = $("<span></span>"); 
        if (status == 2) {
            spaner.addClass("greenColor");
            stateKey = "ongoing";


            var returnButton = $("<button></button>");
            returnButton.data("tournament_id", id);
            returnButton.data("room_id", 0);
            returnButton.attr("id", "tour_room" + id);
            returnButton.addClass("btn  btn-primary translator tournament_play");
            returnButton.data("translate", "play");
            returnButton.css("display", "none");
            returnButton.text(GetTranslatedValue("play"));
            td3.append(returnButton); 

        }
        else if (status == 3) {
            spaner.addClass("redColor");
            stateKey = "ended";
        }
        spaner1.addClass("translator");
        spaner1.data("translate", stateKey);
        spaner1.text(GetTranslatedValue(stateKey));
        div1.addClass("tourn_finished");
        div1.append(spaner);
        div1.append(spaner1);
        td3.append(div1);

    } else {
     
        var small3 = $("<small></small>");
        small3.addClass("group-5");
        var span8 = $("<span></span>");
        span8.addClass("whiteColor"); 
        span8.text(event.start_date); 
        small3.append(span8);


        var registerButton = $("<button></button>");
        registerButton.data("id", id);
        registerButton.addClass(buttonStyle+" translator");
   
        registerButton.data("amount", bet);
        registerButton.data("name", tournamentName);
        registerButton.data("translate", regKey);
     
        registerButton.data("name0", event.name_ge);
        registerButton.data("name2", event.name_ru);
        registerButton.data("name1", event.name);
        registerButton.text(GetTranslatedValue(regKey));

        td3.append(small3);
        td3.append(registerButton);
    }

     
    var infoTd = $("<button></button>");
    infoTd.data("id", id);
    infoTd.addClass("btn btn-default tournament_detail"); 

    var infoI = $("<i></i>");
    infoI.addClass("fa fa-info"); 
    infoTd.append(infoI);


    td3.append(infoTd); 
    maintr.append(td1);
    maintr.append(td2);
    maintr.append(td3);
    
    $("#tournaments-table").append(maintr);
}


function tournamentStatusChange(event) {
    var status = event.state;
    var id = event.id;

    var tournamenttr = $("#tour-" + id);

    var statusspan = tournamenttr.find(".statuses");
    statusspan.removeClass("status1");
    statusspan.removeClass("status2");
    statusspan.removeClass("status3");
    statusspan.addClass("status" + status);


   

    if (status >= 2) {
        var mainSPan = tournamenttr.find(".register_td");
        var infoButton = mainSPan.find(".tournament_detail");
        mainSPan.empty();
        var div1 = $("<div></div>");
        var divClass = "";
        var spaner = $("<span><i class='fa fa-circle'></i></span>");
        spaner.addClass("first-span");
        var spaner1 = $("<span></span>");
        spaner1.addClass("second-span");

        var stateKey = "";
        if (status == 2) {
            spaner.addClass("statuses greenColor");
            stateKey = "ongoing";
       
            var returnButton = $("<button></button>");
            returnButton.data("tournament_id", id);
            returnButton.data("room_id", 0);
            returnButton.attr("id", "tour_room" + id);
            returnButton.addClass("btn  btn-primary translator tournament_play");
            returnButton.data("translate", "play");
            returnButton.css("display", "none");
            returnButton.text(GetTranslatedValue("play"));
            mainSPan.append(returnButton); 

        }
        else if (status == 3) { 
            spaner.addClass("statuses redColor");
            stateKey = "ended";
            div1.css("display", "block");
        }
        else if (status == 4) {
            spaner.addClass("statuses redColor");
            stateKey = "canceled";
            div1.css("display", "block"); 
        }
         
        div1.addClass("tourn_finished");

        spaner1.addClass("translator");
        spaner1.data("translate", stateKey);
        spaner1.text(GetTranslatedValue(stateKey)); 

        div1.append(spaner);
        div1.append(spaner1);
        mainSPan.append(div1);
        mainSPan.append(infoButton);

    }

    if (status > 2) {
        try {
            var mytournirs = $(".mytournirs2");
            for (var i = 0; i < mytournirs.length; i++) {
                var item = $(mytournirs[i]).find(".tournament_play");
                var tour_id = item.data("tournamnet_id");
                if (tour_id == id) {
                    item.remove();
                }
            }
        } catch (ex) {

        }
    }

}

function tournamentRemove(event) {
    var id = event.id;
    var tournamenttr = $("#tour-" + id);
    tournamenttr.remove();
}

function tournamentPlayersCountChange(event) {

    var id = event.id;
    var players_count = event.players_count;
    var max_count = event.max_count;
    var tournamenttr = $("#tour-" + id);
    var playersTd = tournamenttr.find(".players-count");
    playersTd.text(players_count + " / " + max_count);
}


function registerTournamentEvents() {

    $(document).on('click', '.tour-register', function () {
        var id = $(this).data("id");

        if ($(this).hasClass("register")) {
            $(".register-modal").data("id", id);
            $("#reg-popup-amount").html($(this).data("amount"));
            
            $("#reg-tournament-name").text(GetNameByLang($(this).data("name0"), $(this).data("name1"), $(this).data("name2")));

            $("#TurnRegModal").modal();

        } else if ($(this).hasClass("unregister")) {
            $(".unregister-modal").data("id", id);
            $("#unreg-tournament-name").text(GetNameByLang($(this).data("name0"), $(this).data("name1"), $(this).data("name2")));
            $("#TurnLeaveModal").modal();

        }

    });

    $(document).on('click', '.register-modal', function () {
        var dt = {};
        var id = $(this).data("id");
        dt.id = id;
        Send("tournaments_register", dt);
        $("#TurnRegModal").modal('hide');
    });

    $(document).on('click', '.unregister-modal', function () {
        var dt = {};
        var id = $(this).data("id");
        dt.id = id;
        Send("tournaments_unregister", dt);
        $("#TurnLeaveModal").modal('hide');
    });

    $(document).on('click', '.tournament-activate', function () {
        var token = getQueryVariable("token");
        var tour_id = $("#TournActiveModal").data("id");
        var rm_id = $("#TournActiveModal").data("room_id");
        $("#TournActiveModal").modal("hide");
        popups["pop" + rm_id] = window.open(tournament_link + token + "&roomid=" + rm_id + "&tournament_id=" + tour_id, "table" + rm_id, "width=1000, height=715");

    });

    $(document).on('click', '.tournament_play', function () {
        var that = $(this);
        var token = getQueryVariable("token");
        var tour_id = that.data("tournamnet_id");
        var rm_id = that.data("room_id");

        popups["pop" + rm_id] = window.open(tournament_link + token + "&roomid=" + rm_id + "&tournament_id=" + tour_id, "table" + rm_id, "width=1000, height=715");

    });


    $(document).on('click', '#blurbg1', function () {
         
        $(".show_main_menu").trigger("click");
    });

    $(document).on('click', '#rank-increase', function () {

        if (lobby.rank < lobby.point_status.length) {
            $("#rank-increase-fee").text(lobby.point_status[lobby.rank - 1]);
            if (lobby.point_status[lobby.rank - 1] <= lobby.coin) {
                $("#rank-button").attr("disabled", "");
                $("#rank-button").prop("disabled", false);
            } else {
                $("#rank-button").prop("disabled", true);
            }
        } else {
            $("#rank-button").prop("disabled", true);
        }


        $("#rankUpModal").modal();

    });

    $(document).on('click', '#rank-button', function () {
        Send("rank_increase", {});

        $("#rankUpModal").modal("hide");

    });


}

function removeAllClassToBtn(obj) {
    obj.removeClass("register");
    obj.removeClass("unregister");
    obj.removeClass("btn-danger");
    obj.removeClass("btn-primary");
}
function register(event) {
    if (!event.result) {
        event.result = true;
        unregister(event)
    }
    var obj = $("#tour-" + event.id).find(".tour-register");
    removeAllClassToBtn(obj)
    obj.addClass("unregister");
    obj.addClass("btn-danger translator");
    obj.data("translate", "cancel");
    obj.text(GetTranslatedValue("cancel"));
}
function unregister(event) {
    if (!event.result) {
        event.result = true;
        register(event)
    }
    var obj = $("#tour-" + event.id).find(".tour-register");
    removeAllClassToBtn(obj)
    obj.addClass("register translator");
    obj.addClass("btn-primary");
    obj.addClass("translator");
    obj.data("translate", "registration");
    obj.text(GetTranslatedValue("registration"));

}

function tournamentPlay(event) {
    var tournament_id = event.tournament_id;
    var room_id = event.room_id;

    $("#TournActiveModal").data("id", tournament_id);
    $("#TournActiveModal").data("room_id", room_id);
    $("#TournActiveModal").modal();

    addTournamentRoom(event);

}


function addTournamentRooms(rooms) {

    for (var i = 0; i < rooms.length; i++) {
        if (rooms[i].tournament_id > 0) {
            addTournamentRoom(rooms[i]);
        } else {

            playerSitOnTable(rooms[i].room_id);
        }

    }

}

function playerSitOnTable(roomid) {
    var room = $("#" + roomid);
    room.data("creator", true);
    room.data("prior", 0);
    var btn = room.find(".redirect1");
    btn.removeClass("btn-success");
    btn.removeClass("btn-danger");
    btn.addClass("btn-primary translator");
    btn.data("translate","return_type");
    btn.text(GetTranslatedValue("return_type")); 

    btn.prop("disabled", false);
    room.show();
    $("#room-container").prepend(room);
}


function addTournamentRoom(room) {

    var tournamentInstance = $("#" + room.room_id);
    if (tournamentInstance) {
        tournamentInstance.remove();
    }

    var div1 = $("<div></div>");
    div1.addClass("mytournirs2");
    div1.attr("id", room.room_id);

    var a1 = $("<a></a>");
    a1.addClass("playMyTourn tournament_play");
    var i1 = $("<i></i>");
    a1.data("tournament_id", room.tournament_id);
    a1.data("room_id", room.room_id);
    i1.addClass("fa fa-chevron-right");
    a1.append(i1);

    var div2 = $("<div></div>");
    div2.addClass("whiteColor mytourn_name");
    div2.text(room.name);

    var div3 = $("<div></div>");
    div3.addClass("translator");
    div3.data("translate","ongoing");
    div3.text(GetTranslatedValue("ongoing")); 

    div1.append(a1);
    div1.append(div2);
    div1.append(div3);
    $("#my-tournament-rooms").append(div1);



    var button = $("#tour_room" + room.tournament_id);
    button.data("room_id", room.room_id);
    button.show();

    var tournamenttr = $("#tour-" + room.tournament_id);
    var mainSPan = tournamenttr.find(".register_td");
    mainSPan.find("div").hide();
}

function GetTournamentData() {
    if (activeTournamentId > 0 && $("#tournament_info_panel").is(":visible")) {
        GetTournamentDetails(activeTournamentId);
    }
}



function getTournamentInfo(event) {


    var data = event;
    var flow = data.flow;
    var prize = data.prize;

    var fee = parseFloat(data.fee).toFixed(2);

    $("#tour_prize_0").html(0 + " <font class='GEL'>b</font>");
    $("#tour_prize_1").html(0 + " <font class='GEL'>b</font>");
    $("#tour_prize_2").html(0 + " <font class='GEL'>b</font>");

    var nametd = $("#tour_detail_name");
    nametd.text(GetNameByLang(data.name_ge, data.name, data.name_ru));
    nametd.data("name0", data.name_ge);
    nametd.data("name2", data.name_ru);
    nametd.data("name1", data.name);

    $("#tour_detail_type").text(GetTranslatedValue(GetGameType(data.type)));
    $("#tour_detail_date").text(data.full_date);
    $("#tour_detail_players_count").text(data.players_count + " / " + data.max_count);
    $("#tour_detail_prize").html(prize + " <font class='GEL'>b</font>");

    $("#tour_min_players_count").text(data.min_count);

    $("#active_tournament_register").data("id", data.id);
    $("#active_tournament_fee").html(fee + " <font class='GEL'>b</font>");
    $("#active_tournament_register").data("amount", fee);
    $("#active_tournament_register").data("name", data.name);

    $("#active_tournament_unregister").data("name", data.name);
    $("#active_tournament_unregister").data("id", data.id);
    $(".active_tournament_button").hide();
    var finishText = "ended";

    if (data.state == 1) {
        finishText = "active";
        if (data.player_sit) {
            $("#active_tournament_unregister").show();
        } else {
            $("#active_tournament_register").show();
        }
        var days = "";
        if (data.days_difference > 0) {
            days = data.days_difference + " " + GetTranslatedValue("day") + ", ";
        }

        $("#active_tour_time_wait").text(days + GetTimeString(data.hours_difference));


    } else if (data.state == 2) {
        finishText = "active";
        $("#active_tournament_continue").show();
        $("#active_tour_time_passed").text(GetTimeString(data.hours_difference));

    } else if (data.state == 3) {

        $("#active_tournament_finish").show();

    }

    if (data.state == 2) {
        $(".timelapsed_div").show();
    } else {
        $(".timelapsed_div").hide();
    }


    if (data.state == 1 && data.tournament_id == 1) {
        $(".timeleft_div").show();

    } else {
        $(".timeleft_div").hide();

    }



    PrizeListStructure(data.flow.prizes, prize)
    PlayersListStructure(data.flow.players, finishText);
    TournamentMatches(data.flow.matches);
}

function PrizeListStructure(prizes, prize) {
    $("#prize_list_structure").empty();
    for (var i = 0; i < prizes.length; i++) {
        var prizeValue = parseFloat(prizes[i] * prize).toFixed(2);
        var prize_per_user = prizeValue + " <font class='GEL'>b</font>";
        $("#tour_prize_" + i).html(prizeValue + " <font class='GEL'>b</font>");

        var mainTr = $("<tr></tr>");
        var td1 = $("<td></td>");
        td1.text((i + 1));
        var td2 = $("<td></td>");
        td2.addClass("translator");
        td2.data("translate","no")
        td2.text(GetTranslatedValue("no"));
        var td3 = $("<td></td>");
        td3.addClass("text-center");
        td3.html(prize_per_user);
        var td4 = $("<td></td>");

        mainTr.append(td1);
       // mainTr.append(td2);
        mainTr.append(td3);
        mainTr.append(td4);
        $("#prize_list_structure").append(mainTr);
    }

}

function PlayersListStructure(players, finishText) {
    $("#tournament_players_list").empty();
    $("#tournament_my_position").empty();

    for (var i = 0; i < players.length; i++) {
        var prize_per_user = parseFloat(players[i].prize).toFixed(2) + " <font class='GEL'>b</font>"; 

        var mainTr = $("<tr></tr>");
        var td1 = $("<td></td>");
        td1.text((i + 1));
        var td2 = $("<td></td>"); 

        var span = $("<span></span>");
        span.addClass("whiteColor");
        span.text(players[i].username);

        var small = $("<small></small>");  
        var span1 = $("<span></span>"); 
        span1.addClass("translator");
        span1.data("translate", finishText);
        span1.text(GetTranslatedValue(finishText));
        small.append(span1); 
        td2.append(span);
        td2.append(small);

 
        var td3 = $("<td></td>");
        td3.addClass("text-center yellowColor");
        td3.html(prize_per_user); 
  

        mainTr.append(td1);
        mainTr.append(td2); 
        mainTr.append(td3);
 

        if (players[i].id == player_id) {
            var newTr = mainTr.clone();
            $("#tournament_my_position").append(newTr);
        }
        $("#tournament_players_list").append(mainTr);
    }

}

var tournamentLevels = ["final", "final1", "final2", "final3", "final4", "final5", "final6", "final7", "final8", "final9", "final10", "final11"];

var active_tour_row = 3;
function TournamentMatches(tournamentsMatches) {
    $("#tournament_stairs").empty();
    $("#matches_list").empty();
    var matches = tournamentsMatches;

    var mainIndex = $("#tournament_stairs").data("index");
    if (mainIndex > matches.length) {
        mainIndex = 1;
        $("#tournament_stairs").data("index", mainIndex);
    }

    for (var i = matches.length - 1; i >= 0; i--) {


        var mainLi = $("<li></li>");
        mainLi.addClass("row_index_" + i);
        var div1 = $("<div></div>");

        mainLi.attr("id", "draw-" + (matches.length - i));

        var currIndexer = i + 1;
        mainLi.attr("data-index", currIndexer);

        if (currIndexer != mainIndex && currIndexer != mainIndex + 1) {
            mainLi.css("display", "none");
        }

        div1.addClass("group-20 text-center");

        var level = matches[i];

        var span1 = $("<span></span>");
        span1.addClass("translator");
        var text1 = tournamentLevels[matches.length - i - 1];
        span1.data("translate", text1);
        span1.text(GetTranslatedValue(text1));
        div1.append(span1);
        mainLi.append(div1);


        for (var j = 0; j < level.length; j++) {
            var row = level[j];
            var div2 = $("<div></div>");
            div2.addClass("draw_results_main");

            var div3 = $("<div></div>");
            div3.addClass("draw_results1");

            var span2 = $("<div></div>");
            span2.addClass(loser(row.level, row.op_level));
            span2.text(row.username);
            span2.addClass(isMe(row.player_id));

            var div4 = $("<div></div>");
            div4.addClass("draw_results2");
            div4.text(result(row.level, row.op_level));

            var div5 = $("<div></div>");
            div5.addClass("draw_results3");


            var span3 = $("<div></div>");
            span3.addClass(loser(row.op_level, row.level));
            span3.addClass(isMe(row.opponent_id));
            span3.text(row.opponent_name);
          

            var div6 = $("<div></div>");
            div6.addClass("clearfix");

            div3.append(span2);
            div2.append(div3);
            div2.append(div4);
            div2.append(div5);
            div5.append(span3);
            div2.append(div6);
            mainLi.append(div2);

            MatchesList(row, i + 1);
        }

        $("#tournament_stairs").prepend(mainLi);
    }

}

function MatchesList(row, index) {



    var maintr = $("<tr></tr>");
    var td1 = $("<td></td>");
    var td2 = $("<td></td>");

    var small = $("<small></small>");
    var div   = $("<div></div>");  
    var div1  = $("<div></div>");
    var div2  = $("<div></div>");
    var div3 = $("<div></div>"); 



    td1.addClass("text-center");
    div.text(index);
    small.addClass("translator");
    var key = complete(row.level, row.op_level);
    small.data("translate", key);
    small.text( GetTranslatedValue(key)); 

  
    td2.addClass("whiteColor");  
    div.addClass("whiteColor"); 
    div2.addClass("yellowColor");

    div1.text(row.username);
    div2.text(result(row.level, row.op_level));
    div3.text(row.opponent_name);



    td1.append(div);
    td1.append(small);

     
    td2.append(div1);
    td2.append(div2);
    td2.append(div3);


    maintr.append(td1);
    maintr.append(td2);


    $("#matches_list").prepend(maintr);
}

function isMe(plId) {

    if (plId == player_id) {
        return "yellowColor";
    }

    return "";
}

function result(level1, level2) {
    if (level1 == -1 || level2 == -1) {
        return "0 : 0";
    }
    if (level1 == level2) {
        return "0 : 0";
    } else if (level1 > level2) {
        return "1 : 0";
    }
    return "0 : 1";
}

function loser(level1, level2) {
    if (level1 >= level2) {
        return "";
    }
    return "loser";
}

function complete(level1, level2) {
    if (level1 == level2) {
        return "active";
    }
    return "ended";
}

function GetTournamentData() {
    if (activeTournamentId > 0 && $("#tournament_info_panel").is(":visible")) {
        GetTournamentDetails(activeTournamentId);
    }
}


function CalculateTimeDifference(time) {
 
}

function GetTimeString(seconds) {
    seconds = Math.abs(seconds);
    var total_hours = parseInt(seconds / 3600);
    var total_minutes = parseInt((seconds % 3600) / 60);
    var total_seconds = parseInt(seconds % 60);

    return ReturnTimeString(total_hours) + " : " + ReturnTimeString(total_minutes) + " : " + ReturnTimeString(total_seconds);
}
function ReturnTimeString(time) {
    if (time < 10) {
        return "0" + time;
    }
    return time;
}

$(document).on('click', '.moveListLeft', function (e) {

    e.preventDefault();
    e.stopImmediatePropagation();

    var $cnt = $('.tournament_draw');
    var length = $cnt.children().length;
    var currentFirstIndex = parseInt($cnt.find('li[data-index]:visible').first().attr('data-index'));

    if (currentFirstIndex > 1) {

        $cnt.find('li[data-index=' + (currentFirstIndex - 1) + ']').show();
        $cnt.find('li[data-index=' + (currentFirstIndex) + ']').hide();
        $cnt.find('li[data-index=' + (currentFirstIndex + 1) + ']').hide();
        $("#tournament_stairs").data("index", currentFirstIndex - 1);
    }
});

$(document).on('click', '.moveListRight', function (e) {

    e.preventDefault();
    e.stopImmediatePropagation();

    var $cnt = $('.tournament_draw');
    var length = $cnt.children().length;
    var currentLastIndex = parseInt($cnt.find('li[data-index]:visible').last().attr('data-index'));

    if (length > currentLastIndex) {

        $cnt.find('li[data-index=' + (currentLastIndex-1) + ']').hide();
        $cnt.find('li[data-index=' + (currentLastIndex) + ']').hide();
        $cnt.find('li[data-index=' + (currentLastIndex + 1) + ']').show();

        $("#tournament_stairs").data("index", currentLastIndex+1);
    }
});
