﻿function createRoom(evt) { 

    var spd;

    switch (evt.speed) {
        case 1:
            spd = "fast";
            break;
        case 2:
            spd = "normal";
            break;

        case 3:
            spd = "slow";
            break; 
    }

    var room_id = evt.room_id;
    var count = parseInt(evt.player_count);
    var point = evt.points;
    var roomName = evt.room_name;
    var gameType = GetGameType(evt.type);
    var bet_text = evt.min_bet;
    if (evt.min_bet < evt.max_bet) {
        bet_text = evt.min_bet + "-" + evt.max_bet;
    }

    var tr = $("<tr></tr>");
    tr.addClass("selnardi");
    tr.attr('data-playerCount', evt.player_count);
    tr.attr("ID", room_id); 
    tr.data("amount", evt.max_bet);
    tr.data("with_doubles", evt.with_doubles);
   
    //room name
    var td1 = $("<td></td>");
    var span1 = $("<span></span>");
    span1.addClass("WhiteColor");
    span1.text(roomName);

    var small1 = $("<small></small>");
    //info
  
    var span2 = $("<span></span>");
    span2.addClass("translator");
    span2.data("translate", gameType);
    span2.text(GetTranslatedValue(gameType));
    var span4 = $("<span></span>");
    span4.text(",");
    var span3 = $("<span></span>");
    span3.addClass("translator");
    span3.data("translate", spd);
    span3.text(GetTranslatedValue(spd));

    var doublesText = "yesdoubles";
    if (!evt.with_doubles) {
        doublesText = "nodoubles";
    }
    
    small1.append(span2);
    small1.append(span4);
    small1.append(span3);
    //small1.append(span44);
    //small1.append(span55);

    td1.append(span1);  
    td1.append(small1)

    // bet initialize
    var td2 = $("<td></td>");
    td2.addClass("text-center");
    td2.addClass("yellowColor");

    var span4 = $("<span></span>");
    span4.text(bet_text);
    var span5 = $("<span><font class='GEL'>b</font></span>");
    td2.append(span4);
    td2.append(span5);

    //point initialize
    var td3 = $("<td></td>");
    td3.addClass("text-center");


    var span6 = $("<span></span>"); 
    span6.addClass("whiteColor");
    span6.text(point);
    var span7 = $("<span></span>");
    span7.addClass("translator");
    span7.data("translate", "till_post");
    span7.text(GetTranslatedValue("till_post"));
    td3.append(span6);
    td3.append(span7);

    // play item
    var td4 = $("<td></td>");
    td4.addClass("text-right redirect");
    td4.attr("ID", "pls-count-" + evt.room_id);
    td4.data("value", evt.room_id);
 
    var btn = $('<button class="play-tourn btn btn-success redirect1 translator"></button>');
    btn.data("translate", "play");
    btn.text(GetTranslatedValue("play"))
    tr.data("prior", 1);
    if (count === 2) { 
        tr.data("prior", 2);
        btn.prop("disabled", true);
        btn.removeClass("btn-success");
        btn.removeClass("btn-primary");
        btn.addClass("btn-danger");
  
        btn.data("translate", "ongoing");
        btn.text(GetTranslatedValue("ongoing"))
    }
 

    evt.visible = evt.visible || player_id == evt.creator_id;
    if (evt.visible) {
        if (player_id == evt.creator_id) {
            btn.removeClass("btn-success");
            btn.removeClass("btn-danger");
            btn.addClass("btn-primary");
            tr.data("prior", 0);
            tr.data("creator", 1); 

            btn.data("translate", "return_type");
            btn.text(GetTranslatedValue("return_type"))
            btn.prop("disabled", false);
        }
        tr.show();
    } else {
        tr.hide();
    }

      td4.append(btn)

//  append items;
    tr.append(td1);
    tr.append(td2);
    tr.append(td3);
    tr.append(td4);
    $("#room-container").append(tr);

}


function drawHistoryRows(event) {
    $("#game-history-container").empty();
    var rows = event.rows;
    for (var i = 0; i < rows.length; i++) {
        drawHistoryRow(rows[i]);
    }

}

function drawHistoryRow(row) {

    var game_id = row.game_id;
    var game_time = row.time.replace("T", " ");
    var bet = parseFloat(row.total_bet / 2).toFixed(2); 
    var result = row.my_score + " : " + row.op_score;

    var winKey = "lose";
    var winColor = "redColor";

    if (row.winner) {
        winKey = "winning";
        winColor = "greenColor";
    }
    //tr1
    var tr = $("<tr></tr>"); 
    var td1 = $("<td></td>"); 
    //td1
    var span1 = $("<span></span>"); 
    span1.addClass("whiteColor");
    span1.text(row.game_id + ", " + row.op_name);
    td1.append(span1);

    var small1 = $("<small></small>"); 
    small1.text(game_time);
    td1.append(small1);

    var small2 = $("<small></small>");

    var span2 = $("<span></span>"); 
    span2.text("ანგ :");
    small2.append(span2);

    var span3 = $("<span></span>"); 
    span3.addClass("whiteColor");
    span3.text(result);
    small2.append(span3);
    td1.append(small2);

    //td2
    var td2 = $("<td></td>"); 
    td2.addClass("text-center yellowColor");
    var span4 = $("<span></span>");
    span4.text(bet);
    var span5 = $("<span></span>");
    span5.html("<font class='GEL'>b</font>");

    td2.append(span4);
    td2.append(span5);

     //td3
    var td3 = $("<td></td>");
    td3.addClass("text-center");
    var span6 = $("<span></span>");
    span6.addClass(winColor);
    span6.addClass("translator");
    span6.data("translate", winKey)
    
    span6.text(GetTranslatedValue(winKey)); 
 
    td3.append(span6);


    //td4
    var td4 = $("<td></td>");
    td4.addClass("text-right");
  

 
    var watchButton = $("<button></button>");
    watchButton.data("id", row.game_id);
    watchButton.addClass("btn btn-default btn-circle play-history"); 

    var playI = $("<i></i>");
    playI.addClass("fa fa-play");
    watchButton.append(playI);
    td4.append(watchButton);
 



    tr.append(td1);
    tr.append(td2);
    tr.append(td3);
    tr.append(td4);
  

    $("#game-history-container").append(tr);
}




function addShopHistoryItems(items) {
  
    $("#shop-history").empty();
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        addShopHistoryItem(item, i)
    }

}

function addShopHistoryItem(item, index) {
    var mainTr = $("<tr></tr>");
    mainTr.attr("id", "shop_history_" + item.id);

 
    //image
    var imageTd = $("<td></td>");
    var imager = $('<img/>');
    imager.attr('src', 'https://nardi.ge/images/' + item.img_url);

    imageTd.append(imager);

    //name status td
    var nameTd = $("<td></td>");

    var div1 = $("<div></div>");

    div1.addClass("translate_title");
    div1.data("name0", item.name_geo);
    div1.data("name1", item.name);
    div1.data("name2", item.name_rus);
    div1.text(GetNameByLang(item.name_geo, item.name, item.name_rus));
    //
    var div2 = $("<div></div>");
    var span2 = $("<span></span>");
    span2.addClass("greyColor translator");
    span2.data("translate", "price");
    span2.text(GetTranslatedValue("price"));
    var span3= $("<span></span>");  
    span3.text(item.price);

    div2.append(span2);
    div2.append(span3);
    //div datetime
    var div3 = $("<div></div>");
    var dateSpan = $("<span></span>"); 
    dateSpan.addClass("greyColor translator");
    dateSpan.data("translate", "date");
    dateSpan.text(GetTranslatedValue("date"));

    var dateValueSpan = $("<span></span>"); 
    dateValueSpan.text(item.time);

    div3.append(dateSpan); 
    div3.append(dateValueSpan);
    ///status

    var div4 = $("<div></div>")
    div4.addClass("translator");
    div4.data("translate", item.order_status.toLowerCase());

  
    if (item.order_status_value == 1) {
        div4.addClass("greenColor");
    }
    if (item.order_status_value == 2) {
        div4.addClass("yellowColor");
    }
    if (item.order_status_value == 3) {
        div4.addClass("redColor");
    }

    div4.text(GetTranslatedValue(item.order_status.toLowerCase()));


    nameTd.append(div1);
    nameTd.append(div2);
    nameTd.append(div3);
    nameTd.append(div4);
 
 

 
    mainTr.append(imageTd);
    mainTr.append(nameTd);
 

    $("#shop-history").append(mainTr);
}


 