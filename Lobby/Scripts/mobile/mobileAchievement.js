﻿
function drawAchievements(achievements, rank) {
    $("#achievement-table").empty();
    for (var i = 0; i < achievements.length; i++) {
        drawAchievement(achievements[i], rank);
    }
    //translateLobby();

}

function drawAchievement(achievement, rank) {
    var mainTr = $("<tr></tr>");
    mainTr.attr("id", "achievement-" + achievement.id);
    mainTr.addClass("achievement_sort");
    mainTr.data("rank", achievement.rank);
    mainTr.data("sort", achievement.rank*10)
    //name
    var td = $("<td></td>"); 
    var diver = $("<div></div>");
    diver.addClass("missions_tr");

    var div2 = $("<div></div>");  
    div2.addClass("missions_td");
    div2.addClass("translator");
    div2.data("translate", "mission_name");
    div2.text(GetTranslatedValue("mission_name")); 
      
    var div3 = $("<div></div>");
    div3.addClass("missions_td");

    var div4 = $("<div></div>");
    div4.addClass("misn_name whiteColor");

    var spann = $("<span></span>");
    spann.addClass("misn_name whiteColor");
    spann.addClass("translate_title");
    spann.data("name0", achievement.name_ge);
    spann.data("name2", achievement.name_ru);
    spann.data("name1", achievement.name);
    spann.text(GetNameByLang(achievement.name_ge, achievement.name_ru, achievement.name));


    div4.append(spann);
    div3.append(div4);   
    diver.append(div2);
    diver.append(div3);

    td.append(diver);
    // description tr

 

    var diver = $("<div></div>");
    diver.addClass("missions_tr");

    var div2 = $("<div></div>");
    div2.addClass("missions_td");
    div2.addClass("translator");
    div2.data("translate", "description");
    div2.text(GetTranslatedValue("description"));

    var div3 = $("<div></div>");
    div3.addClass("missions_td");

    var div4 = $("<div></div>");
    div4.addClass("misn_desc whiteColor");

    var spann = $("<span></span>");
    spann.addClass("misn_name whiteColor");
    spann.addClass("translate_title");
    spann.data("name0", achievement.description_ge);
    spann.data("name2", achievement.description_ru);
    spann.data("name1", achievement.description);
    spann.text(GetNameByLang(achievement.description_ge, achievement.description, achievement.description_ru));


    div4.append(spann);
    div3.append(div4);
    diver.append(div2);
    diver.append(div3);

    td.append(diver);
    //  rank 

 

    var diver = $("<div></div>");
    diver.addClass("missions_tr");

    var div2 = $("<div></div>");
    div2.addClass("missions_td");
    div2.addClass("translator");
    div2.data("translate", "status");
    div2.text(GetTranslatedValue("status"));

    var div3 = $("<div></div>");
    div3.addClass("missions_td");

    var div4 = $("<div></div>");
    div4.addClass("misn_rank whiteColor");

    var spann = $("<span></span>");
    spann.addClass("misn_name whiteColor"); 
    spann.text(achievement.rank);


    div4.append(spann);
    div3.append(div4);
    diver.append(div2);
    diver.append(div3);

    td.append(diver);
    //prize
    
 

    var diver = $("<div></div>");
    diver.addClass("missions_tr");

    var div2 = $("<div></div>");
    div2.addClass("missions_td");
    div2.addClass("translator");
    div2.data("translate", "prize");
    div2.text(GetTranslatedValue("prize"));

    var div3 = $("<div></div>");
    div3.addClass("missions_td");

    var div4 = $("<div></div>");
    div4.addClass("misn_prize whiteColor");

    var spann = $("<span></span>");
    spann.text(achievement.coin_count);

    var small = $("<small></small>");
    small.addClass("translator greyColor");
    small.data("translate", "point");
    small.text(GetTranslatedValue("point"));

    div4.append(spann);
    div4.append(small);

    div3.append(div4);
    diver.append(div2);
    diver.append(div3);

    td.append(diver);
    // progress


 

    var diver = $("<div></div>");
    diver.addClass("missions_tr");

    var div2 = $("<div></div>");
    div2.addClass("missions_td");
    div2.addClass("translator");
    div2.data("translate", "progress");
    div2.text(GetTranslatedValue("progress"));

    var div3 = $("<div></div>");
    div3.addClass("missions_td");

    var div4 = $("<div></div>");
    div4.addClass("progressdiv1");

    var spann = $("<div></div>");
    spann.addClass("progress_bar"); 
  

    var small = $("<div></div>");
    small.addClass("progress_text");
    small.data("count", achievement.count);
    small.text("0/" + achievement.count);

    div4.append(spann);
    div4.append(small);

    div3.append(div4);
    diver.append(div2);
    diver.append(div3);

    td.append(diver); 
    //
 

    
    var text = GetTranslatedValue("not_enough_status")

    if (achievement.rank > rank) {

        div4.empty();

        div4.removeClass("progressdiv1");
        div4.addClass("misn_desc"); 

        var span = $("<span></span>");
        span.addClass("translator");
        span.data("translate", "not_enough_status");
        span.text(GetTranslatedValue("not_enough_status")); 
        div4.append(span); 
        mainTr.addClass("disabled_mission");
   
    } 

    //add childs
    mainTr.append(td);
 
    $("#achievement-table").append(mainTr);
}

function updateAchievements(playerAchieves) {
    for (var i = 0; i < playerAchieves.length; i++) {
        updateAchievement(playerAchieves[i]);
    }
    var tables = [];

    $(".achievement_sort").each(function (index) {
        var obj = {};
        obj.id = $(this).attr("id");
        obj.rank = parseInt($(this).data("sort"));
        tables.push(obj);
    });

    tables.sort(function (a, b) { return a.rank - b.rank; });

    for (var i = 0; i < tables.length; i++) {
        var obj = tables[i];
        $("#achievement-table").append($("#" + obj.id));
    }

    return tables;
}

function updateAchievement(playerAchieve) {
    var complete = playerAchieve.complete;
    if (complete) {
        var text = GetTranslatedValue("complete")
        var progressBartd = $("#achievement-" + playerAchieve.id).find(".progressdiv1");
        progressBartd.empty();

        progressBartd.removeClass("progressdiv1"); 
        progressBartd.addClass("misn_desc");
 

        var span = $("<span></span>");
        span.addClass("translator");
        span.data("translate", "complete");
        span.text(text);

        progressBartd.append(span);
        $("#achievement-" + playerAchieve.id).addClass("disabled_mission");
        var rank = $("#achievement-" + playerAchieve.id).data("sort");
        $("#achievement-" + playerAchieve.id).data("sort", rank+5);
    } else {
        var progressBar = $("#achievement-" + playerAchieve.id).find(".progress_bar");
        progressBar.css("width", (playerAchieve.count / playerAchieve.full_count) * 100 + "%");
        var progressText = $("#achievement-" + playerAchieve.id).find(".progress_text");
        progressText.text(playerAchieve.count + " / " + playerAchieve.full_count);
    }


}


function InitializeRankPopup(rank) {
 
    var start = 0;
    var maxRank = lobby.point_status.length;
    if (rank < 3) {
        start = 0;
    } else if (rank >= maxRank - 2) {
        start = maxRank - 5;
    } else {
        start = rank - 2;
    }


    for (var i = 1; i <= 5; i++) {
        $("#rank-count-" + i).text(i + start);
        $("#rank-" + i).removeClass("rank_done");
        $("#rank-" + i).removeClass("rank_active");

        if (rank >= i + start) {
            $("#rank-" + i).addClass("rank_done");
        } else if (rank + 1 == i + start) {
            $("#rank-" + i).addClass("rank_active");
        }
    }

    if (rank < maxRank) {
        $("#rank-increase-fee").text(lobby.point_status[rank - 1]);
    } else {

        $("#rank-button").attr("disabled", 'disabled');
        $("#rank-button-text").addClass("translator");
        $("#rank-button-text").data("translate", "max_status");
        $("#rank-button-text").text(GetTranslatedValue("max_status"));
    }



}