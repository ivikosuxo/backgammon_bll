﻿$(document).ready(function (e) {
  

    $(".openItemsModal").click(function () {
        $(".itemModal").show(10);
    });
    $(".closeItemsModal").click(function () {
        $(".itemModal").hide(10);
    });

    $(".blurbg").hide();
    $(".show_main_menu").click(function () {
        $(".main_menu").toggleClass('active');
        $(".blurbg").fadeToggle(200);
    });

    $(".table_filter").hide();
    $(".show_filter").click(function () {
        if ($(this).hasClass('active')) {
            $(".table_filter").hide();
            $(this).removeClass('active');
        } else {
            $(".table_filter").show();
            $(this).addClass('active');
        }
    });

    (function () {
        var toggles = document.querySelectorAll(".c-hamburger");

        for (var i = toggles.length - 1; i >= 0; i--) {
            var toggle = toggles[i];
            if (toggle.classList.contains("notAuto") == true) {
                continue;
            }
            toggleHandler(toggle);
        };

        function toggleHandler(toggle) {
            toggle.addEventListener("click", function (e) {
                e.preventDefault();
                (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
            });
        }

    })();

});