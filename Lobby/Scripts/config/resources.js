  
 var manifest = [

	

     { src: "board/dice_roll.png", id: "dice_roll" },
     { src: "board/single_roll.png", id: "single_roll" },
      { src: "board/board_blue.jpg",   id: "2board" },
      
       
      { src: "board/board.jpg", id: "board" },
        { src: "board/board_red.jpg", id: "1board" }, 
 
  /* 
		{ src: "board/left_bg.png", id: "panel" },  */

 

        { src: "board/white_1.png", id: "1stone_1_1" },
        { src: "board/red_1.png", id: "1stone_2_1" },

        { src: "board/white_2.png", id: "1stone_1_2" },
        { src: "board/red_2.png", id: "1stone_2_2" },

        { src: "board/white_3.png", id: "1stone_1_3" },
        { src: "board/red_3.png", id: "1stone_2_3" },
 
        { src: "board/discard_white.png", id: "3stone_1" },
        { src: "board/discard_red.png", id: "3stone_2" },

        { src: "board/white_1d.png", id: "white_1d" },
        { src: "board/red_1d.png", id: "red_1d" },
        { src: "board/white_2d.png", id: "white_2d" },
        { src: "board/red_2d.png", id: "red_2d" },
        { src: "board/white_3d.png", id: "white_3d" },
        { src: "board/red_3d.png", id: "red_3d" },

     

        { src: "settings/settings_bg.png", id: "settings_bg" },
        { src: "settings/bg1.png", id: "setBg1" },
        { src: "settings/bg1_h.png", id: "setBg1h" },

 

 
      
  
        { src: "settings/color1.png", id: "setcolor1" },
        { src: "settings/color2.png", id: "setcolor2" },
        { src: "settings/color3.png", id: "setcolor3" },


        { src: "settings/color1_h.png", id: "setcolor_h_1" },
        { src: "settings/color2_h.png", id: "setcolor_h_2" },
        { src: "settings/color3_h.png", id: "setcolor_h_3" },

        { src: "settings/set_chip1.png", id: "set_chip1" },
        { src: "settings/set_chip1_h.png", id: "set_chip1h" },
     
        { src: "settings/set_chip2.png", id: "set_chip2" },
        { src: "settings/set_chip2_h.png", id: "set_chip2h" },

        { src: "settings/set_chip3.png", id: "set_chip3" },
        { src: "settings/set_chip3_h.png", id: "set_chip3h" },

         
        { src: "settings/set_color1.png", id: "set_color0" },
        { src: "settings/set_color_h1.png", id: "set_color_h0" },

        { src: "settings/set_color2.png", id: "set_color3" },
        { src: "settings/set_color_h2.png", id: "set_color_h3" },

             { src: "Panel/submit_icon.png", id: "submit_icon" },
             { src: "Panel/undo_icon.png", id: "undo_icon" },
            { src: "Panel/roll_icon.png", id: "roll_icon" },
            { src: "Panel/panel_cup.png", id: "panel_cup" },
     
        { src: "board/blackbg1.png", id: "blbg" },
        { src: "board/whitebg1.png", id: "wtbg" },
       
     
        { src: "board/light.png", id: "hint" },

         { src: "Panel/right-panel.jpg", id: "panelBG1" },
   

        { src: "Panel/close.png", id: "close" },
        { src: "Panel/check.png", id: "check" },
        { src: "Panel/checkact.png", id: "checkact" },
 
           { src: "Panel/giveup_icon.png", id: "giveup_icon" },
           { src: "Panel/point_circle.png", id: "point_circle" },
           { src: "Panel/point_circle.png", id: "point_circle_h" },

   

        { src: "Panel/user_icon.png", id: "user_icon" },
 
        
     
         { src: "Panel/arrowh.png", id: "arrowh" },
  
         { src: "Panel/fullscreen.png", id: "fullscreen" },
         { src: "Panel/fullscreen_h.png", id: "fullscreenh" },
     
         { src: "Panel/settings.png", id: "settings" },
        { src: "Panel/settings_h.png", id: "settingsh" },

         { src: "Panel/volumeoff.png", id: "vol_off" },
         { src: "Panel/volumeon.png", id: "vol_on" },

        { src: "Panel/volumeoff_h.png", id: "vol_offh" },
         { src: "Panel/volumeon_h.png", id: "vol_onh" },

     
      { src: "Panel/problem_icon.png", id: "pr_icon" },
 

   { src: "chat/problem_user.png", id: "problem_user" },
    { src: "chat/problem_roll.png", id: "problem_roll" },
    { src: "chat/problem_pop.png", id: "problem_pop" },
    { src: "chat/chat_pop.png", id: "chat_bg" },
    { src: "chat/chat_circle.png", id: "chat_circle" },
         
     { src: "popup/transfer_input.png", id: "transfer_input" },
    { src: "popup/transfer_popup.png", id: "transfer_popup" },
       
     { src: "popup/game_icon.png", id: "lgame_icon" },
     { src: "popup/round_icon.png", id: "lround_icon" },
     { src: "popup/loader_bg.jpg", id: "loader_bg" },
    
 
    //{ src: "popup/BG_2.png", id: "pbg" },
 
      { src: "popup/give_up_bg.png", id: "give_up_bg" },
      { src: "popup/win_tournament.png", id: "win_tournament" },

    { src: "popup/win_popup.png", id: "lose_popup" },
    { src: "popup/win_popup.png", id: "win_popup" },
    { src: "popup/achievement_popup.png", id: "achievement_popup" },
    { src: "popup/achievement_star.png", id: "achievement_star" },

    { src: "popup/close_icon.png", id: "pclose" },
     { src: "popup/close_icon_h.png", id: "pcloseh" },
    { src: "popup/wait_for_op.png", id: "pwait" },

    { src: "popup/button_red.png", id: "button_red" },
    { src: "popup/button_red_h.png", id: "button_redh" },

   { src: "popup/button_yellow.png", id: "button_yellow" },
    { src: "popup/button_yellow_h.png", id: "button_yellowh" },

   { src: "popup/button_grey.png", id: "button_grey" },
    { src: "popup/button_grey_h.png", id: "button_greyh" },
           { src: "popup/raise_popup.png", id: "raise_popup" }, 
           { src: "popup/button_submit.png", id: "button_submit" },
           { src: "popup/button_submit_h.png", id: "button_submit_h" },


            // 
           { src: "popup/mobile_roll.png", id: "mobile_roll" },
           { src: "popup/mobile_submit.png", id: "mobile_submit" },
           { src: "popup/mobile_undo.png", id: "mobile_undo" },  
           { src: "popup/mobile_roll.png", id: "mobile_rollh" },
           { src: "popup/mobile_submit.png", id: "mobile_submith" },
           { src: "popup/mobile_undo.png", id: "mobile_undoh" },  
           { src: "popup/rotate_icon.png", id: "rotate_icon" },  
            // 


 //board

  
    { src: "popup/connection_lost.png", id: "connection_lost" },
 
    { src: "popup/connection.png", id: "connection" },
    { src: "popup/win_sign.png", id: "pwin" },
      { src: "popup/lose_sign.png", id: "plose" },
    { src: "popup/eroricon.png", id: "error" },
    { src: "popup/doublecheck_bg.png", id: "doublecheck_bg" },
    { src: "popup/yesno_bg.png", id: "yesno_bg" } 


  
 ];

 var history_manifest = [
    { src: "history/player/backward.png", id: "backward" },
    { src: "history/player/backward_h.png", id: "backwardh" },
    { src: "history/player/forward.png", id: "forward" },
    { src: "history/player/forward_h.png", id: "forwardh" },
    { src: "history/player/pause.png", id: "pause" },
    { src: "history/player/pause_h.png", id: "pauseh" },
    { src: "history/player/play.png", id: "play" },
    { src: "history/player/play_h.png", id: "playh" },
    { src: "history/player/player_bg.png", id: "player_bg" },
    { src: "history/player/slider_circle.png", id: "slider_circle" },

    //round
      { src: "history/round/next_round.png", id: "next_round" },
      { src: "history/round/next_round_h.png", id: "next_roundh" },
      { src: "history/round/prev_round.png", id: "prev_round" },
      { src: "history/round/prev_round_h.png", id: "prev_roundh" },
      { src: "history/round/round_num_bg.png", id: "round_num_bg" },
 ]

 var soundManifest = 
      [
        { id: "dice1", src: "dice1.mp3" },
        { id: "dice2", src: "dice2.mp3" },
        { id: "chip", src: "chip.mp3" },
        { id: "beep", src: "beep.mp3" } 
      ]
 

 var calibri_font = " Arial";
 var calibri_bold_font = "Arail"; 

  var mtavruli_font = " Mtavruli";
  var nusxuri_font = " Nusxuri";
  var calibri_font = " Calibri";
  var arial_font   = " Arial";

 var yellow_color = "#ffcc00";
 var grey_color = "#a3adaf";
 var blue_color = "#8dcedf";
 var black_color = "#000000";
 var white_color = "#ffffff";
 var light_blue_color = "#d2dbd9"
 var red_color = "#f22323";
 var panel_color = "#cccccc";
