 
function GetReasonText(trId) {

    var text = [""];
    switch (trId) {

        case 1:
            return "op_removed_stones";
        case 2:
            return "time_run";
        case 3:
            return "player_surrended";
        case 4:
            return "player_surrended";
        case 5:
            return "game_canceled";
             
        default :
            return "game_finish";
    } 
}

function GetFontByGame() {

    switch (game.lang) {
        case 0:
              return "";
            break;
        case 1:
              return "";
            break;
        case 2:
              return "Arial";
            break;

    }
}

function translate()
{ 
   try
   {  
         game.translate(); 
    	 game.popup.translate();
   }
   catch(ex)
   { 
       console.log(ex);
   }
}

function GetErrorText(trId) {
 
    var text = [""];
    switch (trId) { 

        case 0:
            text = "connection_lost";   
            break;
        case 1:
            text = "new_active_session";
            break;

        case 2:
            text = "insufficient_balance";
            break;

        case 3:
            text = "opponent_left_table";
            break;
        case 4:
            text = "cant_authenticate";
            break;
        case 5:
            text = "profile_is_blocked";
            break;
        case 6:
            text = "opponent_left_table";
            break;
        case 7:
            text = "incorrect_move";
            break;
        case 8:
            text = "incorrect_bet";
            break;


        case 9:
            text = "insufficient_coin";
            break;

        case 10:
            text = "max_rank_info";
            break;

        case 11:
            text = "opponent_is_refused";
            break;

        case 13:
            text = "not_enough_game_count";
            break;
       
        case 50:
            text = "room_is_full";
            break;


        case 400:
            text = "room_is_full";
            break;


        case 500:
            text = "room_doesnt_exists";
            break;

        case 550:
            game.engine.incorrectPassword();
            break;

        case 600:
            text = "password_size";
            break;

        case 601:
            game.engine.incorrectPassword();
            break;
        case 602:
            text = "password_size";
            break;

        case 650:
            text = "room_is_full";
            break;

        case 690:
            text = "to_many_room_created";
            break;
        case 700:
            text = "problem_occured";
            break;

        case 900:
            text = "time_out";
            break;
        case 2000:
            text = "tournament_is_full";
            break;
        case 2001:
            text = "tournament_has_started";
            break;
        case 2002:
            text = "insufficient_players";
            break;
        case 2003:
            text = "you_left_tournament";
            break;
        case 2004:
            text = "already_registered";
            break;
        case 2005:
            text = "tournament_cant_found";
            break;
        case 2006:
            text = "tournament_canceled";
            break;

        case 2008:
            text = "user_must_verify";
            break;
            
        case 2501:
            text = "stock_is_empty";
            break;
        case 2503:
            text = "incorrect_count_error";
            break;

        default:
            text = "problem_occured";
            break;


    }

    return text;


}



function GetGameType(intId) {
    switch (intId) {
        case 1:
            return "european"; 
        case 2:
            return "georgian"; 
        case 3:
            return "long_type"; 
        case 4:
            return "hyper";
        case 5:
            return "khachapuri";
        case 6:
            return "eureka";  
        case 7:
            return "blitz";

        default:
            return "long_type";
    }
}


function GetGameSpeed(intId) {
    switch (intId) {
        case 1:
            return GetTranslatedValue("fast");
        case 2:
            return GetTranslatedValue("normal");  
        default:
            return GetTranslatedValue("slow");
    }
}
