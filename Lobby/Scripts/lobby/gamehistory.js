﻿

function drawHistoryRows(event) {
    $("#game-history-container").empty();
    var rows = event.rows;
    for (var i = 0; i < rows.length; i++) {
        drawHistoryRow(rows[i]);
    }

}

function drawHistoryRow(row) {

    var winText =  GetTranslatedValue("lose")
    var winColor = "redColor";
    var winKey = "lose";

    if (row.winner) {
        winKey = "winning";
        winText = GetTranslatedValue("winning")
        winColor = "greenColor";
    }

    var tr = $("<tr></tr>");
    tr.addClass("history_row");
    tr.attr("id", "history_" + row.game_id);
    tr.data("type", row.winner);
    tr.data("bet", row.total_bet/2);
    var statusTd = $("<td></td>");
    statusTd.addClass("tdsize-1");

    var statusSpan = $("<span></span>");
    statusSpan.addClass("statuses");
    statusSpan.addClass(winColor);

    var statusI = $("<i></i>");
    statusI.addClass("fa fa-circle");
    statusSpan.append(statusI);  

    
    var statusfont = $("<font></font>");
    statusfont.text(row.game_id);
    statusTd.append(statusSpan);
    statusTd.append(statusfont);

    var timeTd = $("<td></td>");
    timeTd.addClass("tdsize-14");
   
    timeTd.text(row.time.replace("T", " "));
                                   
    
    var gameTypeTd =  $("<td></td>");
    gameTypeTd.addClass("tdsize-13 translator");
    var gameType = GetGameType(row.type); 
    gameTypeTd.data("translate", gameType);
    gameTypeTd.text(GetTranslatedValue(gameType));



    var opNameTd =  $("<td></td>");
    opNameTd.text(row.op_name);

    var betTd = $("<td></td>");
    betTd.addClass("tdsize-5");
    betTd.html(parseFloat(row.total_bet / 2).toFixed(2) + " <font class='GEL'>b</font>");

    var scoreTd =  $("<td></td>");
    scoreTd.addClass("tdsize-13");
    scoreTd.text(row.my_score + " : " + row.op_score);

    var winTypeTd =  $("<td></td>");
    winTypeTd.addClass("tdsize-15");
    var winTypeA = $('<a  data-placement="left" title= ""></a>');
    winTypeA.prop("title", GetTranslatedValue(GetReasonText(row.win_type)));
    winTypeA.addClass("translator_title");
    winTypeA.data("translate", GetReasonText(row.win_type))
    winTypeA.data("toggle", "tooltip");

    
  var winTypeI = $("<i></i>");
  winTypeI.addClass("fa fa-info-circle");
  winTypeA.append(winTypeI);
  winTypeTd.append(winTypeA);
 
                                    
                                

    var winTd =  $("<td></td>");
    winTd.addClass("tdsize-13");
    var winSpan = $("<span></span>");
    winSpan.addClass(winColor);
    winTd.append(winSpan);
    winSpan.addClass("translator");
    winSpan.data("translate",winKey);
    winSpan.text(winText);

    var winPrizeTd = $("<td></td>");
    winPrizeTd.addClass("tdsize-13");
    var prizeSpan = $("<span></span>");
    prizeSpan.addClass(winColor);
    winPrizeTd.append(prizeSpan);
    prizeSpan.html(parseFloat(row.total_bet - row.rake).toFixed(2) + " <font class='GEL'>b</font>");


    var watchTd =  $("<td></td>");
    watchTd.addClass("tdsize-8 text-right");
    var watchButton = $('<button> <i class="fa fa-play"></i></button>');
    watchButton.data("id", row.game_id);
    watchButton.addClass("btn btn-default btn-circle play-history");
    watchTd.append(watchButton); 
 
     

    tr.append(statusTd);
    tr.append(timeTd);     
    tr.append(opNameTd);
    tr.append(gameTypeTd);
    tr.append(betTd);
    tr.append(scoreTd);
    tr.append(winTd);
    tr.append(winPrizeTd);
    tr.append(winTypeTd);    
    tr.append(watchTd);
                         
    $("#game-history-container").append(tr);
}

 

function historyfilterFunction() {

    var maxbet = parseFloat($("#history-max-bet").text());
    var minbet = parseFloat($("#history-min-bet").text());
 
    var type = $("#history-type-value").data("value");




    $(".history_row").each(function ( ) {

        var id = $(this).attr("id"); 
        var bet = $(this).data("bet");
        var tp = $(this).data("type");  
 
 
        var canBet = (bet >= minbet) && bet <= maxbet;
        var canType = (type == tp) || (type == -1);
     
    

        if  (canBet && canType) {
           
            $("#" + id).css('display', 'table-row');

        } else {
            console.log("lose " +id);
            $("#" + id).css('display', 'none');
        }
    });

     

}

 