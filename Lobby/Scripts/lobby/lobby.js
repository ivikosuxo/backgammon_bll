﻿$(document).ready(function (e) {

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    function closeMobileMenu() {
        $('.show_main_menu.c-hamburger.c-hamburger--htx.is-active').trigger('click');
    }

    $(".show_rules_dropdown").click(function () {
        $(this).parent().find(".dropdown-menu").toggle();
        $(this).toggleClass('active');
    });

    $(".lobby-tournirs").hide();

    $(".rules_button").click(function () {

        var langString = GetLangString();
        window.open("http://nardi.ge/" + langString + "/Home/Rules", "rules", "width=1100, height=715");  
    });
    
    $(".rules_button_mobile").click(function () {
        
        var langString = GetLangString();

        window.location.href = "http://nardi.ge/" + langString + "/Home/Rules";
    });

    $(".logout_mobile").click(function () {

        var langString = GetLangString(); 
        window.location.href = "http://nardi.ge/" + langString + "/"; 
    });

    function GetLangString() { 
        var langs = parseInt(game.lang);
        switch (langs)
        {
            case 0:
                return "ka-GE";
            case 1:
                return "en-US";
            case 2:
                return "ru-RU";

            default:
                return "ka-GE";
        }
    }

    $(".lb-tabs").click(function () {
        if ($(this).hasClass('active')) { }
        else if ($(this).hasClass('tab1')) {
            $(".lb-tabs").removeClass('active');
            $(this).addClass('active');
            $(".lobby-tables").fadeIn(100);
            $(".lobby-tournirs").hide();
        } else {
            $(".lb-tabs").removeClass('active');
            $(this).addClass('active');
            $(".lobby-tables").hide();
            $(".lobby-tournirs").fadeIn(100);
        }
    });

    $('.logouticon').click(function () {
        window.close();

    });

    var $newTableAskModal = $('.newTableAskModal');

    $newTableAskModal.find('.startDateAskModalBtn').on('click', function () {

        var id = $newTableAskModal.find('.id').val(); 
        if (id) {
            ConnectRoom(id);
        }

        $newTableAskModal.modal('hide');
    });

    $(document).on('click', '.redirect', function () {
        
        var $this = $(this);
        var id = $this.data("value"); 
        var hasDoubles = $("#"+id).data('with_doubles');
       

        if (!hasDoubles && $this.parents('.selnardi').data('prior') != 0) {
            $newTableAskModal.find('.id').val(id);
            $newTableAskModal.modal('show');
        } else {
            if (id) {
                ConnectRoom(id);
            }
        }
    });

    $(document).on('dblclick', '.selnardi', function () {

        var $this = $(this);
        var hasDoubles = $this.data('with_doubles');
 
        var id = $this.attr("id");
        var btn = $("#" + id).find(".redirect1");

        if (btn.prop("disabled")) {
            return;
        } 

        if (!hasDoubles && $this.data('prior') != 0) {
            $newTableAskModal.find('.id').val(id);
            $newTableAskModal.modal('show');
        } else {
            if (id) {
                ConnectRoom(id);
            }
        }
    });

    $(document).on('click', '.selnardi', function () {
        if ($(this).hasClass('nardiact')) {
        } else {
            $(".selnardi").removeClass('nardiact');
            $(this).addClass('nardiact');
        }
        var id = $(this).attr("id");
        if (id) {
            activateRoom(id);

        };

    });
     

    //filter history 

    $(document).on('click', '#history-type-dropdown a', function () {

        $("#history-type-value").text($(this).text());
        $("#history-type-value").data("value", $(this).data("value"));
        $("#history-type-value").data("translate", $(this).data("translate"));

        historyfilterFunction();
    });


    $(document).on('click', '#history-min-dropdown a', function () {

        $("#history-min-bet").text($(this).text());
        $("#history-min-bet").data("value", parseFloat($(this).text()));
        
        historyfilterFunction();
    });


    $(document).on('click', '#history-max-dropdown a', function () {

        $("#history-max-bet").text($(this).text());
        $("#history-max-bet").data("value", parseFloat($(this).text()))      
      
        historyfilterFunction();
    });



        //filter  
    $(document).on('click', '.filter-speed a', function () {
        $("#filter-speed").text($(this).text());
        $("#filter-speed").data("value", $(this).data("value"));
        $("#filter-speed").data("translate", $(this).data("translate"));
        filterFunction();

    });



    $(document).on('click', '.filter-type a', function () {

        $("#filter-type").text($(this).text());
        $("#filter-type").data("value", $(this).data("value"));
        $("#filter-type").data("translate", $(this).data("translate"));
     
        filterFunction();
    });


    $(document).on('click', '#bet-check a', function () {

        $('#bet-check-value').text(($(this).data("value")));
      
        filterFunction();
        // showBets();
    });


    $(document).on('click', '#bet-check-max a', function () {
     
        $('#bet-check-max-value').text(($(this).data("value")));
        filterFunction();
        // showBets();
    });



    //  createRoom events

    $(document).on('click', '.newtable', function () {

        if (lobby.can_create_room) {
            $("#create-room-reason").text(lobby.can_create_room_message); 
            $("#newTableModal").modal();
        } else {
            $("#system-error").modal();
        } 

        $(".rules-dropdown").hide();
    });


    $(document).on('click', '.dropdown-toggle', function () {

        $(".rules-dropdown").hide();
    });

    $(document).on('click', '#close-reg-modal', function () {


        $("#TurnRegModal").modal('hide');
    });


    $(document).on('click', '#close-unreg-modal', function () {


        $("#TurnLeaveModal").modal('hide');
    });


    $(document).on('click', '.gametype-dropdown a', function () {
        var geo = $(this).data("value");
        $("#game-type").data('value', geo);
        $("#game-type").text($(this).text());
        $(".rules-dropdown").hide();
        $("#max-bet").text(parseFloat($("#min-bet").val()).toFixed(2)); 
        $("#max-bet").data("multiply",1); 
        if (geo != 1) {
            $(".show_rules_dropdown").prop("disabled", true);
            $(".georgian-maxbet").show();
            $(".european-maxbet").hide();
        } else {
            $(".show_rules_dropdown").prop("disabled", false);
            $(".georgian-maxbet").hide();
            $(".european-maxbet").show();
        }
    });


    $(document).on('click', '.points-dropdown a', function () {
        var value = $(this).text(); 
        $("#points-value").text(value); 
        $("#max-bet").text(parseFloat($("#min-bet").val()).toFixed(2)); 
        $("#max-bet").data("multiply", 1); 
        $(".rules-dropdown").hide(); 

        var val = parseFloat($("#min-bet").val());
        if (parseInt($("#points-value").text()) > 1 && val<0.5 ) {
            $("#min-bet").val(parseFloat(1.00).toFixed(2) + " ₾ " + GetTranslatedValue("from_post"));
            changeMultiplier(parseFloat(1.00));
        }
    });


    $("#min-bet").on("focus", function () {  
        $("#min-bet").val("");
    });

    $("#min-bet").on("focusout", function () {
        var val = 1.00;
        try {
            val = parseFloat($("#min-bet").val()).toFixed(2);
            if (isNaN(val)) {
                throw new DOMException();
            }
            if (val < 0.05) {
                val = 0.05;
            } else if (val > 10000)
            {
                val = 10000;
            }

            if (val < 0.5) {
                $("#points-value").text(1);
                $(".point_dropdown_button").prop("disabled", true);
                $(".maxbet_dropdown_button").prop("disabled", true)
            } else {
                $(".point_dropdown_button").prop("disabled", false);
                $(".maxbet_dropdown_button").prop("disabled", false)
            }
        
        } catch (ex){
            val = 1.00;
        }
        $("#min-bet").val(parseFloat(val).toFixed(2) + " ₾ " + GetTranslatedValue("from_post"));
        changeMultiplier(parseFloat(val));
    });

    $(document).on('click', '.flag_drop_down li', function () { 
     
        game.lang = parseInt($(this).data("value"));
        $("#lang_flag").attr("src", $("#flag"+game.lang).attr("src"));
        translateLobby();
        setCookie("lang", game.lang, 10);
    });

    

    $(document).on('click', '.minbet-dropdown a', function () {

        var val = $(this).text();  

        //$("#points-value").text(1);
        $("#max-bet").text(val);
        $("#max-bet").data("multiply",1); 
        $("#min-bet").val($(this).text() + " ₾"); 

        if (parseFloat(val) < 0.5) {
            $("#points-value").text(1);
            $(".point_dropdown_button").prop("disabled", true);
            $(".maxbet_dropdown_button").prop("disabled", true)
        } else {
            $(".point_dropdown_button").prop("disabled", false)
            $(".maxbet_dropdown_button").prop("disabled", false)
        }

        changeMultiplier(parseFloat(val));

    });

    function changeMultiplier(val) {
        $("#max-bet").data("multiply", 1); 
        $("#max-bet").text(parseFloat(val ).toFixed(2));
        $(".multiplier").each(function () {
            var multiplier = $(this).data("multiply");
            $(this).text( parseFloat(val * multiplier).toFixed(2));

        }); 
    } 
 

    $(document).on('click', '.maxbet-dropdown a', function () {
        var data = $(this).find(".multiplier");
        var val = data.text();
        var multiplier = data.data("multiply"); 
        var val1 = parseFloat($("#min-bet").val());

        if (multiplier>1) {
            $("#points-value").text(1);
        }

        $("#max-bet").text(val);
        $("#max-bet").data("multiply", multiplier);

    });

    $(document).on('focus', '#table-psw', function () {
        $(".rules-dropdown").hide();
    });


    $(document).on('click', '.speed-dropdown a', function () { 
        var $this = $(this)
        $("#speed-select").text($this.text());
        $("#speed-select").data("value", $this.data("value"));

    });

    $(document).on('click', '.doubles-dropdown a', function () {
         
        var $this = $(this);
        $("#doubles-select").text($this.text());
        $("#doubles-select").data("translate", $this.data("translate"));
        $("#doubles-select").data("value", $this.data("value"));

    });


    $(document).on('click', '.password-dropdown a', function () {

        var $this = $(this);
        var val = $this.data("value");
        var text = $this.text();

        var $passwordSelect = $("#password-select");

        $passwordSelect.text(text);
        $passwordSelect.data("value", val);

        var $passwordContainer = $('.passwordContainer');

        if (val == 1) {
            $passwordContainer.hide();
        } else {
            $passwordContainer.show();
        }
    });



    $(document).on('click', '#tournament_tab', function () {


        $(".achievement-container").hide();
        $(".game-container").hide();
        $(".tournament-container").show();
        $(".game-history-container").hide();
        $("#tournament_info_panel").hide();


        $("#game_tab").removeClass("active");
        $("#achievement_tab").removeClass("active");
        $("#tournament_tab").addClass("active");
        $("#history-button").data("active", false);
    });

    $(document).on('click', '.tournament_detail', function () {
        var tourId = $(this).data("id");
        $(".tournament-container").hide();
        $(".mainbody2").hide();
        $(".top_menu").hide(); 
        $("#tournament_info_panel").show();
        GetTournamentDetails(tourId);
    });

    $(document).on('click', '.close_details', function () {
       
        $(".tournament-container").show();
        $("#tournament_info_panel").hide();
        $(".mainbody2").show();
        $(".top_menu").show(); 
    });
    $(document).on('click', '#draw_table_tournament', function () {
        $(".draw_arrows").show();    
    });
    $(document).on('click', '#list_table_tournament', function () {
        $(".draw_arrows").hide();   
    });
    
	$(document).on('click', '#game_tab', function () {

	    $(".achievement-container").hide();
	    $(".game-container").show();
	    $(".tournament-container").hide();
	    $(".game-history-container").hide();
        $("#tournament_info_panel").hide();

	    $("#game_tab").addClass("active");
	    $("#achievement_tab").removeClass("active");
        $("#tournament_tab").removeClass("active");
        $("#history-button").data("active", false);
	});


	$(document).on('click', '#achievement_tab', function () {

	    $(".achievement-container").show();
	    $(".game-container").hide();
	    $(".tournament-container").hide();
        $(".game-history-container").hide();
        $("#tournament_info_panel").hide();

	    $("#game_tab").removeClass("active");
	    $("#achievement_tab").addClass("active");
	    $("#tournament_tab").removeClass("active");
        $("#history-button").data("active", false);
        Send("achievement_info", {});

        closeMobileMenu();
	});
    
    $(document).on('click', '#history-button', function () {

        var active = $("#history-button").data("active");

        if (active) {
         
            $("#history-button").data("active", false);

            $(".achievement-container").hide();
            $(".game-container").show();
            $(".tournament-container").hide();
            $(".game-history-container").hide();
            $("#tournament_info_panel").hide();


            $("#game_tab").addClass("active");
            $("#achievement_tab").removeClass("active");
            $("#tournament_tab").removeClass("active");

        } else { 

            var obj = {};
            obj.token = getQueryVariable("token");
            Send("game_lobby_history", obj);

            $("#history-button").data("active", true);
            $(".achievement-container").hide();
            $(".game-container").hide();
            $(".tournament-container").hide();
            $(".game-history-container").show();

            $("#game_tab").removeClass("active");
            $("#achievement_tab").removeClass("active");
            $("#tournament_tab").removeClass("active");
        }

        closeMobileMenu();
	});

	$(document).on('click', '.play-history', function () {
	    var game_id = $(this).data("id");
	    watchHistory(game_id);

	});

    registerTournamentEvents();
    shopEvents();

	conn();
    try {
        $('.scroller').enscroll();
    } catch (ex) {
        console.log(ex);
    }
 
   
});



function filterFunction() {

    var maxbet = parseFloat($("#bet-check-max-value").text());
    var minbet = parseFloat($("#bet-check-value").text());
    var speed = $("#filter-speed").data("value");
    var type = $("#filter-type").data("value");




    $(".selnardi").each(function (index) {

        var id = $(this).attr("ID");
        var spd = $(this).data("speed");
        var bet = $(this).data("amount"); 
        var tp = $(this).data("type"); 


        var canSpeed = (speed == "0") || (speed == spd);
        var canBet = (bet >= minbet) && bet <= maxbet;
        var canType = (type == tp) || (type == 0);

         

        if (canSpeed && canBet && canType) {
 
            $("#" + id).css('display', 'table-row');

        } else {
 
            $("#" + id).css('display', 'none');
        }
    });




}

function sortPlayers() {
    var tables = [];
    $(".selnardi").each(function (index) {
        var obj = {};
        obj.ID = $(this).attr("ID");
        obj.POINTS = parseInt($(this).data("players_count"));
        tables.push(obj);
    });

    tables.sort(function (a, b) { return a.POINTS - b.POINTS; });

    if (playersSort) {
        for (var i = 0; i < tables.length; i++) {
            $("#room-container").prepend($("#" + tables[i].ID));
        }
    } else {
        for (var i = 0; i < tables.length; i++) {
            $("#room-container").append($("#" + tables[i].ID));
        }
    }
    return tables;
}



 



function sortByPriority() {
    var tables = [];

    $(".selnardi").each(function (index) { 
        var obj = {};
        obj.id = $(this).attr("ID");
        obj.prior = ( 4 - parseInt($(this).data("prior"))) * 10000 + parseFloat($(this).data("amount"));
    
        tables.push(obj);
    });

    tables.sort(function (a, b) {

        if (a.prior == b.prior) {
            return a.id - b.id;
        }

        return a.prior - b.prior;
    }); 
 
    for (var i = 0; i < tables.length; i++) {

        $("#room-container").prepend($("#" + tables[i].id));
    } 

  
    $("#room-container").prepend($("#mobile-row-filter"));
     return tables;
}




function watchHistory(gameid) {
 
    var token = getQueryVariable('token');
    var url = history_link + token + "&game_id=" + gameid;
    OpenHistoryUrl(url, gameid);
 
}

function changeAvt(id) {
    $(".avatars").removeClass('active');
    $("#avt_" + id).addClass('active');

    $("#avatar-image").attr("src", $("#avt_" + id).find("img").attr("src"));
    
}


//setInterval(sortByPriority, 10000);