﻿
function drawAchievements(achievements, rank)
{
    $("#achievement-table").empty();
    for (var i = 0; i < achievements.length; i++) {
        drawAchievement(achievements[i], rank);
    }
    //translateLobby();

}

function drawAchievement(achievement,rank)
{
    var mainTr = $("<tr></tr>");
    mainTr.attr("id", "achievement-" + achievement.id);
    mainTr.addClass("achievement_sort");
    
    //name
    var td = $("<td></td>");
    var span1 = $("<span></span>");
    var font = $("<font></font>");
    font.addClass("translate_title");
    font.data("name0", achievement.description_ge);
    font.data("name2", achievement.description_ru);
    font.data("name1", achievement.description);
    font.text(GetNameByLang(achievement.description_ge, achievement.description, achievement.description_ru))
    td.addClass("msnsize-1");
 
    span1.addClass("mission_name translate_title");
    span1.data("name0", achievement.name_ge);
    span1.data("name2", achievement.name_ru);
    span1.data("name1", achievement.name);
    span1.text(GetNameByLang(achievement.name_ge, achievement.name, achievement.name_ru));

    td.append(span1);
    td.append(font);
    // rank
   var td1 = $("<td></td>");
   td1.addClass("msnsize-2");
   var span2 = $("<span></span>");
    span2.addClass("need_rank_star");
    span2.text(achievement.rank);
    td1.append(span2);
    mainTr.data("rank", achievement.rank);
    mainTr.data("sort", achievement.rank * 10);

    // prize
    var  td2 = $("<td></td>");
    td2.addClass("msnsize-3");
    td2.text(achievement.coin_count);

    //progress bar
   var td3 = $("<td></td>");
    td3.addClass("msnsize-4 progress_bar_td");
   

    var div1 = $("<div></div>");
    div1.addClass("progressdiv1");

    var div2 = $("<div></div>");
    div2.addClass("progress_bar");
    div2.css("width", (0/ achievement.count)*100+"%")
    var div3 = $("<div></div>");
    div3.addClass("progress_text");
    div3.data("count", achievement.count);
    div3.text("0 / " + achievement.count);

    var text = GetTranslatedValue("not_enough_status")
 
    if (achievement.rank > rank) {
        mainTr.addClass("disabled_mission");
        td3.addClass("translator");
        td3.data("translate","not_enough_status");
        td3.append(text);
    } else {
        div1.append(div2);
        div1.append(div3);
        td3.append(div1);
    }

   
    //add childs
    mainTr.append(td);
    mainTr.append(td1);
    mainTr.append(td2);
    mainTr.append(td3);
    $("#achievement-table").append(mainTr);
}

function updateAchievements(playerAchieves) {
    for (var i = 0; i < playerAchieves.length; i++) {
        updateAchievement(playerAchieves[i]);
    }
    var tables = [];

    $(".achievement_sort").each(function (index) {
        var obj = {};
        obj.id = $(this).attr("id");
        obj.rank = parseInt($(this).data("sort"));
        tables.push(obj);
    });

    tables.sort(function (a, b) { return a.rank - b.rank; });
     
    for (var i = 0; i < tables.length; i++) {
        var obj = tables[i];
        $("#achievement-table").append($("#" + obj.id));
    }

    return tables;
}

function updateAchievement(playerAchieve) {
    var complete = playerAchieve.complete;
    if (complete) {     
        var text =  GetTranslatedValue("complete")
        var progressBartd = $("#achievement-" + playerAchieve.id).find(".progress_bar_td"); 
        progressBartd.empty();
        progressBartd.addClass("translator");
        progressBartd.data("translate","complete");
        progressBartd.append(text);
        $("#achievement-" + playerAchieve.id).addClass("disabled_mission");
        var rank = $("#achievement-" + playerAchieve.id).data("sort")
        $("#achievement-" + playerAchieve.id).data("sort", rank+5);
    } else {
        var progressBar = $("#achievement-" + playerAchieve.id).find(".progress_bar");        
        progressBar.css("width", (playerAchieve.count / playerAchieve.full_count) * 100 + "%");
        var progressText = $("#achievement-" + playerAchieve.id).find(".progress_text");
        progressText.text(playerAchieve.count +" / "+playerAchieve.full_count);
    }


}


function InitializeRankPopup(rank) {
    var start = 0;
    var maxRank = lobby.point_status.length; 
    if (rank < 3) {
        start = 0;
    } else if ( rank >= maxRank - 2) {
        start = maxRank - 5;
    } else{
        start = rank - 2;
    }
     

    for (var i = 1; i <= 5; i++) {   
        $("#rank-count-" + i).text(i + start);
        $("#rank-" + i).removeClass("rank_done");
        $("#rank-" + i).removeClass("rank_active");

        if (rank >= i + start) {
            $("#rank-" + i).addClass("rank_done");
        } else if (rank + 1 == i + start) {
            $("#rank-" + i).addClass("rank_active");
        } 
    }
 
    if (rank < maxRank) {
        $("#rank-increase-fee").text(lobby.point_status[rank-1]);
    } else {
       
        $("#rank-button").attr("disabled", 'disabled');
        $("#rank-button-text").addClass("translator");
        $("#rank-button-text").data("translate", "max_status")
        $("#rank-button-text").text(GetTranslatedValue("max_status"));  
    }

     
}