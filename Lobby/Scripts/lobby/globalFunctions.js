﻿var count = 0;
var popupWindow;
var popupWindow1;



var windowWidth = 1020;
var windwoHeight;
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
    windowWidth = 1000;

    windwoHeight = 710;
} else {
    windwoHeight = 733;
}

 

function getQueryVariable1(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return (false);
}


function getQueryVariable(variable)
{
    var result = getQueryVariable1(variable)
    if (result) {
        return result;
    }
    return $("#token").data("token");
}


function GetPlayerByID(plId) {
    if (game.player.player_id == plId) {
        return game.player;
    }
    if (game.opponent.player_id == plId) {
        return game.opponent;
    }

    return null;
}



function CloseWindow() {

    if (game.mobile) {
        window.location.href = mobile_url + getQueryVariable("token");
    } else {
        window.close();
    }
}
 

 

function translateLobby() {
    var langObj = GetLangObject(game.lang); 

    $(".translator").each(function () { 
        try { 
        var key = $(this).data("translate");
        var val = langObj[key];
  
        $(this).text(val);
        } catch (ex){
            console.log(ex);
        }
    }); 

  
    $(".translator_title").each(function () {
        
        try {
            var key = $(this).data("translate");
            var val = langObj[key]; 
            $(this).prop("title", val);
        } catch (ex) {
            console.log(ex);
        }
    }); 
     
    
    $(".translate_title").each(function () {  
        try {
 
            var text = $(this).data("name" + game.lang);  
            $(this).text(text);
        } catch (ex) {
            console.log(ex);
        }
    }); 

    

    $("#table-psw").attr("placeholder", langObj["password"]);
} 

function GetNameByLang(first,second,third) {

    if (game.lang==1) {
        return second;
    }
    if (game.lang == 2) {
        return third;
    }
   
    return first;
   
}


function GetTranslatedValue(key) {
    try {
        var langObj = GetLangObject(game.lang);
        return langObj[key].toUpperCase();
    } catch (ex){
        return key.toUpperCase();;
    }

}

function GetTranslate(key) {
    var langObj = GetLangObject(game.lang); 
    return langObj[key].toUpperCase(); 
}

function GetLangObject(langId) {
    var langs = parseInt(langId);

    switch (langs) { 

        case 0:
            return AppTranslations.ka_GE; 

        case 1:
            return AppTranslations.en_US; 

        case 2:
            return AppTranslations.ru_RU; 

        default:
            return AppTranslations.ka_GE;

    }
    return AppTranslations.ka_GE;
}

function firstJoinLobby() {
    try { 
        var lang = getQueryVariable1("lang") ||  getCookie("lang");;
        if (lang) {
            game.lang = lang;
        } else { 
           game.lang = 0;
        }
    } catch (ex){
        game.lang = 0;
    }
    translateLobby();
    $("#lang_flag").attr("src", $("#flag" + game.lang).attr("src"));

    setCookie("lang",game.lang,10);
}

function getCorrectFont(font) {
    if (game.lang == 2) {
        return nusxuri_font;
    }

    return font;

}

function getCorrectFontSize(fontSize) {

    if (game.lang == 2) {
        return (fontSize-3) +"px";
    }
    return fontSize + "px";

}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCoockieLang() {
    var lang = 0;
    try {
        lang = getCookie("lang");
        if (lang == "") {
            lang = 0;
        } else {
            lang = parseInt(lang);
        }

    } catch (ex){
        lang = 0;
    }
    return lang;
}