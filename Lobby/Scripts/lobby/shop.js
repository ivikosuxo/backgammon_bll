﻿function shopEvents() {

    $(document).on('click', ".open-buy-modal", function () {

        var id = $(this).data("id");
        $(".buy-item-result").hide();
        var text = $(this).find(".shop-item-name").text();
        var price = $(this).find(".shop-item-price").text();
        var src = $(this).find(".shop-item-url").attr("src");
        var stockCount = $(this).find(".shop-item-count").text();


        $("#buyItemModal").data("id", id);
        $("#buy-item-name").text(text);
        $("#buy_description").text(GetNameByLang($(this).data("name0"), $(this).data("name1"), $(this).data("name2")));

        $("#buy-product-count").data("count", stockCount);
        $("#buy-product-count").val(1);
        $("#buy-popup-quantity").text(stockCount);
        $("#buy-popup-image").attr("src", src);
        $("#buy-price").data("price", price);
        $("#buy-price").text(price);
        $("#buyItemModal").show(10);
    });

    $(document).on('click', ".closeItemsModal", function () {
        $("#buyItemModal").hide(10);
        $(".itemsHistoryModal").hide(10);
    });

    $(document).on('click', "#shop_button", function () {
        
        $("#shopModal").modal("show");
        Send("shop_info", {});
    });

    $(document).on('click', ".openHistoryModal", function () {
        Send("order_products", {});
        $(".itemsHistoryModal").show(10);
    });


    $(document).on('click', ".category_title", function () {
        var me = $(this);
       
        $("#category-list li").removeClass("active");
        me.parent().addClass("active");
       
        $("#selected_category").data("name0", me.data("name0"));
        $("#selected_category").data("name1", me.data("name1"));
        $("#selected_category").data("name2", me.data("name2"));
        $("#selected_category").data("id", me.data("id"));
        $("#selected_category").text(GetNameByLang(me.data("name0"), me.data("name1"), me.data("name2")))

      
        if (me.data("id") == -1) {
            $(".category").show();
        } else {
            $(".category").hide();
            $(".category_" + me.data("id")).show();
        }

    });

    $(document).on('click', ".buy-item-down", function () {

        var count = $("#buy-product-count").val();
        if (count > 1) {
            count = count - 1;
        }
        $("#buy-product-count").val(count);

        var pr = $("#buy-price").data("price");
        $("#buy-price").text(pr * count);
    });

    $(document).on('click', ".buy-item-up", function () {
        var stock_count = parseInt($("#buy-product-count").data("count"));
        var count = parseInt($("#buy-product-count").val());
        if (stock_count > count) {
            count = count + 1;
        }
        $("#buy-product-count").val(count);

        var pr = $("#buy-price").data("price");
        $("#buy-price").text(pr * count);
    });

    $(document).on('focusout', "#buy-product-count", function () {
        var stock_count = parseInt($("#buy-product-count").data("count"));
        var count = parseInt($("#buy-product-count").val());
        if (stock_count < count) {
            count = stock_count
            $("#buy-product-count").val(count);
        }

        var pr = $("#buy-price").data("price");
        $("#buy-price").text(pr * count);
    });

    $(document).on('click', "#buy-product-button", function () {
        BuyProduct();
    });


}


function CreateShopItems(items) {

    var $shopContainer = $("#shop-container");
    $shopContainer.empty();
    for (var i = 0; i < items.length; i++) {
        var resultDiv = CreateShopItem(items[i]);
        $shopContainer.append(resultDiv);
    }
}

function CreateShopItem(item) {


    var div = $("<div></div>");

    div.addClass("shop_blocks2 open-buy-modal")
        .data("id", item.id)
        .attr("id", "shop-" + item.id)
        .addClass("category category_" + item.category)
        .data("name0", item.description_geo)
        .data("name1", item.description)
        .data("name2", item.description_rus);


    var a = $("<a></a>")
        .addClass("shop_blocks3 openItemsModal")
        .data("id", item.id);

    var div1 = $("<div></div>");
    div1.addClass("shop_item_title  shop-item-name translate_title")
        .data("name0", item.name_geo)
        .data("name1", item.name)
        .data("name2", item.name_rus)
        .text(GetNameByLang(item.name_geo, item.name, item.name_rus));


    var div2 = $("<div></div>")
    div2.addClass("shop_item_image");

    var img = $('<img >');
    img.addClass("shop-item-url");
    img.attr('src', 'https://nardi.ge/images/' + item.img_url);

    var div3 = $("<div></div>");
    div3.addClass("shop_item_price");

    var div4 = $("<div></div>");
    div4.addClass("item_price1 translator")
        .data("translate", "coin")
        .text(GetTranslatedValue("coin"));


    var div5 = $("<div></div>");
    div5.addClass("item_price2 yellowColor shop-item-price");
    div5.text(item.coin_price);

    var span = $("<span></span>");
    span.addClass("item_light");
    span.text(' ');

    var div6 = $("<div></div>");
    div6.addClass("item_quantity");

    var span1 = $("<span></span>");
    span1.addClass("yellowColor shop-item-stock-count");
    span1.text(item.stock_count + "/");

    var span2 = $("<span></span>");
    span2.addClass("greyColor shop-item-count");
    span2.text(item.count);



    //   div6.append(span1);
    div6.append(span2);
    div3.append(div4);
    div3.append(div5);
    div3.append(span);
    div2.append(img);

    a.append(div1)
        .append(div2)
        .append(div3)
        .append(div6)
    div.append(a);

    return div;


}

function CreateCategories(items) {

    var $categoryList = $("#category-list");
    $categoryList.empty();

    var defaultLi = CreateCategoryDefaultItem();
    $categoryList.append(defaultLi);

    for (var i = 0; i < items.length; i++) {

        var li = CreateCategory(items[i]);
        $categoryList.append(li);
    }
}

function CreateCategory(item) {
    var li = $("<li></li>");
    var a = $("<a></a>");

    a.addClass("translate_title category_title")
        .data("name0", item.name_geo)
        .data("name1", item.name)
        .data("name2", item.name_rus)
        .data("id", item.id)
        .text(GetNameByLang(item.name_geo, item.name, item.name_rus));

    li.append(a);

    return li;
}

function CreateCategoryDefaultItem() {

    var li = $("<li></li>");
    var a = $("<a></a>");
    a.addClass("translator category_title") 
        .data("name0", "ყველა")
        .data("name1", "all")
        .data("name2",  "все")
    a.data("translate", "all")
    a.data("id", -1);
    a.text(GetTranslatedValue("all"));
    li.append(a);

    return li;
}