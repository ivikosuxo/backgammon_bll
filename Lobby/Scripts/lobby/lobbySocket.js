﻿var connection;
var game = {};
game.lang = 0;
var operationType = 0;
var activeRoomID = 0;
var popups = {};
var lobbyTP = 0;
var error = 0;
var lobby = {};
lobby.rank = 1;
lobby.create_room = false;
lobby.can_create_room = true;
 
var player_id = -1;
var player = {};

function conn() {

    firstJoinLobby();

    try {
        
        connection = $.connection(signalRUrl);

        connection.qs = { 'token': getQueryVariable("token") };
        connection.received(function (data) { 
            var dat = JSON.parse(data);
            onResponse(dat);
        });
        connection.start({ transport: ['webSockets'] }).fail(function () {

            connectionProblem(error);
        })

        connection.disconnected(function () {
      
                connectionProblem(error);
           
        }); 
         
          
    } catch (ex) { 
        connectionProblem(error);
    } 
   
}
function connectionProblem(id) {

    error = id;
    var text = GetTranslatedValue(GetErrorText(id));

    $("#error-popup").show();
    $("#error-text").text(text);


    connection.stop();

    $("#error-popup").click(function () {
        window.close();
    });

}

function onResponse(evt) { 
 
    var data = {};
 
    switch (evt.command) {

        case "connect":
            data = {};
            data.token = getQueryVariable("token");
            Send("login", data);
          //  firstJoinLobby();
            break;

        case "login":
            enterLobby();
            player = evt.player;
            player_id = player.player_id;
            $("#user_nick").text(player.username);
            $("#table-name").val(player.username);
            $("#user-balance").data("amount", player.balance);
            $("#user-balance").text(GetCurrentvalueFloat(player.balance));
            $("#user-rank").text(player.rank);
            lobby.rank = player.rank;

            setCoin(player.coin);
        
            changeAvt(player.avatar);
            data = {};
            data.room_id = 0;
            Send("join_lobby", data);

            ServerPing();
            break;

        case "error":
    
            if (evt.fatal) {
                error = evt.id;
                connection.stop();
            } else {
                $("#error-modal-message").text(GetTranslatedValue(GetErrorText(evt.id)));
                $("#system-error-modal").modal(); 
            } 
            break;



        case "creator_join":
            var room_id = evt.room_id;
            $("#" + room_id).show();
            break;

        case "rank_increase_error":

            var text = GetTranslatedValue(GetErrorText(evt.id));
            lobby.game_count = evt.game_count;
         
            text = text.replace("$", lobby.game_counts[lobby.rank-1]);
            text= text.replace("#", lobby.game_count);
            $("#error-modal-message").text(text);
            $("#system-error-modal").modal(); 
            break;

        case "active_connections":
            addTournamentRooms(evt.rooms);
            sortByPriority();
            break;

        case "rooms":
            drawRooms(evt.rooms);
            sortByPriority();
            break;

        case "rank_increase":
            $("#user-rank").text(evt.rank);
            lobby.rank = evt.rank;
            InitializeRankPopup(evt.rank);
            lobby.game_count = 0;
            break;

        case "enter_lobby":
            enterLobby(evt);
            //  lobby.statuses = evt.point_status 
            break;

        case "can_create_room":
            lobby.can_create_room = evt.can_create;
            lobby.can_create_room_message = evt.description;
            lobby.can_create_room_message_geo = evt.description_geo;
            lobby.can_create_room_message_rus = evt.description_rus;

        
            $("#create-room-reason").data("name1", evt.description);
            $("#create-room-reason").data("name0", evt.description_geo);
            $("#create-room-reason").data("name2", evt.description_rus);

            $("#create-room-reason").text(GetNameByLang(evt.description_geo, evt.description, evt.description_rus));

            if (!lobby.can_create_room) {
                $("#system-error").modal();
            }
            break;

        case "sit_on_room":
            playerSitOnTable(evt.room_id);
            $("#games-count").text($(".selnardi[data-playerCount=2]").length);
            sortByPriority();
            break;

        case "remove_room":
            deleteRoom(evt);
            $("#games-count").text($(".selnardi[data-playerCount=2]").length);
            break;

        case "lobby_info":
            LobbyInfo(evt);
       
            lobby.point_status = evt.status_points;
            lobby.game_counts = evt.status_game_count
            lobby.rank = evt.rank;
            InitializeRankPopup(evt.rank);
             
    
            Send("tournaments_info", {}); 
          
         
            break;

        case "tournament_room_create":
            tournamentPlay(evt);
            break;
        case "lobby_data":
             lobbyData(evt);
            break;


        case "room_info":
            RoomInfo(evt);
            break;

        case "update_coin_count":
            setCoinBalances(evt);
            break;

        case "update_balance":
            setCashierBalances(evt);
            break;

        case "join_room":
            playingRooms(evt);

            break;

        case "lobby_count_change":
            countChange(evt);
            sortByPriority();
            break;

        case "count_change":
            countChange(evt);
            sortByPriority();
            break;

        case "create_room":
            createRoom(evt);
            $("#games-count").text($(".selnardi[data-playerCount=2]").length);
            sortByPriority();
            break;

        case "create_result":
            createTableResult(evt);
            break;
        //coin


        // tournament  
        case "tournaments_register":
            if (evt.state === 1) {
                register(evt, "btn-danger");
            } else {
                tournamentStatusChange(evt);
            }
            GetTournamentData();
            break;
        case "tournaments_unregister":
            unregister(evt);
            GetTournamentData();
            break;

        case "tournaments_info":
            AddTournaments(evt);
           
            Send("active_connections", {});
           
            break;

        case "tournament_finish":
            tournamentStatusChange(evt);

            break;

        case "tournaments_players_count":
            tournamentPlayersCountChange(evt);
            break;

        case "tournament_start":
            GetTournamentData();
            break;

        case "tournament_status_change":
            tournamentStatusChange(evt);
            break;

        case "achievement_info":
            drawAchievements(evt.achievements, evt.rank);
            updateAchievements(evt.my_achievements);
            break;

        case "tournament_total_info":
            getTournamentInfo(evt);
            break;

        case "tournament_add":
            addTournament(evt);
            break;

        case "tournament_remove":
            $("#tour-" + evt.tournament_id).remove();
            break;
        //shop segment 


        case "shop_info":
            CreateShopItems(evt.items);
            CreateCategories(evt.category);
            break;

        case "add_product":
            CreateShopItem(evt);
            break;

        case "remove_product":
            $("#shop-" + evt.product_id).remove();
            break;

        case "order_products":
            addShopHistoryItems(evt.products);
            break;

        case "buy_product":
            $(".buy-item-result").hide();
            if (evt.error) {
                $(".buy-item-error-text").show();
                $("#buy-item-error-text").text(GetTranslatedValue(GetErrorText(evt.id))); 
            } else {
            //    $("#shop-" + evt.product_id).find(".shop-item-stock-count").text(evt.stock_count);
                $(".buy-item-success-text").show();
               // $("#buy-popup-quantity").text(evt.stock_count);
            }  
            break;

        case "game_history":
            drawHistoryRows(evt);
            break;

    }
}


function Send(command, obj) {


    obj.command = command;
    connection.send(JSON.stringify(obj));

}



function countChange(evt) {

    var count = parseInt(evt.count);

    var status = $("#" + evt.room_id).find(".statuses");
    $("#" + evt.room_id).data("players_count", count);
    $("#" + evt.room_id).attr("data-playerCount", count);
 

    if (count === 1) {
        status.removeClass("yellowColor");
        status.addClass("greenColor");

    } else {

        var isMe = $("#" + evt.room_id).data("creator");
        var btn = $("#" + evt.room_id).find(".redirect1");

        if (!isMe) {

            $("#" + evt.room_id).data("prior", 2);
            btn.removeClass("btn-success");
            btn.addClass("btn-danger translator");
            btn.data("translate", "ongoing");
            btn.text(GetTranslatedValue("ongoing"));
            status.removeClass("greenColor");
            status.addClass("redColor");
            btn.prop("disabled", true);
        } else {
            status.removeClass("greenColor");
            status.addClass("yellowColor");
            btn.prop("disabled", false);
        }
    }

    if (evt.room_id === activeRoomID) {
        //activateRoom(activeRoomID);
    };
    $("#games-count").text($(".selnardi[data-playerCount=2]").length);
}



function setCashierBalances(evt) { 

    var value = GetCurrentvalueFloat(evt.balance);
    $("#user-balance").text(value);
    $("#user-balance").data("amount", evt.balance);  
}

function setCoinBalances(evt) { 

    var value = evt.coins; 
    setCoin(value); 

}

function setCoin(coin)
{
    lobby.coin = coin;
    var value = coin;
    if (value > 100000) {
        value = parseInt(coin / 1000) + "K";
    } else { 

        if (coin==0) {
            value = 0;
        }else  if (coin < 1) {
            value = parseFloat(coin).toFixed(3);
        } else {
            value = parseInt(coin);
        } 
    }

    $("#coin-balance").text(value);
    $("#coin-balance").attr('title', coin);
    $("#coin-balance").data("amount", coin);
    $("#coin-balance-popup").text(value);
}

function getBalance() {

    var obj = {};
    Send("balance_check", obj);

}

var connRooms;

function playingRooms(evt) {

    connRooms = evt.rooms_info;

    setTimeout(sortRooms, 100);

}

function sortRooms() {

    for (var i = 0; i < connRooms.length; i++) {

        $("#room-container").prepend($("#" + connRooms[i]));
    }

}

function moneyTransfer() {



}

function BuyProduct() {
    var jsn = {};
    jsn.count = $("#buy-product-count").val();
    jsn.product_id = $("#buyItemModal").data("id");
    jsn.jsn = {};

    Send("buy_product", jsn);
}

function enterLobby(evt) {


    //   $("#avatar-image").attr("src", $("#avatar_" + evt.AVATAR).find("img").attr("src"));
    // $(".avatars").removeClass("active");
    //$("#avt_" + evt.AVATAR).addClass("active");
    //$("#avatar_" + evt.AVATAR).addClass("active");


    try {
        var tab = parseInt(getQueryVariable("tab"));
        if (tab) {
            switch (tab) {
                case 1:
                    $("#game_tab").click()
                    break;
                case 2:
                    $("#tournament_tab").click()
                    break;
                case 3:
                    $("#achievement_tab").click();
                    break
                case 4:
                     
                    $("#shop_button").click();
                    break

            }
        }

    } catch (ex) {

    }

}


function LobbyInfo(evt) {

    drawRooms(evt.rooms_info);
    lobbyData(evt);
   // drawAchievements(evt.achievements, evt.rank);
}

function lobbyData(evt) {
    $("#players-count").text(evt.players_count);
    $("#rooms-count").text(evt.rooms_count);
}

function RoomInfo(evt) {

    var spd;
    switch (evt.speed) {
        case 1:
            spd = "fast";
            break;
        case 2:
            spd = "normal";
            break;
        case 3:
            spd = "slow";
            break;

    }


    $("#active-table-id").text(evt.room_id);
    $("#active-amount").data("amount", evt.min_bet);


    $("#minimal-balance").data("amount", evt.max_bet);
    $("#minimal-balance").text(evt.max_bet);


    var value1 = GetCurrentvalue(evt.max_bet);
    $("#active-amount").text(evt.max_bet);
    $("#active-points").text(evt.points);
    $("#active-speed").addClass("translator");
    $("#active-speed").data("translate", spd)
    $("#active-speed").text(GetTranslatedValue(spd));
    if (evt.PSW) {
        $('#play-button').addClass("pwpass");
    } else {
        $('#play-button').removeClass("pwpass");
    }




    var jacob = evt.jacob;
    var davi = evt.davi;
    var crawford = evt.crawford;
    var beaver = evt.beaver;


    if (jacob) {
        $("#active-table-jacob").addClass("active");
    };

    if (beaver) {
        $("#active-table-beaver").addClass("active");

    };

    if (davi) {
        $("#active-table-davi").addClass("active");

    };
    if (crawford) {
        $("#active-table-crawford").addClass("active");
    };


}




function ClearRoomInfo() {

    $("#active-table-id").text("");
    $("#active-amount").text("");
    $("#active-points").text("");
    $("#active-speed").text("");





    $("#active-table-beaver").removeClass("active");
    $("#active-table-davi").removeClass("active");
    $("#active-table-jacob").removeClass("active");
    $("#active-table-crawford").removeClass("active");


}

var canCreateRoom = true;
function createtable() {
    $(".rules-dropdown").hide();
    $(".table-error-msg").hide();

    var data1 = {};
    data1.token = getQueryVariable("token");
    data1.name = $("#table-name").val();
    data1.speed = $("#speed-select").data("value");
    data1.max_bet = $("#max-bet").data("multiply");
    data1.max_point = $("#points-value").text();
    data1.chat_enabled = false;
    data1.type = $("#game-type").data('value');
    data1.double = $("#davi-check").prop('checked');
    data1.beaver = $("#beaver-check").prop('checked');
    data1.jacob = $("#jacob-check").prop('checked');
    data1.raise_count = $("#max-bet").data("multiply");

    data1.crawford = $("#crawford-check").prop('checked');
    data1.min_bet = parseFloat($("#min-bet").val()).toFixed(2);
    data1.with_double = $("#doubles-select").data("value");
    data1.is_protected = false;



    if ($("#table-psw").val().length > 4) {
        data1.is_protected = true;
    };
    data1.password = $("#table-psw").val();

    if (canCreateRoom) {
        canCreateRoom = false;    
    var request = $.ajax({
        url: create_game_url,
        method: "POST",
        async: false,
        data: JSON.stringify(data1),
        dataType: "json",

    }).done(function (msg) {
     
        $("#table-psw").val("");
        createTableResult(msg);
        canCreateRoom = true;
        });
    }
}




function drawRooms(list) {

    var isRooms = false;
    for (var i = 0; i < list.length; i++) {
        isRooms = true;
        if ($('#' + list[i].room_id).length === 0) {
            createRoom(list[i]);
        }
    }
    if (activeRoomID === 0 && isRooms) {
        $('.selnardi').removeClass('nardiact');
        $('#' + list[0].room_id).addClass('nardiact');
        activeRoomID = list[0].room_id;
        activateRoom(activeRoomID);
    }
    
    $("#games-count").text($(".selnardi[data-playerCount=2]").length);
    $("#lob_internet_loader_bg").remove();
}

function deleteRoom(evt) {
    $("#" + evt.room_id).remove();
    if (activeRoomID === evt.room_id) {
        var arr = $('.selnardi').toArray();
        if (arr.length > 0) {
            $('.nardiact').removeClass('nardiact');
            var roomId = $(arr[0]).attr('id');
            $("#" + roomId).addClass('nardiact');
            activateRoom(roomId);
        }
    }

    if (evt.tournament_id > 0) {
        var button = $("#tour_room" + evt.tournament_id);
        var room_id = button.data("room_id");
        if (room_id == evt.room_id) {
            var tournamenttr = $("#tour-" + evt.tournament_id);
            var mainSPan = tournamenttr.find(".register-td"); 
            mainSPan.find("span").show();
            mainSPan.find("div").show();
            button.hide();
        }

    }
}

function ConnectRoom(id) {

    activeRoomID = id;
    connectRoom();
}

function activateRoom(id) {
    ClearRoomInfo();
    activeRoomID = id;
    var obj = {};
    obj.room_id = id;

    Send("room_info", obj);


}

function connectRoom() {
    var token = getQueryVariable("token");

    if (activeRoomID !== 0) {
        if (popups["pop" + activeRoomID] && !popups["pop" + activeRoomID].closed) {
            popups["pop" + activeRoomID].focus();
        } else {

            var link_url = game_link + token + "&roomid=" + activeRoomID;
            OpenPopup(link_url, activeRoomID);
        }
    }

    $(".dialog-bg").hide();
    $(".dialog-box").hide();
}





function changecurr() {



}

function GetCurrentvalue(amount) {

    return parseInt(amount) + " " + $('#currency-active').text();
}

function GetCurrentvalueFloat(amount) {
    return parseInt((amount * Math.pow(10, 2)).toFixed(2)) / Math.pow(10, 2)

    // return parseFloat(amount / rate[activeCurrency]).toFixed(2);
}





function ChangeAvatar(avatarId) {
    var obj = {};
    obj.avatar_id = avatarId;
    Send("avatar_change", obj);
}



function updateTransactions(history) {
    $('#tr-history').html('');
    for (var i = 0; i < history.length; i++) {
        drawtransaction(history[i]);
    }

}

function drawtransaction(item) {
    var tp = "Withdraw";
    if (item.TYPE == 4) {
        tp = "Deposit";
    }

    var tr = $('<tr></tr>');
    tr.addClass("histnardi");
    var td = $('<td></td>');
    td.addClass("hdtd1");
    td.text(item.ID);
    tr.append(td);

    var td1 = $('<td></td>');
    td1.addClass("");
    td1.text(tp);
    tr.append(td1);

    var td2 = $('<td></td>');
    td2.addClass("hdtd2");
    td2.addClass("text-center");
    td2.text(item.AMOUNT);
    tr.append(td2);

    var td3 = $('<td></td>');
    td3.addClass("hdtd3");
    td3.addClass("text-center");
    td3.text(item.BALANCE);
    tr.append(td3);

    var td4 = $('<td></td>');
    td4.addClass("text-center hdtd4");
    td4.text(item.DATE.substring(0, 19));
    tr.append(td4);

    $('#tr-history').append(tr);


}





function createTableResult(evt) {

    if (evt.room_id > 0) {
        $(".modal-backdrop").hide();
        $("#newTableModal").modal('hide');
        openCreatedRoom(evt.room_id); 
    } else {
        $(".table-error-msg").text(GetTranslatedValue(GetErrorText(evt.id)));
        $(".table-error-msg").css("display", "block");
    }

}


function openCreatedRoom1(id) {
    var token = getQueryVariable("token");


    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        popups["pop" + id] = window.open(game_link + token + "&roomid=" + id, "OkeyTable" + id, "width=1100, height=715");
    } else {
        popups["pop" + id] = window.open(game_link + token + "&roomid=" + id, "OkeyTable" + id, "width=1100, height=670");
    }
}



function openCreatedRoom(id) {
    activeRoomID = id;
    connectRoom();
}


function ServerPing() {
    var sendObj = {};
    var date = new Date();
    sendObj.date = date.getTime();
    Send("ping_latency", sendObj);
    setTimeout(ServerPing, 5000);
    GetTournamentData();
}

var activeTournamentId = 0;

function GetTournamentDetails(tour_id) {
    activeTournamentId = tour_id
    var sendObj = {};
    sendObj.tournament_id = tour_id;
    sendObj.token = getQueryVariable("token");
    Send("tournament_total_info", sendObj);
}