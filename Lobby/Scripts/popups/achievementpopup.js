﻿function AchievementPopup(names,coin) {
    createjs.Container.call(this);

    var that = this;
 


    this.source = names;

    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);
            game.popup = null;
        }
    }

    that.resizeContainer = function () {

      

        that.y = 338;
        that.x = 275;
 
    }

    function initialize() {




        var bgImg = loader.getResult("achievement_popup");
        that.bg = new createjs.Bitmap(bgImg);


        var starImg = loader.getResult("achievement_star");
        that.starImg = new createjs.Bitmap(starImg);
        that.starImg.x = 400;
        that.starImg.y = 80;

        that.text = new createjs.Text(GetTranslatedValue("achievement_complete"), getCorrectFontSize(32) +getCorrectFont(mtavruli_font), "white");
        that.text.textAlign = "center";
        that.text.y = 180;
        that.text.x = 436;

        that.text1 = new createjs.Text('"' + that.source[game.lang] + '"', getCorrectFontSize(27)+ getCorrectFont(nusxuri_font), "grey");
        that.text1.textAlign = "center";
        that.text1.y = 235;
        that.text1.x = 436;

        that.text2 = new createjs.Text("+" + coin + " " + GetTranslatedValue("coin"), getCorrectFontSize(32) + getCorrectFont(mtavruli_font), yellow_color);
        that.text2.textAlign = "center";
        that.text2.y = 300;
        that.text2.x = 436;

        that.addChild(that.bg);
        that.addChild(that.starImg);
        that.addChild(that.text);
        that.addChild(that.text1);
        that.addChild(that.text2);
        that.alpha = 0;
        var tween = createjs.Tween.get(that)
            .wait(1500)
            .to({ alpha: 1 }, 500)
            .wait(1500)
            .to({ alpha: 0 }, 750)
            .call(function () {
                game.board.removeChild(that);
            });


        game.board.addChild(that);
    }
 

    initialize();
    that.resizeContainer();

    
 
}


AchievementPopup.prototype = new createjs.Container();
AchievementPopup.prototype.constructor = AchievementPopup;