﻿function RaisePopup(raiseX , canRaise,timer_size) {
    createjs.Container.call(this);
    var that = this;
    var counter = 255;
    initialize();
    var result = false;


    that.resizeContainer = function()
    {

 
          that.x = window.innerWidth/2- 488 *scl;
         that.y=  window.innerHeight/2- 343 *scl;
 
        that.scaleY =  that.scaleX  =scl;
        that.background.scaleX =  that.background.scaleY = 1/scl;

        var pt =  that.localToGlobal(0,0);
        that.background.y = pt.y*(-1/scl);
        that.background.x = pt.x*(-1/scl);
    }
  

    function initialize() {

        game.removePopup();

        that.x = 360;
        that.y = 200;

        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.85; 
        if (!game.mobile) {

            that.background.cache(0, 0, screen.width, screen.height);
            that.addChild(that.background);
        }


        var bg = loader.getResult("raise_popup");
        that.bg = new createjs.Bitmap(bg);
        that.bg.x=-150;
        that.bg.y=-150;
        that.addChild(that.bg);



        that.wintext1 = new createjs.Text(GetTranslatedValue("raise") + "X"+ raiseX , getCorrectFontSize(34)+ getCorrectFont(mtavruli_font), white_color);
        that.wintext1.textAlign = "center";
        that.wintext1.x = 498;
        that.wintext1.y = 48;
        that.addChild(that.wintext1);


         


        that.wintext2 = new createjs.Text(GetTranslatedValue("op_wants_raise"), getCorrectFontSize(32) + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign = "center";
        that.wintext2.lineWidth = 800;
        that.wintext2.x =498;
        that.wintext2.y =165;
        that.addChild(that.wintext2);
         

 
        that.timerLoader = new PlayerTimer(0, 255, 4, 6);
        that.timerLoader.x = 405;
        that.timerLoader.y = 320;
        that.addChild(that.timerLoader);

     
 
        that.yes = new Button("button_yellow", "accept",false,"black");
        that.yes.setSize (142);
        that.yes.x = 368;
        that.yes.y = 398;
       
        that.addChild(that.yes);


 
        that.no = new Button("button_red",  "decline",false,"white");
        that.no.setSize (142);
        that.no.x = 63;
        that.no.y = 398; 
        that.addChild(that.no);

 

        that.raise = new Button("button_grey", "raise",false,"white");
        that.raise.setSize (142);
      
        that.raise.txt.text = GetTranslatedValue("raise") + " X" + game.logic.NextRaise(raiseX);
        

        that.raise.addChild(that.raise.sprite);
        that.raise.addChild(that.raise.raiseText);

        that.raise.x = 673;
        that.raise.y = 398;


        if (!canRaise) {
            that.raise.mouseEnabled = false;
            that.raise.alpha = 0.6;
        }
          
     
        that.addChild(that.raise);

         
        that.raise.cursor = "pointer";
        that.raise.on("click", function () {
            result = true;
            game.engine.raiseAnswer(2);
           
            that.removePopup();
            game.popup = new WaitPopup("wait_op",false);

        });




        that.yes.cursor = "pointer";
        that.yes.on("click", function () {
            result = true;
            game.engine.raiseAnswer(1);
            that.removePopup();

        });


        that.no.cursor = "pointer";
        that.no.on("click", function () {
            result = true;
            game.engine.raiseAnswer(0);
            that.removePopup();

        });

        stage.addChild(that);
        that.timerLoader.start(timer_size, timer_size, yellow_color);
     
    }
    that.removePopup = function ()
    {

        if (this.parent) {
            this.parent.removeChild(this);
            game.popup = null;
        }
    }

  
    that.timeOut=function(){
        that.removePopup();
    }

that.resizeContainer();
}

RaisePopup.prototype = new createjs.Container();
RaisePopup.prototype.constructor = RaisePopup;