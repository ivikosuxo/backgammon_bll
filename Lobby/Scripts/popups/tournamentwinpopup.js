﻿function WinPopup(value) {
    createjs.Container.call(this);
    var that = this;
    that.value = parseFloat(value).toFixed(2);

    var counter = 260;
    var result = false;
    initialize();

 
    that.resizeContainer = function()
    {
    
        that.x = window.innerWidth/2- 428 *scl;
        that.y=  window.innerHeight/2- 450 *scl;
   
 
 
         that.scaleY =  that.scaleX  =scl;
        

         var pt = that.globalToLocal(0, 0);
         that.background.scaleX = 1/ scl;
         that.background.scaleY = 1 / scl;
         that.background.y = pt.y;
         that.background.x = pt.x;
    }




    function initialize() {

        game.removePopup();

    
        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.85;
        if (!game.mobile) {

            
            that.addChild(that.background);
        }

        var winImgTexture = "pwin";
        var winText = GetTranslatedValue("next_step");
        if (game.final) {
            winImgTexture = "win_tournament";
            winText = GetTranslatedValue("first_place");
        }
        var winimg = loader.getResult(winImgTexture);
        that.win = new createjs.Bitmap(winimg);
        that.win.x = 495;
        that.win.y =  0;
        that.win.cache(0,0,154,148);
        that.addChild(that.win);

         var bg = loader.getResult("win_popup");
    
         that.bg = new createjs.Bitmap(bg);
         that.bg.x=0;
         that.bg.y=0;
         that.addChild(that.bg);


         that.wintext1 = new createjs.Text(GetTranslatedValue("you_win"), getCorrectFontSize(34) + getCorrectFont(mtavruli_font), color_yellow);
        that.wintext1.textAlign="center";
        that.wintext1.x = 580;
        that.wintext1.y = 195;
        that.addChild(that.wintext1);

 


        that.wintext2 = new createjs.Text(winText, getCorrectFontSize(32) + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign="center";
        that.wintext2.x = 580;
        that.wintext2.y = 360;
        that.addChild(that.wintext2);

  

        that.no = new Button("button_yellow", "close", false, color_black);
        that.no.x = 430;
        that.no.y = 521;
        that.no.setSize(142);
        
        that.addChild(that.no);
 
 
 

        that.no.cursor = "pointer";
        that.no.on("click", function ()
        {

            CloseWindow();
            that.removePopup();

        });


     
        stage.addChild(that);
       // that.timerLoader.start(10, 10, yellow_color);
    

    }

  

    that.removePopup = function () {

        if (this.parent) {
            window.close();
        }
    }

    that.timeOut = function () {
    
            window.close();        
    
    }
 
     that.resizeContainer();
}

WinPopup.prototype = new createjs.Container();
WinPopup.prototype.constructor = WinPopup;

 



 