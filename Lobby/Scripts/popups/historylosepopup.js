﻿function LosePopup(reason,last) {
    createjs.Container.call(this);
    var that = this;
    

    var counter = 260;
    var result = false;
    initialize();


    that.resizeContainer = function () {

        that.x = window.innerWidth / 2 - (that.bg.image.width/2) * scl;
        that.y = window.innerHeight / 2 - 450 * scl;
         
        that.scaleY = that.scaleX = scl;

        that.background.scaleX = that.background.scaleY = 1 / scl;

        var pt = that.globalToLocal(0, 0);
        that.background.y = pt.y;
        that.background.x = pt.x;
    }




    function initialize() {

        game.removePopup();


        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.95;
        if (!game.mobile) { 
        
            that.addChild(that.background);
        }

        var winimg = loader.getResult("plose");
        that.win = new createjs.Bitmap(winimg);
        that.win.x = 495;
        that.win.y = 0;
        that.win.cache(0, 0, 154, 148);
        that.addChild(that.win);

        var bg = loader.getResult("win_popup");

        that.bg = new createjs.Bitmap(bg);
        that.bg.x = 0;
        that.bg.y = 0;
        that.addChild(that.bg);

        var winText = "round";
        if (last) {            
            winText = "game";
        }


        that.wintext1 = new createjs.Text(GetTranslatedValue("you_lose") + " " + GetTranslatedValue(winText), getCorrectFontSize(34) + getCorrectFont(mtavruli_font), red_color);
        that.wintext1.textAlign = "center";
        that.wintext1.x = 580;
        that.wintext1.y = 195;
        that.addChild(that.wintext1);

 
        that.wintext2 = new createjs.Text(GetTranslatedValue(reason), getCorrectFontSize(32) + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign = "center";
        that.wintext2.x = 580;
        that.wintext2.y = 360;
        that.addChild(that.wintext2);




 

        that.no = new Button("button_red", "close", false, color_white);
        that.no.x = 430;
        that.no.y = 521;
        that.no.setSize(142);
   
        that.addChild(that.no);

 


        that.no.cursor = "pointer";
        that.no.on("click", function () {

           // window.close();
            game.removePopup();

        });



        stage.addChild(that);
       // that.timerLoader.start(10, 10, yellow_color);
        

    }



    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);
        }
    }
     
    that.resizeContainer();
}

LosePopup.prototype = new createjs.Container();
LosePopup.prototype.constructor = LosePopup;

 