 
//font_tamplate_size24.font_size = "24px";

function Button(bg, text, icon, color) {
    createjs.Container.call(this);
    var that = this;
    that.textKey = text;
    //color="white";

    that.cursor = "pointer";
    var texture = loader.getResult(bg);
    that.buttonBG = new createjs.Bitmap(texture);
    that.addChild(that.buttonBG);
    var start = 0;

    if (icon) {
        var iconImage = loader.getResult(icon);
        that.iconBg = new createjs.Bitmap(iconImage);
        that.iconBg.x = 53;// 64; //64
        that.iconBg.y = 24;
        start = 67;//64;

        that.addChild(that.iconBg);

    }
    if (text) {
        that.txt = new createjs.Text(GetTranslatedValue(text).toUpperCase(), getCorrectFontSize(24) + getCorrectFont(mtavruli_font), color);
        that.txt.textAlign = "center";
        that.txt.textBaseline = "middle";
        that.txt.x = start + 96;
        that.txt.y = 36;
        that.addChild(that.txt);

        that.on("rollover", function () {
            that.buttonBG.image = loader.getResult(bg + "h");
        });

        that.on("rollout", function () {
            that.buttonBG.image = loader.getResult(bg);
        });

        that.setSize = function (size) {
            that.txt.x = size;
        }

        that.setFontSize = function (size) {
          //  that.txt.font = size + "px Mtavruli";
        }
        that.setFont = function (size) {
           
        }


        that.setHeight = function (size) {
            //that.txt.y=32; 
        }

        that.setIconX = function (size) {
            that.iconBg.x = size;
        }


        that.setText = function (txt) {
            that.txt.text = txt;
        }

        that.translate = function () { 
            that.txt.text = GetTranslatedValue(that.textKey);
            that.txt.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);
        }
    }
}

Button.prototype = new createjs.Container();
Button.prototype.constructor = Button;

function ButtonPassword(img, text) {
    createjs.Container.call(this);

    this.sprite = new createjs.Bitmap(loader.getResult(img));
    this.cursor = "pointer";

    this.text = new createjs.Text(text, "30px" +getCorrectFont(mtavruli_font) , black_color);
    this.text.x = this.sprite.image.width / 2;
    this.text.y = this.sprite.image.height / 2 - 15;
    this.text.textAlign = "center";

    this.on("rollover", function () {
        this.sprite.image = loader.getResult(img + "_h");
    });

    this.on("rollout", function () {
        this.sprite.image = loader.getResult(img);
    });

    this.addChild(this.sprite);
    this.addChild(this.text);

}

ButtonPassword.prototype = new createjs.Container();
ButtonPassword.prototype.constructor = ButtonPassword