﻿function WaitPopup(text ,error) {
    createjs.Container.call(this);

    var that = this;
    that.error = error;


this.source = text;

    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);
            game.popup = null;
        }
    }

    that.resizeContainer = function()
    {
      that.y= game.board.y +  (brdY*scl)/2 -154*scl; 
      that.x=  game.board.x+  (brdX*scl)/2-428*scl;
    
      that.scaleX = that.scaleY= scl;
     

    }

    function initialize() {
        game.removePopup();
        

        that.scaleX = that.scaleY= scl;

        that.y= game.board.y +  (brdY*scl)/2 -154*scl; 
        that.x=  game.board.x+  (brdX*scl)/2-428*scl;
 
    
        var bgImg = loader.getResult("pwait");
        that.bg = new createjs.Bitmap(bgImg);
 
        that.text = new createjs.Text(GetTranslatedValue(text), getCorrectFontSize(26)+getCorrectFont(mtavruli_font), "white");
        that.text.textAlign = "center";
        that.text.y = 144;
        that.text.x= 443;


   
        that.addChild(that.bg);
 
        that.addChild(that.text);


        if (that.error) {
            that.addEventListener("click", function () { 
                CloseWindow(); 
            }); 
        }
    


      stage.addChild(that);
    
    }


    initialize();

    this.translate = function()
    {

        this.text.text = GetTranslatedValue(this.source);
        this.text.font = getCorrectFontSize(26) + getCorrectFont(mtavruli_font);
    }
    
}


WaitPopup.prototype = new createjs.Container();
WaitPopup.prototype.constructor = WaitPopup;