﻿function AddWaiter(timer_size, user_name) {
    createjs.Container.call(this);
    var that = this;
    

    var counter = 260;
    var result = false;
    initialize();

 
    that.resizeContainer = function()
    {
    
        that.x =  window.innerWidth / 2;
        that.y =  window.innerHeight *(0.45);
    
    that.scaleY = that.scaleX = scl; 

     that.background.scaleX = window.innerWidth / scl;
     that.background.scaleY = window.innerHeight / scl;

    var pt = that.globalToLocal(0, 0);
    that.background.y = pt.y  ;
    that.background.x = pt.x ;
    }




    function initialize() {

        game.removePopup();

       
       
            that.background = new createjs.Shape();
            that.background.graphics.beginFill(color_dark).drawRect(0, 0, 100, 100);
            that.background.alpha = 0.95; //0.90

            if (!game.mobile)
            {  
                that.addChild(that.background);
            }
 


        var bg = loader.getResult("win_popup"); 
        that.bg = new createjs.Bitmap(bg); 
        that.bg.regX = bg.width / 2;
        that.bg.regY = bg.height / 2; 
        that.addChild(that.bg);

 
        that.wintext1 = new createjs.Text(GetTranslatedValue("offer"), getCorrectFontSize(34) + getCorrectFont(mtavruli_font), white_color);
           that.wintext1.textAlign="center";
            that.wintext1.x = 0;
            that.wintext1.y = -200;
           that.addChild(that.wintext1);

 
 


        that.wintext2 = new createjs.Text(user_name + GetTranslatedValue("op_wants_play"), getCorrectFontSize(32) + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign = "center";
        that.wintext2.lineWidth = 800;
        that.wintext2.x = 0;
        that.wintext2.y = -90;
        that.addChild(that.wintext2); 
 

         

        that.timerLoader = new PlayerTimer(0, 260, 4, 6);
        that.timerLoader.x = -120;
        that.timerLoader.y = 50;
        that.addChild(that.timerLoader);


         
      
        that.yes = new Button("button_yellow",'accept',false,color_black);
        that.yes.x = -320;
        that.yes.y = 130;
        that.yes.setSize(142);
    
        that.addChild(that.yes);



   
        that.no = new Button("button_red", 'decline', false,color_white);
        that.no.x = 50; 
        that.no.y = 130;
        that.no.setSize(142);      
        that.addChild(that.no);

     
 

        that.yes.cursor = "pointer";
        that.yes.on("click", function () {
            that.timerLoader.stop();
            game.engine.connection.acceptWaiter(true);
            that.removePopup();

        });

        that.no.cursor = "pointer";
        that.no.on("click", function ()
        { 
            that.timerLoader.stop();
            game.engine.connection.acceptWaiter(false);
            that.removePopup();
            game.popup = new WaitPopup("wait_op", false);
        });

        that.timeOut = function () {
            
                game.engine.connection.acceptWaiter(false);
                that.removePopup();
                game.popup = new WaitPopup("wait_op", false);
        }
     
        stage.addChild(that);
        that.timerLoader.start(timer_size, timer_size, yellow_color);
 

    }

   
    that.removePopup = function () {
        that.timerLoader.stop();
        if (this.parent) {
            this.parent.removeChild(this); 
            game.popup = null; 
        }
    }
     
    PlaySound("beep");
     that.resizeContainer();
}

AddWaiter.prototype = new createjs.Container();
AddWaiter.prototype.constructor = AddWaiter;