﻿function SettingsPopup() {
    createjs.Container.call(this);

    var that = this;
    that.visible = false;

    that.fadeIn = new createjs.Shape();
    that.fadeIn.graphics.beginFill(color_black).drawRect(0, 0, screen.width, screen.height);
    that.fadeIn.cache(0,0, screen.width, screen.height);
    that.fadeIn.alpha = 0.80;
    that.fadeIn.x = 0;
    that.fadeIn.visible = false;
    that.fadeIn.y = 0;
   
    that.fadeIn.on("click", function() {
        game.settingsPopup.visible = false;
        that.fadeIn.visible =  false;
    })



    that.bg = new createjs.Bitmap(loader.getResult("settings_bg")); 
    that.bg.x=-50;
    that.bg.y=0; 
    that.x = 55;
    that.y = 0; 
    that.addChild(that.bg);


 
  
     
    var setbg = loader.getResult("settings");
    that.closer = new createjs.Bitmap(setbg);
    that.closer.x = 35;
    that.closer.y = 25;
    that.closer.scaleX= that.closer.scaleY = 1.0;
    that.addChild(that.closer);


    that.langCont = new createjs.Container();

    that.lanText = new createjs.Text(GetTranslatedValue("choose_lang"), getCorrectFontSize(28)  + getCorrectFont(mtavruli_font), color_grey);
    that.lanText.y=-17;
    that.langCont.addChild(that.lanText);
    that.langCont.x = 87;
    that.langCont.y = 133;

    that.langs = [];

    that.geoButton = new LangButton("Geo");
    that.geoButton.x = 0;
    that.langCont.addChild(that.geoButton);

    that.engbutton = new LangButton("Eng");
    that.engbutton.x = 110;
    that.langCont.addChild(that.engbutton);

    that.rusButton = new LangButton("Rus");
    that.rusButton.x = 220;
    that.langCont.addChild(that.rusButton);

    that.langs.push(that.geoButton);
    that.langs.push(that.engbutton);
    that.langs.push(that.rusButton);

    that.engbutton.cursor = "pointer";
    that.engbutton.on("click", function() {

        game.lang = 1;
        that.engbutton.activate();
        that.rusButton.deactivate();
        that.geoButton.deactivate();
        translate();
    });

    that.rusButton.cursor = "pointer";
    that.rusButton.on("click", function() {
        game.lang =2;
        that.rusButton.activate();
        that.engbutton.deactivate();
        that.geoButton.deactivate();
        translate();
    });

    that.geoButton.cursor = "pointer";
    that.geoButton.on("click", function () {

        game.lang = 0;
        that.geoButton.activate();
        that.engbutton.deactivate();
        that.rusButton.deactivate();
        translate();
    });

    that.addChild(that.langCont);

 

    that.boardCont = new createjs.Container();

    that.boarText = new createjs.Text(GetTranslatedValue("choose_board_color"), getCorrectFontSize(28)  + getCorrectFont(mtavruli_font),color_grey);
    that.boarText.y=-17;
    that.boardCont.addChild(that.boarText);
    that.boardCont.x = 87;
    that.boardCont.y = 279;
 
    

    that.woodbutton = new BoardButton( 1);
    that.woodbutton.x = 0;
    that.boardCont.addChild(that.woodbutton);


   


    that.redButton = new BoardButton( 2);
    that.redButton.x = 110;
    that.boardCont.addChild(that.redButton);

    that.blueButton = new BoardButton(3);
    that.blueButton.x = 220;
    that.boardCont.addChild(that.blueButton);
   
    $("#main-board").css({ 'background-color': '#101010' }); 

    that.woodbutton.cursor = "pointer";
    that.woodbutton.on("click", function () {
          

      
        game.board.changeColor("");
        that.woodbutton.activate();
        that.blueButton.deactivate();
        that.redButton.deactivate();
    });


    that.blueButton.cursor = "pointer";
    that.blueButton.on("click", function () {
     
        
        game.board.changeColor("2");
         
        that.blueButton.activate();
        that.woodbutton.deactivate();
        that.redButton.deactivate();
    });

    that.redButton.cursor = "pointer";
    that.redButton.on("click", function () {
 
        
        game.board.changeColor("1");
        that.redButton.activate();
        that.woodbutton.deactivate();
        that.blueButton.deactivate();
    });



    that.addChild(that.boardCont);


    


    that.stoneCont = new createjs.Container();

    that.viewStoneText = new createjs.Text(GetTranslatedValue("stone_type"), getCorrectFontSize(28) + getCorrectFont(mtavruli_font), color_grey);
    that.viewStoneText.y=-17;
    that.stoneCont.addChild(that.viewStoneText);
    that.stoneCont.x = 87;
    that.stoneCont.y = 466;


    that.defStone = new StoneButton(1);
    that.defStone.x=0;
    that.stoneCont.addChild(that.defStone);
    that.hybridStone = new StoneButton(2);
    that.hybridStone.x=110;
    that.stoneCont.addChild(that.hybridStone);

    that.clasicStone = new StoneButton(3);
    that.clasicStone.x=220;
    that.stoneCont.addChild(that.clasicStone);


    that.defStone.cursor = "pointer";
    that.defStone.on("click", function() {
        that.defStone.activate();
        that.hybridStone.deactivate();
        that.clasicStone.deactivate();
    });

    that.hybridStone.cursor = "pointer";
    that.hybridStone.on("click", function() {
        that.hybridStone.activate();
        that.defStone.deactivate();
        that.clasicStone.deactivate();
    });

    that.clasicStone.cursor = "pointer";
    that.clasicStone.on("click", function () {
        that.clasicStone.activate();
        that.defStone.deactivate();
        that.hybridStone.deactivate();
    });



    // stone color change

    that.stoneColorCont = new createjs.Container();

    that.viewStoneColorText = new createjs.Text(GetTranslatedValue("stone_color"), getCorrectFontSize(28)  + getCorrectFont(mtavruli_font), color_grey);
    that.viewStoneColorText.y = -17;
    that.stoneColorCont.addChild(that.viewStoneColorText);
    that.stoneColorCont.x = 87;
    that.stoneColorCont.y = 665;


    that.WhiteStone = new StoneColorButton(0);
    that.WhiteStone.x = 0;
    that.stoneColorCont.addChild(that.WhiteStone);
    that.blackColorStone = new StoneColorButton(3);
    that.blackColorStone.x = 110;
    that.stoneColorCont.addChild(that.blackColorStone);

    /* that.clasicStone = new StoneButton(3);
     that.clasicStone.x = 220;
     that.stoneCont.addChild(that.clasicStone); */


    that.WhiteStone.cursor = "pointer";
    that.WhiteStone.on("click", function () {
        that.WhiteStone.activate();
        that.blackColorStone.deactivate();
        game.engine.connection.changeStoneColor(1);
    });

    that.blackColorStone.cursor = "pointer";
    that.blackColorStone.on("click", function () {
        that.blackColorStone.activate();        
        that.WhiteStone.deactivate();
        game.engine.connection.changeStoneColor(0);
    });
   

    that.translate = function () {
        try {



            that.lanText.text = GetTranslatedValue("choose_lang"); 
            that.lanText.font = getCorrectFontSize(28) + getCorrectFont(mtavruli_font);
            that.boarText.text = GetTranslatedValue("choose_board_color");
            that.boarText.font = getCorrectFontSize(28)  + getCorrectFont(mtavruli_font);
            that.viewStoneText.text = GetTranslatedValue("stone_type");
            that.viewStoneText.font = getCorrectFontSize(28) + getCorrectFont(mtavruli_font);
            that.viewStoneColorText.text = GetTranslatedValue("stone_color")
            that.viewStoneColorText.font = getCorrectFontSize(28)  + getCorrectFont(mtavruli_font);

        } catch (ex) {
            console.log(ex);
        }

}

that.activate = function () {
    that.langs[game.lang].activate(); 

    that.woodbutton.activate();

    that.hybridStone.activate();

    that.WhiteStone.activate();

    game.board.changeBoardType();
}
 that.addChild(that.stoneCont);
 that.addChild(that.stoneColorCont);

    //activate


    game.rightPanel.addChild(that.fadeIn);
    game.rightPanel.addChild(that);


    this.activateCorrectStone = function (color) {
        if (color == 1) {
            that.WhiteStone.activate();
            that.blackColorStone.deactivate();
        } else {
            that.WhiteStone.deactivate();
            that.blackColorStone.activate();
        }  
    }
        that.resizeContainer = function()
        {
  
              that.fadeIn.scaleX=  that.fadeIn.scaleY=1/scl;  
        }

            that.resizeContainer();
}

SettingsPopup.prototype = new createjs.Container();
SettingsPopup.prototype.constructor = SettingsPopup;

 

function LangButton(text)
{
    
    createjs.Container.call(this);


    var that = this;

    that.y = 30;

    that.sp = new createjs.Shape();
    that.sp.graphics.beginFill("#ffffff").drawRoundRect(0, 0, 55, 32, 4);
    that.sp.alpha = 0.01;
    that.sp.cache(0,0,55,32);
    that.addChild(that.sp);

    var bg = loader.getResult("setBg1");
    that.bg = new createjs.Bitmap(bg);
    that.addChild(that.bg);
 
    that.text  = new createjs.Text(text, "26px arial", color_white);
    that.text.textAlign ="center";
    that.addChild(that.text);
    that.text.x = 46;
    that.text.y = 12;

   


    that.activate = function () {

        that.bg.image = loader.getResult("setBg1h");
  
        that.text.color = color_black;

    }

    that.deactivate = function () {

        that.bg.image = loader.getResult("setBg1");
   
        that.text.color = color_white;
    }

}
LangButton.prototype = new createjs.Container();
LangButton.prototype.constructor = LangButton;





function BoardButton( index) {

    createjs.Container.call(this);

    var that = this;
    that.y = 30;

 
 

    var bg = loader.getResult("setcolor" + index);
    that.bg = new createjs.Bitmap(bg);
    that.addChild(that.bg);

 
     
    that.activate = function () {

        that.bg.image = loader.getResult("setcolor_h_" + index);
       //// that.logo.image = loader.getResult("setcolor"+index+);
         //that.text.color="#f1aa4f";

    }

    that.deactivate = function () {

        that.bg.image = loader.getResult("setcolor" + index);
       // that.logo.image = loader.getResult("setcolor"+index);
       //  that.text.color="#eccfa9";
    }


    that.translate= function()
    {
     that.text.text = text[game.lang];

    }

}
BoardButton.prototype = new createjs.Container();
BoardButton.prototype.constructor = BoardButton;




 
 

function StoneButton(index) {


    createjs.Container.call(this);

    var that = this;

    that.y = 30;

    that.sp = new createjs.Shape();
    that.sp.graphics.beginFill(color_white).drawRoundRect(0, 0, 40, 40, 4);
    that.sp.alpha = 0.01;
    that.sp.cache(0, 0, 40, 40);
    that.addChild(that.sp);

 

    var logo = loader.getResult("set_chip" + index);
    that.logo = new createjs.Bitmap(logo);
    that.logo.x =5;
    that.logo.y = 5;
    that.addChild(that.logo);
   

    that.activate = function ()
    {
       
        game.changeStoneColor(index);
        that.logo.image =  loader.getResult("set_chip" + index+"h");

    }

    that.deactivate = function () {
    that.logo.image =  loader.getResult("set_chip" + index);    

    }

}
StoneButton.prototype = new createjs.Container();
StoneButton.prototype.constructor = StoneButton;

function StoneColorButton(index) {


    createjs.Container.call(this);

    var that = this;
    that.index = index;
    that.y = 30;

    that.sp = new createjs.Shape();
    that.sp.graphics.beginFill(color_white).drawRoundRect(0, 0, 40, 40, 4);
    that.sp.alpha = 0.01;
    that.sp.cache(0, 0, 40, 40);
    that.addChild(that.sp);

 

    var logo = loader.getResult("set_color" + index);
    that.logo = new createjs.Bitmap(logo);
    that.logo.x =5;
    that.logo.y = 5;
    that.addChild(that.logo);
   

    that.activate = function ()
    {
        game.stoneColor = that.index;

        game.board.changeStoneColor();
        that.logo.image =  loader.getResult("set_color_h"+index);
        
    }

    that.deactivate = function () {
        that.logo.image =  loader.getResult("set_color" + index);    

    }

}
StoneColorButton.prototype = new createjs.Container();
StoneColorButton.prototype.constructor = StoneColorButton;