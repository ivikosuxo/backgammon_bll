﻿function RotationScreen() {
    createjs.Container.call(this);

    var that = this;


    that.background = new createjs.Shape();
    that.background.graphics.beginFill("black").drawRect(0, 0, screen.width, screen.height);
    that.background.alpha =1.00;
    that.addChild(that.background);

    that.container = new createjs.Container();
    that.addChild(that.container);

    that.rotateImage = new createjs.Bitmap(loader.getResult("rotate_icon"));
    that.rotateImage.y = -50;
    that.rotateImage.regX = 97;
    that.rotateImage.regY = 57;
    that.container.addChild(that.rotateImage);

    that.rotateText = new createjs.Text(GetTranslatedValue("rotate_message"), getCorrectFontSize(22) + getCorrectFont(mtavruli_font), color_grey);
    that.rotateText.y = 40;
    that.rotateText.textAlign = "center";
    that.rotateText.textBaseline = "middle";
    that.container.addChild(that.rotateText);

    this.resizeContainer = function () {

        that.container.x = window.innerWidth / 2;
        that.container.y = window.innerHeight / 2;
        if (window.innerWidth < 500) {
            that.container.scaleX = that.container.scaleY = window.innerWidth / 500;
        } else {
            that.container.scaleX = that.container.scaleY =1;
        }
       

        var pt = that.globalToLocal(0, 0);
        that.background.y = pt.y;
        that.background.x = pt.x;

    }
    this.resizeContainer();
}
RotationScreen.prototype = new createjs.Container();
RotationScreen.prototype.constructor = RotationScreen;



function rotateScreen() {

    if (game.mobile) {
        try {
            if (window.innerHeight > window.innerWidth) {
                game.rotateScreen.visible = true;
                game.rotateScreen.resizeContainer();
                game.rotateScreen.rotateText.text = GetTranslatedValue("rotate_message");
                canv.addChild(game.rotateScreen);
            } else {
                game.rotateScreen.visible = false;
            }
        } catch (ex) {
            console.log(ex);
        }
    }
}

