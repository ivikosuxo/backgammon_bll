﻿function GiveUpPopup() {
    createjs.Container.call(this);
    var that = this;

    initialize();


    function initialize() {

        game.removePopup();
 
 


        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.95; 
        if (!game.mobile) {

            that.background.cache(0, 0, screen.width, screen.height);
            that.addChild(that.background);

        }

        var bg = loader.getResult("give_up_bg");

        that.bg = new createjs.Bitmap(bg);
          that.bg.x=-150;
          that.bg.y=-150;
        that.addChild(that.bg);



        that.wintext1 = new createjs.Text(GetTranslatedValue("give_up"), getCorrectFontSize(34) + getCorrectFont(mtavruli_font), color_white);
        that.wintext1.textAlign ="center";
        that.wintext1.x = 428;
        that.wintext1.y = 45;
        that.addChild(that.wintext1);

 



        that.wintext2 = new createjs.Text(GetTranslatedValue("round_giveup_text") + GetTranslatedValue(game.logic.giveUpPoint()), getCorrectFontSize(32) + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign = "center";
        that.wintext2.lineWidth = 800; 
        that.wintext2.x = 428;
        that.wintext2.y = 190;
        that.addChild(that.wintext2);



       /* that.wintext3 = new createjs.Text(giveupText1_tr[game.lang] + game.logic.giveUpPoint(), "32px Nusxuri", panel_color);
        that.wintext3.textAlign ="center";
        that.wintext3.x = 428;
        that.wintext3.y = 240;
        that.addChild(that.wintext3); */

 


        var closeImg = loader.getResult("pclose");
        that.close = new createjs.Bitmap(closeImg);
        that.close.x = 768;
        that.close.y = 35;        
        that.addChild(that.close);

 
     

        that.giveRound =new Button("button_grey", "round", "lround_icon" ,color_white);        
        that.giveRound.x = 137;
        that.giveRound.y = 370;
        that.addChild(that.giveRound);
 


        that.giveupGame = new Button("button_red", "game", "lgame_icon",color_white );        
        that.giveupGame.x = 442;
        that.giveupGame.y = 370;
        that.addChild(that.giveupGame);

 
  

       
        that.close.cursor = "pointer";
        that.close.on("click", function () {
            that.removePopup();
        });

        that.background.on("click", function () {
            that.removePopup();
        });

        that.close.on("rollover", function () {
         that.close.image = loader.getResult("pcloseh");
            });

       that.close.on("rollout", function () {
          that.close.image = loader.getResult("pclose");
        });

        that.giveRound.on("click", function () {
            game.engine.surrender(false);
            that.removePopup();
        });


        that.giveupGame.on("click", function () {

            game.engine.surrender(true);
            that.removePopup();

        });

 
         
        stage.addChild(that);
    }
    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);             
            game.popup = null;
        }
    }

      that.resizeContainer = function()
    {
       
    that.x = window.innerWidth/2- 428 *scl;
    that.y=  window.innerHeight/2- 313 *scl;
        that.scaleY =that.scaleX = scl;
 
        that.background.scaleX=that.background.scaleY= 1/scl;

        var pt =  that.localToGlobal(0,0);
        that.background.y = pt.y*(-1/scl);
        that.background.x = pt.x*(-1/scl);
    }
      that.resizeContainer();

     
}

GiveUpPopup.prototype = new createjs.Container();
GiveUpPopup.prototype.constructor = GiveUpPopup;