﻿function LosePopup(timer_size, coin_count) {
    createjs.Container.call(this);
    var that = this;
    

    var counter = 260;
    var result = false;
    initialize();

 
    that.resizeContainer = function()
    {
    
        that.x = window.innerWidth/2- 428 *scl;
        that.y=  window.innerHeight/2- 308 *scl;
    
         that.scaleY =  that.scaleX  =scl;
         that.background.scaleX =  that.background.scaleY = 1/scl;

        var pt =  that.localToGlobal(0, 0);
        that.background.y = pt.y*(-1/scl);
        that.background.x = pt.x*(-1/scl);
    }




    function initialize() {

        game.removePopup();

   
 

        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.95;
        if (!game.mobile) {

            that.background.cache(0, 0, screen.width, screen.height);
            that.addChild(that.background);
        }

        var winimg = loader.getResult("plose");
        that.win = new createjs.Bitmap(winimg);
        that.win.x = 360;
        that.win.y =  -150;
        that.win.cache(0,0,154,148);
        that.addChild(that.win);

         var bg = loader.getResult("win_popup");
    
        that.bg = new createjs.Bitmap(bg);
         that.bg.x=-150;
         that.bg.y=-150;
        that.addChild(that.bg);

 
         that.wintext1 = new createjs.Text(GetTranslatedValue("you_lose"), getCorrectFontSize(34)  + getCorrectFont(mtavruli_font), color_red);
         that.wintext1.textAlign="center";
         that.wintext1.x = 428;
         that.wintext1.y = 45;
         that.addChild(that.wintext1);


         var coins = parseFloat(coin_count).toFixed(2);

         that.wintext3 = new createjs.Text("+" + coins + " " + GetTranslatedValue("coin"), getCorrectFontSize(32) + getCorrectFont(nusxuri_font), white_color);
        that.wintext3.textAlign = "center";
        that.wintext3.x = 428;
        that.wintext3.y = 140;
        that.addChild(that.wintext3);


        that.wintext2 = new createjs.Text(GetTranslatedValue("play_again_same_rules"), getCorrectFontSize(32)  + getCorrectFont(nusxuri_font), panel_color);
        that.wintext2.textAlign="center";
        that.wintext2.x = 428;
        that.wintext2.y = 210;
        that.addChild(that.wintext2);



  

   

        that.timerLoader = new PlayerTimer(0, 260, 4, 6);
        that.timerLoader.x = 345;
        that.timerLoader.y = 300;
        that.addChild(that.timerLoader);





   


      
        that.yes = new Button("button_yellow","yes",false,color_black);
        that.yes.x = 450;
        that.yes.y = 381;
        that.yes.setSize(142);
    
        that.addChild(that.yes);



   
        that.no = new Button("button_red", "no",false,color_white);
        that.no.x = 140;
        that.no.y = 381;
        that.no.setSize(142);
      
        that.addChild(that.no);

     
 

        that.yes.cursor = "pointer";
        that.yes.on("click", function () {
            result = true;
            game.engine.PlayAgain();
            that.removePopup();

        });

        that.no.cursor = "pointer";
        that.no.on("click", function ()
        {

            CloseWindow();
            that.removePopup();

        });

        that.timeOut = function () {
            if (!result) {            
                CloseWindow();
                that.removePopup();
            }
        }
     
        stage.addChild(that);
        that.timerLoader.start(timer_size, timer_size, yellow_color);
 

    }

  

    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);
            game.popup = new WaitPopup("wait_op",false);
        }
    }






   

     that.resizeContainer();
}

LosePopup.prototype = new createjs.Container();
LosePopup.prototype.constructor = LosePopup;