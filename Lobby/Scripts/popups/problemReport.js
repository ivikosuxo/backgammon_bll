function Report(pp)
{
createjs.Container.call(this);
var that = this;
    
that.isReport= true;

        that.background = new createjs.Shape();
        that.background.graphics.beginFill(color_dark).drawRect(0, 0, screen.width, screen.height);
        that.background.alpha = 0.95;
        if (!game.mobile) {

            that.background.cache(0, 0, screen.width, screen.height);
            that.addChild(that.background);
        }

that.bg = new createjs.Bitmap(loader.getResult("problem_pop"));
that.bg.x=-150;
that.bg.y=-150;
that.addChild(that.bg);
 
var pop =pp; 

that.problemText = new createjs.Text(GetTranslatedValue("problem_report"), getCorrectFontSize(34)  + getCorrectFont(mtavruli_font), color_white);
that.problemText.textAlign="center";
that.problemText.x = 510;
that.problemText.y = 50;
that.addChild(that.problemText);

that.userIcon = new createjs.Bitmap(loader.getResult("problem_user"));
that.userIcon.x = 100;
that.userIcon.y = 140;
that.addChild(that.userIcon);



 that.userText = new createjs.Text(game.player.username, getCorrectFontSize(28)  + getCorrectFont(calibri_font), panel_color);
that.userText.x = 155;
that.userText.y = 140;
that.addChild(that.userText);


that.rollIcon = new createjs.Bitmap(loader.getResult("problem_roll"));
that.rollIcon.x = 450;
that.rollIcon.y = 140;
that.addChild(that.rollIcon);


that.rollText = new createjs.Text(GetTranslatedValue("game_id"), getCorrectFontSize(24)  + getCorrectFont(nusxuri_font),panel_color);
that.rollText.x = 510;
that.rollText.y = 145;
that.addChild(that.rollText);

that.gameIdText = new createjs.Text("G" +game.game_id, getCorrectFontSize(24) + getCorrectFont(nusxuri_font), yellow_color);
    that.gameIdText.x = 650;
    that.gameIdText.y = 145;
    that.addChild(that.gameIdText);

 

    that.infoText = new createjs.Text(GetTranslatedValue("screenshot_text"), getCorrectFontSize(24) + getCorrectFont(nusxuri_font),"#666666");
//that.infoText.textAlign= "center";
that.infoText.x = 100;
that.infoText.y = 415;
that.addChild(that.infoText);



    that.cancel = new Button("button_grey", "cancel" ,false, "white");
that.cancel.x=210;
that.cancel.y=530;
that.cancel.setSize(141);
that.addChild(that.cancel);
that.cancel.cursor="pointer";
that.cancel.on("click",function(){

 that.removePopup();

});

that.send = new Button("button_yellow", "send" ,false, "black");
that.send.x=530;
  that.send.y = 530;
that.send.setSize(141);
that.addChild(that.send); 


that.send.cursor = "pointer";



    that.send.on("click", function () { 

        var data1 = {};
        data1.token = getQueryVariable("token");
      
        data1.room_id = game.room.room_id;        
        data1.game_id = game.game_id; 
        data1.round_id = game.round_id;
        data1.text = $("#problem-text").val().substring(0, 512); 

        if (data1.text.length==0) {
            return;
        }

    $.ajax({
        type: "POST",
        url: problem_url,
        data: JSON.stringify(data1),
        dataType: "json",
    
    }).done(function (data) {
        if (data.success) {
            that.problemText.text = GetTranslatedValue("problem_report");
            that.problemText.color = color_white;
            that.removePopup();
            if (game.popup) {
                game.popup.visible = false;
            }
            var popup = new DelayPopup(GetTranslatedValue("report_success"));
            setTimeout(function () {
                if (game.popup) {
                    game.popup.visible = true;
                }
            }, 3000);
          

        } else { 
            that.problemText.text = GetTranslatedValue("report_unsuccess");
            that.problemText.color = color_red;
        }

    }); 
});

that.removePopup= function(){

	if (that.parent)
	 {
       that.parent.removeChild(that);
       if (pop) { 
       	 pop.visible =true;
       }
       game.popup = pop;
	 }
$("#problem-text").hide();
}

stage.addChild(that); 
 

that.resizeContainer =function()
{
	 
    that.x = window.innerWidth/2- 498 *scl;
    that.y=  window.innerHeight/2- 400 *scl;
    that.scaleX = that.scaleY = scl;

         var pt =  that.localToGlobal(0, 0);
         that.background.scaleY=  that.background.scaleX=1/scl;
        that.background.y = pt.y*(-1/scl);
        that.background.x = pt.x*(-1/scl);

    $("#problem-text").css('top',  that.y+ 190*scl  + "px");
    $("#problem-text").css('left',  that.x+ 100*scl   + "px");

    $("#problem-text").css('width',  820*scl+  "px");
    $("#problem-text").css('height',   200*scl + "px");
     $("#problem-text").css('font-size',  28*scl+  "px");

     $("#problem-text").attr("placeholder" ,GetTranslatedValue( "enter_text"));
}
$("#problem-text").show();
 
that.resizeContainer();
}
Report.prototype = new createjs.Container();
Report.prototype.constructor= Report;