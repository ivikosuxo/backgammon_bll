function ChatPopup() {
    createjs.Container.call(this);
    var that = this;

    var close = false;

    var startX = 50;
    var startY = 25;
    var delay = 20;
    var bigDelay = 25;

    var lastX = startY;
    that.scaleX = that.scaleY = that.scale = 0.6;
    that.x = window.innerWidth - 84 * that.scale;
    that.y = (window.innerHeight - 532 * that.scale) / 2;



    var upFont = "14px Arial";
    var downFont = "12px Arial";

    that.cursor = "pointer";

    this.infoBG = new createjs.Bitmap(loader.getResult("chat_bg"));
    that.addChild(that.infoBG);

    this.chatText = new createjs.Text(GetTranslatedValue("chat"), "30px"+nusxuri_font, "white");
    this.chatText.textAlign = "center";
    this.chatText.rotation = 270;
    this.chatText.x = 32;
    this.chatText.y = 240;
    that.addChild(that.chatText);


    that.infoCircle = new createjs.Container();
    this.infoCircle.y = 145;
    this.infoCircle.x = -12;
    that.addChild(that.infoCircle);

    that.infoCircle.visible = false;

    this.infoBGCircle = new createjs.Bitmap(loader.getResult("chat_circle"));
    that.infoCircle.addChild(that.infoBGCircle);

    this.chatCount = new createjs.Text("1", "bold 30px"+calibri_font, "#2a221b");
    this.chatCount.textAlign = "center";

    this.chatCount.x = 30;
    this.chatCount.y = 10;
    that.infoCircle.addChild(that.chatCount);


    that.close = function () {
        var destX = window.innerWidth - 84 * that.scale//*scl;
        close = true;
        createjs.Tween.removeTweens(that);

        createjs.Tween.get(that)
            .to({ x: destX }, 300, createjs.Ease.quartOut)
        $("#Chat-content").hide();

    }

    that.open = function () {
        var destX = window.innerWidth - 423 * that.scale;



        close = false;

        $("#Chat-content").css("left", destX + 52);
        $("#Chat-content").css("top", that.y + 15);


        createjs.Tween.removeTweens(that);

        createjs.Tween.get(that)
            .to({ x: destX }, 300, createjs.Ease.quartOut)
            .call(function () { $("#Chat-content").show(); })
    }

    that.on("click", function () {
       // toggleFullScreen();
        if (close) {
            game.chatPopup.infoCircle.visible = false;
            that.open();
        } else {
            that.close();

        }
    });


    that.resizeContainer = function () {

        that.close();
        that.x = window.innerWidth - 84 * that.scale;
        that.y = (window.innerHeight - 532 * that.scale) / 2;


    }

    $("#chatInput").keyup(function (event) {
        if (event.keyCode == 13) {

            ChatSend();
        }
    });


    that.translate = function () {

        $("#chatInput").attr("placeholder", GetTranslatedValue("enter_text"));
        this.chatText.text = GetTranslatedValue("chat");

        switch (game.lang) {
            case 0:
                this.chatText.font = "30px"+mtavruli_font
                this.chatText.x = 37
                break;


            case 1:
                this.chatText.font = "30px"+nusxuri_font
                this.chatText.x = 32
                break;


            case 2:
                this.chatText.font = "30px" + nusxuri_font
                this.chatText.x = 32
                break
        }

    }
    that.setVisible = function () {

        if (close) {
            game.infoCircle.visible = true;
        }

    }


    stage.addChild(that);

    that.resizeContainer();
    that.translate();

    if (game.mobile) {
        that.visible = false;
    }
}
ChatPopup.prototype = new createjs.Container();
ChatPopup.prototype.constructor = ChatPopup;





function ChatSend() {
    var sendText = $("#chatInput").val();

    if (sendText.length > 0) {
        sendText = sendText.substring(0, 256);
        game.engine.connection.sendChatText(sendText); 
    }

}

function logAction(text, color) {

    var msg = text;
    var msgContent = $("<div></div>");
    msgContent.css("display", "none");
    var msgText = $("<p></p>");
    var font1 = $("<font style='font-size:15px; color:yellow;'>Info: </font>");
    var font2 = $("<font style='font-size:14px; color:" + color + ";'></font>");
    font2.text(text);
    msgContent.addClass("chatInfo");
    msgText.append(font1);
    msgText.append(font2);
    msgContent.append(msgText); 

    $("#chat-header").append(msgContent);
    //  $("#chatInput").val("");
    $("#chat-header").animate({ scrollTop: $("#chat-header")[0].scrollHeight }, "fast");

}

function logMessage(evt) {


    var msgContent = $("<div></div>");
    var msgText = $("<p></p>");

    if (evt.player_id != game.player.player_id) {

        msgContent.addClass("ChatMessage");
        game.chatPopup.infoCircle.visible = true;
    } else {
        msgContent.addClass("ChatMessage1");
          
    }

    // msgContent.css("display","none");
    // var font1 = $("<font style='font-size:1px; color:MediumOrchid;'>"+ evt.USER+ "</font>");
    var font2 = $("<font style='font-size:14px; font-family:Nusxuri'> </font>");
    font2.text(evt.text);
    //  msgText.append(font1)
    msgText.append(font2);
    msgContent.append(msgText);



    $("#chat-header").append(msgContent);
    $("#chatInput").val("");
    $("#chat-header").animate({ scrollTop: $("#chat-header")[0].scrollHeight }, "fast");
    // Game.chat.setMessageText();

}