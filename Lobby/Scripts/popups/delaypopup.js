﻿function DelayPopup(text) {
    createjs.Container.call(this);

    var that = this;
 
  
    that.removePopup = function () {

        if (this.parent) {
            this.parent.removeChild(this);
            game.popup = null;
        }
    }

    that.resizeContainer = function () {
         
        that.y = (brdY) / 2 - 154;  
        that.x = 275;

    }

    function initialize() {
         
        var bgImg = loader.getResult("pwait");
        that.bg = new createjs.Bitmap(bgImg);


        var starImg = loader.getResult("achievement_star");
        that.starImg = new createjs.Bitmap(starImg);
        that.starImg.x = 400;
        that.starImg.y = 80;

        that.text = new createjs.Text(GetTranslatedValue(text), getCorrectFontSize(26) + getCorrectFont(mtavruli_font), "white");
        that.text.textAlign = "center";
        that.text.y = 144;
        that.text.x = 443;

  
        that.addChild(that.bg);
       // that.addChild(that.starImg);
        that.addChild(that.text);
  
        that.alpha = 0;
        var tween = createjs.Tween.get(that)
            .to({ alpha: 1 }, 500)
            .wait(1500)
            .to({ alpha: 0 }, 750)
            .call(function () {
                game.board.removeChild(that);
            });


        game.board.addChild(that);
    }


    initialize();
    that.resizeContainer();



}


DelayPopup.prototype = new createjs.Container();
DelayPopup.prototype.constructor = DelayPopup;