﻿function PasswordPopup()
{
 createjs.Container.call(this);


var that = this;
 
 this.password_popup = true;

//bg
 this.popup_bg = new createjs.Bitmap(loader.getResult("transfer_popup"));
 this.popup_bg.scaleX = 1.05;
 this.popup_bg.x = -20;

this.input_bg = new createjs.Bitmap(loader.getResult("transfer_input"));
this.input_bg.y=57;
this.input_bg.x=40;



this.background = new createjs.Bitmap(loader.getResult("loader_bg"));
this.addChild(that.background);

 
    this.submit_button = new ButtonPassword("button_submit", GetTranslatedValue("enter"));
    this.submit_button.y = 57;
    this.submit_button.x = 365;
 

this.enter_password = new createjs.Text(GetTranslatedValue("enter_password"), getCorrectFontSize(32) + getCorrectFont(mtavruli_font), yellow_color);
 this.enter_password.textAlign ="center";
this.enter_password.x=265;
this.enter_password.y=-60;

this.incorrect_password = new createjs.Text(GetTranslatedValue("incorrect_password"), getCorrectFontSize(28)  + getCorrectFont(mtavruli_font), red_color);
    this.incorrect_password.textAlign = "center";
this.incorrect_password.x = 265;
this.incorrect_password.y=210;
 this.incorrect_password.visible =false;
 

this.submit_button.on("click",function(){
   $("#password").blur();
 that.submit_button.mouseEnabled = false;
 game.engine.password = $("#password").val();
 game.engine.joinRoom();
 
  //that.remove();
});

 


//inert child objects

this.addChild(this.background);
this.addChild(this.popup_bg); 
this.addChild(this.input_bg);
 this.addChild(this.incorrect_password);
 this.addChild(this.enter_password);
this.addChild(this.submit_button);
 
 

stage.addChild(this);
    this.resizeContainer();
}

PasswordPopup.prototype= new createjs.Container();
PasswordPopup.prototype.constructor= PasswordPopup;


PasswordPopup.prototype.resizeContainer=function()
{
   this.scaleX = this.scaleY= scl;  
   this.x= (window.innerWidth-519*scl)/2;
   this.y= (window.innerHeight-185*scl)/2;
   
    $("#password").css("top", this.y+ scl*57 +"px");
    $("#password").css("left", this.x+ scl*45  +"px");

    $("#password").css("width",  scl*273 +"px");
    $("#password").css("height", scl*72  +"px");
    $("#password").css("font-size", scl*30  +"px");

 

   this.background.x= -1*this.x;
   this.background.y= -1*this.y;


   this.background.scaleX =  window.innerWidth/(1910*scl);
   this.background.scaleY = window.innerHeight/ (1080*scl);
  this.background.x = -1 * (this.x/scl);
  this.background.y = -1 * (this.y/scl);


  $("#password").show();
}

PasswordPopup.prototype.setErrorText =function()
{

 this.incorrect_password.visible =true;

 this.submit_button.mouseEnabled = true;
   $("#password").val("");
}

PasswordPopup.prototype.removePopup=function()
{   
  this.remove();
}

PasswordPopup.prototype.remove =function()
{
    if (this.parent) 
    {
    	this.parent.removeChild(this);
    }

    $("#password").hide();
}