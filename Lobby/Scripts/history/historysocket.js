﻿

BGSocket = {
    BgSock: function () {
        var that = this;
        this.connection = null;
    }
}



BGSocket.BgSock.prototype.connect = function () {
    var connection = $.connection(signalRUrl);
    this.connection = connection;
    connection.qs = { 'token': getQueryVariable("token") };
    connection.received(function (data) {
        var dat = JSON.parse(data);
        onResponse(dat);
    });
    connection.start({ transport: ['webSockets'] }).fail(function () {
        game.removePopup();
        game.popup = new WaitPopup("connection_lost", true);
    });

    connection.disconnected(function () {
        if (!error) {
            game.removePopup();
            game.popup = new WaitPopup("connection_lost", true);
        }
    }); 
}

 

 

 
BGSocket.BgSock.prototype.Send = function (command, data) {
    data.command = command;
    this.connection.send(JSON.stringify(data));
}
 

BGSocket.BgSock.prototype.gameInfo = function (game_id) {
    var obj = {};
    obj.game_id = getQueryVariable("game_id");;
    obj.token = getQueryVariable("token");
    this.Send("game_history", obj);
}

BGSocket.BgSock.prototype.roundInfo = function (round_id) {
    var obj = {};
    obj.round_id = round_id;
    this.Send("round_data", obj);
}
 

 
 
 