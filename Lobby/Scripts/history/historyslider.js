﻿function HistorySlider() {
    createjs.Container.call(this);
    var that = this;
    var sizer = 405;
    game.slider = this;


    this.slider_bg = new createjs.Shape();
    this.slider_bg.graphics.beginFill(grey_color).drawRoundRect(0, 0, sizer, 10, 4);
    this.slider_bg.x = 20;
    this.slider_bg.y = 0;
    this.slider_bg.alpha = 0.4;
    that.addChild(this.slider_bg)
    
    this.slider = new createjs.Shape();    
    this.slider.x = 20;
    this.slider.y = 0;
    this.slider.alpha =1.0;
    that.addChild(this.slider);

    this.sliderIcon = new createjs.Bitmap(loader.getResult("slider_circle"));
    this.sliderIcon.regX = 26;
    this.sliderIcon.regY = 26;
    this.sliderIcon.y = this.slider.y+8;
    this.sliderIcon.x = 23;
    this.addChild(this.sliderIcon);

    this.pointer = new createjs.Text("0" + (game.pointer + 1), "22px"+arial_font, "white");
    this.pointer.x = 20;
    this.pointer.y = -40;
    this.addChild(this.pointer);

    this.maxPointer = new createjs.Text("0", "22px"+arial_font, "white");
    this.maxPointer.textAlign = "right";
    this.maxPointer.x = 425;
    this.maxPointer.y = -40;
    this.addChild(this.maxPointer);

    this.setSize = function () {
        this.slider.graphics.clear();
        var moves = 1;
        var pointer = 1;
        if (game.moves.length> 0) {
            moves = game.moves.length;
        }   
        var length = (game.pointer + 1) * (sizer / moves);
        this.slider.graphics.beginFill(yellow_color).drawRoundRect(0, 0, length, 10, 4);
        this.pointer.text = (game.pointer + 1);
        this.maxPointer.text = game.moves.length;
        this.sliderIcon.x = 23 + length;

        if (game.pointer + 1 == game.moves.length) {
            game.engine.finish();
        }
    }
}
HistorySlider.prototype = new createjs.Container();
HistorySlider.prototype.constructor = HistorySlider;