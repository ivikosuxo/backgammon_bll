﻿function HistoryPlayer() {
    createjs.Container.call(this);
     
    var that = this;
    that.sprite = new createjs.Bitmap(loader.getResult("player_bg"));
    that.addChild(that.sprite);

    that.historySelector = new HistorySelector();
    that.historySelector.y = 30;
    that.addChild(that.historySelector);

    that.historySlider = new HistorySlider();
    that.historySlider.y = 170;
    that.addChild(that.historySlider);

    that.historyButtons = new HistoryButtons();
    that.historyButtons.y = 210;
    that.addChild(that.historyButtons);

    this.translate = function () {
        that.historyButtons.translate();
        that.historySelector.translate(); 
    }
    
}
HistoryPlayer.prototype = new createjs.Container();
HistoryPlayer.prototype.constructor = HistoryPlayer;