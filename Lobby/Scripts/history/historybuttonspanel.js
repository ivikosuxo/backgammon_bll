﻿function HistoryButtons() {
    createjs.Container.call(this);

    var that = this;
  
    that.backwardButton = new Button("backward", "", "", "");
    that.backwardButton.x = 20;
    that.addChild(that.backwardButton);

    that.backwardButtonText = new createjs.Text(GetTranslatedValue("back"), getCorrectFontSize(24)  + getCorrectFont(mtavruli_font), "white");
    that.backwardButtonText.alpha = 0.5;
    that.backwardButtonText.textAlign = "center";
    that.backwardButtonText.y = 70;
    that.backwardButtonText.x = 83;
    that.addChild(that.backwardButtonText);


    that.pauseButton = new Button("pause", "", "", "");
    that.pauseButton.x = 162;
    that.pauseButton.visible = false;
    that.addChild(that.pauseButton);

    that.pauseButtonText = new createjs.Text(GetTranslatedValue("pause"), getCorrectFontSize(24)  + getCorrectFont(mtavruli_font), "white");
    that.pauseButtonText.visible = false;
    that.pauseButtonText.alpha = 0.5;
    that.pauseButtonText.textAlign = "center";
    that.pauseButtonText.y = 70;
    that.pauseButtonText.x = 225;
    that.addChild(that.pauseButtonText);

    that.playButton = new Button("play", "", "", "");
    that.playButton.x = 162;
    that.addChild(that.playButton);

    that.playButtonText = new createjs.Text(GetTranslatedValue("play_history"), getCorrectFontSize(24)  + getCorrectFont(mtavruli_font), "white");
    that.playButtonText.alpha = 0.5;
    that.playButtonText.textAlign = "center";
    that.playButtonText.y = 70;
    that.playButtonText.x = 225;
    that.addChild(that.playButtonText);

    that.forwardButton = new Button("forward", "", "", "");
    that.forwardButton.x = 304;
    that.addChild(that.forwardButton);

    that.forwardButtonText = new createjs.Text(GetTranslatedValue("forward"), getCorrectFontSize(24)  + getCorrectFont(mtavruli_font), "white");
    that.forwardButtonText.alpha = 0.5;
    that.forwardButtonText.textAlign = "center";
    that.forwardButtonText.y = 70;
    that.forwardButtonText.x = 367;
    that.addChild(that.forwardButtonText);



    that.playButton.on('click', function () {
        that.play();
    });
    that.stop = function () {
        clearTimeout(game.rollInterval);
        clearTimeout(game.playInterval);

        game.player.StopTimer();
        game.opponent.StopTimer();

        that.pauseButtonText.visible = false;
        that.pauseButton.visible = false;

        that.playButtonText.visible = true;
        that.playButton.visible = true;


    }
    that.play = function () {
        game.player.StopTimer();
        game.opponent.StopTimer();
        clearTimeout(game.rollInterval);
        clearTimeout(game.playInterval);

        if (setBoardState(game.pointer + 1)) {
            
            that.playButtonText.visible = false;
            that.playButton.visible = false;

            that.pauseButtonText.visible = true;
            that.pauseButton.visible = true;
        }
         
    }
    that.pauseButton.on('click', function () {            
        that.stop();
    });

    that.backwardButton.on('click', function () {
        that.stop();
        if (game.pointer>-1) {
            if (game.pointer == 0) {
                game.logic.drawRows();
                game.pointer = -1;
                game.slider.setSize();
            } else if (game.pointer > 0 && game.pointer<game.moves.length) {

                SetBoardCurrentState(game.pointer-1);
            }
        }
    });

    that.forwardButton.on('click', function () {
        that.stop();
        if (game.pointer >= -1 && game.pointer < game.moves.length-1) {           
                SetBoardCurrentState(game.pointer + 1);           
        }
    });


    this.translate = function () {

        that.forwardButtonText.text = GetTranslatedValue("forward");//forward_tr[game.lang];
        that.forwardButtonText.font = getCorrectFontSize(24)+ getCorrectFont(mtavruli_font);
        that.playButtonText.text =  GetTranslatedValue("play_history");// play_tr[game.lang];
        that.playButtonText.font = getCorrectFontSize(24)+ getCorrectFont(mtavruli_font);
        that.backwardButtonText.text = GetTranslatedValue("back");// backward_tr[game.lang];
        that.backwardButtonText.font = getCorrectFontSize(24)+  getCorrectFont(mtavruli_font);
        that.pauseButtonText.text = GetTranslatedValue("pause");// pause_tr[game.lang];
        that.pauseButtonText.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);
    }
}
HistoryButtons.prototype = new createjs.Container();
HistoryButtons.prototype.constructor = HistoryButtons;