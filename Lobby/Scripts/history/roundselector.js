﻿function HistorySelector() {
    createjs.Container.call(this);
    var that = this;
    that.count=1;
  
    that.prevButton = new Button("prev_round", "", "", "");
    that.prevButton.x = 20
    that.addChild(that.prevButton);

    that.currentRoundImage = new createjs.Bitmap(loader.getResult("round_num_bg"));
    that.currentRoundImage.x = 91
    that.addChild(that.currentRoundImage);


    that.currentRoundText = new createjs.Text(GetTranslatedValue("round")+" 1 / 1", getCorrectFontSize(24)+getCorrectFont(mtavruli_font), white_color);
    that.currentRoundText.x = 225
    that.currentRoundText.y = 17
    that.currentRoundText.textAlign = "center";
    that.addChild(that.currentRoundText);

    that.nextButton = new Button("next_round", "", "", "");
    that.nextButton.x = 372;
    that.addChild(that.nextButton);


    that.setRoundsCount = function () {
        that.count = game.game.rounds.length;
        that.currentRoundText.text = GetTranslatedValue("round") + "  "+(game.round_index + 1) + " / " + that.count; 
    }



    that.nextButton.on('click', function () {
        if (game.round_index < that.count - 1) {
            game.round_index = game.round_index+1;
            game.engine.getRoundInfo();
        }
        
    });
    that.prevButton.on('click', function () {
        if (game.round_index > 0) {
            game.round_index = game.round_index-1;
            game.engine.getRoundInfo();
        }

    });


    this.translate = function () {
        that.setRoundsCount();
        that.currentRoundText.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);
    }
  
}
HistorySelector.prototype = new createjs.Container();
HistorySelector.prototype.constructor = HistorySelector;