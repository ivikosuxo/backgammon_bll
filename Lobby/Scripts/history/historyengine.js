﻿ 
function onResponse(event) {
 
    switch (event.command) {
         
        case "connect":
            game.engine.onConnect();
            break; 

        case "game_history":
            game.engine.GameInfo(event);
            game.loadingBar.visible = false;
            stage.visible = true;
            break;

        case "round_data":
            game.engine.RoundInfo(event);

            game.roundPlayer.historyButtons.play();
            break;
             
  
 
   
    }
}

 

Engine = {
    Mediator: function () {
     
        this.connection = {};    
 
        this.initialize();
        this.connection.connect();
    }
}



Engine.Mediator.prototype.onConnect = function () { 
    this.connection.gameInfo();
}

Engine.Mediator.prototype.finish = function () {
    game.removePopup();
    var reason = GetReasonText(game.game.game_info.win_type);
    var last =  (game.round_index+1) == game.game.rounds.length;
    if (game.player.player_id == game.game.rounds[game.round_index].winner_id) {
        game.popup = new WinPopup(reason, last);
    } else {
        game.popup = new LosePopup(reason, last);
    }
   
}



Engine.Mediator.prototype.initialize = function () {

    this.connection = new BGSocket.BgSock(); 
 
 
}

 
Engine.Mediator.prototype.GameInfo = function (evt) {
  
    game.rightPanel.giveup.alpha = 0.5;
    game.rightPanel.giveup.mouseEnabled = false;
    game.rightPanel.raiseCounter.alpha = 0.5;
    game.rightPanel.myPoint.alpha = 0.5;
    game.rightPanel.enPoint.alpha = 0.5;
 
    game.rightPanel.autoSubCont.visible = false;
    game.rightPanel.autoRollCont.visible = false;
    game.rightPanel.giveup.visible = false;
    game.rightPanel.changeSideCont.visible = false;


    game.rightPanel.autoSubCont.alpha = 0.5;
    game.rightPanel.autoSubCont.mouseEnabled = false;

    game.rightPanel.autoRollCont.alpha = 0.5;
    game.rightPanel.autoRollCont.mouseEnabled = false;

 
    game.game = evt;
    game.round_index = 0;
    var players =  evt.players;
    game.rightPanel.setMaxPoints(game.game.game_info.winning_score);
    game.rightPanel.setBetText(game.game.game_info.total_bet/2);
    game.room.type = evt.game_info.game_type;

    game.rightPanel.setRuleText();

    game.player.setName(evt.user_name);
    game.opponent.setName(evt.opponent_name);
    for (var i = 0; i <players.length; i++) {
        if (players[i].me) {
            game.player.player_id = players[i].player_id;
      
            if (players[i].color ==-1) {
                game.player.colors = 2;
            } else {
                game.player.colors = 1;
            }
           
            game.player.setPoint(players[i].score);
        } else {
            if (players[i].color == -1) {
                game.opponent.colors = 2;
            } else {
                game.opponent.colors = 1;
            }
            game.opponent.player_id = players[i].player_id;
            game.opponent.setPoint(players[i].score);
        }
    }
    game.logic = GetLogicByType(game.game.game_info.game_type);
    game.logic.drawRows();
 
    if (game.completed && game.game.rounds.length < 1) {
        //show player not completed popup
    } else {
        this.getRoundInfo();
    }
}

Engine.Mediator.prototype.getRoundInfo = function () {

    this.connection.roundInfo(game.game.rounds[game.round_index].round_id);
}
Engine.Mediator.prototype.RoundInfo = function (evt) {
    game.roundPlayer.historyButtons.stop();
    game.roundPlayer.historySelector.setRoundsCount();
   
    game.moves = evt.moves;
    game.pointer = -1;
    game.logic.drawRows();
    game.slider.setSize();
  /*  setTimeout(function () {
         setBoardState(0);
    }, 500); */
   
}

function setBoardState(index) {

   
    if (game.moves.length > index && index >= 0) {

        game.pointer = index;

    
        var timer = 700;
        var pl = game.player;
        game.board.dicer.x = 735;
        if (game.moves[index].player_id == game.opponent.player_id) {
            pl = game.opponent;
            game.board.dicer.x = 150;

        }


        game.board.firstDice.visible = false;
        game.board.secondDice.visible = false;

        if (game.moves[index].first_roll) {

            game.board.firstDice.visible = true;
            game.board.secondDice.visible = true;

            game.player.StopTimer();
            game.opponent.StopTimer();

            var first = game.moves[index].dice_1;
            var second = game.moves[index].dice_2;

            var big = first > second ? first : second;
            var small = first <second ? first : second;
            if (game.moves[index].player_id == game.player.player_id) { 
                game.board.firstRollHistory(big, small);
            } else {
                game.board.firstRollHistory(small, big);
            } 
 

            timer = 750;
        } else if (game.moves[index].roll) {
        
            game.board.dicer.visible = true;
            game.player.StopTimer();
            game.opponent.StopTimer();

            pl.StartTimer(15, 15, yellow_color);
            game.board.dicer.roll(game.moves[index].dice_1, game.moves[index].dice_2);
            PlaySound("dice1");
            timer = 750;
        }  


        game.rollInterval = setTimeout(function () {
            game.slider.setSize();
            game.board.clearBoard();
            game.board.reconnectBoard(getBoardState(game.moves[index].board_state));

            var kill1 = game.moves[index].my_kill;
            var kill2 = game.moves[index].op_kill;

            var discard1 = game.moves[index].my_discard;
            var discard2 = game.moves[index].op_discard;

            game.player.killedStones.removeKills();
            game.opponent.killedStones.removeKills();

            game.player.discardPanel.removeDiscards();
            game.opponent.discardPanel.removeDiscards();

            if (game.player.colors == 2) {
                kill1 = game.moves[index].op_kill;
                kill2 = game.moves[index].my_kill;

                discard1 = game.moves[index].op_discard;
                discard2 = game.moves[index].my_discard;
            }
            game.player.killedStones.addChips(kill1);
            game.opponent.killedStones.addChips(kill2);

            game.player.discardPanel.addDiscards(discard1);
            game.opponent.discardPanel.addDiscards(discard2);

            if (!game.moves[index].roll) {
                PlaySound("chip");
            }

          
            // setBoardState(game.pointer + 1);
            game.playInterval = setTimeout(function () { setBoardState(game.pointer + 1) }, timer);
        }, timer);

        return true;
    } else {

        game.player.StopTimer();
        game.opponent.StopTimer();
        return false;
    }
}

function SetBoardCurrentState(index) {

 
    if (game.moves.length <= index || index < 0) {
        return; 
    }


   
    game.pointer = index;
    game.slider.setSize();
    game.board.dicer.x = 735;
    if (game.moves[index].player_id == game.opponent.player_id) {
        pl = game.opponent;
        game.board.dicer.x = 150;
    }
    game.board.firstDice.visible = false;
    game.board.secondDice.visible = false;
    if (game.moves[index].first_roll) {
        game.board.firstDice.visible = true;
        game.board.secondDice.visible = true;
        game.board.dicer.visible = false;
       // game.board.firstRollHistory(game.moves[index].dice_1, game.moves[index].dice_2);
    } else { 
        game.board.dicer.visible = true;
        game.board.dicer.setScores(game.moves[index].dice_1, game.moves[index].dice_2);
    }

    game.board.clearBoard();
    game.board.reconnectBoard(getBoardState(game.moves[index].board_state));

    var kill1 = game.moves[index].my_kill;
    var kill2 = game.moves[index].op_kill;

    var discard1 = game.moves[index].my_discard;
    var discard2 = game.moves[index].op_discard;

    game.player.killedStones.removeKills();
    game.opponent.killedStones.removeKills();

    game.player.discardPanel.removeDiscards();
    game.opponent.discardPanel.removeDiscards();

    if (game.player.colors == 2) {
        kill1 = game.moves[index].op_kill;
        kill2 = game.moves[index].my_kill;

        discard1 = game.moves[index].op_discard;
        discard2 = game.moves[index].my_discard;
    }
    game.player.killedStones.addChips(kill1);
    game.opponent.killedStones.addChips(kill2);

    game.player.discardPanel.addDiscards(discard1);
    game.opponent.discardPanel.addDiscards(discard2);

 
}

function  getBoardState(boardString){
    var result = boardString.split(',');
    var board = [];

    for (var i = 0; i < 24 ; i++) {
        var obj = result[i].split('_');
   
;       var row = {};
        row.color = parseInt( obj[0]);
        row.number = parseInt(obj[1]);

        board.push(row);
        
        if (game.player.colors==2) {
            row.color =3 - parseInt(obj[0]);
        }
      
    }

    if (game.player.colors == 2) {

        board.reverse();
    }
    return board;
}

  
Engine.Mediator.prototype.Disconnect = function (evt) {
 
    var text = GetErrorText(evt.id);    
 
    if (evt.fatal) {
        error = true;
        game.removePopup();
        game.popup = new WaitPopup(text, true);
 
        this.connection.ws.close();
    }
 

}


 
 
 
 

 
 