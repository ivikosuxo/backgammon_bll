

var ss = ss || {};
var sss = sss || {};
Backgammon = {
   
    Game: function () {
 
        var that = this;
        this.background = {};
        this.player = {};
        this.opponent = {};
        this.rightPanel = {};
        this.board = {};
        this.bgSocket = {};
        this.stoneIndex = 1;
 
 
        this.engine = {};

        this.room = {};
        this.room.room_id = 0;
        this.game_id = getQueryVariable("game_id");
        this.round_id = 0;
      
        that.popup = null;
        that.settingsPopup = {};
 
        that.lang = getCoockieLang();
  
        that.sound = true;
        that.board2D = false;
        that.reverseBoard = 1;
    

        that.georgian = false;
        that.beaver = false;
        that.crawford = false;
        that.jacob = false;
        that.davi = false;

        that.stoneIndex = 2;
        that.board2dString =""; 

    }
}

Backgammon.Game.prototype.initialize = function (canvas) {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        this.mobile = true;
    }
    this.initializeBoard(canvas);

}

Backgammon.Game.prototype.changeStoneColor=function(index)
{
 

var list = game.board.getBoard();
for (var i = list.length - 1; i >= 0; i--) {
    if (list[i].children && list[i].children.length>0) {
             for (var j = list[i].children.length - 1; j >= 0; j--) {
                   list[i].children[j].changeBG(i<12);
             }
         }
}

game.player.killedStones.stone.changeBG();
game.opponent.killedStones.stone.changeBG();
}

Backgammon.Game.prototype.initializeBoard = function (canvas) {


 
    canv = new createjs.Stage(canvas);

    createjs.Touch.enable(canv);
    canv.enableMouseOver(10);
    canv.mouseMoveOutside = true;
    w = canv.canvas.width;
    h = canv.canvas.height;

    stage = new createjs.Container();
    canv.addChild(stage);
    stage.visible = false;
     
     this.loadingBar = new LoadingBar();
     canv.addChild(this.loadingBar);


   

     for (var i = 0; i <history_manifest.length; i++) {
         var manifestObject = history_manifest[i];
         manifest.push(manifestObject);
     }

     createjs.Ticker.addEventListener("tick", tick);
     createjs.Ticker.setFPS(30);

    loader = new createjs.LoadQueue(false);
    loader.on("progress", handleFileProgress);
    loader.addEventListener("complete", function (evt) { game.handleComplete(evt); });
    loader.loadManifest(manifest, true, "../Content/Images/");

    createjs.Sound.alternateExtensions = ["mp3"];
    createjs.Sound.registerSounds(soundManifest, "../Content/Sounds/");


    canv.on("stagemousemove", function (evt) {

   

    });

    function hideHintsArrows(){
         
    }

    canv.on("stagemouseup", function (evt) {
    });
        
}

 

Backgammon.Game.prototype.initializePanel = function () {
    this.rightPanel = new RightPanel();
    this.roundPlayer = new HistoryPlayer();
    this.roundPlayer.y = 892;
    this.rightPanel.addChild(this.roundPlayer);
   
    stage.addChild(this.rightPanel);


    this.rotateScreen = new RotationScreen();
}

Backgammon.Game.prototype.handleComplete = function () {

    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [loader.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }

    var sssMetadata = library.ssMetadata;
    for (i = 0; i < sssMetadata.length; i++) {
        sss[sssMetadata[i].name] = new createjs.SpriteSheet({ "images": [loader.getResult(sssMetadata[i].name)], "frames": sssMetadata[i].frames })
    }
    

    this.board = new Board();
    this.board.initialize();
    this.initializePlayers();
    this.initializePanel();

    this.settingsPopup = new SettingsPopup();

  
 
    this.chatPopup = new ChatPopup();
    this.chatPopup.visible = false;
    this.settingsPopup.activate();
    this.engine = new Engine.Mediator();
   
    // $("#main-board").css({ 'background-color': '#2A221B' }); 

    resizeListener();
   
}

Backgammon.Game.prototype.removePopup = function ()
{
    if (this.popup) {
        this.popup.removePopup();
       this.popup = null;
    }

}


Backgammon.Game.prototype.initializePlayers = function () {
    this.player = new Player(1, "");
    this.opponent = new Player(2, "");

    this.board.addChild(this.player);
    this.board.addChild(this.opponent);
}
Backgammon.Game.prototype.translate = function () {

    try { 
        game.settingsPopup.translate();
        game.roundPlayer.translate();
        game.rightPanel.translatePanel();
        game.chatPopup.translate()
    } catch (exc) {
        console.log(exc);
    }
   
}


function handleFileProgress() {
 
  game.loadingBar.loading(loader.progress* 100);
}

function tick(event) {

    if (update) {
        update = true;
        canv.update(event);
    }
}

 Backgammon.Game.prototype.playSoundGlobal=function()
     {
        try{ 
                if (game.player.audio.sound) {
                      createjs.Sound.play("turn");
                }   
            }catch(ex)
            {
                 //   if (!createjs.Sound.initializeDefaultPlugins()) {return;}
                console.log("can't load sound")
            }      


    }

 function PlaySound(name) {

     if (game.sound) {
         createjs.Sound.play(name);
     }
 }