﻿
 
BGSocket = {
    BgSock: function () {
        var that = this; 
        this.connection = null;
    }
}



BGSocket.BgSock.prototype.connect = function () {

    var connection = $.connection(signalRUrl);
    this.connection = connection;
    connection.qs = { 'token': getQueryVariable("token") };
    connection.received(function (data) {
        var dat = JSON.parse(data);
        onResponse(dat);
    });

    connection.start({ transport: ['webSockets'] }).fail(function () {
        game.removePopup();
        game.popup = new WaitPopup("connection_lost", true);
    });

    connection.disconnected( function () {
        if (!error) {
            game.removePopup();
            game.popup = new WaitPopup("connection_lost", true);
        } 
    }); 
 
}



BGSocket.BgSock.prototype.joinRoom = function (roomId, password) {
    var obj = {};
    obj.room_id = roomId;
    obj.password = password;
    if (game.mobile) {
        obj.mobile = true;
    }
    this.Send("join_room", obj);
}

BGSocket.BgSock.prototype.addWaiting = function (roomId) {
    var obj = {};
    obj.room_id = roomId;    
    this.Send("waiting_add", obj);
}
BGSocket.BgSock.prototype.logIn = function () {
    var obj = {};
    var token = getQueryVariable("token");
    obj.token = token;
    this.Send("login", obj);

}

BGSocket.BgSock.prototype.Send = function (command, data) {
    data.command = command;

    this.connection.send(JSON.stringify(data));
}

BGSocket.BgSock.prototype.Submit = function () {
    var send = {};
  
    this.Send("submit", send);
}

BGSocket.BgSock.prototype.sendChatText = function (text) {
    var send = {};
    send.text = text;
    
    this.Send("chat", send);
}

BGSocket.BgSock.prototype.changeStoneColor = function (color) {
    var send = {};
    send.color = color;
    this.Send("change_stone", send);
}

BGSocket.BgSock.prototype.preJoinRoom = function (RMid) {
    var obj = {};
    obj.room_id = RMid;
    this.Send("pre_join_room", obj);
}

BGSocket.BgSock.prototype.Roll = function () {
    var obj = {};
 
    this.Send("roll", obj);
}

 
BGSocket.BgSock.prototype.play = function (action, startpos,endpos,index,mute) {
    var obj = {};
    obj.action = action;
    obj.index = index;
    obj.end = endpos;
    obj.start = startpos;
    obj.mute = false
    if (mute) {
        obj.mute = true;
    }
    
    this.Send("play", obj);
}
BGSocket.BgSock.prototype.playSeveralMoves = function ( startpos, endposArr) {
    var obj = {};
    
    obj.end_positions = endposArr;
    obj.start = startpos;
    this.Send("play_several_moves", obj);
}

BGSocket.BgSock.prototype.acceptWaiter = function ( accept) {
    var obj = {}; 
    obj.accept = accept; 
    this.Send("waiting_accept", obj);
}



BGSocket.BgSock.prototype.undo = function ()
{
    var obj = {};
    this.Send("undo", obj);

}

BGSocket.BgSock.prototype.surrender = function (result) {
    var obj = {};
    obj.game = result;
    this.Send("give_up", obj);

}

BGSocket.BgSock.prototype.raise = function () {
    var obj = {};
    this.Send("raise", obj);

}

BGSocket.BgSock.prototype.raiseAnswer = function (value) {
    var obj = {};
    obj.answer = value;
    this.Send("raise_answer", obj);

}



BGSocket.BgSock.prototype.PlayAgain = function () {
    var obj = {};
    this.Send("play_again", obj);

}

BGSocket.BgSock.prototype.doubleCheck = function (ready) {
    var obj = {};
    if (ready) {
        obj.ready=ready;
        this.Send("double_check", obj);
    }
}

 