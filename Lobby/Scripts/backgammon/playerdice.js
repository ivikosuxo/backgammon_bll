function Dicer()
{
	createjs.MovieClip.call(this);

   //this.scaleX=this.scaleY=1.5;
   var mc  = new lib.WhiteDiceFLA();
   this.mc= mc;
 var biggerDice;
 var smallerDice;
 var _onDropComplete;
 var enterFrameHandler;

 this.dice1= function()
 {
    return biggerDice+1;

 }

  this.dice2= function()
 {
   return smallerDice+1;
 }


 this.setAlphaBig = function(alp)
 {
     mc.dots1.alpha = alp;
        
 }


 this.SetFullAlpha = function (list)
 {
     this.setAlphaBig(1);
     this.setAlphaSmall(1);
     if (list.length==0) {
         this.setAlphaBig(0.65);
         this.setAlphaSmall(0.65);
         return;
     }
     if (list.length==1) {
         if (this.dice2() == list[0]) {
             this.setAlphaBig(0.65);
         } else {
             this.setAlphaSmall(0.65);
         }
     }
     if ( list.length == 2) {
         if (list[0] == list[1]) {
             this.setAlphaBig(0.65);
         }  
     }
      
 }


this.setOpacity = function(val, isDbl)
{

       if (isDbl) {
           this.setAlphaBig(0.65);
        } else  {
            if (this.dice1()==val) {

                this.setAlphaSmall(0.65);
           }

          if (this.dice2()==val) {
              this.setAlphaBig(0.65);
            }
            
        }

}

 this.setAlphaSmall = function(alp)
 {
         
         mc.dots2.alpha=alp;
 }

  
         mc.gotoAndStop(0);
         mc.visible = false;
         this.addChild(mc);
 
      
         this.roll = function (param1, param2, param3)
         {
         this.visible = true;
         mc.tickEnabled = true;
         mc.dots1.alpha = 1.0;
         mc.dots2.alpha = 1.0;    
         mc.visible = true;
         biggerDice = param1 - 1;
         smallerDice = param2 - 1;
         _onDropComplete = param3;
         mc.gotoAndPlay(0);        
         }

       
      
       function dispose() 
      {
           mc.tickEnabled = false;
          mc = null;
         _onDropComplete = null;
      }
      
      function onEnterFrame(param1) 
      {
         
         if(mc.currentFrame == 17)
         {
             setScores(biggerDice, smallerDice);
         }
         else if(mc.currentFrame == 25)
         {
            // setScores(biggerDice, smallerDice);
            mc.stop();
            mc.tickEnabled = false;
            if(_onDropComplete != null)
            {
               _onDropComplete.call(this);
            }
            
         }
      }
      
      function setScores(param1, param2) 
      {
        
           var _loc3_ = mc.dots1;
           var _loc4_ = mc.dots2;
           _loc3_.gotoAndStop(param1);
           _loc4_.gotoAndStop(param2);

 
      }
    
      this.setScores = function (param1, param2,param3) {

          this.visible = true;
          mc.tickEnabled = true;
          mc.dots1.alpha = 1.0;
          mc.dots2.alpha = 1.0;
          mc.visible = true;
          biggerDice = param1 - 1;
          smallerDice = param2 - 1;
          _onDropComplete = param3;
          setScores(biggerDice, smallerDice);
          mc.gotoAndStop(25);
          setScores(biggerDice, smallerDice);
          
      }
      enterFrameHandler = mc.on("tick", onEnterFrame);;
}

Dicer.prototype = new createjs.MovieClip();
Dicer.prototype.constructor= Dicer;



