﻿var transformX = [2, 2, 1, 1, 0, 0, 0, 0, -1, -1, -2, -2, 2, 2, 1, 1, 0, 0, 0, 0, -1, -1, -2, -2];
var transformX2d = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


GameLogic.KhachapuriRule.prototype.drawRows = function () {

    var board = game.board.getBoard();
    game.board.clearBoard();
    board[0].addChips(15, 1);
    board[12].addChips(15, 2);
}


GameLogic.KhachapuriRule.prototype.rowClick = function (rowObj) {

    var activePl = game.player;
    if (game.opponent.turn) {
        return;
    }
    if (activePl.turn && rowObj.getColor() === activePl.color && !this.isBlocked(rowObj)) {
        if (activePl.state === 0) {
            this.movingState(activePl, rowObj);
        } else if (activePl.state === 1) {
            this.reviveState(activePl.color);
        } else if (activePl.state === 2) {
            this.discardState(activePl, rowObj);
        }
    }
}

GameLogic.KhachapuriRule.prototype.getStoneCount = function () {
    return 15;
}

GameLogic.KhachapuriRule.prototype.isBlocked = function (chipRow) {
    return false;
}


GameLogic.KhachapuriRule.prototype.NextRaise = function (raise) {
    return raise + 1;
}

GameLogic.KhachapuriRule.prototype.giveUpPoint = function () {

    if (game.player.discardPanel.count > 0) {
        return "oin";
    }

    return "mars";  
}

GameLogic.KhachapuriRule.prototype.freezeCount = function (freeze, index) {

    return false;
}

GameLogic.KhachapuriRule.prototype.roll = function (event) {

    var containers = game.board.getBoard();
 

    for (var i = 0; i < 24; i++) {
        containers[i].blocked = false;
    }
}


GameLogic.KhachapuriRule.prototype.movingState = function (activePl, rowObj) {


    var containers = game.board.getBoard();
    var color = rowObj.getColor();


    var result = this.canMove(activePl.color, rowObj.index, activePl.movesList);




    if (result.result) {
        var stone = new Stone(color);
        rowObj.removeChip();
        if (containers[result.pos].killing(color)) {

            if (result.pos > 17 && color === 1) {
                containers[result.pos].blocked = true;
            }
            activePl.getOpponent().state = 1;
            activePl.getOpponent().killedStones.addKilledStone();
            containers[result.pos].removeChip();

        }
        containers[result.pos].addChip(stone);

        activePl.spliceMove(result.index);

        if (activePl.state !== 2 && activePl.isDiscardState(result.pos)) {
            activePl.state = 2;
        }
        if (activePl.movesList.length === 0) //!canPlaySomething(activePl)
        {
            // changeTurn(activePl);

        }

        game.engine.play(0, rowObj.index, result.pos, result.index);
    }

}


GameLogic.KhachapuriRule.prototype.movingStateDrag = function (index, moveIndex, rowObj, mute) {

    var state = game.player.state;
    if (state === 2) {
        state = 0;
    }

    this.movePlay(index, moveIndex, rowObj, game.player);

    game.engine.play(state, rowObj.index, index, moveIndex, mute);

}

GameLogic.KhachapuriRule.prototype.movePlay = function (endPos, index, rowObj, player) {


    var activePl = player;
    var containers = game.board.getBoard();
    var color = activePl.color;



    rowObj.removeChip();
    var stone = new Stone(color);
    containers[endPos].addChip(stone);

    activePl.spliceMove(index);

    if (activePl.state !== 2 && activePl.isDiscardState(endPos)) {

        activePl.state = 2;

    }
    if (activePl.movesList.length === 0) //!canPlaySomething(activePl)
    {
        //changeTurn(activePl);

    }



}



GameLogic.KhachapuriRule.prototype.reviveState = function (color) {

    var activePl = game.player;

    if (activePl.color === color) {
        var containers = game.board.getBoard();


        var startPosition = -1;
        if (color === 2) {
            startPosition = 24;
        }


        var result = this.canMove(color, startPosition, activePl.movesList);

        if (result.result) {

            var isEmpty = activePl.killedStones.removeKilledStone();
            var stone = new Stone(color);
            if (containers[result.pos].killing(color)) {

                activePl.getOpponent().state = 1;
                activePl.getOpponent().killedStones.addKilledStone();
                containers[result.pos].removeChip();
            }
            containers[result.pos].addChip(stone);
            activePl.spliceMove(result.index);
            game.engine.play(1, startPosition, result.pos, result.index);
            if (isEmpty) {
                activePl.state = 0;
            }
            if (activePl.movesList.length === 0) // !canPlaySomething(activePl)
            {
                //  changeTurn(activePl);

            }
        } else {
            changeTurn(activePl);
        }

    }


}

GameLogic.KhachapuriRule.prototype.canMoveOnPosition = function (rowObj, color, blocked) {

    if (blocked) {
        return false;
    }

    if (rowObj.numChildren == 0 || rowObj.getColor() === color) {
        return true;
    }

    return false;

}

GameLogic.KhachapuriRule.prototype.notBlocked = function (clr, pos, killing) {

    return !killing;
}

GameLogic.KhachapuriRule.prototype.killing = function (row, color) {


    return false;
}

GameLogic.KhachapuriRule.prototype.canPlaySixCards = function (position, start, color) {


    return true;

    var pl = game.player;
    var brs = game.board.getBoard();

    if (position > 17 || pl.gameState === 2) {
        return true;
    }

    if (position < 24 && brs[position].getColor() === color) {

        return true;
    }

    var backCount = 0;
    for (var i = 1; i < 7; i++) {
        var pos = (position - i) % 24;

        if ((pos > -1 && pos < 24) && brs[pos].getColor() === color && (!(pos == start && brs[start].numChildren == 1))) {
            backCount++;
        } else {
            break;
        }
    }
    var forCount = 0;
    for (var i = 1; i < 7; i++) {
        var pos = (position + i) % 24;
        if ((pos > -1 && pos < 24) && brs[pos].getColor() === color && (!(pos == start && brs[start].numChildren == 1))) {
            forCount++;
        }
        else {
            break;
        }
    }



    return (backCount + 1 + forCount) < 6;
}




GameLogic.KhachapuriRule.prototype.canMove = function (color, startPosition, list) {
    var obj = {};
    var containers = game.board.getBoard();
    obj.result = false;
    obj.index = -1;
    var coeficient = 1;
    if (color === 2) {
        coeficient = -1;
    }

    if (startPosition < 0 && startPosition > 24) {
        if ((containers[0].blocked)) {
            return obj;
        }
    }

    if (game.leftMouse) {


        for (var i = 0; i < list.length; i++) {
            var pos = startPosition + coeficient * list[i];
            if (pos > -1 && pos < 24) {


                if ((containers[pos].numChildren == 0 || containers[pos].getColor() == color) && this.canPlaySixCards(pos, startPosition, color)) {
                    obj.result = true;
                    obj.index = i;
                    obj.pos = pos;
                    return obj;
                }
            }
        }
    } else {

        for (var j = list.length - 1; j >= 0; j--) {
            var pos = startPosition + coeficient * list[j];
            if (pos > -1 && pos < 24) {


                if ((containers[pos].numChildren == 0 || containers[pos].getColor() == color) && this.canPlaySixCards(pos, startPosition, color)) {
                    obj.result = true;
                    obj.index = j;
                    obj.pos = pos;
                    return obj;
                }
            }
        }



    }
    return obj;
}





GameLogic.KhachapuriRule.prototype.canPlaySomething = function (activePl) {
    var color = activePl.color;
    var startingPosition = -1;
    if (color === 2) {
        startingPosition = 24;
    }
    if (activePl.state === 0) {
        return this.canPlayMoveState(activePl);

    } else if (activePl.state === 1) {
        var result = this.canMove(color, startingPosition, activePl.movesList);
        return result.result;
    } else if (activePl.state === 2) {

        var candisc = this.canPlayDiscard(activePl);


        if (candisc.result) {
            return true;
        }


        var res = this.canPlayMoveState(activePl);

        return res;




    }
    return false;
}

GameLogic.KhachapuriRule.prototype.canPlayMoveState = function (activePl) {
    var color = activePl.color;
    var arr = game.board.getBoard();
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].getColor() === color) {
            var result = this.canMove(color, i, activePl.movesList);
            if (result.result) {

                return true;
            }
        }
    }

    return false;

}

GameLogic.KhachapuriRule.prototype.canPlayDiscard = function (activePl) {
    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1) {
        startPos = 24;
        desc = -1;
    }
    var board = game.board.getBoard();

    for (var i = 0; i < activePl.movesList.length; i++) {
        var posit = startPos + desc * activePl.movesList[i];
        if (board[posit].getColor() === activePl.color) {// && !board[posit].blocked
            res.result = true;
            res.index = i;
            return res;
        }

        if (this.canDiscardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i])) {
            res.result = true;
            res.index = i;
            return res;
        }
    }




    res.result = false;
    res.index = -1;
    return res;
}


GameLogic.KhachapuriRule.prototype.canDiscardWithoutMatch = function (board, color, startPos, desc, value) {


    var index = this.getDiscardIndex(startPos);


    if (value === 6) {


        return true;

    }

    if (index === value) {
        return true;
    }
    if (index > value) {
        return false;
    }


    for (var j = value; j < 7; j++) {

        if (board[startPos + desc * j].getColor() === color) {
            return false;
        }

    }
    return true;
}
GameLogic.KhachapuriRule.prototype.discardWithoutMatch = function (board, color, startPos, desc, value, rowobjIndex) {
    var ind = this.getDiscardIndex(rowobjIndex);

    if (ind > value) {
        return false;
    }
    if (ind === value) {
        return true;
    }

    var pointer = 6 - ind;
    for (var j = 1; j < 6; j++) {
        var posit = rowobjIndex + desc * j;


        if (board[posit].getColor() === color) {
            return false;
        }

    }
    return true;
}

GameLogic.KhachapuriRule.prototype.getDiscardIndex = function (number) {

    if (number < 6) {
        return number + 1;
    } else if (number > 17) {
        return 24 - number;
    }
}

GameLogic.KhachapuriRule.prototype.discardState = function (activePl, rowObj) {
    var result = this.discardResultFactory(activePl, rowObj);
    if (result.result) {
        this.discardPlay(activePl, rowObj, result.index);

        if (activePl.movesList.length === 0) // !canPlaySomething(activePl)
        {
            if (activePl.discardPanel.count === 15) {

            } else {

                //    changeTurn(activePl);
            }

        }

        game.engine.play(2, rowObj.index, 24, result.index);

    } else {

        this.movingState(activePl, rowObj);

    }
}

GameLogic.KhachapuriRule.prototype.discardPlay = function (activePl, rowObj, index) {
    rowObj.removeChip();
    activePl.discardPanel.addDiscard();
    activePl.spliceMove(index);

    if (activePl.movesList.length === 0 || activePl.discardPanel.count === 15) //|| !canPlaySomething(activePl) 
    {
        // changeTurn(activePl);

    }

}

GameLogic.KhachapuriRule.prototype.discardResultFactory = function (activePl, rowObj) {

    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1) {
        startPos = 24;
        desc = -1;
    }
    res.index = -1;
    res.result = false;
    var board = game.board.getBoard();
    for (var i = 0; i < activePl.movesList.length; i++) {

        if (this.discardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i], rowObj.index)) {
            res.index = i;
            res.result = true;
            return res;
        }
    }
    return res;
}






GameLogic.KhachapuriRule.prototype.getCorrectPosition = function (startPosition) {

    return (12 + startPosition) % 24;

}



GameLogic.KhachapuriRule.prototype.PlayUndo = function (activePl, startPosition, endPosition, value, enemyState, state, killer, action, freeze) {
    var opp = activePl.getOpponent();
    var cont = game.board.getBoard();


    if (action === 0) {
 

        cont[endPosition].removeChip();
        cont[startPosition].addChip(new Stone(activePl.color));


    } else if (action === 2) {

        activePl.discardPanel.removeDiscard();
        cont[startPosition].addChip(new Stone(activePl.color));

    }




    opp.state = enemyState;
    activePl.state = state;


    game.board.blurDices();

    activePl.movesList.push(value);
    activePl.movesList.sort();
}

GameLogic.KhachapuriRule.prototype.calculatePips = function (pl) {
    var counter = 0;
    var container = game.board.getBoard();
    for (var i = 0; i < 24; i++) {
        if (container[i].getColor() === pl.color) {
            if (pl.color === 1) {
                counter += container[i].numChildren * (24 - i);
            } else {
                counter += container[i].numChildren * (24 - ((12 + i) % 24));
            }

        }

    }



    pl.setPips(counter);

    return counter;


}

