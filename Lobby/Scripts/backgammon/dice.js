﻿function Dice()
{
    createjs.MovieClip.call(this);

    //this.scaleX=this.scaleY=1.5;
    var mc = new library.WhiteDiceFLA();
    this.mc = mc;
    var diceValue = 1;
    this.addChild(mc);
    this.rollDice = function (param1,param2) {

        mc.tickEnabled = true;
        mc.dot.alpha = 1.0; 
        mc.visible = true;
        diceValue = param1 - 1; 
        mc.gotoAndPlay(0);
        _onDropComplete = param2;
    }


    function onEnterFrame(param1) {

        if (mc.currentFrame == 18) {
            setScores(diceValue);
        }
        else if (mc.currentFrame == 25) {
            // setScores(biggerDice, smallerDice);
            mc.stop();
            mc.tickEnabled = false;
            if (_onDropComplete != null) {
                _onDropComplete.call(this);
            }

        }
    }

    function setScores(param1) {   

       
        mc.dot.gotoAndStop(param1);
         
    }
    enterFrameHandler = mc.on("tick", onEnterFrame);
}

Dice.prototype = new createjs.MovieClip();
Dice.prototype.constructor = Dice;