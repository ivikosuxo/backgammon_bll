function Board() {
    createjs.Container.call(this);
    this.x = 275;
    this.y = 42;

  
    var firstDice = new Dice();
    this.firstDice = firstDice;
    this.firstDice.y = 550;
    firstDice.x = 900;

     
    var secondDice = new Dice();
    this.secondDice = secondDice;
    this.secondDice.x = 300;
    this.secondDice.y = 550;
    secondDice.skewX = 180;

    this.discards_you = {};
    this.discards_op = {};
    this.roller = {};
    this.undo = {};
    this.submit = {};

 
    var that = this;
    var bg;
    var containers = [];

    that.stick1 = {};
    that.stick2 = {};

    that.colorIndex = "";

 


    var beaver = false;
    var crawford = false;
    var jacob = false;
    var dicer ;
    that.dicer = dicer;
    that.firstDice = firstDice;
    that.secondDice = secondDice;
 

 

that.resizeContainer = function()
{
    var sclaler = scl;
    if (game.mobile) {
        sclaler = scl * 1.09;
    }
     that.scaleX = that.scaleY = sclaler;  
     game.board.x = (window.innerWidth - (brdX * sclaler)+(rghtX*scl) )/2;

     game.board.y = (window.innerHeight * 3) / 100;
     if (game.mobile) {
         game.player.y = 1112;
     }
     

}
that.firstRollHistory = function (random1, random2) {

    dicer.visible = false;
    that.addChild(firstDice);
    that.addChild(secondDice); 

    PlaySound("dice1");


    firstDice.rollDice(random1); 
    secondDice.rollDice(random2);  
 
}

 

    that.firstRoll = function (random1, random2, r1, r2) {

        dicer.visible = false;
        that.addChild(firstDice);
        that.addChild(secondDice);
     
   
        PlaySound("dice1");
      

        firstDice.rollDice(random1);

        secondDice.rollDice(random2);

    
        setTimeout(function () {
            game.firstRoll =false; 
        
            that.removeChild(firstDice);
            that.removeChild(secondDice);
            
        },1500);
    }


    that.reconnectDices = function (r1, r2) {


        var activePl = game.player;
        dicer.x = 150;

        if (game.opponent.turn) {
            activePl = game.opponent;
            dicer.x=735;
        }

 
     
       
        dicer.roll(r1,r2);

    }

    that.historyDice = function (first ,second) {
        dicer.setParams(first, second);
    }

    that.setAlpha = function (val, isDbl) {
        dicer.setOpacity(val, isDbl);
    }

    that.setAlphaBoth = function () {
        dicer.setAlphaBig(0.65);
        dicer.setAlphaSmall(0.65);
    }
    this.canPlayDoubleFromStart= function() {
        return (dicer.dice1() ===dicer.dice2()) && (dicer.dice1()  === 3 || dicer.dice1()  === 4 || dicer.dice1()  === 6);

    }

    this.getBoard = function () {
        return containers;
    }

    that.showAllChips =function()
{

    for (var i = containers.length - 1; i >= 0; i--) {
        containers[i].showChip();
    }

     game.player.killedStones.showChip();
     game.opponent.killedStones.showChip();
}

    that.setPosition = function () {

        var activePl = game.player;
        dicer.x = 800;
        dicer.mc.skewX = 0;
        dicer.y = 500;
        if (game.opponent.turn) {
            dicer.x = 200;
            dicer.mc.skewX = 180;
            dicer.y = 620;
            activePl = game.opponent;
        }
        return activePl;

    }

    that.rollDice = function (random1, random2) {

         
        var activePl = that.setPosition();
        activePl.movesList = [];
 
      //dice1.rollDice(random1, activePl.color,true);
      //  dice2.rollDice(random2, activePl.color);

        dicer.rotation = 10 * Math.random();
        PlaySound("dice1");
        dicer.roll(random1,random2);
         
    }



    that.blurDices = function () {
         dicer.setAlphaBig(1.0);
        dicer.setAlphaSmall(1.0);
    }

    this.getDice1 = function () {
        return dice1;
    }
     
    that.initialize= function() {

      //  var boardImg = loader.getResult(that.colorIndex + "board");
        bg = new createjs.Bitmap( );
        that.addChild(bg);


        for (var j = 0; j < 24; j++) {
            var cont = new ChipsRow(j);
            containers.push(cont);
            that.addChild(cont);
        }
 
      dicer = new Dicer();
      this.dicer=dicer;
      dicer.y=500;
      that.addChild(dicer);

 


//that.dice1= dice1;
//that.dice2 = dice2;
       



        var stick1 = loader.getResult(that.colorIndex + "stick1");
        that.stick1 = new createjs.Bitmap(stick1);
        that.stick1.x = 49;
        that.stick1.y = 465;
     //   that.addChild(that.stick1);

        var stick2 = loader.getResult(that.colorIndex + "stick2");
        that.stick2 = new createjs.Bitmap(stick2);
        that.stick2.x = 370;
        that.stick2.y = 465;
  //      that.addChild(that.stick2);

        that.stick2.visible = false;
        that.stick1.visible = false;


        that.roller = new Roller();

        that.addChild(that.roller);

        initializeUndo();

        game.hint1 = new Hint();
        game.hint2= new Hint();
        game.hint3 = new Hint();
        game.hint4 = new Hint();
        game.hint5 = new Hint();
        game.hint1.visible = false; 
        game.hint2.visible = false; 
        game.hint3.visible = false; 
        game.hint4.visible = false; 
        game.hint5.visible = false; 
        that.addChild( game.hint1);
        that.addChild( game.hint2);
        that.addChild( game.hint3);
        that.addChild( game.hint4);
        that.addChild( game.hint5);
    }

 


    function initializeUndo() {
        if (game.mobile) {
          
            that.undo = new Button("mobile_undo", "", null, "white");
            that.undo.x = 1287;
            that.undo.y = 404; 
        } else {

            that.undo = new Button("button_grey", "undo" , "undo_icon", "white");

            that.undo.x = 409;
            that.undo.y = 1114;

        }
   
     /*   var undoImg = loader.getResult("undo");
        that.undo1 = new createjs.Bitmap(undoImg);
        that.undo.addChild(that.undo1);


        var undoImg1 = loader.getResult("undo_icon");
        that.undo2 = new createjs.Bitmap(undoImg1);
        that.undo2.x=45;
        that.undo2.y=18;
        that.undo.addChild(that.undo2);



            that.undoTxt = new createjs.Text( undo_tr[game.lang], "26px Mtavruli", "black");
            that.undoTxt.textAlign= "center";
            that.undoTxt.x = 160;
            that.undoTxt.y = 12;
            that.undo.addChild(that.undoTxt);

            */

       


        that.undo.visible = false;
        that.addChild(that.undo);
        that.undo.cursor = "pointer";
        that.undo.alpha = 0.8;
        that.undo.on('click', function () {
            that.submit.mouseEnabled = false;
            game.board.submit.alpha = 0.3;
            game.engine.undo();
        });
 /* 
        that.undo.on('mouseover', function () {
            that.undo1.image= loader.getResult("undoh");
    
        });

       that.undo.on('mouseout', function () {
            that.undo1.image= loader.getResult("undo");
    
        }); */


 

 

        if (game.mobile) {
            that.submit = new Button("mobile_submit", "", null, "black");
            that.submit.x = 1287;
            that.submit.y = 610; 
        } else {
            that.submit = new Button("button_yellow", "submit" , "submit_icon", "black");
            that.submit.x = 737;
            that.submit.y = 1114;
        }
      

      /*  var submitImg = loader.getResult("submit");
        that.submit1 = new createjs.Bitmap(submitImg);
        that.submit.addChild(that.submit1);

        var submitImg = loader.getResult("submit_icon");
        that.submit2 = new createjs.Bitmap(submitImg);
        that.submit2.x=45;
        that.submit2.y=18;
        that.submit.addChild(that.submit2);



          that.submitTxt = new createjs.Text(submit_tr[game.lang], "26px Mtavruli", "black");
          that.submitTxt.textAlign= "center";
            that.submitTxt.x = 160;
            that.submitTxt.y =12;
            that.submit.addChild(that.submitTxt);

            */


       
        that.submit.mouseEnabled = false;
        that.submit.visible = false;
        that.addChild(that.submit);




        that.submit.cursor = "pointer";
        that.submit.on('click', function () {
            that.submit.mouseEnabled = false;
            game.board.turnOffUndo();
            game.engine.Submit();
        });

    /*    that.submit.on('mouseover', function () {
            that.submit1.image= loader.getResult("submith");
    
        });

       that.submit.on('mouseout', function () {
            that.submit1.image= loader.getResult("submit");
    
        }); */

 
 



    }

  that.playUndo = function(value,activePl)
  {
    if (dicer.dice1()===dicer.dice2()) 
    {
          var size =  activePl.movesList.length;
          if (size>2) {
              dicer.setAlphaSmall( 1.0);
              dicer.setAlphaBig( 1.0);     
          }else{
             dicer.setAlphaSmall( 1.0);
             dicer.setAlphaBig( 0.65);
          }
         
     }else{
          var size =  activePl.movesList.length;
           if (dicer.dice1() ===value) 
          {
               dicer.setAlphaBig( 1.0);
                     
          }else
          {
               dicer.setAlphaSmall( 1.0);  
          }  

          if (size==2) {
             dicer.setAlphaSmall( 1.0);
             dicer.setAlphaBig( 1.0);   
          }

     }

  }

  that.setAlphaToDices = function (activePl) {
      var size = activePl.movesList.length;
      if (size==0) {
          dicer.setAlphaSmall(0.65);
          dicer.setAlphaBig(0.65);
          return;
      }

      if (dicer.dice1() === dicer.dice2()) {
        
          if (size > 2) {
              dicer.setAlphaSmall(1.0);
              dicer.setAlphaBig(1.0);
          } else {
              dicer.setAlphaSmall(1.0);
              dicer.setAlphaBig(0.65);
          }

      } else {

          if (size == 2) {
              dicer.setAlphaSmall(1.0);
              dicer.setAlphaBig(1.0);
              return;
          }

          
          if (dicer.dice1() === activePl.movesList[0]) {
              dicer.setAlphaBig(1.0);
              dicer.setAlphaSmall(0.65);

          } else {
              dicer.setAlphaBig(0.65);
              dicer.setAlphaSmall(1.0);
          }

       
      }

    }

    stage.addChild(that);



    that.showUndo = function () {
        that.submit.mouseEnabled = false;
        that.undo.mouseEnabled = false;
        that.submit.visible = true;
        that.undo.visible = true;

        that.undo.alpha = 0.4;
        that.submit.alpha = 0.3;

        that.roller.visible = false;
        that.roller.mouseEnabled = false;
        that.roller.alpha = 0.3;
    }


    that.lightUndo = function (last) {
        that.submit.mouseEnabled = last;
        that.undo.mouseEnabled = true;

        if (last) {
            that.submit.alpha = 1;
        } else {
            that.submit.alpha = 0.3;
        }
           that.submit.visible=true;
     
           that.undo.visible=true;
           that.undo.alpha = 1;
    }
   
    that.showRoller = function() {
        

        that.roller.visible = true;
        that.roller.mouseEnabled = true;
        that.roller.alpha = 1.0;

    }

    that.turnOffUndo = function () {
        that.submit.mouseEnabled = false;
        that.undo.mouseEnabled = false;
        that.submit.visible = false;
        that.undo.visible = false;

        that.undo.alpha = 0.4;
        that.submit.alpha = 0.3;

        that.roller.visible = true;
        that.roller.mouseEnabled = false;
        that.roller.alpha = 0.3;
    }


    that.emptyBoard = function () {
        game.player.discardPanel.removeDiscards();
        game.player.killedStones.removeKills();

        game.opponent.killedStones.removeKills();
        game.opponent.discardPanel.removeDiscards();

        that.removeStones();
    }

    that.clearBoard = function () {

       
        that.roller.visible = false;
        that.undo.visible = false;

         
            game.player.movesList = [];
      
            game.player.turn = false;
            game.player.state = 0;
            game.player.discardPanel.removeDiscards();
            game.player.killedStones.removeKills(); 
 
            game.opponent.movesList = [];           
            game.opponent.turn = false;
            game.opponent.state = 0;
            game.opponent.killedStones.removeKills();
            game.opponent.discardPanel.removeDiscards();
    

        for (var i = 0; i < 24; i++) {
            containers[i].removeAllChildren();
        }
    }

    that.removeStones = function () {
        for (var i = 0; i < 24; i++) {
            containers[i].removeAllChildren();
        }
    }

    that.reconnectBoard = function ( board) {
        
            for (var i = 0; i < 24; i++) {
                if (board[i].number !== 0) {
                    containers[i].addChips(board[i].number, board[i].color);
                    containers[i].blocked = board[i].block;
                    containers[i].freezeCount = board[i].freeze_count;
                }
            }     

    }


    that.changeBoardType = function () {

        var conts = that.getBoard();
 
            game.player.killedStones.y=585;
            game.opponent.killedStones.y=450;
            game.board2D = true;
  
            var boardImg = loader.getResult(that.colorIndex + "board");
            bg.image = boardImg;
 
  
        game.player.discardPanel.activateCorrectBoard();
        game.opponent.discardPanel.activateCorrectBoard();

        for (var i = 0; i < conts.length; i++) {
            conts[i].updatePosition();
            conts[i].updateChildrenPositions();
        }

    }

    that.changeColor = function (index) {
        that.colorIndex = index;
             
      
        bg.image = loader.getResult(that.colorIndex + "board");
  
        that.stick1.image = loader.getResult(that.colorIndex + "stick1");
        that.stick2.image = loader.getResult(that.colorIndex + "stick2");

    }


    that.changeStoneColor = function ( ) {
 
        try{
            game.player.killedStones.stone.changeBG();
            game.opponent.killedStones.stone.changeBG();

            game.player.discardPanel.changeBG();
            game.opponent.discardPanel.changeBG();

            var brd = that.getBoard();
            for (var i = 0; i < brd.length; i++) {
                var row = brd[i];
                for (var j = 0; j < row.children.length; j++) {
                    row.children[j].changeBG();                 
                }
            }
        }catch(ex){
        
        }
    }
}




Board.prototype = new createjs.Container();
Board.prototype.constructor = Board;


