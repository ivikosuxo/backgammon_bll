﻿
 
 
var connection = true;
var error = false;
var text = "";

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] === variable) { return pair[1]; }
    }
    return (false); 
}
 
function getTime(times) {
    if (times < 10) {
        return "0" + times;
    }
    return "" + times;
}


function GetPlayerByID(plId) {
    if (game.player.player_id == plId) {
        return game.player;
    }
    if (game.opponent.player_id == plId) {
        return game.opponent;
    }

    return null;
}



function CloseWindow() {

    if (game.mobile) {
        window.location.href = mobile_url + getQueryVariable("token");
    } else {
        window.close();
    } 
}

