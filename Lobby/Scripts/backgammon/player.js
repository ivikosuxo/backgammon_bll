function Player(color,name)
{
    createjs.Container.call(this);

    var that = this;
    that.player_id =0;
    that.visible = true;
    that.username = "";
    that.usernameTxt = {};
 
    that.killedStones = {};
    that.sprite= {};
    that.turn = false;
    that.point = {};
    that.movesList = [];
    that.color = color;
    that.discardCount = 0;
    that.discardPanel = {};
    that.bigDice = {};
    that.state = 0;     
    that.isDouble = false;
 
     
    that.canPlay = true;
    that.canRaise = false;
    that.audio = new Audio();
    that.spd = 1;
    that.timer = new PlayerTimer(that.color - 1,200,4,6);
 
 
    that.StartTimer = function (fullSize, leftSize, color) {
        that.timer.stop();
        that.timer.start(fullSize, leftSize, color);
    };


    that.StopTimer = function ()
    {
        that.timer.stop();
    };

    
 

    this.getOpponent = function () {
        if (that.color === game.player.color) {
            return game.opponent;
        } else {
            return game.player;
        }

    }

    this.setPips = function(count) {

        that.pips.text=count;

    }
    this.setPoint = function (pts) {

        this.point = pts;

        if (this.color === 1) {
            game.rightPanel.myPoint.text = pts;
        } else {
            game.rightPanel.enPoint.text = pts;
        }       
    
    }
    this.setName = function (name) {

        that.username = name;

        if (this.color===1) {
            game.rightPanel.userName.text = name.substring(0, 11);;
        }else{
            game.rightPanel.opName.text = name.substring(0, 11);;
        }
        that.usernameTxt.text = that.username;
        that.visible = true;
    }
  
    function initialize() {

 
            var userPanel = loader.getResult("user_icon");
            that.sprite = new createjs.Bitmap(userPanel);
            that.sprite.cache(0, 0, 50, 53);
            that.addChild(that.sprite);
            
            var userConnection = loader.getResult("connection");
            that.sprite1 = new createjs.Bitmap(userConnection);
 
            that.sprite1.x = 1142;
            that.sprite1.y = -6;
            that.addChild(that.sprite1);

            that.usernameTxt = new createjs.Text(that.username, "28px"+calibri_font, panel_color);
            that.usernameTxt.x = 55;
            that.usernameTxt.y = 0;
            that.addChild(that.usernameTxt);
 

            that.pipsTxt = new createjs.Text("Pips:", "28px" + calibri_font, panel_color);
            that.pipsTxt.x = 1220;
            that.pipsTxt.y = 0;
            that.addChild(that.pipsTxt);

            that.pips = new createjs.Text("167", "28px" + calibri_font, "white");
            that.pips.x = 1290;
            that.pips.y = 0;
            that.addChild(that.pips);
 

            that.addChild(that.timer);
            that.timer.x = 550;
            that.killedStones = new KilledStones(that.color, that.color===2);

            game.board.addChild(that.killedStones);

            that.discardPanel = new DiscardPanel(that.color);
            game.board.addChild(that.discardPanel);
 


            that.x = 45;
            that.y = -game.board.y*scl+10*scl;
            that.turn = false;
            that.visible = true;
        

          if (that.color===1) {
          
             that.y =  ( 1300- 52  -40);

          }else
          {            
             that.y =  -20;
          }
 
         
    }



    that.isDiscardState = function (index)
    {
        if (that.killedStones.count > 0) {
          
            return false;
        }else {
            var containers = game.board.getBoard();
            var count = 0;
            var startPosition = 18;
      
            if (that.color===2) {
                  startPosition = 0;
            }

            if (index < startPosition  && index > startPosition +5 )
            {
                 
                return false;
            }
            for (var i = 0; i < 6; i++)
            {
                if (containers[startPosition + i].getColor() === that.color)
                {
                    count += containers[startPosition + i].numChildren;
                }
            }
            if (that.discardPanel.count + count === game.logic.getStoneCount())
            {
                 
                return true;
            }
            
        }
        return false;
    }
    that.changeTurn = function ()
    {
        that.turn = false;      
        that.StopTimer();
    }

    that.spliceMove = function (index)
    {
        that.movesList.splice(index, 1);
         if (that.movesList.length === 2 && that.isDouble ) {
            game.board.setAlpha(that.movesList[0], that.isDouble);
        
         }else  if (that.movesList.length === 1 && !that.isDouble) {
             
  
               game.board.setAlpha(that.movesList[0], that.isDouble);
 
        }
         else if (that.movesList.length === 0) {
        

                 game.board.setAlphaBoth();
        }

    }


this.minimize = function()
{

    that.sprite.x=90;
    that.usernameTxt.x = 145;
    that.pips.x = 1200;
    that.pipsTxt.x = 1130;
   that.sprite1.x = 1052;

}


this.maximize = function()
{
    that.sprite.x=0;
    that.usernameTxt.x = 55;
    that.pips.x = 1290;
    that.pipsTxt.x = 1220;
    that.sprite1.x = 1142;

}
    initialize();
}

 

Player.prototype = new createjs.Container();
Player.prototype.constructor = Player;

function Audio() {
    this.sound = true;
    this.chatSound = true;
    this.SystemSound = true;
    this.language = 1;

    this.playSound = function (soundName) {


        var song = document.getElementById(soundName);

        if (game.sound) {
            song.volume = 1;
        } else {
            song.volume = 0;
        }
        song.currentTime = 0;

        song.play();

    }

    this.stopSound = function (soundName) {


        var song = document.getElementById(soundName);
        song.pause();
    }

    this.playChatSound = function () {
        if (this.chatSound) {

        }

    }

    this.chatSoundOn = function (result) {

        var soundName = "clock";

        this.sound = result;
        if (result) {
            var song1 = document.getElementById(soundName);
            song1.volume = 1;
        } else {
            var song2 = document.getElementById(soundName);
            song2.volume = 0;
        }
    }

}