function KilledStones(color,op)
{
    createjs.Container.call(this);
    
    var that = this;
    that.index = -1;
	that.count = 0;
	that.color = color;
	that.text = {};
    that.bg = {};
    that.stone = new Stone(color);
    that.addChild(that.stone);
	that.op = op;
	that.dragStart=false;
	that.visible=false;


    initialize();
    that.getColor = function () {

        return that.color;
    }

   
	function initialize() {

	    var textColor = "2stone_"+Math.abs(game.stoneIndex-that.color);
 
	    that.text = new createjs.Text("0", "bold 32px"+arial_font, "white");
        that.text.textAlign= "center";
		that.text.x = 42;
		that.text.y = 48;
		that.text.textBaseline = "alphabetic";


		that.addChild(that.text);
         
         that.x=672;
         that.y=555;
		if (that.op)
		{
		    that.index = 24;
     		that.y=420;
		};

		//that.addEventListener('click', function () {
	 
		//  //  reviveState(that.color);
		//    if (that.count===0) {
		//        var activePl = getActivePlayer();
		//        activePl.state = 0;
		//    }
		//    that.showChip();
		//});





	     that.on('mousedown', function(){hintCalculate(true)});
	     that.on('mouseover', function(){hintCalculate(false)});
	     that.on('mouseout', function(){hideHints()});
 
	}
	 function  hideHints()
	{
if (  that.dragStart) {

	return false;
}
    if (game.hint1.visible) {
                 
                game.hint1.visible = false;
            }
            if (game.hint2.visible) {
               
                game.hint2.visible= false;

            }


                if (game.hint3.visible) {
                
                game.hint3.visible= false;

            }

                if (game.hint4.visible) {
                 
                game.hint4.visible= false;

            }
                if (game.hint5) {
              
                game.hint5.visible= false;

            }

            if (game.hintArrow1) {
                game.board.removeChild(game.hintArrow1);
                game.hintArrow1 = null;
            }
            if (game.hintArrow2) {
                game.board.removeChild(game.hintArrow2);
                game.hintArrow2 = null;
            }
            if (game.hintArrow3) {
                game.board.removeChild(game.hintArrow3);
                game.hintArrow3 = null;
            }
            if (game.hintArrow4) {
                game.board.removeChild(game.hintArrow4);
                game.hintArrow4 = null;
            }

                    if (game.hintArrow5) {
                game.board.removeChild(game.hintArrow5);
                game.hintArrow5 = null;
            }

	}
	 function  hintCalculate(isDrag)
	    {


	    	hideHints();

	        that.dragStart= isDrag;
	        var activePl = game.player;
	        var isDragable = false;
	        if (activePl.color === that.color && activePl.movesList.length !== 0 && activePl.killedStones.count > 0) {
	            game.lastTime = new Date();
	            var containers = game.board.getBoard();
	            var color = that.color;

		    var canPlay1 = false;
		    var canPlay2 = false;
		    var canPlay3  = false;
		    var canPlay4  = false;
	            var pos = that.index + activePl.movesList[0];
	            if (color === 2) {
	                pos = that.index - activePl.movesList[0];
	            }
	            if (pos < 24 && pos >= 0 && containers[pos].canMove(color)) {
	               
	                var posY = containers[pos].y - 2 + containers[pos].getChipsLastPosition();
	                var posX = containers[pos].x - 2 + containers[pos].getChipsHintPositionX();
	                
	                game.hint1.x = posX;
	                game.hint1.y = posY;

                    game.hint1.visible =true;
	                game.hint1.moveIndex = 0;
	                game.hint1.movePos = pos;
	                game.hintArrow1 = containers[pos].getHintArrow();
	                game.board.addChild(game.hintArrow1);
	           //     game.hint1.alpha = 0.5;


	                isDragable = true;

				    canPlay1    =true;

	            }
	            if (activePl.movesList.length > 1 && !activePl.isDouble) {


	                var pos1 = that.index + activePl.movesList[1];
	                if (color === 2) {
	                    pos1 = that.index - activePl.movesList[1];;
	                }
	                if (pos1 < 24 && pos1 >= 0 && containers[pos1].canMove(color)) {
	                    
	                    var posY2 =   containers[pos1].y - 2 + containers[pos1].getChipsLastPosition();
	                    var posX2 =   containers[pos1].x - 2 + containers[pos1].getChipsHintPositionX();
	                    game.hint2.visible =true;
	                    game.hint2.x = posX2;
	                    game.hint2.y = posY2;
	                    game.hint2.moveIndex = 1;
	                    game.hint2.movePos = pos1;
	                    game.hintArrow2 = containers[pos1].getHintArrow();
	                    game.board.addChild(game.hintArrow2);
	              //      game.hint2.alpha = 0.5;
	                    isDragable = true;
	                     canPlay2 = true;
	                }
	            }



                if (activePl.movesList.length>1  && (((canPlay1 || canPlay2 )&& !activePl.isDouble )||((canPlay1 )&& activePl.isDouble )) &&activePl.killedStones.count==1) 
                {
                        var diff = 1;
                        if (canPlay1) {
                             
                                diff  =0;
                        }
                        var pos2 = that.index - activePl.movesList[diff];
                        var pos1 = that.index - activePl.movesList[diff]- activePl.movesList[1-diff];
                        if (color === 1) {
                             var pos2 = that.index + activePl.movesList[diff];
                            pos1 = that.index + activePl.movesList[diff]+ activePl.movesList[1-diff];
                        }
                        if (pos1 < 24 && pos1 >= 0 && containers[pos1].canMove(color) )
                        {
                           
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                             
                            game.hint3.x = posX2;
                            game.hint3.y = posY2;

                               game.hint3.visible =true;

                            game.hint3.moveIndex = diff;
                            game.hint3.moveIndex1 = 0; 
 
                            game.hint3.movePos = pos2;
                            game.hint3.movePos1 = pos1;
                    
                            game.hintArrow3= containers[pos1].getHintArrow();
                            game.hint3.parentRow =containers[pos2];
                            game.board.addChild(game.hintArrow3);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay3 = true;
                        }


                }

                if (activePl.movesList.length>2  && canPlay3) 
                {

                        var pos3 = that.index - activePl.movesList[0];
                        var pos2 = that.index - activePl.movesList[0]- activePl.movesList[0];
                        var pos1 = that.index - activePl.movesList[0]- activePl.movesList[0]-activePl.movesList[0];
                        if (color === 1) {
                            pos3 = that.index + activePl.movesList[0];
                            pos2 = that.index + activePl.movesList[0]+ activePl.movesList[0];
                            pos1 = that.index + activePl.movesList[0]+ activePl.movesList[0]+activePl.movesList[0];
                        }
                        if (pos1 < 24 && pos1 >= 0 && containers[pos1].canMove(color) )
                        {
                           
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                            
                            game.hint4.x = posX2;
                            game.hint4.y = posY2;
                            game.hint4.visible =true;
                            game.hint4.moveIndex = 0;
                            game.hint4.moveIndex1 = 0;
                            game.hint4.moveIndex2 = 0;

 
 
                            game.hint4.movePos = pos3;
                            game.hint4.movePos1 = pos2;
                            game.hint4.movePos2 = pos1;
                    
                            game.hintArrow4= containers[pos1].getHintArrow();
                            game.hint4.parentRow =containers[pos3];
                            game.hint4.parentRow1 =containers[pos2];
                            game.board.addChild(game.hintArrow4);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                           canPlay4=true;

                        }


                }


              if (activePl.movesList.length>3  && canPlay4) 
                {

                        var pos4 = that.index - activePl.movesList[0];
                        var pos3 = that.index - activePl.movesList[0]- activePl.movesList[0];
                        var pos2 = that.index - activePl.movesList[0]- activePl.movesList[0]-activePl.movesList[0];
                        var pos1 = that.index - activePl.movesList[0]- activePl.movesList[0]-activePl.movesList[0]-activePl.movesList[0];
                        if (color === 1) {
                            pos4 = that.index + activePl.movesList[0];
                            pos3 = that.index + activePl.movesList[0]+ activePl.movesList[0];
                            pos2 = that.index + activePl.movesList[0]+ activePl.movesList[0]+activePl.movesList[0];
                            pos1 = that.index + activePl.movesList[0]+ activePl.movesList[0]+activePl.movesList[0]+activePl.movesList[0];
                        }
                        if (pos1 < 24 && pos1 >= 0 && containers[pos1].canMove(color) )
                        {
                          
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                      
                            game.hint5.x = posX2;
                            game.hint5.y = posY2;
                            game.hint5.visible =true;
                            game.hint5.moveIndex = 0;
                            game.hint5.moveIndex1 = 0;
                            game.hint5.moveIndex2 = 0;
                            game.hint5.moveIndex3 = 0;

 
                            game.hint5.movePos = pos4;
                            game.hint5.movePos1 = pos3;
                            game.hint5.movePos2 = pos2;
                            game.hint5.movePos3 = pos1;
                    
                            game.hintArrow5= containers[pos1].getHintArrow();
                            game.hint5.parentRow =containers[pos4];
                            game.hint5.parentRow1 =containers[pos3];
                            game.hint5.parentRow2 =containers[pos2];
                            game.board.addChild(game.hintArrow5);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay5=true;
                         
                        }


                }






	            if (isDragable &&isDrag) {
	                that.hideChip();
	                game.dragStone = new Stone(color);
	                game.dragStone.alpha = 0.8;
	                game.dragStone.x = game.board.x + that.x - 16;
	                game.dragStone.y = game.board.y + that.y  - 16;
	                game.dragStone.parentRow = that;
	                
	                game.dragStone.visible = false;
	                stage.addChild(game.dragStone);


	            }

	        }


	    }

    that.hideChip= function() {
        

        if (that.count === 1) {
            that.text.text = "";
            that.visible = false;
            that.stone.setVisibleCounterBg(false);
        }
        if (that.count > 1) {
            that.text.text = "" + that.count - 1;
            that.visible = true;
            that.stone.setVisibleCounterBg(true);

        }


    }

    that.showChip = function () {


        if (that.count === 1) {
            that.text.text = "";
            that.visible = true;
            that.stone.setVisibleCounterBg(false);
        }
        if (that.count > 1) {
            that.text.text = "" + that.count;
            that.visible = true;
            that.stone.setVisibleCounterBg(true);
        }


    }

   that.getLastChipPosit = function () {
    if (this.numChildren>0)
    {
        var st =  new Stone(that.color);
        st.visible =false;
        that.addChild(st);

        return st;

    } 

}

    that.addKilledStone = function () {
	    that.count += 1;

	    if (that.count === 1) {
	        that.text.text = "";
	        that.visible = true;
	        that.stone.setVisibleCounterBg(false);
	    }
	    if (that.count > 1) {
	        that.text.text = "" + that.count;
	        that.stone.setVisibleCounterBg(true);

	    }

	}

    that.addChips = function(count) {
        
        for (var i = 0; i < count; i++)
        {
            that.addKilledStone();
        }
    }

    that.addChip = function () {

	    that.addKilledStone();
	    var activePl = getActivePlayer();
	    activePl.state = 1;
	}

	that.removeChip = function () {

	    that.removeKilledStone();
	    if (that.count ===0) {
	        
	        var activePl = getActivePlayer();
	        activePl.state = 0;
	    }
        
    }
    that.removeKilledStone =function()
	{
	    that.count -= 1;
	    if (that.count === 1)
	    {
	        that.text.text = "";
	        that.visible = true;
	        that.stone.setVisibleCounterBg(false);
	    }
	    if (that.count > 1)
	    {
	        that.text.text = "" + that.count;
	        that.stone.setVisibleCounterBg(true);
	    } if (that.count === 0)
	    {
	        that.stone.setVisibleCounterBg(false);
	        this.visible = false;
	        return true;
	    };

	    return false;

    }

    that.removeKills= function() {

        that.count =1 ;

        that.removeKilledStone();
    }


}






KilledStones.prototype = new createjs.Container();
KilledStones.prototype.constructor = KilledStones;