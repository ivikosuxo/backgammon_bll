﻿function Roller()
{
    createjs.Container.call(this);
 
    this.x = 575;
    this.y = 1114;
    var thit = this;
    thit.visible = false;
    thit.firstDice = 0;
    thit.secondDice = 0;

    thit.rollTxt = new createjs.Text(GetTranslatedValue("roll"), "28px"+mtavruli_font, "black");
    thit.rollTxt.textBaseline = "middle";
    thit.rollTxt.textAlign = "center";

    if (game.mobile) {

        thit.roller = new Button("mobile_roll", "", null, "white");
        thit.roller.x = 711;
        thit.roller.y = -504; 
        thit.addChild(thit.roller);

    } else {

 
        thit.cont = new createjs.Container();
        thit.addChild(thit.cont);

    var rollImg = loader.getResult("button_yellow");
    thit.sprite = new createjs.Bitmap(rollImg);
    thit.cont.addChild(thit.sprite);


    var rollImg1 = loader.getResult("roll_icon");
    thit.sprite1 = new createjs.Bitmap(rollImg1);
    thit.sprite1.x=30;
    thit.sprite1.y=19;
    thit.cont.addChild(thit.sprite1);

    thit.cont.addChild(thit.rollTxt);

    thit.rollTxt.x = 165;
    thit.rollTxt.y = 37;
    }

 


 
thit.translate= function()
{
    thit.rollTxt.text = GetTranslatedValue("roll") ;// roll_tr[game.lang];


}
    thit.cursor = "pointer";

    thit.on("click", function () {
        thit.visible = false;
 
        game.board.showUndo();
        game.board.rollDice(thit.firstDice, thit.secondDice);
        game.engine.Roll();
        setTimeout( function(){ thit.alpha = 0.3; },750);
    });

    thit.on("mouseover", function () {
              if (thit.alpha === 1.0){
                    thit.alpha = 0.85;
            
            }
    });
    thit.on("mouseout", function () {
      if (thit.alpha === 0.85){   
          thit.alpha = 1.0;
       }

    });

 
    this.autoRoll = function (firstDice, secondDice) {
        this.firstDice = firstDice;
        this.secondDice = secondDice;

        if (game.autoRoll  && game.player.turn)
        {
       
            game.board.rollDice(firstDice, secondDice);
            game.engine.Roll();
            game.board.showUndo();
        } else {
             
            if (!game.opponent.turn) {
                game.board.showRoller();  
            } 
        } 
         
    }
    this.setDices = function (firstDice, secondDice)
    {
        this.firstDice = firstDice;
        this.secondDice = secondDice;
    }


    this.roll= function() {
        game.board.rollDice(this.firstDice, this.secondDice);
    }
    this.historyDice = function () {

     
    }
   
    this.kickDice = function ( ) {


        if (game.autoRoll && game.player.turn) {

            game.board.rollDice(thit.firstDice, thit.secondDice);
            game.engine.Roll();
            game.board.showUndo();
        } else  {
            if (game.player.turn) {
                game.board.showRoller();
            }
        }

    }


}




Roller.prototype = new createjs.Container();
Roller.prototype.constructor = Roller;