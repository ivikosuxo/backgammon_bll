

function RightPanel()
{
    createjs.Container.call(this);

    var raiseCount=1;



    var that = this;
    this.myPoint= {};
    this.enPoint = {};
    var panelbg = {};
    var panelbg1 = {};
    that.text = {};
    this.settings = {};
    this.volume = {};
    this.fullScreen = {};
    this.close = {};
    that.raise = {};
    that.betAmountTxt = {};
    that.betAmount = {};
    that.betAmountDetails = {};
    that.pointsToWin = {};
    this.maxPoint=1;

    that.x = 0;
    that.y = 0;

    initialize();
    initializeTopBtns();
    initializeGameParameters();
    initializeRaise();
    initializePoints();
    initializeGiveUp();
    initializeChecks();
    function initialize()
    {
        var panelImg = loader.getResult("panelBG1");
        panelbg = new createjs.Bitmap(panelImg);
        panelbg.cache(0, 0, 468, 1300);
        that.addChild(panelbg);

   
    }

    function initializeTopBtns()
    {
        var settingsImg = loader.getResult("settings");
        that.settings = new createjs.Bitmap(settingsImg);
        //that.settings.cache(0, 0, 44, 45);
        that.settings.x = 89;
        that.settings.y = 24;
        that.settings.alpha = 0.7;
        that.settings.cursor = "pointer";
        that.addChild(that.settings);

            var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 44, 45);
            that.settings.hitArea = hit;

        that.settings.mouseEnabled =true;
        that.settings.on("click", function () {
            
            game.settingsPopup.visible = !game.settingsPopup.visible;
            if (!game.mobile) {
                game.settingsPopup.fadeIn.visible = true;
            }  
        });

          that.settings.on("rollover", function () {
            
           that.settings.image= loader.getResult("settingsh");

        });

          that.settings.on("rollout", function () {
            
           that.settings.image= loader.getResult("settings");

        });



        var volumeImg = loader.getResult("vol_on");
        that.volume = new createjs.Bitmap(volumeImg);
      //  that.volume.cache(0, 0, 49, 45);
        that.volume.x = 205;
        that.volume.y = 24;
        that.volume.alpha = 0.7;
        that.volume.cursor = "pointer";
        that.addChild(that.volume);

            var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 49, 45);
            that.volume.hitArea = hit;


         that.volume.on("rollover", function () {
            if (game.sound) {

                that.volume.image= loader.getResult("vol_onh");

            }else{

                that.volume.image= loader.getResult("vol_offh");
            }
        

        });

          that.volume.on("rollout", function () {
            
          if (game.sound) {

                that.volume.image= loader.getResult("vol_on");

            }else{

               that.volume.image= loader.getResult("vol_off");
            }
        

        });
 that.volume.mouseEnabled =true;

         that.volume.on("click", function () {
            game.sound =!game.sound;
          if (game.sound) {

                that.volume.image= loader.getResult("vol_on");

            }else{

               that.volume.image= loader.getResult("vol_off");
            }
        

        });



        var fullScreenImg = loader.getResult("fullscreen");
        that.fullScreen = new createjs.Bitmap(fullScreenImg);
       // that.fullScreen.cache(0, 0, 44, 45);
        that.fullScreen.x = 322;
        that.fullScreen.y = 24;
        that.fullScreen.alpha = 0.7;
        that.fullScreen.cursor = "pointer";
        that.addChild(that.fullScreen);
            var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(-5, -5, 50, 50);
            that.fullScreen.hitArea = hit;

    that.flscreen = false;
     that.fullScreen.mouseEnabled =true;
    that.fullScreen.on("click",function ()
  {
    
         try {
            toggleFullScreen(); 
            //  document.body.requestFullscreen();

         } catch (ex) {
             
         }
       
        that.flscreen   = true;
    
  });



          that.fullScreen.on("rollover", function () {
            
           that.fullScreen.image= loader.getResult("fullscreenh");

        });

          that.fullScreen.on("rollout", function () { 
           that.fullScreen.image= loader.getResult("fullscreen"); 
        });
         
    }

    that.setBetText = function (txt) {
        that.betAmount.text = (2*parseFloat(txt)).toFixed(2);

    }

    that.getBetText = function () {
        return  parseInt(that.betAmount.text);

    }

    that.changeBg= function(col)
    {

         panelbg.uncache();
         panelbg.image= loader.getResult("panelBG"+col); 
         panelbg.cache(0, 0, 468, 1300);
    }

    function initializeGameParameters()
    {
        that.betAmountTxt = new createjs.Text(GetTranslatedValue("bid").toUpperCase(), getCorrectFontSize(30) + getCorrectFont(mtavruli_font), panel_color);
        that.betAmountTxt.textAlign= "center";
        that.addChild(that.betAmountTxt);
        that.betAmountTxt.x = 200;
        that.betAmountTxt.y = 163;

        that.betAmountTxt1 = new createjs.Text("b", "32px GEL", panel_color); 
        that.addChild(that.betAmountTxt1);
        that.betAmountTxt1.x = 264;
        that.betAmountTxt1.y = 162;


        that.betAmount = new createjs.Text("00.00", " bold 60px"+ calibri_font, color_yellow);
        that.addChild(that.betAmount);
        that.betAmount.textAlign= "center";
        that.betAmount.x = 220;
        that.betAmount.y = 200;

       /* that.betAmountValute = new createjs.Text("ლარი", "30px Mtavruli", "#f1aa4f");
        that.addChild(that.betAmountValute);
        that.betAmountValute.textAlign= "center";
        that.betAmountValute.x = 220;
        that.betAmountValute.y = 270;
 */

        that.panelCup = new createjs.Bitmap(loader.getResult("panel_cup"));
        that.panelCup.x = 90;
        that.panelCup.y = 603;    
        that.addChild(that.panelCup);

        that.pointsToWin = new createjs.Text(GetTranslatedValue("points"), getCorrectFontSize(24) + getCorrectFont(mtavruli_font), panel_color);
        that.addChild(that.pointsToWin);
        that.pointsToWin.x = 142;
        that.pointsToWin.y = 595;


        that.gameType = new createjs.Text(GetTranslatedValue("rules") + ": " + GetTranslatedValue("georgian"), getCorrectFontSize(24) + getCorrectFont(mtavruli_font), panel_color);
        that.addChild(that.gameType);
        that.gameType.x = 142;
        that.gameType.y = 626;

    }


    function initializeRaise() 
    {

        that.raiseCounter = new createjs.Text("x" + raiseCount, "bold 35px"+calibri_font, "white");
        that.raiseCounter.textAlign="center";
       
        that.raiseCounter.x = 222;
        that.raiseCounter.y = 683;
        that.addChild(that.raiseCounter);


 
       

       that.raise = new Button("button_yellow", "raise",false,"black");
      
       // raise.cache(0, 0, 73, 35);
        that.raise.x = 85;
        that.raise.y = 742;

 

        that.addChild(that.raise);

        that.raise.alpha = 0.5;
        that.raise.mouseEnabled = false;
        that.raise.setSize(141);
        that.raise.setHeight(20);
        that.raise.setFontSize(28);

       that.raise.on('click', function () {
            that.raise.alpha = 0.5;
            that.raise.mouseEnabled = false;
            game.engine.raise();
        });

 
    }
    this.animate = function(counter) {
 

        var localX = that.raiseCounter.x;
        var localY = that.raiseCounter.y;

        that.raiseCounter.text = "x" + counter;
        createjs.Tween.get(that.raiseCounter)
        .to({ scaleX: 1.5, scaleY: 1.5, x: localX - 5, y: localY - 5, color: "black" }, 75, createjs.Ease.bounceOut)
       .to({ scaleX: 1.0, scaleY: 1.0, x: localX, y: localY, color: "white" }, 150, createjs.Ease.circInOut).call(handleComplete);
        function handleComplete() {
                that.raiseCounter.x = 222;
                that.raiseCounter.y = 683;
        }
    }

    this.setMaxPoints = function (pts) {
      this.maxPoint=pts;
      that.pointsToWin.text = GetTranslatedValue("game") + ": " + GetTranslatedValue("till_pre") +pts+" "+ GetTranslatedValue("till_post"); 
    }

    this.increaseMyPoints = function (pts) {
        that.myPoint.text = parseInt(that.myPoint.text) + pts;
    }
    this.increaseEnPoints = function (pts) {
        that.enPoint.text = parseInt(that.enPoint.text) + pts;
    }

    function initializePoints()
    {

        that.pointsTitle = new createjs.Text(GetTranslatedValue("score"), getCorrectFontSize(30) + getCorrectFont(mtavruli_font), panel_color);
        that.addChild(that.pointsTitle);
        that.pointsTitle.textAlign = "center";
        that.pointsTitle.x = 225;
        that.pointsTitle.y = 342;
        that.addChild(that.pointsTitle);

        that.myPointBG = new createjs.Bitmap(loader.getResult("point_circle_h"));
        that.myPointBG.x = 137;
        that.myPointBG.y = 435;
        that.myPointBG.regX = 48;
        that.myPointBG.regY = 48;
        that.addChild(that.myPointBG);
        that.myPoint = new createjs.Text("0", "bold 48px"+ calibri_font, "white");
      
        that.myPoint.textAlign= "center";
        that.myPoint.x = 137;
        that.myPoint.y = 403;
        that.addChild(that.myPoint);

        that.enPointBG = new createjs.Bitmap(loader.getResult("point_circle_h"));
        that.enPointBG.x = 316;
        that.enPointBG.y = 435;
        that.enPointBG.regX = 48;
        that.enPointBG.regY = 48;
        that.addChild(that.enPointBG);
        that.enPoint = new createjs.Text("0", "bold 48px"+calibri_font, "white");
     
        that.enPoint.textAlign= "center";
        that.enPoint.x = 316;
        that.enPoint.y = 403;
        that.addChild(that.enPoint);

        that.userName = new createjs.Text("".substring(0, 11), "29px" + calibri_font, panel_color);  //Mtavruli
     
        that.userName.textAlign= "center";
        that.userName.x = 137;
        that.userName.y = 490;
        that.addChild(that.userName);

        that.opName = new createjs.Text("".substring(0, 11), "29px" + calibri_font, panel_color); //Mtavruli    
        that.opName.textAlign= "center";
        that.opName.x = 314;
        that.opName.y = 490;
        that.addChild(that.opName);


 
    }

    function initializeGiveUp()
    {
             that.giveup =new Button("button_grey","give_up","giveup_icon","white");

 
        that.giveup.setSize(151);
 
        that.giveup.setFontSize(28);
        that.giveup.setIconX(50);
        
       
      //  giveup.cache(0,0,147,44);
        that.giveup.x = 83;
        that.giveup.y = 1097;
        that.addChild(that.giveup);
    
       
      
 



  


         that.giveup.on('click', function() {

          
            if (!game.popup) {
                
                game.popup = new GiveUpPopup();
            }   
 
        }); 

 

    }



    function initializeChecks() {

        //change side
        that.changeSideCont = new createjs.Container();
        that.changeSideCont.x = 85;
        that.changeSideCont.y = 880;



        var checkBgImg = loader.getResult("checkBG");

        var checkbg = new createjs.Bitmap(checkBgImg);
        checkbg.scaleX = 0.4;
        checkbg.scaleY = 0.1;
        checkbg.alpha = 0.01;
        that.changeSideCont.addChild(checkbg);

        var checkImg = loader.getResult("check");
        var check = new createjs.Bitmap(checkImg);
        that.changeSideCont.addChild(check);



        var checkactImg = loader.getResult("checkact");
        var checkact = new createjs.Bitmap(checkactImg);
        that.changeSideCont.addChild(checkact);
        checkact.x = 1;
        that.changeSide = new createjs.Text(GetTranslatedValue("change_side"), getCorrectFontSize(24)+ getCorrectFont(mtavruli_font), panel_color);
        that.changeSide.x = 50;
        that.changeSide.y = 4;
        checkact.visible = false;
        that.changeSideCont.addChild(that.changeSide);

           that.changeSideCont.cursor="pointer"

           var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 260, 40);
            that.changeSideCont.hitArea = hit;

        that.addChild(that.changeSideCont);
        that.changeSideCont.on("click", function () {

            if (checkact.visible) {
                game.reverseBoard = 1;
            } else {
                game.reverseBoard = -1;
            }

            var cont = game.board.getBoard();
            for (var i = 0 ; i < 24; i++) {
                cont[i].reverseRow();

               
            
            }
                game.player.discardPanel.reverse();
                game.opponent.discardPanel.reverse();

            checkact.visible = !checkact.visible;
        });




        // auto roll
        that.autoRollCont = new createjs.Container();
        that.autoRollCont.x = 85;
        that.autoRollCont.y = 935;
         that.autoRollCont.cursor="pointer"

            var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 280, 40);
            that.autoRollCont.hitArea = hit;

       

        var checkbg1 = new createjs.Bitmap(checkBgImg);
        checkbg1.scaleX = 0.4;
        checkbg1.scaleY = 0.1;
        checkbg1.alpha = 0.01;

        that.autoRollCont.addChild(checkbg1);

        var check1 = new createjs.Bitmap(checkImg);
        that.autoRollCont.addChild(check1);

       
        var checkact1 = new createjs.Bitmap(checkactImg);
        checkact1.x = 1;
        checkact1.visible =false;
        that.autoRollCont.addChild(checkact1);

        that.autoRoll = new createjs.Text(GetTranslatedValue("auto_roll"), getCorrectFontSize(24)+ getCorrectFont(mtavruli_font), panel_color);
        that.autoRoll.x = 50;
        that.autoRoll.y = 4;

      

 
  
        that.autoRollCont.addChild(that.autoRoll);

        that.addChild(that.autoRollCont);

        that.autoRollCont.on("click", function() {
 
            checkact1.visible = !checkact1.visible;
            game.autoRoll = checkact1.visible;
        });

        //auto roll mobile
        checkact1.visible = false; 
        game.autoRoll = false;
       

        // auto submit
        that.autoSubCont = new createjs.Container();
        that.autoSubCont.x = 85;
        that.autoSubCont.y = 990;



        var checkbg1 = new createjs.Bitmap(checkBgImg);
        checkbg1.scaleX = 0.4;
        checkbg1.scaleY = 0.1;
        checkbg1.alpha = 0.01;

        that.autoSubCont.addChild(checkbg1);

        var check1 = new createjs.Bitmap(checkImg);
        that.autoSubCont.addChild(check1);


        var checkact2 = new createjs.Bitmap(checkactImg);
        checkact2.x = 1;
        checkact2.visible = false;
        that.autoSubCont.addChild(checkact2);

        that.autoSub = new createjs.Text(GetTranslatedValue("auto_submit"), getCorrectFontSize(24) + getCorrectFont(mtavruli_font), panel_color);
        that.autoSub.x = 50;
        that.autoSub.y = 4;

         that.autoSubCont.cursor="pointer"
             var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 320, 40);
            that.autoSubCont.hitArea = hit;



        that.autoSubCont.addChild(that.autoSub);

        that.addChild(that.autoSubCont);

        that.autoSubCont.on("click", function () {

            checkact2.visible = !checkact2.visible;
            game.autoSubmit = checkact2.visible;
        });

         



        that.problemCont = new createjs.Container();
        that.problemCont.x = 45;
        that.problemCont.y = 1240;



  

     

        var check1 = new createjs.Bitmap( loader.getResult("pr_icon"));
        that.problemCont.addChild(check1);


 

        that.autoSub1 = new createjs.Text(GetTranslatedValue("problem_report"), getCorrectFontSize(22) + getCorrectFont(mtavruli_font), panel_color);
         that.autoSub1.textAlign="center";
        that.autoSub1.x = 190;
        that.autoSub1.y = 6;

            that.problemCont.cursor="pointer"
             var hit = new createjs.Shape();
            hit.graphics.beginFill("#000").drawRect(0, 0, 400, 40);
            that.problemCont.hitArea = hit;



        that.problemCont.addChild(that.autoSub1);
        that.addChild(that.problemCont);

           that.problemCont.on("click", function () {
             var pop = game.popup;
           if (pop &&!pop.isReport) {
             pop.visible = false;
           }

           if (pop && pop.isReport) {
               return
             
           }
              
               game.popup = new Report(pop);

              // game.popup = new RaisePopup(2,true);
           //   game.popup = new WaitPopup( activeSession_tr[game.lang],false);
        });

    }

    this.setRuleText = function () { 
        that.gameType.text = GetTranslatedValue("rules")+": "+ GetTranslatedValue(GetGameType(game.room.type));
    }



   this.translatePanel = function(font)
   {
 
       try { 

           that.autoSub1.text = GetTranslatedValue("problem_report");  
           that.autoSub1.font = getCorrectFontSize(22)+ getCorrectFont(mtavruli_font);

           that.autoSub.text = GetTranslatedValue("auto_submit");  //   autoSubmit_tr[game.lang];
           that.autoSub.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);

           that.autoRoll.text = GetTranslatedValue("auto_roll"); // autoRoll_tr[game.lang];
           that.autoRoll.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);

           that.changeSide.text = GetTranslatedValue("change_side"); 
           that.changeSide.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);

           that.betAmountTxt.text = GetTranslatedValue("bid"); //betAmount_tr[game.lang];
           that.betAmountTxt.font = getCorrectFontSize(30) + getCorrectFont(mtavruli_font);

           that.pointsToWin.text = GetTranslatedValue("game") + ": " + GetTranslatedValue("till_pre") + " " + this.maxPoint + " " + GetTranslatedValue("till_post");                     //   GetTranslatedValue("pointss"); //pointsToWin_tr[game.lang] + this.maxPoint;
           that.pointsToWin.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font);

           that.pointsTitle.text = GetTranslatedValue("score");
           that.pointsTitle.font = getCorrectFontSize(30) + getCorrectFont(mtavruli_font);

    
           that.gameType.text = GetTranslatedValue("rules") + ": " + GetTranslatedValue(GetGameType(game.room.type));
           that.gameType.font = getCorrectFontSize(24) + getCorrectFont(mtavruli_font); 

           that.giveup.translate();
          // that.giveup.setText(GetTranslatedValue("give_up"));
        //   that.giveup.txt.font ="24px" + this.getCorrectFont(mtavruli_font); 

         //  that.raise.setText(GetTranslatedValue("raise"));
           that.raise.translate();
           game.board.undo.translate();
           game.board.submit.translate();
           game.board.roller.translate();

         //  game.board.undo.setText(GetTranslatedValue("undo"));
         //  game.board.submit.setText(GetTranslatedValue("submit"));
          // game.board.roller.translate();

       } catch (ex){
           console.log(ex);
       }
  }

}

RightPanel.prototype = new createjs.Container();
RightPanel.prototype.constructor = RightPanel;