﻿function DiscardPanel(color) {
    createjs.Container.call(this);
    var that = this;
    that.count = 0;
    that.color = color;
    that.asc = 1;
    that.col = "";
  
  
 
var right= true;



    function initialize() {
        that.container2D = new createjs.Container();
        that.container2D.visible = true;
 
        that.addChild(that.container2D);

      

        that.x = 1293;
        that.y = 57;

        that.col = "3stone_" + Math.abs(game.stoneColor - that.color);
  
        if (that.color==1)
        {
           
            that.asc = -1;
            that.y = 1027;
        }


       that.drawDiscards();


    }

    that.addDiscards = function(count ) {
        
        for (var i = 0; i < count; i++) {
            that.addDiscard();
        }
    }
    that.activateCorrectBoard = function() {
 
            that.container2D.visible = true;
   
        
    }
 
    this.reverse = function()
    {

      that.x = 1347-that.x;

        right =!right;
  
    }

    that.addDiscard = function () {

        PlaySound("chip");
   
  
        var index = that.count;
        if (that.color === 1) {
            index = 14 - that.count;

        }


        that.container2D.children[index].visible = true;
   
        that.count += 1;
  
    }

    that.drawDiscards = function ()
    {
    
        if (that.color == 1)
        {
            for (var i = 14; i>=0; i--)
            {
                 
                var chipImg = loader.getResult(that.col);
                that.sprite = new createjs.Bitmap(chipImg);
                that.sprite.y = that.asc * 22* i;
                that.sprite.visible = false;
                      
                that.container2D.addChild(that.sprite);
  
            }
           
        } else {
            for (var j = 0; j <15; j++)
            { 
                    var chipImg = loader.getResult(that.col);
                    that.sprite = new createjs.Bitmap(chipImg);
                    that.sprite.y = that.asc * 22 * j;
                    that.sprite.visible = false;
 
                    that.container2D.addChild(that.sprite);
 
            }
         
        }


    }

    that.changeBG = function () {
      
        that.col = "3stone_" + Math.abs(game.stoneColor - that.color);
 
        var child = that.container2D.children;
        for (var i = 0; i < child.length; i++) {
            child[i].image = loader.getResult(that.col);
        }

    }

    that.removeDiscard = function () {

        that.count -= 1;
        var index = that.count;
        if (that.color === 1) {
              index = 14 - that.count;

        }  


        that.container2D.children[index].visible = false;
 
    }

    that.removeDiscards = function () {

        for (var i = 0; i < 15; i++) {
            that.container2D.children[i].visible = false;         
        }
        that.count = 0;

    }

    initialize();

}

DiscardPanel.prototype = new createjs.Container();
DiscardPanel.prototype.constructor = DiscardPanel;