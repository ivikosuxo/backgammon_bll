﻿var dragMute = false;

function onResponse(event) {
  
    switch (event.command) {

        case "start":
            game.engine.StartGame(event);
           break;

        case "login":
            game.engine.onLogin(event);
            break;

        case "connect":
            game.engine.onConnect();
            break;

        case "disconnect_opponent":
            game.engine.opponnetStatusChange(event);
            break;
       
             
        case "balance":              
          //  game.popup = new WaitPopup(waitOponnent_tr, false);
            break;  

        case "waiting_add":
            game.removePopup();
            game.popup = new AddWaiter(10, event.username); 
            break;

   
        case "waiting_remove":
            game.removePopup(); 
            game.popup = new WaitPopup("wait_op", false);
            break;


        case "achievement_unlock":
         game.achievement_popup = new AchievementPopup(event.names,event.coin_count)
        break;
 

        case "end_game":
              game.engine.endGame(event);
            break;

        case "incorrect_move":
           
            game.moves = [];
            game.engine.InvalidMoveState(event); 
            if (event.undo_sound) {
                PlaySound("chip");
            } 
            break;
  
        case "invalid_move": 
            game.moves = [];
            game.engine.InvalidMove(event);  
            break;

        case "play_you":
            game.engine.playYou(event);
            break;

        case "play_en":
            dragMute = game.sound;
            game.sound = game.sound && !event.mute;
            game.engine.playEnemy(event);
            game.sound = dragMute;
            dragMute = false;
            break;

        case "submit_you":
            dragMute = game.sound;
            game.sound = false ; 
            game.engine.SubmitYou(event); 
            game.sound = dragMute;
            dragMute = false;
            break;

        case "submit_enemy":
            dragMute = game.sound;
            game.sound = false;
             game.engine.SubmitEnemy(event);
            game.sound = dragMute;
            dragMute = false;
            break;
     
        case "opponent_info":
            game.opponent.player_id = event.player_id;
            game.opponent.setName(event.username);
            game.rightPanel.opName.text = event.username.substring(0, 11);
            game.removePopup();
            break;

        case "error":
            stage.visible = true;
            game.loadingBar.visible = false;
           
            game.engine.Disconnect(event);
            break;

        case "roll": 
        
            game.opponent.canPlay = event.can_play;
            game.player.canPlay = event.can_play_1;
            game.board.roller.setDices(event.first, event.second);
            game.board.roller.roll();
 
      
            game.rightPanel.raise.alpha = 0.5;
            game.rightPanel.raise.mouseEnabled = false;



            break;


        case "roll_you": 
            game.can_discard_kill = event.can_discard_kill;
            var activePl = game.player;
            game.rightPanel.raise.alpha = 0.5;
            game.rightPanel.raise.mouseEnabled = false;
            game.player.canPlay = event.can_play;
            activePl.movesList = [];
            if (activePl.canPlay) {
                if (event.first === event.second) {
                    activePl.movesList.push(event.first);
                    activePl.movesList.push(event.first);
                    activePl.movesList.push(event.first);
                    activePl.movesList.push(event.first);
                    activePl.isDouble = true;
                    game.can_discard_kill =true;
                } else {
                    activePl.movesList.push(event.first);
                    activePl.movesList.push(event.second);
                    activePl.isDouble = false;
                }

                activePl.movesList.sort(); 
            } else { 
                activePl.movesList = []; 
            }

            if (!event.can_play) { 
                game.player.canPlay = false; 
                game.board.setAlphaBoth(); 
                game.opponent.canPlay = event.can_play_1; 
                game.board.turnOffUndo(); 
            }
             
            break;
            
        case "undo": 
            game.engine.UndoYou(event);
            break;


        case "raise":
            game.engine.Raise(event);
            break;

     

        case "raise_answer":
            game.engine.RaiseAnswer(event);
            break;

  

        case "undo_enemy":
            game.engine.UndoEnemy(event);
            break;

        case "waiter":
            game.removePopup();
            game.popup = new WaitPopup("wait_op",false);
            break;
 

        case "draw":
            game.engine.deactivateAll();
            Game.enemy.timer.reset();
            Game.me.timer.reset();
            break;

        case "turn_timer":
            game.engine.turnTimer(event);
            break;

        case "reserve_timer":
            var pl = GetPlayerByID(event.player_id);

            if (!pl) {
                pl = game.opponent;
            }

            pl.StartTimer(event.timer_size, event.left_size, red_color);          
            break;

        case "reconnect":
            game.engine.reconnect(event);
            break;

        case "next_game":
            game.engine.NextGame(event);
            break;
 
        case "correct_password":
              game.engine.correctPassword(event);
            break;

        case "incorrect_password":
              game.engine.incorrectPassword(event);
            break;

 
        case "ping": 
            lastPingDate = new Date();
            break;

        case "cant_raise":
            game.board.roller.visible = true;
            break;
 

        case "start_bg":           
            game.engine.NewRound(event);
            break;
 

        case "join_room":
            game.engine.correctPassword();
            break;

        case "new_round":
            game.engine.newRound();
            break;

        case "new_game":
            game.engine.newRound();
            break;

        case "pre_join_room":
            game.engine.preJoinRoom(event);
            break;

        case "game_initialize":

            game.engine.gameInitialize(event);
            break;

        case "message": 
            logMessage(event);
            break;

   
    }
}

 

Engine = {
    Mediator: function () {
     
        this.connection = {};    
        this.activeId = -1;
        this.roomID = "";
        this.password = "";
        this.initialize();
        this.connection.connect();
    }
}



Engine.Mediator.prototype.onConnect = function () { 
        this.connection.logIn(); 
}


Engine.Mediator.prototype.Login = function () {
    this.connection.logIn();

}




Engine.Mediator.prototype.opponnetStatusChange = function (evt) {

    var status = evt.connect;

    var userConnection = loader.getResult("connection_lost");
    var text = "internet_lost_text";
    if (status) {
        text = "op_return_text";
        userConnection = loader.getResult("connection");
    }

    game.opponent.sprite1.image = userConnection;  
    var popup =  new DelayPopup(text);
             
}
Engine.Mediator.prototype.opponentStatusChanger=function (status)
{
 
    var userConnection = loader.getResult("connection_lost");
    var text = "internet_lost_text";
    if (status) {
        text = "op_return_text";
        userConnection = loader.getResult("connection");
    }

    game.opponent.sprite1.image = userConnection;
    var popup= new DelayPopup(text);
}


Engine.Mediator.prototype.onLogin = function (evt) {   

    var player = evt.player;
    game.player.setName(player.username);
    game.player.point = 0;
    game.player.canRaise = false;
    game.player.balance = player.balance;
    game.player.avatar = player.avatar;
    game.player.player_id = player.player_id;
    game.player.stone_color = player.stone_color;


    this.connection.preJoinRoom(this.roomID);  
    game.settingsPopup.activateCorrectStone(player.stone_color);
    ServerPing();
}


Engine.Mediator.prototype.preJoinRoom = function (evt) {

    game.room.room_id = evt.room_id;
    game.room.room_name = evt.room_name;
    game.room.min_bet = evt.min_bet;
    game.room.max_bet = evt.max_bet;
    game.room.speed = evt.speed;
    game.room.type = evt.type;
    game.room.max_point = evt.points;
    game.room.protected = evt.protected;
    game.room.chat_enabled = evt.chat_enabled;


    game.room.raise_count = evt.raise_count;
    game.room.jacob_rule = evt.jacob;
    game.room.double_rule = evt.double;
    game.room.crawford_rule = evt.crawford;
    game.room.beaver_rule = evt.beaver;

    game.logic = GetLogicByType(evt.type);
    game.georgian = evt.type == 2;
    game.long = evt.type == 3;

    game.rightPanel.gameType.text = GetTranslatedValue("rules")+": " + GetTranslatedValue(GetGameType(evt.type));

    game.rightPanel.setMaxPoints(game.room.max_point);
    game.rightPanel.setBetText(game.room.min_bet);
    game.logic.drawRows();



    if ( evt.reconnect) {
        
        this.connection.joinRoom(this.roomID, this.password);
    }
    else if (!evt.protected)
    {
       this.connection.addWaiting(this.roomID);
    }
    else if (evt.protected) {

        game.popup = new PasswordPopup();     
    }

    game.final = evt.is_final
    stage.visible = true;
    game.loadingBar.visible = false;


     game.chatPopup.visible = false;
}





Engine.Mediator.prototype.initialize = function () {

    this.connection = new BGSocket.BgSock(); 
    this.roomID = getQueryVariable("roomid");
 
}



Engine.Mediator.prototype.gameInitialize = function (event) {

    game.logic.drawRows();
    game.rightPanel.setBetText(game.room.min_bet);
     
    game.rightPanel.myPoint.text = 0;
    game.rightPanel.enPoint.text = 0;
    game.raise_count = 1;
    game.rightPanel.animate(1);

 
    game.removePopup();


}

 

Engine.Mediator.prototype.StartGame = function (event) {
     
    game.logic.drawRows();
   

}


Engine.Mediator.prototype.NextGame = function (event) {
 
}

Engine.Mediator.prototype.Roll = function ( ) {
  

    this.connection.Roll();
    
}

Engine.Mediator.prototype.NewRound = function (event) {
 
    game.rightPanel.animate(1);

    if (event.first_game) {
        game.board.firstRoll(event.dice1, event.dice2, event.r1, event.r2);
    }
   
    game.board.roller.firstDice = event.r1;
    game.board.roller.secondDice = event.r2;
 
  
    game.player.canRaise = event.can_raise&&!game.autoRoll;


    game.logic.calculatePips(game.player);
    game.logic.calculatePips(game.opponent);

    game.round_id = event.round_id;
    game.game_id = event.game_id;

}


Engine.Mediator.prototype.Disconnect = function (evt) {
 
    var text = GetErrorText(evt.id);     

    if (evt.fatal) { 
        error = true;
        game.removePopup();
        game.popup = new WaitPopup(text, true); 
        this.connection.connection.stop();
    }
    //if (resultBoard) {
    //    resultBoard.remove();
    //}


}



Engine.Mediator.prototype.correctPassword = function (card) {
    game.removePopup();
    if (game.opponent.player_id == 0) {
       // ServerPing();
        game.popup = new WaitPopup("wait_op", false);   
    }
   
}

Engine.Mediator.prototype.incorrectPassword = function () {
    if (game.popup.password_popup) {
        game.popup.setErrorText();
    }

}


Engine.Mediator.prototype.joinRoom = function () {

    this.connection.joinRoom(this.roomID, this.password);

}
Engine.Mediator.prototype.play = function (action, startpos, endpos, index,mute)
{
    
    this.connection.play(action, startpos, endpos, index,mute);


}

Engine.Mediator.prototype.playYou = function (event)
{
  
    if (game.autoSubmit && event.submit) {
        game.board.turnOffUndo();
        this.Submit();
    } else {
        PlayDrag();
        game.board.lightUndo(event.submit);
    }
    


    if (event.last)
    {
        game.board.turnOffUndo();
    }

    game.player.movesList = event.moves;
    game.logic.calculatePips(game.player);
}

Engine.Mediator.prototype.playEnemy = function (event)
{

    if (event.action === 0)
    {
        var rowobj = game.board.getBoard()[event.start];
        game.logic. movePlay(event.end, event.index, rowobj, game.opponent);
    
    } else if (event.action === 1)
    {
        var rowobj1 =  game.opponent.killedStones;
        game.logic.movePlay(event.end, event.index, rowobj1, game.opponent);
    } else if (event.action === 2) {
        var rowobj2 = game.board.getBoard()[event.start];
        game.logic. discardPlay(game.opponent, rowobj2, event.index);
    }


    game.logic.calculatePips(game.opponent);
 
    if (event.last) {
        game.opponent.stopTimer();
        game.opponent.turn = false;

        game.player.stopTimer();
        game.player.turn = false;
    }  
}

Engine.Mediator.prototype.undo = function () {

    this.connection.undo();

}


Engine.Mediator.prototype.UndoYou = function (event)
{
    var startPosition =   event.start;
    var endPosition =  event.end;
    var value = event.value;
    var enemyState = event.opstate;
    var state = event.state;
    var killer = event.kill;
    var last = event.last;
    var action = event.action;

    if (last)
    {
        game.board.showUndo();
    }

    game.logic.PlayUndo(game.player, startPosition, endPosition, value, enemyState, state, killer, action);

    game.logic.calculatePips(game.player);

  
    game.player.undo = event.round.player.undo_list;
    game.player.movesList = event.round.player.moves;

    game.board.emptyBoard();
    game.board.reconnectBoard(event.round.board_state);
    game.board.showUndo();
    if (game.player.undo.length > 0) {
        game.board.lightUndo(false);
    }

}




Engine.Mediator.prototype.UndoEnemy = function (event)
{
    var startPosition = game.logic.getCorrectPosition(event.start);
    var endPosition = game.logic.getCorrectPosition( event.end);
    var value = event.value;
    var enemyState = event.op_state;
    var state = event.state;
    var killer = event.kill;
    var action = event.action;


    game.logic.PlayUndo(game.opponent, startPosition, endPosition, value, enemyState, state, killer, action);

    game.logic.calculatePips(game.opponent);
}

 

Engine.Mediator.prototype.InvalidMoveState = function (event) {
    var soundCheck = game.sound;
    game.sound = false;
    game.board.emptyBoard();
    game.board.reconnectBoard(event.board_state);

    game.player.killedStones.removeKills();
    game.player.killedStones.addChips(event.kills);

    game.player.discardPanel.removeDiscards();
    game.player.discardPanel.addDiscards(event.discards);


    game.opponent.killedStones.removeKills();
    game.opponent.killedStones.addChips(event.op_kills);

    game.opponent.discardPanel.removeDiscards();
    game.opponent.discardPanel.addDiscards(event.op_discards);
 
    if (true) {
        game.player.state = event.state 
        game.player.movesList = event.moves_list
        game.player.undo = event.undo;

    }else  {
        game.opponent.state = event.state
        game.opponent.movesList = event.moves_list
        game.opponent.undo = event.undo;
    }

    if (event.you && game.player.undo.length > 0) {
        game.board.lightUndo(false);
    } else if (event.you && game.player.undo.length ==0) {
        game.board.showUndo();
    }

    if (!event.roll) {
        game.player.movesList = [];
    }

    try {
        var activePlayer = game.opponent;
        if (game.player.turn) {
            activePlayer = game.player;
        }

        game.board.dicer.SetFullAlpha(activePlayer.movesList);


    } catch (ex) {
        
    }
    game.sound = soundCheck;
}

Engine.Mediator.prototype.InvalidMove = function (event) {

    var soundCheck = game.sound;
    game.sound = false;

    var round = event.round;
    var board = round.board_state;

    game.board.clearBoard();
    game.board.reconnectBoard(board);
    var round_players = round.players;     

    for (var i = 0; i < round_players.length; i++) {
        var player1 = round_players[i];

        var currentPlayer = GetPlayerByID(player1.player_id);
        currentPlayer.killedStones.removeKills();
        currentPlayer.killedStones.addChips(player1.kills);
        currentPlayer.discardPanel.removeDiscards();
        currentPlayer.discardPanel.addDiscards(player1.discards);
        currentPlayer.turn = player1.turn;
        currentPlayer.state = player1.state;
        currentPlayer.movesList = player1.moves;
        currentPlayer.roll = player1.roll;
        currentPlayer.submit = player1.submit;
        currentPlayer.undo = player1.undo_list;
        currentPlayer.reserverTime = player1.reserve_timer_size;
        currentPlayer.turnTime = player1.turn_timer_size;
        currentPlayer.dice = player1.dice;
    } 

    if (game.player.turn) {
         game.board.showUndo();
        if (game.player.undo.length > 0) {
            game.board.lightUndo(game.player.submit);
        }
    }  

    game.logic.calculatePips(game.player);
    game.logic.calculatePips(game.opponent);



    try {
        var activePlayer = game.opponent;
        if (game.player.turn) {
            activePlayer = game.player;
        }

        game.board.dicer.SetFullAlpha(activePlayer.movesList);


    } catch (ex) {
        console.log(ex);
    }

    game.sound = soundCheck;
}

Engine.Mediator.prototype.playSeveralMoves = function (startpos, endposArr) {

    this.connect.playSeveralMoves(startpos, endposArr);
}

Engine.Mediator.prototype.reconnect = function (event) {
    game.sound = false;
    game.removePopup();

    var raise_count = event.raise_count;
    game.raise_count = event.raise_count;
    var total_bet = event.bet;
    var state = event.state;
    var players = event.players;

    var round = event.round;
  
    game.game_id = event.game_id;

    game.can_discard_kill = event.can_discard_kill;
    if (state == 2 && round) {
        var round_players = round.players;       

        game.round_id = round.round_id;

        var board = round.board_state;     
        game.board.clearBoard();
        game.board.reconnectBoard(board);
        for (var i = 0; i < players.length; i++)
        {
            var player2 = players[i]; 
            var player = game.opponent;
            player.is_connected = player2.is_connected;
            if (player2.player_id == game.player.player_id) {
                player = game.player;
            } else {
                if (!player.is_connected) { 
                    game.engine.opponentStatusChanger(false);
                } 
                game.opponent.player_id = player2.player_id;
            }
           
            player.setPoint(player2.point);
            player.setName(player2.username);
            player.can_raise = player2.can_raise;
            player.canRaise = player2.can_raise;
          
        }

        for (var i = 0; i < round_players.length; i++) {
            var player1 = round_players[i];

            var currentPlayer = GetPlayerByID(player1.player_id);

            currentPlayer.killedStones.addChips(player1.kills);  
            currentPlayer.discardPanel.addDiscards(player1.discards); 
            currentPlayer.turn = player1.turn;
            currentPlayer.state = player1.state;
            currentPlayer.movesList = player1.moves;
            currentPlayer.roll = player1.roll;
            currentPlayer.submit = player1.submit;
            currentPlayer.undo = player1.undo_list;
            currentPlayer.reserverTime = player1.reserve_timer_size;
            currentPlayer.turnTime = player1.turn_timer_size;
            currentPlayer.dice = player1.dice; 

      
             
        }
        if (game.player.turn && game.player.dice&& game.player.dice.is_double) {
            game.can_discard_kill = true;
        }
         
      
    }

    
    game.rightPanel.animate(raise_count);

    game.rightPanel.setBetText(total_bet);  
 
  
 

    if (game.opponent.turn) {
            game.board.turnOffUndo();
            
    } else if (game.player.turn) {
 
        if (game.player.roll) {
            game.board.showUndo();
           
            if (game.player.undo.length > 0) {
                  game.board.lightUndo(game.player.submit);
                }                
        } else {
            game.player.movesList = [];
            if (game.player.canRaise) {

                game.rightPanel.raise.mouseEnabled = game.player.canRaise;
                game.rightPanel.raise.alpha = 1.0;
            } else {
                game.rightPanel.raise.alpha = 0.5;
            }
                game.board.showRoller();
            }
    }

 
 

    game.logic.calculatePips(game.player);
    game.logic.calculatePips(game.opponent);

    var pl;
    if (game.player.turn) {
        pl = game.player;
    } else {
        pl = game.opponent;
    }
    game.sound = true;
    if (pl.roll) {
        game.board.setPosition();
        game.board.dicer.roll(pl.dice.first, pl.dice.second);
        
    } else {
        if (game.player.turn) {           
            game.board.roller.setDices(pl.dice.first, pl.dice.second);            
        }
    }
 
    var difference = event.time_passed;

    if (difference - pl.turnTime > 0) {
        pl.StartTimer(event.full_reserve_size, event.full_reserve_size - (difference - pl.turnTime - 2), red_color)

    } else {
        pl.StartTimer(pl.turnTime, pl.turnTime - (difference),  yellow_color)
    }


    try {
        var activePlayer = game.opponent;
        if (game.player.turn) {
            activePlayer = game.player;
        }

        game.board.dicer.SetFullAlpha(activePlayer.movesList);


    } catch (ex) {
        console.log(ex);
    }
}


Engine.Mediator.prototype.endGame = function(event) {
     

    var point = event.point;
    var last = event.last;
    var bet = event.money;
    var pl = getActivePlayer();
    if (pl)
    {
        pl.turn = false;
        pl.StopTimer();
    }


    game.rightPanel.setBetText(bet);

    if (event.win)
    {
        game.rightPanel.myPoint.text = point;
    } else {
        
        game.rightPanel.enPoint.text = point;
    }

    game.player.movesList = [];
    game.opponent.movesList = [];

    if (last) {
        game.firstRoll = true;
     
        if (event.win) {
            game.popup = new WinPopup( event.timer_size,event.coin_count);
        } else {
            game.popup = new LosePopup(event.timer_size, event.coin_count);
        }
           game.board.turnOffUndo();

    } else {
     

        game.removePopup();
        game.board.clearBoard();
        game.logic.drawRows();
        game.board.turnOffUndo();
    }

    game.board.roller.visible = false;

}

Engine.Mediator.prototype.surrender = function(result) {

    this.connection.surrender(result);
}


Engine.Mediator.prototype.raise  = function() {
    game.board.roller.visible = false;
    //game.player.StopTimer();
    this.connection.raise();
}

Engine.Mediator.prototype.raiseAnswer = function (ans)
{
   
    this.connection.raiseAnswer(ans);
}

Engine.Mediator.prototype.SubmitYou = function (event)
{
  
}


Engine.Mediator.prototype.SubmitEnemy = function (event) {   
    if (event.blocked) {

    } else {
   
        game.player.canPlay = true;  
    }
     
}


Engine.Mediator.prototype.turnTimer = function (event) {
    game.can_play = true
    var pl = GetPlayerByID(event.player_id);
    if (!pl) {
        pl = game.opponent;
        this.connection.Send("opponent_info", obj);
    }


 


    var opponent = pl.getOpponent();
    opponent.changeTurn();
    game.board.turnOffUndo();

    game.player.movesList = [];
    
    pl.turn = true;
    pl.StartTimer(event.timer_size, event.left_size, yellow_color);
     
    game.board.roller.autoRoll(event.r1, event.r2);
    game.logic.roll(event);
    pl.canRaise = event.can_raise;
    opponent.canRaise = false;

    if (game.autoRoll) {
        game.rightPanel.raise.alpha = 0.5;
        game.rightPanel.raise.mouseEnabled = false;
    } else {
        if (game.player.canRaise) {
            game.rightPanel.raise.alpha = 1.0;
            game.rightPanel.raise.mouseEnabled = true;
        } else {
            game.rightPanel.raise.alpha = 0.5;
            game.rightPanel.raise.mouseEnabled = false;
        }
    }

}
 
Engine.Mediator.prototype.PlayAgain = function () {

    this.connection.PlayAgain();
}

Engine.Mediator.prototype.Raise = function (event) {

    game.board.roller.visible = false;
   
    if (event.you) {
        game.player.StopTimer();
        game.removePopup();
        game.popup= new WaitPopup("wait_op",false);
        game.player.canRaise = false;

    } else {
        game.opponent.StopTimer();
        game.removePopup();
        game.popup = new RaisePopup(game.logic.NextRaise(event.raise), event.beaver, event.raise_timer);
    }

}

Engine.Mediator.prototype.Submit= function() {

    this.connection.Submit();
}

Engine.Mediator.prototype.RaiseAnswer = function (event) {

    var you = event.you;
    game.raise_count = event.raise_count;
    var raisex = event.raise_count;
    var currBet = event.current_bet;


    game.player.canRaise = event.can_raise;

    game.rightPanel.animate(raisex);
 
    game.rightPanel.setBetText(currBet);
 
 
    game.removePopup();
 
}


function ServerPing() {

    if (!error) {
   
    var sendObj = {};
    var date = new Date();
    sendObj.date = date.getTime();
    game.engine.connection.Send("ping_latency", sendObj);

    //setTimeout(ServerPing, 5000);

    }
}

function movingStateDrag(firstArg, secondArg, thirdArg,mute) {
    var arg = {};
    arg.firstArg= firstArg;
    arg.secondArg= secondArg;
    arg.thirdArg = thirdArg; 
    arg.mute = mute; 
    game.moves.push(arg);

}

function PlaySecondHint(start) {
 
    if (game.moves.length>0) {
        var arr = [];
        var last_posit;
        var last_index;
        var last_row = game.moves[0].thirdArg;
  
    for (var i = 0; i < game.moves.length; i++) {
            arr.push(game.moves[i].firstArg);
            game.logic.movePlayFast(game.moves[i].firstArg, game.moves[i].secondArg, game.moves[i].thirdArg, game.player);
            last_posit = game.moves[i].firstArg;
            last_index = game.moves[i].secondArg;
            last_row = game.moves[i].thirdArg;
            game.moves.splice(0,1);
        }  

    game.logic.animate(last_posit, last_index, last_row, game.player);

    }
  //game.engine.playSeveralMoves(   game.player);
}

function PlayDrag() {
 
    if (game.moves.length==0) {
        return;
    }

    dragMute = game.sound || dragMute;
    game.sound = false;
 
    var arg = game.moves[0];
    game.moves.splice(0, 1);
    game.logic.movingStateDrag(arg.firstArg, arg.secondArg, arg.thirdArg, arg.mute);

    if (game.moves.length == 0) {
        game.sound = dragMute;
        dragMute = false;
        return;
    }
}

function ServerPing() {
    try {
         
        var sendObj = {};
        var date = new Date();
        sendObj.date = date.getTime();
        Send("ping_latency", sendObj);
        setTimeout(ServerPing, 5000);
    } catch (ex){

    }
}