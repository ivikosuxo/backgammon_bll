﻿

BGSocket = {
    BgSock: function () {
        var that = this;
        this.ws = null;
    }
}



BGSocket.BgSock.prototype.connect = function () {
    var connection = $.connection(signalRUrl);
    this.connection = connection;
    connection.qs = { 'token': getQueryVariable("token") };
    connection.received(function (data) {
        var dat = JSON.parse(data);
        onResponse(dat);
    });
    connection.start({ transport: ['webSockets'] }).fail(function () {
        game.removePopup();
        game.popup = new WaitPopup("connection_lost", true);
    });

    connection.disconnected(function () {
        if (!error) {
            game.removePopup();
            game.popup = new WaitPopup("connection_lost", true);
        }
    }); 
}

 

BGSocket.BgSock.prototype.joinRoom = function (roomId, password) {
    var obj = {};
    obj.room_id = roomId;
    obj.password = password;
    obj.tournament_id = getQueryVariable("tournament_id");
    if (game.mobile) {
        obj.mobile = true;
    }
    this.Send("tournament_join", obj);
}

BGSocket.BgSock.prototype.logIn = function () {
    var obj = {};
    var token = getQueryVariable("token");
    obj.token = token;
    this.Send("login", obj);

}
BGSocket.BgSock.prototype.changeStoneColor = function (color) {
    var send = {};
    send.color = color;

    this.Send("change_stone", send);
}
BGSocket.BgSock.prototype.Send = function (command, data) {
    data.command = command;
    this.connection.send(JSON.stringify(data));
}

BGSocket.BgSock.prototype.Submit = function () {
    var send = {};

    this.Send("submit", send);
}

BGSocket.BgSock.prototype.sendChatText = function (text) {
    var send = {};
    send.text = text;
    this.Send("chat", send);
}

BGSocket.BgSock.prototype.preJoinRoom = function (RMid) {
    var obj = {};
    obj.room_id = RMid;
    obj.tournament_id = getQueryVariable("tournament_id");
    this.Send("tournament_pre_join", obj);
}

BGSocket.BgSock.prototype.Roll = function () {
    var obj = {};

    this.Send("roll", obj);
}


BGSocket.BgSock.prototype.play = function (action, startpos, endpos, index,mute) {
    var obj = {};
    obj.action = action;
    obj.index = index;
    obj.end = endpos;
    obj.start = startpos;
    obj.mute = false
    if (mute) {
        obj.mute = true;
    }

    this.Send("play", obj);
}


BGSocket.BgSock.prototype.undo = function () {
    var obj = {};
    this.Send("undo", obj);

}

BGSocket.BgSock.prototype.surrender = function (result) {
    var obj = {};
    obj.game = result;
    this.Send("give_up", obj);

}

BGSocket.BgSock.prototype.raise = function () {
    var obj = {};
    this.Send("raise", obj);

}

BGSocket.BgSock.prototype.raiseAnswer = function (value) {
    var obj = {};
    obj.answer = value;
    this.Send("raise_answer", obj);

}

 