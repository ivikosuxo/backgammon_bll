function LoadingBar() {
    createjs.Container.call(this);
    var that = new createjs.Container();
    var thit = this;

    var text1 = GetTranslatedValue("loading") + "...";


    thit.background = new createjs.Bitmap("../../Content/Images/Loadingbg.jpg");
    thit.background.regX = 955;
    thit.background.regY = 540;
    thit.addChild(thit.background);
    thit.addChild(that); 


    thit.logo = new createjs.Bitmap("../../Content/Images/nardiLogo.png");
    thit.logo.x = 110;
    thit.logo.y = -160;
    that.addChild(thit.logo);

    that.timer1 = new createjs.Shape();
    that.timer1.x =  70;
    that.timer1.y = -15;
    that.addChild(that.timer1);

    that.blur1 = new createjs.Shape();
    that.blur1.x = 70;
    that.blur1.y = -15;
    that.addChild(that.blur1);

    that.blur1.graphics.beginFill("#313131").drawRect(0, 0, 300, 8); 

    that.timer = new createjs.Shape();
    that.timer.x = 70;
    that.timer.y = -15;
    that.addChild(that.timer);


    that.timerText = new createjs.Text(text1 + " 0%", "25px Nusxuri", "#999999");
    that.timerText.textAlign = "center"
    that.timerText.x = 220;
    that.timerText.y = 30;
    that.addChild(that.timerText);

    thit.loading = function (percent) { 
        if (percent < 8) {
            percent = 8;
        } 
        that.timer.graphics.beginFill(yellow_color).drawRect(0, 0, (3 * percent), 8);
        that.timerText.text = text1 + " " + Math.ceil(percent) + "%"; 
    }

    thit.resizeContainer = function () {

        var sY = window.innerHeight / fullsCreenY;

        that.scaleX = sY;
        that.scaleY = sY;
        that.x = (window.innerWidth - 440 * sY) / 2
        that.y = (window.innerHeight) / 2;


        var scl1 = window.innerHeight / 1080;
        var scl2 = window.innerWidth / 1910;
        if (scl1 < scl2) {
            scl1 = scl2;
        }
        thit.background.scaleX = scl1 + 0.1;
        thit.background.scaleY = scl1 + 0.1;
        thit.background.x = window.innerWidth / 2;
        thit.background.y = window.innerHeight / 2;

    }
    thit.resizeContainer();
}
LoadingBar.prototype = new createjs.Container();
LoadingBar.prototype.constructor = LoadingBar