var stage, w, h, loader;
var canv;
var bg, chip;
var update = true;
var game;
var error = false;
var scaler1 = 1;
var scaler2 = 1;
var scaleX = 1;
var scaleY = 1;

function stop() {
	createjs.Ticker.removeEventListener("tick", tick);
}

 
$(document).ready(function(){

   ready();
         
});


 var scl = window.innerHeight/fullsCreenX;
function ready()
{ 
 	game = new Backgammon.Game();
 	game.initialize("main-board");

  $(window).resize(function(){
  
    resizeListener();
});

  setInterval(function () { window.scrollTo(0, 1);  },1000)

 }


 



function resizeListener()
{
    
      var height = window.innerHeight;
      var width = window.innerWidth;
      scl = height / fullsCreenY;
      scaleX = width / fullsCreenX;
      scaleY = scl;
      var canvas = document.getElementById("main-board");

      var w = 1920;
      var h = 1080;
      var canvaWidth =  w * window.devicePixelRatio;
      var canvaHeight = h * window.devicePixelRatio; 



      if (canvaWidth>4096) {
          scaler1 = canvaWidth / 4096;
      } 
      if (canvaHeight > 4096) {
          scaler2 = canvaHeight / 4096;
      }
      
      canvas.width = canvaWidth / (scaler1*scaler2);
      canvas.height = canvaHeight / (scaler1 * scaler2);

      canvas.style.width = w + "px";
      canvas.style.height = h + "px";

      canv.scaleX = window.devicePixelRatio / (scaler1 * scaler2);
      canv.scaleY = window.devicePixelRatio / (scaler1 * scaler2);
   try{
        
        game.board.resizeContainer();
      //scl = game.board.scaleX;
        game.rightPanel.scaleX = game.rightPanel.scaleY = scl;

 
        if (game.leftPanel) {
            game.leftPanel.resizeContainer();
        }
        
        if (game.settingsPopup) {
            game.settingsPopup.resizeContainer();
        }


        if (game.popup) {
            game.popup.resizeContainer();
        } 

        if (game.chatPopup) {
            game.chatPopup.resizeContainer();
        }  

    }catch(ex)
    {
      console.log(ex);
      }
  rotateScreen();
 game.loadingBar.resizeContainer();
 
}

function requestFullScreen1() {

  var el = document.body;

  // Supports most browsers and their versions.
  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen 
  || el.mozRequestFullScreen || el.msRequestFullScreen;

  if (requestMethod) {

    // Native full screen.
    requestMethod.call(el);

  } else if (typeof window.ActiveXObject !== "undefined") {

    // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");

    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
}

function requestFullScreen2() {
     console.log("shemovida");
  var elem = document.body;
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        
        if (elem.requestFullScreen) {
          console.log("error1");
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
           console.log("error2");
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
               console.log("error3");
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
               console.log("error4");
            elem.msRequestFullscreen();
        }
        else{
           console.log("error10");
        }
    } else {
        if (document.cancelFullScreen) {
      
            document.cancelFullScreen();
             console.log("error5");
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
             console.log("error6");

        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
             console.log("error7");
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
             console.log("error8");
        }else{
           console.log("error9");
        }
    }
}
 

 function requestFullScreen( ) {

  var element = document.body;
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}

function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
 }

function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
    else {
        cancelFullScreen.call(doc);
    }
}









