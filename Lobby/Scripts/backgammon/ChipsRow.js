﻿function ChipsRow(ind) {
    createjs.Container.call(this);
    this.index = ind;
    this.asc = 1;
    var that = this;
     if (ind>11) {
        this.asc= -1;
     }
     this.freezeCount = 1;
    that.blocked = false;
     
    that.hintArrow = {};
   
   that.dragStart = false;
    
    function initialize() {

        that.x = detectPositionX();
        that.y = detectPositionY();
        that.asc = detectAscending();

       // var hintArr = loader.getResult("arrowh");
        that.hintArrow = new createjs.Bitmap()//hintArr;

        if (that.asc === 1) {
            that.hintArrow.rotation = 180;
            that.hintArrow.y = that.y - 13 * that.asc;
            that.hintArrow.x = that.x+50;
            that.hintArrow.rotation = 180;
        } else {
            
            that.hintArrow.y = that.y - 85 * that.asc;
            that.hintArrow.x = that.x + 38;
 
        } 
        that.hintArrow.y.visible = false;
         
      // game.board.addChild(that.hintArrow);
         that.on('mouseout', function(){hideHints()});
         that.on('mousedown', function(){hintCalculate(true)});
         that.on('mouseover', function(){hintCalculate(false)});
    
    }
 
     
        //that.addEventListener("click", function () {
        //    if (!game.player.turn) {
        //        return false;
        //    }
         
        //    var activePl =game.player;
        //    if (activePl.state === 2 && that.getColor() === activePl.color  )
        //    {
        //        discardState(activePl, that);
        //        that.showChip();
        //    }

    
         
        //});

        function  hintCalculate(isDrag) {


         /* if (!game.can_play) {
                console.log("canplay");
                return;
            } */
            
    

            hideHints();      

            if (isDrag) {
               
                game.can_play = false;
            }

            that.dragStart = isDrag
            var activePl = game.player;
            var isDragable = false;
            if (!activePl)
            { 
                return;
            }
            if (that.getColor() != 1 ||activePl.movesList.length == 0 ||game.player.killedStones.count > 0) {
                return;
            }
           

                var canPlay1=false;
                var canPlay2 = false;
                var canPlay3 = false;
                var canPlay4 = false;
                var canPlay5= false;

                var killing1 = false;
                var killing2 = false;
                var killing3 = false;
                var killing4 = false;
                var killing5= false;

                game.lastTime = new Date();
                var containers = game.board.getBoard();
                var color = that.getColor();
             
                
                var pos = that.index + activePl.movesList[0];
          

                 
                if (pos < 24 && pos >= 0 && game.logic.canMoveOnPosition(containers[pos], color, that.isBlocked()) && game.logic.canPlaySixCards(pos, that.index, color)) {

                    killing1 = game.logic.killing(containers[pos], color, pos);
                 
                    var posY = containers[pos].y+ containers[pos].getChipsLastPosition();
                    var posX = containers[pos].x + containers[pos].getChipsHintPositionX();
              
                    game.hint1.x = posX;
                    game.hint1.y = posY;

                    game.hint1.visible =true;
                    game.hint1.moveIndex = 0;
                
                    game.hint1.movePos = pos;
                    game.hintArrow1 = containers[pos].getHintArrow();
                    game.board.addChild(game.hintArrow1);

                    //  game.hint1.alpha = 0.5;

                    canPlay1 = true;             
                    isDragable = true;

                  
                    

                }
                if (activePl.movesList.length>1 && !activePl.isDouble) {
                    
                       
                        var pos1 = that.index + activePl.movesList[1];
                      
                        killing2 = (pos1 < 24 && pos1 >= 0 && game.logic.killing(containers[pos1], color,pos1));
                        if (pos1 < 24 && pos1 >= 0 && game.logic.canMoveOnPosition(containers[pos1], color, that.isBlocked()) && game.logic.canPlaySixCards(pos1, that.index, color))
                        {
                            
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX() ;
                   
                             game.hint2.x = posX2;
                             game.hint2.y = posY2;
                             game.hint2.visible =true;
                             game.hint2.moveIndex = 1;
                        
                            game.hint2.movePos = pos1;
                            game.hintArrow2= containers[pos1].getHintArrow();
                            game.board.addChild(game.hintArrow2);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay2 = true;
                          
                        }
                        
                }


                if (activePl.movesList.length>1  && (((canPlay1 || canPlay2 )&& !activePl.isDouble )||((canPlay1 )&& activePl.isDouble )) ) 
                {
                         
                       koef= 1;
                      
  

                        var diff = 1;                        
                        var canPl1 =false;
                        var canPl2= false;

                        var pos1 = that.index + activePl.movesList[0] + activePl.movesList[1];
                        if (canPlay2 && game.logic.notBlocked(color, that.index + activePl.movesList[1], killing2, pos1)) {
                            diff = 1;
                            canPl2 = true;
                        } 
                         else   if (canPlay1 && game.logic.notBlocked(color, that.index + activePl.movesList[0],   killing1, pos1)) {
                             
                                diff  =0;
                                canPl1 = true;
                        } 
                      
                            var pos2 = that.index + activePl.movesList[diff];
                            var pos1 = that.index + activePl.movesList[diff] + activePl.movesList[1 - diff];

                            killing3 = (pos1 < 24 && pos1>17 && game.logic.killing(containers[pos1], color,pos1));

                        if (pos1 < 24 && pos1 >= 0 && game.logic.canMoveOnPosition(containers[pos1], color, that.isBlocked()) && (canPl1 || canPl2) && game.logic.canPlaySixCards(pos1, that.index, color))
                        {
                          
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                          
                            game.hint3.x = posX2;
                            game.hint3.y = posY2;
                            game.hint3.visible =true;
                            game.hint3.moveIndex = diff;
                            game.hint3.moveIndex1 = 0;
                       
 
 
                            game.hint3.movePos = pos2;
                            game.hint3.movePos1 = pos1;
                    
                            game.hintArrow3= containers[pos1].getHintArrow();
                            game.hint3.parentRow =containers[pos2];
                            game.board.addChild(game.hintArrow3);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay3 =true;

                          

                        }

                        
                }

                if (activePl.movesList.length>2  && canPlay3) 
                {

                    
                            var pos3 = that.index + activePl.movesList[0];
                            var pos2 = that.index + activePl.movesList[0] + activePl.movesList[0];
                            var pos1 = that.index + activePl.movesList[0] + activePl.movesList[0] + activePl.movesList[0];
                        
                            killing4 = (pos1 < 24 && pos1 >= 17 && game.logic.killing(containers[pos1], color, pos1));
                            if (pos1 < 24 && pos1 >= 0 && game.logic.canMoveOnPosition(containers[pos1], color, that.isBlocked()) && game.logic.notBlocked(color, pos2,killing1|| killing3, pos1) && game.logic.canPlaySixCards(pos1, that.index, color))
                        {
                        
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                      
                            game.hint4.x = posX2;
                            game.hint4.y = posY2;

                            game.hint4.visible = true;
                            game.hint4.moveIndex = 0;
                            game.hint4.moveIndex1 = 0;
                            game.hint4.moveIndex2 = 0;
                      
 
 
                            game.hint4.movePos = pos3;
                            game.hint4.movePos1 = pos2;
                            game.hint4.movePos2 = pos1;
                    
                            game.hintArrow4= containers[pos1].getHintArrow();
                            game.hint4.parentRow =containers[pos3];
                            game.hint4.parentRow1 =containers[pos2];
                            game.board.addChild(game.hintArrow4);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay4=true;
                            

                        }

                        
                }
               if (activePl.movesList.length>3  && canPlay4) 
                {

                   
                            var pos4 = that.index + activePl.movesList[0];
                            var pos3 = that.index + activePl.movesList[0] + activePl.movesList[0];
                            var pos2 = that.index + activePl.movesList[0] + activePl.movesList[0] + activePl.movesList[0];
                            var pos1 = that.index + activePl.movesList[0] + activePl.movesList[0] + activePl.movesList[0] + activePl.movesList[0];
                      
                            killing5  =  (pos1 < 24 && pos1 >= 0 && containers[pos1].killing(color));

                       if (pos1 < 24 && pos1 >= 0 && game.logic.canMoveOnPosition(containers[pos1], color, that.isBlocked()) &&( game.logic.notBlocked(color, pos2, killing1 || killing3 || killing4, pos1) )&& game.logic.canPlaySixCards(pos1, that.index, color))
                        {
                        
                            var posY2 =   containers[pos1].y + containers[pos1].getChipsLastPosition();
                            var posX2 = containers[pos1].x + containers[pos1].getChipsHintPositionX();
                               
                            game.hint5.x = posX2;
                            game.hint5.y = posY2;
                            game.hint5.visible = true;

                            game.hint5.moveIndex = 0;
                            game.hint5.moveIndex1 = 0;
                            game.hint5.moveIndex2 = 0;
                            game.hint5.moveIndex3 = 0;

 
                            game.hint5.movePos = pos4;
                            game.hint5.movePos1 = pos3;
                            game.hint5.movePos2 = pos2;
                            game.hint5.movePos3 = pos1;
                    
                            game.hintArrow5= containers[pos1].getHintArrow();
                            game.hint5.parentRow =containers[pos4];
                            game.hint5.parentRow1 =containers[pos3];
                            game.hint5.parentRow2 =containers[pos2];
                            game.board.addChild(game.hintArrow5);
                         //   game.hint2.alpha = 0.5;
                            isDragable = true;
                            canPlay5=true;
                           
                         
                        }

                        
                }

                
     
                if (isDragable &&isDrag)
                { 
               
                    that.hideChip();
                    game.dragStone = new Stone(color);
                    game.dragStone.alpha = 0.8;
                    game.dragStone.x = game.board.x + that.x- 16 ;
                    game.dragStone.y = game.board.y + that.y + that.getChipsLastPosition()-16;
                    game.dragStone.parentRow = that;
                    game.dragStone.visible = false;
                    stage.addChild(game.dragStone);                 
               
                }else{
                     game.dragStone = null;
               }

                if (game.player.state === 2) {
              
              if (!(game.hint1.visible || game.hint2.visible || game.hint3.visible)) {                 
                   game.lastTime = new Date();
                   game.dragStone = {};
                   game.dragStone.parentRow = that;

                }

            }
    

        }

 


 function  hideHints()
    {

        if ( game.dragStone) {
            return;
        }
 

 if (game.hint1.visible) {
                
                game.hint1.visible = false;
            }
            if (game.hint2.visible ) {
               
                game.hint2.visible = false;

            }


                if (game.hint3.visible ) {
               
                game.hint3.visible = false;

            }

                if (game.hint4.visible ) {
               
                game.hint4.visible = false;

            }
                if (game.hint5.visible ) {
              
                game.hint5.visible = false;

            }

            if (game.hintArrow1) {
                game.board.removeChild(game.hintArrow1);
                game.hintArrow1 = null;
            }
            if (game.hintArrow2) {
                game.board.removeChild(game.hintArrow2);
                game.hintArrow2 = null;
            }
            if (game.hintArrow3) {
                game.board.removeChild(game.hintArrow3);
                game.hintArrow3 = null;
            }
            if (game.hintArrow4) {
                game.board.removeChild(game.hintArrow4);
                game.hintArrow4 = null;
            }

                    if (game.hintArrow5) {
                game.board.removeChild(game.hintArrow5);
                game.hintArrow5 = null;
            }

    }


        that.getHintArrow = function() {
           


                if (that.asc === 1) {
                    that.hintArrow.rotation = 180;
                    that.hintArrow.y = that.y - 10 * that.asc;
                    that.hintArrow.x = that.x + 26;
                    that.hintArrow.rotation = 180;
                } else {


                    that.hintArrow.y = that.y - 45 * that.asc;
                    that.hintArrow.x = that.x + 12;

                }
            
            return that.hintArrow;
        }

 

      
 



    that.getDifference = function() {

 
   
        return 78;


    }

    that.updateChildrenPositions = function()
    {
        for (var i = 0; i < that.numChildren; i++) {
         
            if (i<5) {
                that.children[i].y = that.asc * that.getDifference() * ((i) % 5);
                   that.children[i].x = that.getOffsetX() * i;
            } else {
                that.children[i].y = that.asc * that.getDifference() * (4);
                that.children[i].x = that.getOffsetX() * 4;
            }
            
            that.children[i].changeSprite(that.index<12);
        }

    }

    that.getOffsetX = function ()
    {
 
            return 0;
         
    }


    that.updatePosition = function() {
   
        that.x = detectPositionX();
        if (game.board2D && game.reverseBoard<0) {
            that.x = endPoint+163 - that.x;
        }

        that.y = detectPositionY();
    }


    that.reverseRow = function()
    {
      
            that.x = (endPoint+163  - that.x);
        
    }

    function getNearIndex()
    {
        if (game.board2D) {
            return that.index;
        }

        if (game.reverseBoard === -1) {
            if (that.index <12) {
                return  5+(6 - that.index);
 
            } else if (that.index < 24) {
                return 17 + (18 - that.index);
            }
        }
        return that.index;
    }
var endPoint = 1178;

var stoneSize = 87;

    function detectPositionX()
    {
        

            if (that.index < 6) {

                return endPoint - that.index * stoneSize;
            } else if (that.index < 12) {
                return endPoint - 58 - that.index * stoneSize;
            } else if (that.index < 18) {
                return endPoint - 58 - (23 - that.index) * stoneSize;
            } else if (that.index < 24) {
                return endPoint - (23 - that.index) * stoneSize;
            }
         

        return 0;
    }

    function detectPositionY()
    {
       
          if (that.index<12) {
            return 60;
          }
         return 976;
        

    }

    function detectAscending()
    {
       
        if (that.index < 12) {
            return 1;
        }
        return - 1;
    }

    initialize();
}

ChipsRow.prototype = new createjs.Container();
ChipsRow.prototype.constructor = ChipsRow;

ChipsRow.prototype.addChip=function (stone) {

   
    this.addChild(stone);
    stone.setCount(this.numChildren, this.asc);
    stone.changeBG(this.index<12);
    stone.x = this.getUndoPositionX();
    stone.y = this.getChipsPosition();
     

    PlaySound("chip");
    
     return stone;
}
 
ChipsRow.prototype.isBlocked = function () {

 return game.logic.isBlocked(this)
}



ChipsRow.prototype.getChipsHintPositionX = function () {
    if (game.board2D) {
        return 0;
    }

    if (this.asc === 1) {
        if (this.numChildren > 4) {
            return this.getOffsetX() * (5);
        }
        return this.getOffsetX() * (this.numChildren) -1;
    } else {
        if (this.numChildren > 4) {
            return this.getOffsetX() * (5) - 2;
        }
        return this.getOffsetX() * (this.numChildren) - 2;
    }
}



ChipsRow.prototype.addChipToRow = function (stone) {
  
     
   
    this.addChild(stone);
    stone.setCount(this.numChildren, this.asc);
    stone.changeBG(this.index<12);
    stone.y = this.getChipsPosition();
    stone.x = this.getUndoPositionX();

  
}

ChipsRow.prototype.showChip = function () {
    if (this.numChildren>0) {
        this.children[this.numChildren - 1].visible = true;
    }
    


}

ChipsRow.prototype.hideChip = function () {
    if (this.numChildren > 0) {
    this.children[this.numChildren - 1].visible = false;
}

}


ChipsRow.prototype.getColor = function () {
    var result = -1;
    if (this.numChildren>0) {
        result= this.children[0].type;
    }
    return result;

}

ChipsRow.prototype.addChips = function (count,color) {


    for (var i = 0; i < count; i++)
    {
        var stone = new Stone(color);
        this.addChipToRow(stone);
    }

}

ChipsRow.prototype.removeChip = function () {
    if (this.numChildren>0)
    { 
        if (this.index === 0) {          
            this.freezeCount = this.freezeCount - 1 ;         
            this.blocked = false;     
        }

        var index = this.numChildren - 1;
        var chip = this.children[index];
        this.removeChild(chip); 
        return chip;
    } 
}

ChipsRow.prototype.getLastChipPosit = function () {
    if (this.numChildren>0)
    {

        var index = this.numChildren - 1;
        var chip = this.children[index];

 
        return chip;

    } 

}

ChipsRow.prototype.getChipsPosition = function ()
{
    var difference = 78;
 


    if (this.numChildren >5) {
        return this.asc * (4) * (difference);
    } else {
        return this.asc * ((this.numChildren-1 )) * (difference);
    }
}

ChipsRow.prototype.getChipsLastPosition = function () {

    var difference = 74;
  
    
   
    if (this.numChildren > 4) {
        return this.asc * (5) * (difference);
    } else {
        return this.asc * ((this.numChildren) % 5) * (difference);
    }
}

ChipsRow.prototype.getChipsLastPositionX = function () {
 
    
        return 0;
 

    }

ChipsRow.prototype.getUndoPositionX = function () {
 
  
        return 0;
   
    }




ChipsRow.prototype.canMove = function (color)
{
  
    if (this.numChildren <2 || this.children[0].type === color) {
        return true;
    }
    return false;
}

ChipsRow.prototype.killing = function (color) {
    if (this.numChildren ===1 && this.children[0].type !== color) {
        return true;
    }
    return false;
}

 
