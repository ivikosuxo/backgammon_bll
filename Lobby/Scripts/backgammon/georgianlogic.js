﻿var transformX = [5, 4, 3, 2, 1, 0, 0, -1, -2, -3, -4, -5, 5, 4, 3, 2, 1, 0, 0, -1, -2, -3, -4, -5];
var transformX2d = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

GameLogic = {
    
    GeorgianRule : function () {

    },

     LongRule: function () {

    },
     EuropeanRule: function () {

     },
      HyperRule: function () {

     },
      KhachapuriRule: function () {

      },
      EurekaRule: function () {
      },
      NewEurekaRule: function () {
      }
}

GameLogic.GeorgianRule.prototype.rowClick=function(rowObj)
{
    var activePl = game.player;
    if (game.opponent.turn) {
        return;
    }
    if (activePl.turn && rowObj.getColor() === activePl.color )
    {
        if (activePl.state === 0)
        {
            this.movingState(activePl, rowObj);
        } else if (activePl.state === 1)
        {
            this.reviveState(activePl.color);
        } else if (activePl.state === 2)
        {
            this.discardState(activePl, rowObj);
        }

        
    }
}

GameLogic.GeorgianRule.prototype.getStoneCount = function () {
    return 15;
} 

GameLogic.GeorgianRule.prototype.giveUpPoint = function () {
    

    if (game.player.discardPanel.count>0) {
        return "oin";//game.raise_count;
    }

    if (game.opponent.discardPanel.count>0) {
        if (game.player.killedStones.count > 0) {
            return "kox"; //game.raise_count*3;
        }
    } 

    return "mars"; //game.raise_count * 2;
}

GameLogic.GeorgianRule.prototype.drawRows = function () {


    game.board.clearBoard();
    var board = game.board.getBoard();
    board[0].addChips(2, 1);
    board[5].addChips(5, 2);
    board[7].addChips(3, 2);
    board[11].addChips(5, 1);
    board[12].addChips(5, 2);
    board[16].addChips(3, 1);
    board[18].addChips(5, 1);
    board[23].addChips(2, 2); 

    /*   board[0].addChips(2, 1);
    board[1].addChips(2, 2);
    board[2].addChips(2, 2);
    board[3].addChips(3, 2);
    board[4].addChips(3, 2);
    board[5].addChips(3, 2);

    board[23].addChips(2, 2);
    board[22].addChips(2, 1);
    board[21].addChips(2, 1);
    board[20].addChips(3, 1);
    board[19].addChips(3, 1);
    board[18].addChips(3, 1); */


}

GameLogic.GeorgianRule.prototype.canPlaySixCards = function () {
    return true;
}


GameLogic.GeorgianRule.prototype.killing = function (chipRow,color,pos) {

    if (chipRow.numChildren === 1 && chipRow.getColor() !== color && pos>17) {
        return true;
    }
    return false;
}

GameLogic.GeorgianRule.prototype.canMoveOnPosition = function (rowObj, color,blocked) {


    if (rowObj.index>17&& blocked && rowObj.getColor() == color) {
        return false;
    }

    if (rowObj.numChildren <2|| rowObj.getColor() === color) {
        return true;
    }

    return false;

}

GameLogic.GeorgianRule.prototype.NextRaise = function(raise){

    return raise + 1;
} 

GameLogic.GeorgianRule.prototype.notBlocked = function (clr, pos, killing,pos1) {
    var containers = game.board.getBoard();
    if (pos > 17 && pos1<24 && killing && containers[pos1].getColor() == 1) {
        return false;
    }
    return true;

}

GameLogic.GeorgianRule.prototype.roll = function (event) {


    var containers = game.board.getBoard();
 
    for (var i = 0; i < 24; i++) {
        containers[i].blocked = false;
    }
 
}
GameLogic.GeorgianRule.prototype.movingState=function(activePl,rowObj)
{
  

    var containers = game.board.getBoard();
    var color = rowObj.getColor();


    var result = this.canMove(activePl.color, rowObj.index,activePl.movesList);
  
     
        if (result.result) {
 
            this.movePlay(result.pos, result.index, rowObj , activePl);

            game.engine.play(0, rowObj.index, result.pos, result.index);
        } 
    
}

GameLogic.GeorgianRule.prototype.movingStateDrag=function(index , moveIndex, rowObj,mute) {

    var state = game.player.state;
    if (state===2) {
        state = 0;
    }

    this.movePlay(index, moveIndex, rowObj , game.player);

    game.engine.play(state, rowObj.index, index, moveIndex, mute);

}

GameLogic.GeorgianRule.prototype.freezeCount = function (freeze, index) {
 
    return false
}

GameLogic.GeorgianRule.prototype.movePlayFast = function (endPos, index, rowObj, player) {
    var activePl = player;
    var containers = game.board.getBoard();
    var color = activePl.color;


    if (containers[endPos].killing(color)) {
        if (endPos > 17) {
            containers[endPos].blocked = true;
        }

        activePl.getOpponent().state = 1;
        activePl.getOpponent().killedStones.addKilledStone();
        containers[endPos].removeChip();
    }

    
    activePl.spliceMove(index);
}
GameLogic.GeorgianRule.prototype.animate = function (endPos, index, rowObj, player) {
    var activePl = player;
    var containers = game.board.getBoard();
    var color = activePl.color;
     
    var stone = rowObj.getLastChipPosit();
    if (game.tween && game.tween.st === stone) {
        game.tween.setPaused(true);
    }

    var pt = containers[endPos].localToLocal(0, 0, stone);
    rowObj.removeChip();
    stone.reset();
    containers[endPos].addChip(stone);
    var destX = stone.x;
    var destY = stone.y;
    activePl.spliceMove(index);


    stone.x = pt.x * -1;
    stone.y = pt.y * -1;
    stone.visible = true;

    if (activePl.state !== 2 && activePl.isDiscardState(endPos)) {
        activePl.state = 2;
    }

    var tm = (Math.sqrt(Math.pow(stone.x - stone.y, 2) + Math.pow(stone.y - destY, 2)) / 5.2) / 2;

    game.tween = createjs.Tween.get(stone, { override: true })
        .to({ x: destX, y: destY }, tm);
    game.tween.st = stone;
}




GameLogic.GeorgianRule.prototype.movePlay=function(endPos, index, rowObj , player) {



    var activePl = player;
    var containers = game.board.getBoard();
    var color = activePl.color; 
 
 
    if (containers[endPos].killing(color)) {
        if (endPos > 17) {
            containers[endPos].blocked = true;
        }

        activePl.getOpponent().state = 1;
        activePl.getOpponent().killedStones.addKilledStone();
        containers[endPos].removeChip();
    }

    var stone = rowObj.getLastChipPosit();
    if ( game.tween &&game.tween.st ===stone) {
        game.tween.setPaused(true);
    }  
     
     var pt =  containers[endPos].localToLocal(0, 0, stone);
     rowObj.removeChip();   
     stone.reset();  
     containers[endPos].addChip(stone);
     var destX = stone.x ;
     var destY = stone.y;
     activePl.spliceMove(index);

      
    stone.x = pt.x*-1;
    stone.y= pt.y*-1;
    stone.visible = true; 

    if (activePl.state !== 2 && activePl.isDiscardState(endPos)) { 
        activePl.state = 2; 
    }
  
    var tm =( Math.sqrt(Math.pow(stone.x-stone.y, 2) + Math.pow(stone.y-destY, 2))/5.2)/2;
  
    game.tween = createjs.Tween.get(stone ,{override : true})
    .to({x: destX, y: destY}, tm);
    game.tween.st=stone;
 
}


GameLogic.GeorgianRule.prototype.reviveState=function(color)
{
  
    var activePl = game.player;
 
    if (activePl.color === color)
    {
        var containers = game.board.getBoard();
       
 
        var startPosition = -1;
        if (color === 2) {
            startPosition = 24;
        }


        var result = this.canMove(color, startPosition, activePl.movesList);

        if (result.result) {
             
           this.movePlay(result.pos,result.index,activePl.killedStones,activePl);
           if (activePl.killedStones.count ===0) {
               activePl.state = 0;
           }

            game.engine.play(1, startPosition, result.pos, result.index);
        }  
        
    }


}

 


GameLogic.GeorgianRule.prototype.canMove=function(color , startPosition , list) {
    var obj = {};
    var containers = game.board.getBoard();
    obj.result = false;
    obj.index = -1;
    var coeficient = 1;
    if (color===2) {
        coeficient = -1;
    }

    if (startPosition > -1 && startPosition < 24)
    {
        if ((containers[startPosition].isBlocked())) {
            //  return obj;
        }
    }
     
    if (game.leftMouse) {


        for (var i = 0; i < list.length; i++) {
            var pos = startPosition + coeficient * list[i];
            if (pos > -1 && pos < 24) {
                var canPlayBlocked = false;
                if (startPosition>-1) {
                    canPlayBlocked = containers[startPosition].isBlocked() && containers[pos].getColor() == 1;
                }
              
                
                if (!canPlayBlocked && containers[pos].canMove(color)) {
                        obj.result = true;
                        obj.index = i;
                        obj.pos = pos;
                        return obj;
                    }
              
            }
        }
    } else {

        for (var j =list.length-1; j >=0; j--) {
            var pos = startPosition + coeficient * list[j];
            if (pos > -1 && pos < 24) {
                var canPlayBlocked = false;
                if (startPosition > -1) {
                    canPlayBlocked = containers[startPosition].isBlocked() && containers[pos].getColor() == 1;
                }
                if (!canPlayBlocked && containers[pos].canMove(color)) {
                    obj.result = true;
                    obj.index = j;
                    obj.pos = pos;
                    return obj;
                }
            }
        }



    }
    return obj;
}




GameLogic.GeorgianRule.prototype.canPlaySomething=function(activePl) {
    var color = activePl.color;
    var startingPosition = -1;
 
    if (activePl.state === 0) {
        return this.canPlayMoveState(activePl);

    }else if (activePl.state === 1) 
    {
        var result = this.canMove(color, startingPosition, activePl.movesList);
        return result.result;
    } else if (activePl.state === 2) {

        var candisc = this.canPlayDiscard(activePl);

 
        if (candisc.result)
        {
            return true;
        }
        

        var res = this.canPlayMoveState(activePl);
        
        return res;
      
       
     

    }
    return false;
}

GameLogic.GeorgianRule.prototype.canPlayMoveState=function(activePl) {
    var color = activePl.color;
    var arr = game.board.getBoard();
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].getColor() === color) {
            var result = this.canMove(color, i, activePl.movesList);
            if (result.result) {
                 
                return true;
            }
        }
    }

    return false;

}

GameLogic.GeorgianRule.prototype.canPlayDiscard=function(activePl) {
    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1)
    {
        startPos = 24;
        desc = -1;
    }

    var board = game.board.getBoard();

    for (var i = 0; i < activePl.movesList.length; i++) {
        var posit = startPos + desc * activePl.movesList[i];
        if (board[posit].getColor() === activePl.color && !board[posit].isBlocked()) {
            res.result = true;
            res.index = i; 
            return res;
        }
      
        if (this.canDiscardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i]))
        {
            res.result = true;
            res.index = i;
            return res;
       }
    }

 

 
    res.result = false;
    res.index = -1;
    return res;
}


GameLogic.GeorgianRule.prototype.isBlocked = function (chipRow) {

    return chipRow.blocked && chipRow.numChildren == 1;
       
  

}
 
GameLogic.GeorgianRule.prototype.canDiscardWithoutMatch=function(board, color, startPos, desc, value) {


    var index = this.getDiscardIndex(startPos);


    if (board[startPos + desc * value].isBlocked()) {
        return false;
    }


        for (var i = 24-value; i <24; i++)
        {
            if (board[i].color === color)
            {
                if (board[i].isBlocked()) {
                    return false;
                }
                break;
            }
        }
  
  

    if (value === 6) {


        return true;

    }

    if (index === value)
    {
        return true;
    }
    if (index>value) {
        return false;
    }
 
   
    for (var j = value; j <7; j++) {
         
        if (board[startPos +  desc * j].getColor() === color)
        {
            return false;
        }
   
    }
    return true;
}
GameLogic.GeorgianRule.prototype.discardWithoutMatch=function(board, color, startPos, desc, value, rowobjIndex)
{
    var ind = this.getDiscardIndex(rowobjIndex);
 
    if (ind > value) {
        return false;
    }
    if (ind === value) {
        return true;
    }

    var pointer = 6 - ind;
    for (var j = 1; j < 6; j++) {
        var posit = rowobjIndex + desc * j; 
        if (board[posit].getColor() === color)
        {
            return false;
        }
    }
    return true;
}

GameLogic.GeorgianRule.prototype.getDiscardIndex=function( number)
{

    if (number < 6)
    {
        return number + 1;
    } else if (number > 17)
    {
        return 24 - number;
    }
}

GameLogic.GeorgianRule.prototype.discardState=function(activePl, rowObj) {
    var result = this.discardResultFactory(activePl, rowObj);

   /* if (this.isBlocked(rowObj)) {
        if (game.can_discard_kill)//  this.canPlayMoveState(activePl)) {
            return true;
        }  
    }*/

if ((!this.isBlocked(rowObj) || game.can_discard_kill) &&result.result) {
        this.discardPlay(activePl, rowObj,result.index);
   
        game.engine.play(2, rowObj.index, 24, result.index);
    
    } else {
   
        this.movingState(activePl, rowObj);

    }
}

 

GameLogic.GeorgianRule.prototype.discardPlay =function(activePl, rowObj, index)
{
    rowObj.removeChip();
    activePl.discardPanel.addDiscard();
    activePl.spliceMove(index);

    if (activePl.movesList.length === 0 || activePl.discardPanel.count === 15) //|| !canPlaySomething(activePl) 
    {
            // changeTurn(activePl);
 
    }

}

GameLogic.GeorgianRule.prototype.discardResultFactory=function(activePl,rowObj ) 
{
    
    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1) {
        startPos = 24;
        desc = -1;
    }
    res.index =-1;
    res.result = false;
    var board = game.board.getBoard();
    for (var i = 0; i < activePl.movesList.length; i++) 
    {
     
        if (this.discardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i], rowObj.index))
        {
            res.index = i;
            res.result = true;
            return res;
        }
    }
    return res;
}


function changeTurn(activePl)
{
   
    game.player.changeTurn();
    game.opponent.changeTurn();
 
    activePl.StopTimer();
 
}


function passwordEnter() 
{
    game.engine.password = $("#password").val();
    game.engine.connection.joinRoom(game.engine.roomID, game.engine.password);
}



function getActivePlayer()
{
    if (game.player.turn ) {
        return game.player;
    }
    if (game.opponent.turn) {
        return game.opponent;
    }
    return null;
}
GameLogic.GeorgianRule.prototype.getCorrectPosition = function (startPosition) {

    return 23 - startPosition;

}


GameLogic.GeorgianRule.prototype.PlayUndo = function (activePl, startPosition, endPosition, value, enemyState, state, killer, action) {

    
    var opp = activePl.getOpponent();
    var cont = game.board.getBoard();
    if (action === 0) {      

        cont[endPosition].removeChip();
        cont[startPosition].addChip(new Stone(activePl.color));


    } else if (action === 1)
    {
        cont[endPosition].removeChip();
        activePl.killedStones.addChip();

    } else if (action === 2)
    {
   
        activePl.discardPanel.removeDiscard();
        cont[startPosition].addChip(new Stone(activePl.color));

    }



    if (killer) {
        opp.killedStones.removeChip();
        cont[endPosition].addChip(new Stone(opp.color));
        cont[endPosition].blocked = false;

    }



    opp.state = enemyState;
    activePl.state = state;

    activePl.movesList.push(value);
    activePl.movesList.sort();

  
    game.board.playUndo(value,activePl);

 
}

GameLogic.GeorgianRule.prototype.calculatePips =function(pl)
{
    var counter = 0;
    var container = game.board.getBoard();
    for (var i = 0; i < 24; i++)
    {
        if (container[i].getColor() === pl.color)
        {
            if (pl.color === 1) {
                counter += container[i].numChildren * (24 - i);
            } else
            {
                counter += container[i].numChildren * (i+1);
            }

        }
        
    }

    counter += pl.killedStones.count * 25;

    pl.setPips(counter);

    return counter;


}



function GetLogicByType(type) { 

    switch (type) {
        case 1:
            return new GameLogic.EuropeanRule();
        case 2:
            return new GameLogic.GeorgianRule();
        case 3:
            return new GameLogic.LongRule();
        case 4:
            return new GameLogic.HyperRule();
        case 5:
            return new GameLogic.KhachapuriRule();
        case 6:
            return new GameLogic.EurekaRule();
        case 7:
            return new GameLogic.NewEurekaRule();
    }
    return new GameLogic.LongRule();
}