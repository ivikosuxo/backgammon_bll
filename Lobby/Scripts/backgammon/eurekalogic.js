﻿ 

 

GameLogic.EurekaRule.prototype.rowClick=function(rowObj)
{
    var activePl = game.player;
    if (game.opponent.turn) {
        return;
    }
    if (activePl.turn && rowObj.getColor() === activePl.color )
    {
      
            this.discardState(activePl, rowObj);
       
        
    }
}

GameLogic.EurekaRule.prototype.drawRows = function () {


    game.board.clearBoard();
    var board = game.board.getBoard();


    board[0].addChips(3, 2);
    board[1].addChips(3, 2);
    board[2].addChips(3, 2);
    board[3].addChips(2, 2);
    board[4].addChips(2, 2);
    board[5].addChips(2, 2);


    board[18].addChips(2, 1);
    board[19].addChips(2, 1); 
    board[20].addChips(2, 1);
    board[21].addChips(3, 1); 
    board[22].addChips(3, 1); 
    board[23].addChips(3, 1); 

    game.player.state = 2;

}

GameLogic.EurekaRule.prototype.getStoneCount = function () {
    return 15;
} 

GameLogic.EurekaRule.prototype.canPlaySixCards = function () {
    return true;
}

GameLogic.EurekaRule.prototype.giveUpPoint = function () {

 
        if (game.player.discardPanel.count > 0) {
            return "oin";//game.raise_count;
        }

        if (game.player.killedStones.count > 0) {
            return "mars"; //game.raise_count*3;
        }
        var board = game.board.getBoard();
        if (game.opponent.discardPanel.count>0) { 
        for (var i = 0; i < 6; i++) { 
                if (board[i].getColor()==1 ) {
                    return "kox"; //game.raise_count*3;
                } 
            }
        }
        return "mars"; //game.raise_count * 2;
  
}

GameLogic.EurekaRule.prototype.killing = function (chipRow,color) {

 
    return false;
}

GameLogic.EurekaRule.prototype.canMoveOnPosition = function (rowObj, color) {

 

    return false;

}

GameLogic.EurekaRule.prototype.NextRaise = function(raise){

    return raise*2;
} 

GameLogic.EurekaRule.prototype.notBlocked = function (clr, pos, killing) {

    return true; 

}

GameLogic.EurekaRule.prototype.roll = function (event) {


    var containers = game.board.getBoard();
 
    for (var i = 0; i < 24; i++) {
        containers[i].blocked = false;
    }
 
}
GameLogic.EurekaRule.prototype.movingState=function(activePl,rowObj)
{
  
     
}

GameLogic.EurekaRule.prototype.movingStateDrag=function(index , moveIndex, rowObj,mute) {

 

}

GameLogic.EurekaRule.prototype.freezeCount = function (freeze, index) {
   
    return false
}

GameLogic.EurekaRule.prototype.movePlay=function(endPos, index, rowObj , player) {

     
 
}


GameLogic.EurekaRule.prototype.reviveState=function(color)
{
 


}

 


GameLogic.EurekaRule.prototype.canMove=function(color , startPosition , list) {
    return false;
}




GameLogic.EurekaRule.prototype.canPlaySomething=function(activePl) {
    var color = activePl.color;
    var startingPosition = -1;
    if (color === 2) {
        startingPosition = 24;
    }
  
        var candisc = this.canPlayDiscard(activePl);

         
        if (candisc.result)
        {
            return true;
        }
         
        
        return false; 
 
}

GameLogic.EurekaRule.prototype.canPlayMoveState=function(activePl) {
 
    return false;

}

GameLogic.EurekaRule.prototype.canPlayDiscard=function(activePl) {
    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1)
    {
        startPos = 24;
        desc = -1;
    }

    var board = game.board.getBoard();

    for (var i = 0; i < activePl.movesList.length; i++) {
        var posit = startPos + desc * activePl.movesList[i];
        if (board[posit].getColor() === activePl.color) {
            res.result = true;
            res.index = i; 
            return res;
        } 

        if (this.canDiscardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i])) {
            res.result = true;
            res.index = i;
            return res;
        }
       
    }

 

 
    res.result = false;
    res.index = -1;
    return res;
}

 
GameLogic.EurekaRule.prototype.canDiscardWithoutMatch=function(board, color, startPos, desc, value) {


    var index = this.getDiscardIndex(startPos); 
 

    if (index === value) {
        return true;
    }

  
}
GameLogic.EurekaRule.prototype.discardWithoutMatch=function(board, color, startPos, desc, value, rowobjIndex)
{
    var ind = this.getDiscardIndex(rowobjIndex);

    if (ind > value) {
        return false;
    }
    if (ind === value) {
        return true;
    }

  
    return false;
}

GameLogic.EurekaRule.prototype.getDiscardIndex=function( number)
{

    if (number < 6)
    {
        return number + 1;
    } else if (number > 17)
    {
        return 24 - number;
    }
}

GameLogic.EurekaRule.prototype.discardState=function(activePl, rowObj) {
    var result = this.discardResultFactory(activePl, rowObj);
   
    if (result.result) {
        this.discardPlay(activePl, rowObj,result.index); 

        game.engine.play(2, rowObj.index, 24, result.index);
    
    }  
}

GameLogic.EurekaRule.prototype.discardPlay =function(activePl, rowObj, index)
{
    rowObj.removeChip();
    activePl.discardPanel.addDiscard();
    activePl.spliceMove(index);
 

}

GameLogic.EurekaRule.prototype.discardResultFactory=function(activePl,rowObj ) 
{
    
    var res = {};
    var startPos = -1;
    var desc = 1;
    if (activePl.color === 1) {
        startPos = 24;
        desc = -1;
    }
    res.index =-1;
    res.result = false;
    var board = game.board.getBoard();
    for (var i = 0; i < activePl.movesList.length; i++) 
    {
     
        if (this.discardWithoutMatch(board, activePl.color, startPos, desc, activePl.movesList[i], rowObj.index))
        {
            res.index = i;
            res.result = true;
            return res;
        }
    }
    return res;
}

 
 
GameLogic.EurekaRule.prototype.getCorrectPosition = function (startPosition) {

    return 23 - startPosition;

}

GameLogic.EurekaRule.prototype.isBlocked = function (chipRow) {

    return false;
}


GameLogic.EurekaRule.prototype.PlayUndo = function (activePl, startPosition, endPosition, value, enemyState, state, killer, action) {

    
    var opp = activePl.getOpponent();
    var cont = game.board.getBoard();
 
        activePl.discardPanel.removeDiscard();
        cont[startPosition].addChip(new Stone(activePl.color));
 

 
    activePl.movesList.push(value);
    activePl.movesList.sort();
 
    game.board.playUndo(value,activePl);

 
}

GameLogic.EurekaRule.prototype.calculatePips =function(pl)
{
    var counter = 0;
    var container = game.board.getBoard();
    for (var i = 0; i < 24; i++)
    {
        if (container[i].getColor() === pl.color)
        {
            if (pl.color === 1) {
                counter += container[i].numChildren * (24 - i);
            } else
            {
                counter += container[i].numChildren * (i+1);
            }

        }
        
    }

    counter += pl.killedStones.count * 25;

    pl.setPips(counter);

    return counter;


}


 