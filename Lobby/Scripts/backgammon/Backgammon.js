var imageLoaded = false;
var soundLoaded = false;
var soundCount = 0;
function ImageLoad(evt) { 
    imageLoaded = true;
    if (soundLoaded) {
        game.handleComplete(evt);  
    } 
}

function SoundLoad(evt) { 
    soundCount++;
 
    if (soundCount >= soundManifest.length) {
        soundLoaded = true;
        if (imageLoaded) {
            game.handleComplete(evt);
        }
    }

}

var ss = ss || {};
var sss = sss || {};
Backgammon = {

    Game: function () {
        this.mobile = false;
        this.can_Play = true;
        this.moves = [];
        var that = this;
        this.background = {};
        this.player = {};
        this.opponent = {};
        this.gameStoneColor = 1;
        this.rightPanel = {};
        this.board = {};
        this.bgSocket = {};
        this.hint1 = {};
        this.hint2 = {};
        this.hintArrow1 = {};
        this.hintArrow2 = {};
        this.autoRoll = false;
        this.engine = {};
        this.firstRoll = true;
        this.room = {};
        that.lastTime = 0;
        that.dragStone = {};
        that.popup = null;
        that.settingsPopup = {};
        that.leftMouse = false;
        that.lang = getCoockieLang();

        that.sound = true;
        that.board2D = false;
        that.reverseBoard = 1;
        that.autoSubmit = false;

        that.georgian = false;
        that.beaver = false;
        that.crawford = false;
        that.jacob = false;
        that.davi = false;

        that.stoneIndex = 0;
        that.crawfordCheck = false; 
        that.board2dString = "";

        that.room_id = 0;
        that.game_id = 0;
        that.round_id = 0;


    }
}

Backgammon.Game.prototype.initialize = function (canvas) {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        this.mobile = true;
    }
    this.initializeBoard(canvas);

}

Backgammon.Game.prototype.changeStoneColor = function (index) {

    game.stoneIndex = index;  

    var list = game.board.getBoard();
    for (var i = list.length - 1; i >= 0; i--) {
        if (list[i].children && list[i].children.length > 0) {
            for (var j = list[i].children.length - 1; j >= 0; j--) {
                list[i].children[j].changeBG();
            }
        }
    }

    game.player.killedStones.stone.changeBG();
    game.opponent.killedStones.stone.changeBG();
}

Backgammon.Game.prototype.translate = function () {

    game.rightPanel.translatePanel();
    game.settingsPopup.translate();
    game.chatPopup.translate();

}

Backgammon.Game.prototype.initializeBoard = function (canvas) {



    canv = new createjs.Stage(canvas);

    createjs.Touch.enable(canv);
    canv.enableMouseOver(10);
    canv.mouseMoveOutside = true;
    w = canv.canvas.width;
    h = canv.canvas.height;

    stage = new createjs.Container();
    canv.addChild(stage);
    stage.visible = false;

    this.loadingBar = new LoadingBar();
    canv.addChild(this.loadingBar);






    loader = new createjs.LoadQueue(false);
    loader.on("progress", handleFileProgress);
    loader.addEventListener("complete", ImageLoad );
    loader.loadManifest(manifest, true, "../Content/Images/");
    createjs.Ticker.addEventListener("tick", tick);
    createjs.Ticker.setFPS(30);


    createjs.Sound.alternateExtensions = ["mp3"];
    createjs.Sound.registerSounds(soundManifest, "../Content/Sounds/");
    createjs.Sound.on("fileload", SoundLoad , this)



    canv.on("stagemousemove", function (evt) {

        if (game.dragStone) {
            game.dragStone.scaleX = game.dragStone.scaleY = scl;
            game.dragStone.x = (evt.stageX - 44 * scl) / (window.devicePixelRatio / (scaler1 * scaler2));
            game.dragStone.y = (evt.stageY - 44 * scl) / (window.devicePixelRatio / (scaler1 * scaler2));
            game.dragStone.visible = true;
        }

    });



    function hideHintsArrows() {


        if (game.hint1.visible) {

            game.hint1.visible = false;
        }
        if (game.hint2) {

            game.hint2.visible = false;

        }


        if (game.hint3.visible) {

            game.hint3.visible = false;

        }

        if (game.hint4.visible) {

            game.hint4.visible = false;

        }
        if (game.hint5.visible) {

            game.hint5.visible = false;

        }

        if (game.hintArrow1) {
            game.board.removeChild(game.hintArrow1);
            game.hintArrow1 = null;
        }
        if (game.hintArrow2) {
            game.board.removeChild(game.hintArrow2);
            game.hintArrow2 = null;
        }
        if (game.hintArrow3) {
            game.board.removeChild(game.hintArrow3);
            game.hintArrow3 = null;
        }
        if (game.hintArrow4) {
            game.board.removeChild(game.hintArrow4);
            game.hintArrow4 = null;
        }

        if (game.hintArrow5) {
            game.board.removeChild(game.hintArrow5);
            game.hintArrow5 = null;
        }



    }

    if (game.mobile) {
        var listener = stage.on("click", function (evt) {
            toggleFullScreen();
            window.scrollTo(0, 1);
            stage.off("click", listener);
        });
    }

    canv.on("stagemouseup", function (evt) {

        var activePl = getActivePlayer();


        if (activePl === null) {
            game.board.showAllChips();
            game.enablePlay();
            return;
        }
        game.leftMouse = true;



        if (activePl.state === 0) {
             
            if (game.lastTime === 0) {
                game.board.showAllChips();
                game.enablePlay();

                return;
            }
            var lstDate = new Date();
            if (lstDate - 300 < (game.lastTime)) {

                if (game.dragStone) {
                    clicker();
                    game.logic.rowClick(game.dragStone.parentRow);
                }
            } else if (game.dragStone) {

                if (game.hint1.visible && CalculateX(game.hint1.x) && CalculateY(game.hint1.y)) {

                    game.logic.movingStateDrag(game.hint1.movePos, game.hint1.moveIndex, game.dragStone.parentRow, false);

                    stage.removeChild(game.dragStone);

                    game.dragStone = null;
                } else if (game.hint2.visible && CalculateX(game.hint2.x) && CalculateY(game.hint2.y)) {

                    game.logic.movingStateDrag(game.hint2.movePos, game.hint2.moveIndex, game.dragStone.parentRow, false);

                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint3.visible && CalculateX(game.hint3.x) && CalculateY(game.hint3.y)) {

                    game.logic.movingStateDrag(game.hint3.movePos, game.hint3.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint3.movePos1, game.hint3.moveIndex1, game.hint3.parentRow, false);

                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint4.visible && CalculateX(game.hint4.x) && CalculateY(game.hint4.y)) {

                    game.logic.movingStateDrag(game.hint4.movePos, game.hint4.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint4.movePos1, game.hint4.moveIndex1, game.hint4.parentRow, true);
                    movingStateDrag(game.hint4.movePos2, game.hint4.moveIndex2, game.hint4.parentRow1, false);
                    stage.removeChild(game.dragStone);

                    game.dragStone = null;
                } else if (game.hint5.visible && CalculateX(game.hint5.x) && CalculateY(game.hint5.y)) {

                    game.logic.movingStateDrag(game.hint5.movePos, game.hint5.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint5.movePos1, game.hint5.moveIndex1, game.hint5.parentRow, true);
                    movingStateDrag(game.hint5.movePos2, game.hint5.moveIndex2, game.hint5.parentRow1, true);
                    movingStateDrag(game.hint5.movePos3, game.hint5.moveIndex3, game.hint5.parentRow2, false);

                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                }




                else {

                    stage.removeChild(game.dragStone);
                    game.dragStone.parentRow.showChip();
                    game.dragStone = null;

                }


            }

            hideHintsArrows();
            if (game.dragStone) {
                stage.removeChild(game.dragStone);
                game.dragStone = null;

            }
            game.lastTime = 0;
        } else if (activePl.state === 1) {

            if (game.lastTime === 0) {
                game.board.showAllChips();
                return;
            }
            lstDate = new Date();
            if (lstDate - 300 < (game.lastTime)) {
                clicker();
                if (game.dragStone) {
                    game.logic.reviveState(activePl.color);
                }


            } else if (game.dragStone) {

                if (game.hint1.visible && CalculateX(game.hint1.x) && CalculateY(game.hint1.y)) {
                    game.logic.movingStateDrag(game.hint1.movePos, game.hint1.moveIndex, game.dragStone.parentRow, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint2.visible && CalculateX(game.hint2.x) && CalculateY(game.hint2.y)) {
                    game.logic.movingStateDrag(game.hint2.movePos, game.hint2.moveIndex, game.dragStone.parentRow, false);

                    stage.removeChild(game.dragStone);

                    game.dragStone = null;
                } else if (game.hint3.visible && CalculateX(game.hint3.x) && CalculateY(game.hint3.y)) {

                    game.logic.movingStateDrag(game.hint3.movePos, game.hint3.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint3.movePos1, game.hint3.moveIndex1, game.hint3.parentRow, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint4.visible && CalculateX(game.hint4.x) && CalculateY(game.hint4.y)) {

                    game.logic.movingStateDrag(game.hint4.movePos, game.hint4.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint4.movePos1, game.hint4.moveIndex1, game.hint4.parentRow, true);
                    movingStateDrag(game.hint4.movePos2, game.hint4.moveIndex2, game.hint4.parentRow1, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint5.visible && CalculateX(game.hint5.x) && CalculateY(game.hint5.y)) {

                    game.logic.movingStateDrag(game.hint5.movePos, game.hint5.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint5.movePos1, game.hint5.moveIndex1, game.hint5.parentRow, true);
                    movingStateDrag(game.hint5.movePos2, game.hint5.moveIndex2, game.hint5.parentRow1, true);
                    movingStateDrag(game.hint5.movePos3, game.hint5.moveIndex3, game.hint5.parentRow2, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                }


                else {
                    stage.removeChild(game.dragStone);
                    game.dragStone.parentRow.showChip();
                    game.dragStone = null;

                }

            }
            hideHintsArrows();

            if (activePl.killedStones.count === 0) {
                activePl.state = 0;
            }
            if (game.dragStone) {
                stage.removeChild(game.dragStone);
                game.dragStone = null;

            }
            game.lastTime = 0;

        } else if (activePl.state === 2) {

            if (game.lastTime === 0) {

                game.board.showAllChips();

                return;
            }
            lstDate = new Date();

            if (lstDate - 300 < (game.lastTime)) {
                clicker();
                if (game.dragStone && game.dragStone.parentRow) {
                    game.logic.rowClick(game.dragStone.parentRow);
                }


            } else if (game.dragStone) {


                if (game.hint1.visible && CalculateX(game.hint1.x) && CalculateY(game.hint1.y)) {
                    game.logic.movingStateDrag(game.hint1.movePos, game.hint1.moveIndex, game.dragStone.parentRow, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint2.visible && CalculateX(game.hint2.x) && CalculateY(game.hint2.y)) {
                    game.logic.movingStateDrag(game.hint2.movePos, game.hint2.moveIndex, game.dragStone.parentRow, false);

                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint3.visible && CalculateX(game.hint3.x) && CalculateY(game.hint3.y)) {

                    game.logic.movingStateDrag(game.hint3.movePos, game.hint3.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint3.movePos1, 0, game.hint3.parentRow, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint4.visible && CalculateX(game.hint4.x) && CalculateY(game.hint4.y)) {

                    game.logic.movingStateDrag(game.hint4.movePos, game.hint4.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint4.movePos1, game.hint4.moveIndex1, game.hint4.parentRow, true);
                    movingStateDrag(game.hint4.movePos2, game.hint4.moveIndex2, game.hint4.parentRow1, false);

                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                } else if (game.hint5.visible && CalculateX(game.hint5.x) && CalculateY(game.hint5.y)) {

                    game.logic.movingStateDrag(game.hint5.movePos, game.hint5.moveIndex, game.dragStone.parentRow, true);
                    movingStateDrag(game.hint5.movePos1, game.hint5.moveIndex1, game.hint5.parentRow, true);
                    movingStateDrag(game.hint5.movePos2, game.hint5.moveIndex2, game.hint5.parentRow1, true);
                    movingStateDrag(game.hint5.movePos3, game.hint5.moveIndex3, game.hint5.parentRow2, false);
                    stage.removeChild(game.dragStone);
                    game.dragStone = null;
                }


                else {
                    stage.removeChild(game.dragStone);
                    game.dragStone.parentRow.showChip();
                    game.dragStone = null;
                }
                /*   else {
                       stage.removeChild(game.dragStone);
                       game.dragStone.parentRow.showChip();
                       game.dragStone = null;
   
                   } */

            }
            hideHintsArrows();

            if (game.dragStone) {
                stage.removeChild(game.dragStone);
                game.dragStone = null;

            }
            game.lastTime = 0;

        }
        game.board.showAllChips();
        game.enablePlay();

    });


}


Backgammon.Game.prototype.enablePlay = function () {

    setTimeout(function () {
        game.can_play = true;
    }, 200);

}

Backgammon.Game.prototype.initializePanel = function () {
    this.rightPanel = new RightPanel();
    stage.addChild(this.rightPanel);

    this.rotateScreen = new RotationScreen();
}

Backgammon.Game.prototype.handleComplete = function () {

    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet({ "images": [loader.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames })
    }

    var sssMetadata = library.ssMetadata;
    for (i = 0; i < sssMetadata.length; i++) {
        sss[sssMetadata[i].name] = new createjs.SpriteSheet({ "images": [loader.getResult(sssMetadata[i].name)], "frames": sssMetadata[i].frames })
    }


    this.board = new Board();
    this.board.initialize();
    this.initializePlayers();
    this.initializePanel();

    this.settingsPopup = new SettingsPopup();


    this.settingsPopup.activate();
    this.chatPopup = new ChatPopup();


    this.engine = new Engine.Mediator();

    // $("#main-board").css({ 'background-color': '#2A221B' }); 

    resizeListener();

}

Backgammon.Game.prototype.removePopup = function () {
    if (this.popup) {
        this.popup.removePopup();
        this.popup = null;
    }

}


Backgammon.Game.prototype.initializePlayers = function () {
    this.player = new Player(1, "");
    this.opponent = new Player(2, "");

    this.board.addChild(this.player);
    this.board.addChild(this.opponent);
}


function handleFileProgress() {

    game.loadingBar.loading(loader.progress * 100);
}

function tick(event) {

    if (update) {
        update = true;
        canv.update(event);
    }
}

Backgammon.Game.prototype.playSoundGlobal = function () {
    try {

        if (game.player.audio.sound) {
            createjs.Sound.play("turn");
        }
    } catch (ex) {
        //   if (!createjs.Sound.initializeDefaultPlugins()) {return;}
        console.log("can't load sound")
    }


}

function PlaySound(name) {

    try {
        if (game.sound) {
            var instance = createjs.Sound.play(name);
            instance.volume = 0.6;
        }
    } catch (ex) {
        console.log(ex);
    }
}

function clicker() {

    try {

        if (game.mobile) {
            game.leftMouse = false;
            return;
        }
        if (event.which === 1) {
            game.leftMouse = game.mobile;
        } else {
            game.leftMouse = !game.mobile;
        }
    } catch (ex) {
        game.leftMouse = true;
    }
}


function CalculateY(mainY) {

    var scaler = scl;
    if (game.mobile) {
        scaler = scl * 1.09;
    }
    return Math.abs(mainY * scaler - game.dragStone.y + game.board.y) < 60;
}

function CalculateX(mainX) {
    var scaler = scl;
    if (game.mobile) {
        scaler = scl * 1.09;
    }

    return Math.abs(mainX * scaler - game.dragStone.x + game.board.x) < 30;
}