function Stone(color)
{
    createjs.Container.call(this);
    this.sprite= {};
    this.type = color;
    var that = this;
    that.countTxt = {};
    that.parentRow = {};
    that.moveIndex = 0;
    that.movePos = 0;
    that.tickEnabled = false;
    that.counterBg = {};
    that.count = 0;

    that.firstImage = {};
 

    var ascending = 1;

  
    function initialize()
    {


         
        var col = "#000000";
  
          
        that.firstImage = loader.getResult("1stone_" + Math.abs(game.stoneColor - that.type)+"_"+game.stoneIndex);
          
      
     
        that.sprite = new createjs.Bitmap(that.firstImage);

        
    	that.addChild(that.sprite);

         var graphs = new createjs.Graphics();

    	 var circle1 =   graphs.beginFill(col).drawCircle(0, 0, 30);
         that.counterBg = new createjs.Shape(circle1);
         that.counterBg.alpha = 0.4;
 
    	that.counterBg.x = 42;
    	that.counterBg.y =39;
    	that.counterBg.visible = false;
    	that.addChild(that.counterBg);

		that.cursor = "pointer";
    }
    that.reset = function()
    {
        that.countTxt.text="";
        that.counterBg.visible = false;

    }
    that.changeBG = function(index)
    {
      
        that.firstImage = loader.getResult("1stone_" + Math.abs(game.stoneColor - that.type) + "_" + game.stoneIndex);
         
        that.sprite.image =    that.firstImage;
       
    }

    function setFonts()
    {

        that.countTxt.font = "bold 28px Arial";  
        
    }

    that.setCount = function (count, asc) {
        ascending = asc;
     
        that.count = count;

        if (count > 5) {
           that.countTxt = new createjs.Text(count, "bold 28px"+arial_font, "white");
           that.countTxt.textAlign = "center";
        
           if (Math.abs(game.stoneIndex - that.type) === 1) {
      
                that.countTxt.color = "black";
                
                that.addChild(that.countTxt);
                that.counterBg.visible = true;
            } else {

               that.countTxt.color = "white";
              
                that.addChild(that.countTxt);

                that.counterBg.visible = true;

            }
        

        }

        that.changeSprite(asc === 1);


    }

    that.setVisibleCounterBg = function (visible)
    {
        that.counterBg.visible = visible;
         
    }



    that.changeSprite = function (isTop) {
 
 

        that.counterBg.scaleX = 1.0;
        that.counterBg.scaleY = 1.0;
        that.counterBg.x = 42;
        that.counterBg.y = 39;

       that.countTxt.x = 43;
       that.countTxt.y = 21;
  
        setFonts();

    }
   
    initialize();
}

Stone.prototype = new createjs.Container();
Stone.prototype.constructor = Stone;