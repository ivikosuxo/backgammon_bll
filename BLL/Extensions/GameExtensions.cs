﻿using BLL.Classes.Entities;
using BLL.Classes.GamePlay;
 
using  EntityModels.Exceptions;
using BLL.Managers;
 
using EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InterfaceLibrary.Enums;

namespace BLL.Extensions
{
    public static class GameExtensions
    {
        public static void CloseConnection(this string session, int errorCode)
        {

            session.CloseConnection(errorCode, ConnectionType.Void);

        }

        public static void CloseConnection(this string session, int errorCode, ConnectionType type)
        {

            try
            {
             var socketManager=    BaseApplication.Current.BaseGameSocketManagerInstance;
          
                var messageObject = new JObject
                {
                    [CommandKeys.command] = CommandKeys.error,
                    [StaticStrings.Id] = errorCode,
                    [StaticStrings.Fatal] = true
                };
                socketManager.SetConnectionType(session, type);
                socketManager.SendMessage(session, messageObject.ToString());

               // session.Close();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"CloseConnection: session={session}" ,ex);
            }

        }

        public static void CloseConnection(this string session, ConnectionType type)
        { 
            try
            {
                var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                socketManager.SetConnectionType(session , type);
         
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"CloseConnection: session={session}", ex);
            }

        }




        public static  ConnectionType GetConnectionType(this string session)
        {
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            return socketManager.GetConnectionData(session).Type; 
        }

        public static void SetUserId(this string session, int value)
        { 
            try
            {
                var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var obj = socketManager.GetConnectionData(session);
                obj.PlayerId = value;
            }
            catch (Exception ex) {
                ApplicationManager.Logger.Error($"SetUserId: session={session}",ex);
            }
        }


        public static void SetConnectionType(this string session, ConnectionType value)
        {
            try
            {
                var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var obj = socketManager.GetConnectionData(session);
                obj.Type = value;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SetConnectionType: session={session}", ex);
            }
        }

        public static void SetRoomId(this string session, int value)
        {
            try
            {
                var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var obj = socketManager.GetConnectionData(session);
                obj.RoomId = value;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SetRoomId: room_id={value}", ex);
            }
        }
        public static void SetTournamentId(this string session, int value)
        {
            try
            {
                var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var obj = socketManager.GetConnectionData(session);
                obj.TournamentId = value;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SetTournamentId: tournament_id={value}", ex);
            }
        }
        public static int GetUserId(this string session)
        {
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var obj = socketManager.GetConnectionData(session); 
            return obj.PlayerId;
        }

        public static int GetRoomId(this string session)
        {
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var obj = socketManager.GetConnectionData(session);
            return obj.RoomId;
        }
     

        
        public static GameSocket GetGameSocket(this string session)
        {
            try
            {
                var roomId = session.GetRoomId();
                var userId = session.GetUserId();
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                return gameSocketManager.GetGameSocket(  userId , roomId);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"GetGameSocket: session={session}", ex);
            }

            return null;
        }


        public static List<BoardPlace> Clone(this List<BoardPlace> board)
        {
            var brd =new  List<BoardPlace>();            
            board.ForEach(e =>
            {
                brd.Add(e.Clone());
            });

            return brd;

        }

        public static BoardPlace Clone(this BoardPlace place)
        {
            var plc = new BoardPlace()
            {
                Color = place.Color,
                Count = place.Count,
                IsBlocked = place.IsBlocked,
                FreezeCount = place.FreezeCount
            };

            return plc;

        }
        public static JArray ToJArray(this List<int> list) {
            var arr = new JArray();
            list.ForEach(e => arr.Add(e));

            return arr;
        }

        public static JArray ToJArray(this List<decimal> list)
        {
            var arr = new JArray();
            list.ForEach(e => arr.Add(e));

            return arr;
        }

    }
}
