﻿using BLL.Managers;
using InterfaceLibrary.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Extensions
{
    public static class EngineExtensions
    {
        public static decimal GetCurrentBalance(this IPlayerInfo info)
        {
            info.Balance = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetPlayerBalance(info.PlayerId);
            return info.Balance;
        } 
 
    }
}
