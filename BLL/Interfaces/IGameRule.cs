﻿using BLL.Classes;
using BLL.Classes.GameClass;
using BLL.Classes.GamePlay;
using BLL.Classes.StateCalculator;
using  EntityModels.Config;
using EntityModels.StateCalculator;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IGameRule
    {
        List<BoardPlace> FirstBoard
        {
            get; set;
        }
        List<BoardPlace> SecondBoard
        {
            get; set;
        }

        int StoneCount { get; set; }
        void Initialize();



        List<BoardPlace> GetBoard(int color);
        bool EndRound(RoundPlayer gu, bool calcMars, Game game);
        Dice RollDice(RoundPlayer pl,bool withDouble);
 
        int GetCorrectPosition(int position);

        decimal GetCurrentBet(RoundPlayer pl, bool bl, Game game);
        bool winRound(RoundPlayer rp, bool calcMars, Game game);
        bool CanRaiseX2(Game game);
        bool CanRaise(Game game);
        bool ValidateMove(RoundPlayer pl, int action, int move, int startPos, int endPos, Game game);
        int discardIndex(RoundPlayer activePl);

        bool IsBlocked(int color, int startIndex);
        List<BoardPlace> GetBoardByColor(int color);
        bool canDiscardWithoutMatch(int color, int startPos, int value);
        bool canDiscard(RoundPlayer pl);
        bool canMove(int startPos, int endPos, int color);
        bool IsDiscardState(RoundPlayer pl);
        int IncreaseRaise(int raiseCount, int maxRaiseCount);
        bool canPlaySomething(RoundPlayer activePl, int maxMove, bool check);
        MoveState canPlayDiscard(RoundPlayer activePl);
        bool canPlayMoveState(RoundPlayer activePl);
        MoveState canMove(int color, int startpos, List<int> moves);
        void PlayUndo(RoundPlayer activePl, IMove move);
        void UnblockBoard();
        bool getFinalMoves(RoundPlayer pl, Dice dice, int start, int end, int action);
        int IsMars(RoundPlayer pl, bool calc, RoomConfig cfg, int raiseCount);
        bool Play(RoundPlayer activePl, int action, int startPos, int endPos, int moveIndex);
        bool Move(RoundPlayer activePl, int startPos, int endPos, int moveIndex);
        bool Discard(RoundPlayer activePl, int startPos, int endPos, int moveIndex);
        bool Revive(RoundPlayer activePl, int endPos, int moveIndex);
        bool canDiscardOnBoard(int color, int move, int startPos);
        bool canPlayMoveOnBoard(int color, int move, int startPos);
        void PlayerRoll(int color, int freezeCount);
        PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer); 
        bool RollReset(bool first);

        bool FirstRollDicePlay();

    }
}
