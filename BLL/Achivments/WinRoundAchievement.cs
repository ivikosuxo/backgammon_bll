﻿ 
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
    public class WinRoundAchievement : Achievement
    {
        public WinRoundAchievement()
        {
            StartEvent = AchievementEvents.EndRound;
        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                result = data["win"].Value<bool>();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"WinRoundAchievement: player_id={info.PlayerId} , data={data.ToString()}", ex);
            }
            return result;
        }
    }
}
