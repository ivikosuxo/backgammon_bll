﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  EntityModels.Config;
using Newtonsoft.Json.Linq;
using  EntityModels.Types;

using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
   public class RollAchievement :Achievement
    {
        public int FirstDice { get; set; }
        public int SecondDice { get; set; }

        public RollAchievement()
        {
            StartEvent  = AchievementEvents.Roll;
        }

        public override void SetDices(int dice1, int dice2)
        {
            FirstDice = dice1;
            SecondDice = dice2;
        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var token1 = data["first"];
            var token2 = data["second"];
            if (token1==null || token2==null)
            {
                return false;
            }
            var firstDice = token1.Value<int>();
            var secondDice = token2.Value<int>();
            bool result = false;
            if (FirstDice == 0)
            {
                result = (firstDice == secondDice);
            }
            else {
                result= (FirstDice == firstDice && SecondDice == secondDice) || (FirstDice == secondDice && SecondDice == firstDice);
            }
            return result;
        }
    }
}
