﻿using  EntityModels.Config;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InterfaceLibrary;
using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
    public class KillerAchievement :Achievement
    {
        public KillerAchievement()
        {
            StartEvent = AchievementEvents.MoveSubmit;
        }

        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                var token = data["killer"];
                if (token != null)
                {
                    result = token.Value<bool>();
                } 
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("killing exception:" +data.ToString(),  ex);
            }
            return result;
        }

        public override int IncreasePlayerAchievement(IPlayerAchievement achievement, JObject data)
        {
            var result = 1;
            try
            {
                var count = data["count"];
                if (count != null)
                {
                    result = count.Value<int>();
                    achievement.Count += result;
                } 
            }
            catch (Exception)
            {
                achievement.Count++;
            }
            return result;
        }

    }
}
