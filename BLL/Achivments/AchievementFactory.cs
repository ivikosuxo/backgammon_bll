﻿
using EntityModels.Types;
using InterfaceLibrary.Achievements;
using InterfaceLibrary.Enums;
using System;
namespace BLL.Achivments
{
  public  class AchievementFactory
    {
        public static IAchievement CreateAchievement(AchievementType type) {
            switch (type)
            {
                case AchievementType.Roll:
                    return new RollAchievement();
                   
                case AchievementType.RoyalDeffence:
                    return new DadianiAchievement();
                  
                case AchievementType.WinRound:
                    return new WinRoundAchievement();
                case AchievementType.LoseRound:
                    return new LoseRoundAchievement();

                case AchievementType.LoseGame:
                    return new LoseGameAchievement();
                case AchievementType.WinGame:
                    return new WinGameAchievement();

                case AchievementType.TournamentWin:
                    return new TournamentWinAchievement();

                case AchievementType.TournamentPlay:
                    return new TournamentPlayAchievement();

                case AchievementType.TournamentTopPlace:
                    return new TournamentTopPlaceAchievement();

                case AchievementType.Mars:
                    return new MarsAchievement();
                  
                case AchievementType.DoubleMars:
                    return new DoubleMarsAchievement();

                case AchievementType.PlayGame:
                    return new PlayGameAchievement();

                case AchievementType.StoneKiller:
                    return new KillerAchievement();
                    
            }
            return new RollAchievement();
        }

    
    }
}
