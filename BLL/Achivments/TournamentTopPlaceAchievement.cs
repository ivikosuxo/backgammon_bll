﻿
using BLL.Managers;
using  EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Achivments
{
  public  class TournamentTopPlaceAchievement :Achievement
    {
        public TournamentTopPlaceAchievement()
        {
            StartEvent = AchievementEvents.TournamentTopPlace;
        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        { 
             return true;
        }

    }
}
