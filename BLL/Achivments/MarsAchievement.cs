﻿using System; 

using Newtonsoft.Json.Linq;
using BLL.Managers;
using  EntityModels.Types;
using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
   public class MarsAchievement :Achievement
    {
        public MarsAchievement()
        {
            StartEvent = AchievementEvents.EndRound;
        }

        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                var win = data["win"].Value<bool>();
                var token = data["mars"];
                if (win&&token != null)
                {
                    result = token.Value<bool>();
                }
                
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"MarsAchievement: player_id={info.PlayerId} , data={data.ToString()}", ex);
            }
            return result;
        }
    }
}
