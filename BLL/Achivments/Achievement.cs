﻿using BLL.Managers;
using EntityModels.Types;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using InterfaceLibrary;
using InterfaceLibrary.Game;
using InterfaceLibrary.Achievements;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
   public class Achievement :IAchievement
    {
        public int AchievementId { get; set; } = 1;
        public int BlockGroup { get; set; } = 0;
        public string AchievementTitle { get; set; } = "";
        public string AchievementTitleRus { get; set; } = "";
        public string AchievementTitleGeo { get; set; } = "";

        public string AchievementDescription { get; set; } = "";
        public string AchievementDescriptionGeo { get; set; } = "";
        public string AchievementDescriptionRus { get; set; } = "";
        public int MinRank { get; set; } = 1;
 
        public int Count { get; set; } = 1;
        public int CoinCount { get; set; } = 100;
        public bool IsActive { get; set; } = true;
        public AchievementEvents StartEvent { get; set; }
       public  bool IsValid(int rank, AchievementEvents startEvent)
        {
            return IsActive && rank == MinRank && startEvent == StartEvent;
        }
        public virtual bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
           

            return true;
        }

        public virtual void Execute(IPlayerInfo info,int roomId, JObject data)
        {
            Task.Factory.StartNew(() =>
            {
               var plAchievement =    CreatePlayerAchievement(info);
            
                if (plAchievement.IsActive && !plAchievement.Complete && CalculateAchievement(info, data))
                {

                     var count=  IncreasePlayerAchievement(plAchievement,data);
                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                    plAchievement.Complete= achievementManager.PlayerAchievementIncrease(info, AchievementId, count,Count);
                    if (plAchievement.Complete)
                    {
                        Openachievement(info.PlayerId, roomId);
                    }
                }  
            });

        }

        public virtual int  IncreasePlayerAchievement(IPlayerAchievement achievement, JObject data)
        {
            achievement.Count++;
            return 1;
        }

        public IPlayerAchievement CreatePlayerAchievement(IPlayerInfo info) {

            var playerAchievement = info.GetAchievement(AchievementId);
            if (playerAchievement == null)
            {
                playerAchievement = new PlayerAchievement(AchievementId, 0, Count, false, true);
                info.Achievements.Add(playerAchievement);

            }
            return playerAchievement;
        }

        public virtual void Openachievement(int playerId,int roomId)
        {
            var result = ToJson();
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(playerId, roomId, CommandKeys.achievement_unlock,result);
        }

        public virtual void SetDices(int dice1,int dice2)
        {


        }

        public virtual JObject ToJson()
        {
            var namesArr = new JArray();
            namesArr.Add(AchievementTitleGeo);
            namesArr.Add(AchievementTitle);          
            namesArr.Add(AchievementTitleRus);


            var descriptions = new JArray();
            descriptions.Add(AchievementDescriptionGeo);
            descriptions.Add(AchievementDescription); 
            descriptions.Add(AchievementDescriptionRus);

            var data = new JObject()    
            {
                ["id"]=AchievementId,
                ["type"]=(int)StartEvent,
                ["count"]=Count,
                ["rank"]=MinRank,                
                ["coin_count"]=CoinCount,
                ["name"] = AchievementTitle,
                ["description"] = AchievementDescription,
                ["name_ge"] = AchievementTitleGeo,
                ["description_ge"] = AchievementDescriptionGeo,
                ["name_ru"] = AchievementTitleRus,
                ["description_ru"] = AchievementDescriptionRus,
                ["names"] = namesArr,
                ["descriptions"] = descriptions
            };
       

            return data;

        }
    }
}
