﻿using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  EntityModels.Config;
using Newtonsoft.Json.Linq;

using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
  public  class TournamentWinAchievement :Achievement
    {
        public TournamentWinAchievement()
        {
            StartEvent = AchievementEvents.TournamentWin;
        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            var token = data["winner_id"];
            if (token!=null)
            {
                var playerId = token.Value<int>();
                result = playerId == info.PlayerId;
            }
            
            return result;
        }
    }
}
