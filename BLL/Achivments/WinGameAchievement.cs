﻿using  EntityModels.Config;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
   public class WinGameAchievement :Achievement
    {
        public WinGameAchievement()
        {
            StartEvent = AchievementEvents.EndGame;
        }

        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                result = data["win"].Value<bool>();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"WinGameAchievement: player_id={info.PlayerId} , data={data.ToString()}", ex);
            }
            return result;
        }
    }
}
