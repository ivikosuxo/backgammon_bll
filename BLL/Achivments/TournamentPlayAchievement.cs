﻿
using  EntityModels.Config;
using  EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Achivments
{
  public  class TournamentPlayAchievement :Achievement
    {
        public TournamentPlayAchievement()
        {
            StartEvent = AchievementEvents.TournamentFinish;
        }

        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
           
            var token = data["fee"];
            if (token !=null)
            {
                var fee = token.Value<decimal>();
                return fee > 0;
            }
            return false;
        }
    }
}
