﻿
using BLL.Managers;
using  EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Achivments
{
    public class DadianiAchievement :Achievement
    {
        public DadianiAchievement()
        {
            StartEvent = AchievementEvents.MoveSubmit;
        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                var token = data["royal"];
                if (token!=null)
                {
                    result = token.Value<bool>();
                }
              
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"dadiani exception: {data.ToString()}",ex);
            }
            return result;
        }
    }
}
