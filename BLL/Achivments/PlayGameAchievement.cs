﻿using Newtonsoft.Json.Linq;
using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
   public class PlayGameAchievement :Achievement
    {
        public PlayGameAchievement()
        {
            StartEvent = AchievementEvents.EndGame;

        }
        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {

            return true;
        }
    }
}
