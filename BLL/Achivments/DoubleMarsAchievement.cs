﻿using  EntityModels.Config;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InterfaceLibrary.Game;
using InterfaceLibrary.Enums;

namespace BLL.Achivments
{
    public class DoubleMarsAchievement :Achievement
    {
        public DoubleMarsAchievement()
        {
            StartEvent = AchievementEvents.EndRound;
        }

        public override bool CalculateAchievement(IPlayerInfo info, JObject data)
        {
            var result = false;
            try
            {
                var token = data["double_mars"];
                var win = data["win"].Value<bool>();
                if (win&&token != null)
                {
                    result = token.Value<bool>();
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"MarsAchievement: player_id={info.PlayerId}", ex);
            }
            return result;
        }
    }
}
