﻿using BLL.Interfaces;
using DAL.Database;
using InterfaceLibrary.Database;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Interfaces;
using InterfaceLibrary.Manager;
using Ninject;
using System;
 

namespace BLL.Managers
{
    public class BaseApplication
    {

        public static BaseApplication Current { get; set; }

        public IAchievementManager AchievementManagerInstance { get; set; }
        public ITournamentManager TournamentManagerInstance { get; set; }
        public GameSocketManager BaseGameSocketManagerInstance { get; set; }
        public ApplicationManager ApplicationManagerInstance { get; set; }

        public IHistoryManager BaseHistoryManagerInstance { get; set; }
        public IRoomManager RoomManagerInstance { get; set; }
        public IPlayerManager PlayerManagerInstance { get; set; }
        public IShopManager BaseShopManagerInstance { get; set; }
        public IGameRepository BaseGameRepository { get; set; }
    
 

        public static void Initialize() {
           Current = new BaseApplication();
        }

        private BaseApplication() {

            IKernel kernel = new StandardKernel();

            kernel.Bind<IFactory>().To<Factory>();
          
         

            //bind managers
            kernel.Bind<IShopManager>().To<ShopManager>(); 
            kernel.Bind<IAchievementManager>().To<AchievementManager>();
            kernel.Bind<ITournamentManager>().To<TournamentManager>();
            kernel.Bind<IHistoryManager>().To<HistoryManager>();
            kernel.Bind<IPlayerManager>().To<PlayerManager>();
            kernel.Bind<IRoomManager>().To<RoomManager>();

            //bind repositories
            kernel.Bind<IShopRepository>().To<ShopRepository>(); 
            kernel.Bind<IAchievementRepository>().To<AchievementRepository>();
            kernel.Bind<ITournamentRepository>().To<TournamentRepository>();
            kernel.Bind<IHistoryRepository>().To<HistoryRepository>();
            kernel.Bind<IPlayerRepository>().To<PlayerRepository>();
            kernel.Bind<IRoomRepository>().To<RoomRepository>();
            kernel.Bind<IGameRepository>().To<GameRepository>();

            ApplicationManagerInstance = new ApplicationManager();

            //initialize managers
            AchievementManagerInstance = kernel.Get<IAchievementManager>(); 
            TournamentManagerInstance = kernel.Get<ITournamentManager>(); 
            BaseHistoryManagerInstance = kernel.Get<IHistoryManager>();
            RoomManagerInstance = kernel.Get<IRoomManager>();
            PlayerManagerInstance= kernel.Get<IPlayerManager>();
            BaseShopManagerInstance = kernel.Get<IShopManager>(); 
            BaseGameSocketManagerInstance = new GameSocketManager(); 
        
             
        } 
   

        public   void ServerStart(IMessageBroker broker)
        {
            try
            {
                BaseGameSocketManagerInstance.RegisterMessageBroker(broker);
                ApplicationManagerInstance.Initialize();
                //RandomGenerator.Initialize();
               
                 RoomManagerInstance.Initialize();
                TournamentManagerInstance.Initialize();
                AchievementManagerInstance.Initialize(); 
                BaseShopManagerInstance.Initialize();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("ServerStart: ", ex);
            }

        }

    }
}
