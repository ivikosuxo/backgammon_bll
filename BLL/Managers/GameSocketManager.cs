﻿using BLL.Classes.Entities;
using BLL.Extensions;
using EntityModels.Types;
using SuperSocket.SocketBase;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using  EntityModels.Exceptions;
using InterfaceLibrary.Enums;
using System.Collections.Concurrent;
using EntityModels.Entities;
using InterfaceLibrary.Interfaces;

namespace BLL.Managers
{
    public class GameSocketManager 
    {

        private object locker = new object();
        private IMessageBroker GameMessageBroadcaster;
        private ConcurrentDictionary<string, ConnectionData> ConnectionInfo = new ConcurrentDictionary<string, ConnectionData>();
        public  List<GameSocket> Connections = new List<GameSocket>();


        public void RegisterMessageBroker(IMessageBroker broker)
        {
            GameMessageBroadcaster = broker;

        }

        public void OnMessage(string sessionId, string data)
        {
            BaseApplication.Current.ApplicationManagerInstance.OnMessage(sessionId, data);

        }

        private void RemoveSession(string session)
        {
            //   lock (locker)
            // {  
            try
            {
                ConnectionInfo.TryRemove(session, out var data);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"RemoveSession session={session}", ex);
            }
            // }
        }

        public void OnConnect(string sessionId, string token)
        {
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var plId = playerManager.GetConnectedPlayerIdByToken(token);
                if (plId != 0)
                {
                    var obj = new JObject { [CommandKeys.command] = CommandKeys.connect };
                    SetConnectionData(sessionId, new ConnectionData());
                    SendMessage(sessionId, obj.ToString());
                }
                else
                {
                    throw new AuthenticateException();
                }
            }
            catch (GameException gex)
            {
                var obj = gex.ToJson();
                obj["command"] = CommandKeys.error;
                SendMessage(sessionId, obj.ToString());
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("OnConnect : token ", ex);
            }
        }

        public  void OnDisconnect(string session)
        {

            var roomManager = BaseApplication.Current.RoomManagerInstance;
             
            try
            {
                var type = session.GetConnectionType();
                switch (type)
                {
                    case ConnectionType.Game:
                        var gameSocket = session.GetGameSocket();
                        gameSocket.Active = false;
                        var room = roomManager.GetRoomById(gameSocket.RoomId);
                        room.Disconnect(gameSocket.PlayerId);
                        break;
                         
                    case ConnectionType.Tournament:
                        session.GetGameSocket().Active = false;
                        break;

                    case ConnectionType.Lobby:                       
                         CloseConnection(session);
                        break;

                    case ConnectionType.Void:
                        // do nothing 
                        break;
                    case ConnectionType.Waiting: 
                          room = roomManager.GetRoomById(session.GetRoomId());
                          room.DisconnectWaiter(session.GetUserId()); 
                        break;

                    case ConnectionType.Close:
                        CloseConnection(session);
                        break;
 
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SaveGameState: session={session} ", ex);
            }

            RemoveSession(session);
        }

        public  int GetPlayersCount() {
             return Connections.Where(e => e.Active).Select(e => e.PlayerId).Distinct().Count();
        }
        public  List<int> GetActivePlayers()
        {
            return Connections.Where(e => e.Active).Select(e => e.PlayerId).Distinct().ToList();
        }
        public  int GetUserConnectionCount(int playerId)
        { 

            return Connections.Count(e => e.PlayerId == playerId && e.RoomId > 0);
        }



        public  void RemoveAllSameConnection(GameSocket sock) {
            lock (locker)
            {
                try
                {
                    Connections.RemoveAll(e => e.Equals(sock));
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"RemoveAllSameConnection: ", ex);
                }
            }
        }

        public   GameSocket GetGameSocket(int playerId,int roomId) {

            lock (locker)
            {
                return Connections.First(gs => gs.Active && gs.PlayerId == playerId && gs.RoomId == roomId);
            }
          
        }

        public  void CloseConnection(string session)
        {
            lock (locker)
            {
                try
                {
                    var gameSocket = session.GetGameSocket();
                    if (gameSocket != null)
                    {
                        Connections.Remove(gameSocket);
                        gameSocket.Destroy();
                    }
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"CloseConnection: session={session}", ex);
                }
            }
        }

        public  void CloseAllConnection(int playerId)
        {
            lock (locker)
            {
                try
                {
                    var connections = Connections.Where(e => e.PlayerId == playerId).ToList();
                    connections.ForEach(e =>
                    {
                        SendMessage(e.Connection, CommandKeys.error, new BlockedException().ToJson());
                    }
                    );
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"CloseAllConnection: player_id={playerId}", ex);
                }
            }
        }

        public  GameSocket JoinRoom(string session , int playerId, int roomid, int connectionType)
        {
            lock (locker)
            {
                var gameSocket = Connections.First(gs => gs.RoomId == roomid && gs.PlayerId == playerId);

                if (gameSocket == null)
                {
                    gameSocket = new GameSocket() { };
                    gameSocket.RoomId = roomid;
                    gameSocket.PlayerId = playerId;
                    gameSocket.Connection = session;
                    gameSocket.ConnType = connectionType;
                    Connections.Add(gameSocket);
                }
                else
                {
                    var socket = gameSocket.Connection;
                    gameSocket.Connection = session;
                    socket.CloseConnection(ErrorCodes.LobbySingleInstanse, ConnectionType.Void);
                   
                } 

                return gameSocket;
            }
        }


        public  string GetConnectionOnRoom(int playerId, int roomId)
        {
            lock (locker)
            {
                string result = string.Empty;
                try
                {

                    var socket = Connections.FirstOrDefault(conn => conn.RoomId == roomId && conn.PlayerId == playerId);

                    result = socket.Connection;

                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"GetConnectionOnRoom: player_id={playerId} , room_id={roomId}", ex);
                }
                return result;
            }
        }


        public  string GetLobbyConnection(int playerId)
        {
            return GetConnectionOnRoom(playerId, 0);
        }


        public  void SendMessage(string session, string message)
        {

            Task.Factory.StartNew(() => Send(session, message));

        }

        public  void Send(string session, string message)
        {

            try
            { 

                GameMessageBroadcaster.SendMessage(session, message);

            }
            catch (Exception)
            {
               // ApplicationManager.Logger.Error($"Send:", ex);
            }

        }


        public  void SendMessage(string session, string command, JObject data)
        {
            data[CommandKeys.command] = command;
            SendMessage(session, data.ToString());
        }

        //Send Message from room
 
        public  void SendMessage(int playerId, int roomId, string command ,JObject data)
        {
            data[CommandKeys.command] = command;
            var session = GetConnectionOnRoom(playerId, roomId);
            SendMessage(session, data.ToString());
        }

        public  void SendMessageToAllLobby(string command , JObject data)
        {
            data[CommandKeys.command] = command;
            var message = data.ToString(); 
            var lobbyConnections = Connections.Where(e => e.ConnType == (int) ConnectionType.Lobby).ToList();
            lobbyConnections.ForEach(e=> SendMessage(e.Connection, message) );
        }
        public  void SendMessageToAll(string message)
        {
            Connections.ForEach(e => SendMessage(e.Connection, message));
        }

        public void SetConnectionType(string session, ConnectionType value)
        {
            try
            {
                ConnectionData data = GetConnectionData(session);
                if (data != null)
                {
                    data.Type = value;
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SetConnectionType session={session}", ex);
            }
        }




        public ConnectionData GetConnectionData(string sessionId)
        {

            lock (locker)
            {
                try
                {
                    ConnectionInfo.TryGetValue(sessionId, out ConnectionData data);

                    return data;
                }
                catch (Exception)
                {
                    return new ConnectionData();
                }
            }

        }
        public void SetConnectionData(string sessionId, ConnectionData data)
        {
            lock (locker)
            {
                try
                {
                    ConnectionInfo.TryAdd(sessionId, data);
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"SetConnectionData: session={sessionId}", ex);
                }
            }
        }
    }
}

