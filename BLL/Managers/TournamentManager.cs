﻿using BLL.Classes.Entities;

using  EntityModels.Exceptions;
using EntityModels.Types;
using InterfaceLibrary.Database;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Manager;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Managers
{
    public   class TournamentManager :ITournamentManager
    {
        private object locker { get; set;  } = new object();
        public List<ITournament> ActiveTournaments { get; set; } = new List<ITournament>();

        public ITournamentRepository TournamentDbInstance { get; set; }
 

        public TournamentManager(ITournamentRepository repository)
        {
            TournamentDbInstance = repository;
        }

        public   void Initialize()
        {

            var list = TournamentDbInstance.GetTournnamentList();
            try
            {
                list.ForEach(e => ActivateTournament(e));
                OrderTournaments();  
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"TournamentManager Initialize:", ex); 
            }

        }

        public   void CleanTournament()
        {

            lock (locker)
            {

                var removers = new List<ITournament>();
                var day = DateTime.Now.Day;
                ActiveTournaments.ForEach(tour =>
                {
                    if (tour.State == TournamentState.Finished || tour.State == TournamentState.Declined)
                    {
                        if (tour.FinishTime == null || tour.FinishTime.Day != day)
                        {
                            removers.Add(tour);
                        }
                    }
                });

                for (int i = 0; i < removers.Count; i++)
                {
                    ActiveTournaments.Remove(removers[i]);
                }

            }

        }

        public   void OrderTournaments()
        {
            try
            {
                ActiveTournaments = ActiveTournaments.OrderBy(e => e.StartDate).ToList();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"OrderTournaments: ", ex);
            }
        }

        public   bool ChangeTournamentName(int tournamentId)
        {
            try
            {
                var tournament = GetTournamentById(tournamentId);
                TournamentDbInstance.GetTournamentNames(tournament);
                return true;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"ChangeTournamentName: tournament_id={tournamentId}", ex);
                return false;
            }


        }
        public   JObject GetTournamentsInfo(int playerId)
        {
            var result = new JObject();
            var arr = new JArray();
            ActiveTournaments.ForEach(e => arr.Add(e.ToJson(playerId)));
            result["tournaments"] = arr;
            return result;
        }
       


        public   bool ActivateTournament(int tournamentId)
        {

            var result = true;
            lock (locker)
            {

                try
                {
                    if (ActiveTournaments.Exists(e => e.TournamentId == tournamentId))
                    {
                        return false;
                    }
                    var tournament = TournamentDbInstance.GetTournamentById(tournamentId);
                    ActiveTournaments.Add(tournament);

                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }


        public   ITournament GetTournamentById(int tournamentId)
        {

            lock (locker)
            { 
                var tournament = ActiveTournaments.First(e => e.TournamentId == tournamentId);
                if (tournament == null)
                {
                    throw new TournamentCantFindException();
                }
                return tournament;
            }
        }


        public   bool CancelTournament(int tournamentId, int errorId)
        {
            var tournament = ActiveTournaments.First(e => e.TournamentId == tournamentId);
            if (tournament != null)
            {
                tournament.CancelTournament(errorId);
                return true;
            }
            return false;
        }

        public   bool DeleteTournament(int tournamentId) {

            try
            {
                lock (locker)
                {
                    var tournament = ActiveTournaments.First(e => e.TournamentId == tournamentId);
                    if (tournament != null)
                    {
                        tournament.DeleteTournament();
                        ActiveTournaments.Remove(tournament);
                        return true;
                    } 
                }
            }
            catch (Exception ex) {

                ApplicationManager.Logger.Error($"DeleteTournament  tournament_id={tournamentId}", ex);
             
            }
            return false;
        }
    }
}
