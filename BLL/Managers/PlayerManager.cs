﻿using EntityModels.Exceptions;
using BLL.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Classes.GameClass;
using EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using InterfaceLibrary.Database;
using InterfaceLibrary.Manager;

namespace BLL.Managers
{
    public class PlayerManager:IPlayerManager
    {

        private object locker = new object();
        private List<IMerchant> Merchants { get; set; }
        private Dictionary<string, IPlayer> Players { get; set; } = new Dictionary<string, IPlayer>();      
        public  IPlayerRepository PlayerRepository { get; set; }


        public PlayerManager(IPlayerRepository repository)
        {
            PlayerRepository = repository;
            initialize();
        }

        public  void initialize() { 
            Merchants = PlayerRepository.GetAllMerchants();
        }

        public int PlayersCount() {
            return Players.Count; 
        }
        public   void Login(string session, JObject json)
        {

            try
            {
                var token = json["token"].Value<string>();
                var player = PlayerLogin(token);

                session.SetUserId(player.GetPlayerId());
                session.SetConnectionType(ConnectionType.Void);


                dynamic response = new JObject();
                response.player = player.ToJson();
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session, CommandKeys.login, response);
                // session.GetGameSocket().Destroy();
            }
            catch (GameException ex)
            {
                session.CloseConnection(ex.ErrorId);
                ApplicationManager.Logger.Error($"login exception: {json.ToString()}" , ex);
            }
            catch (Exception ex)
            {
                session.CloseConnection(ErrorCodes.UnexpectedToken);
                ApplicationManager.Logger.Error($"login exception: {json.ToString()}", ex);
            }

        }

        public   List<int> GetActivePlayers()
        {
              var result = new List<int>();
             var players = Players.Values.ToList();
             players.ForEach(e => result.Add(e.GetPlayerId()));

            return result; 
        }
        public   IPlayer GetPlayerById(int playerId)
        {
            IPlayer pl;
            Players.TryGetValue(playerId.ToString(), out pl);
            return pl;
        }

        public   void ClearPlayers()
        {
            lock (locker)
            {
                var players = Players.Keys.ToList();
                for (int i = 0; i < players.Count; i++)
                {
                    IPlayer pl;
                    Players.TryGetValue(players[i], out pl);
                    if (pl.LastActiveDate + 30000 < DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond)
                    {
                        Players.Remove(players[i]);
                    }
                }
            }
        }
        public   bool BlockPlayer(int playerId)
        {
            try
            {
                lock (locker)
                {
                    if (Players.ContainsKey(playerId.ToString()))
                    {
                        var pl = GetPlayerInfoById(playerId);
                        pl.IsBlocked = false;
                    }
                    var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                    gameSocketManager.CloseAllConnection(playerId);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public   bool UnblockPlayer(int playerId)
        {
            try
            {
                if (Players.ContainsKey(playerId.ToString()))
                {
                    var pl = GetPlayerInfoById(playerId);
                    pl.IsBlocked = false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public   bool PlayerExists(int playerId)
        {
            return GetPlayerById(playerId) != null;

        }

        public   IPlayerInfo GetPlayerInfoById(int playerId)
        {

            var pl = GetPlayerById(playerId);
            if (PlayerExists(playerId))
            {
                return GetPlayerById(playerId).Info;
            }
            else
            {
                var info = PlayerRepository.GetPlayerInfo(playerId);
                info.Achievements = BaseApplication.Current.AchievementManagerInstance.AchievementRepositoryInstance.UpdatePlayerAchievementsList(playerId);
                info.OrderedProducts = BaseApplication.Current.BaseShopManagerInstance.ShopDBManager.GetOrderedProducts(playerId);

                return info;
            }
        }
        public   JObject GetPlayerOrderProducts(int playerId)
        {
            var shoprepository = BaseApplication.Current.BaseShopManagerInstance.ShopDBManager;
            var products = shoprepository.GetOrderedProducts(playerId);
            if (products != null)
            {
                GetPlayerInfoById(playerId).OrderedProducts = shoprepository.GetOrderedProducts(playerId);
            }
            else
            {
                products = GetPlayerInfoById(playerId).OrderedProducts;
            }

            var result = new JObject();
            var arr = new JArray();
            products.ForEach(e => arr.Add(e.ToJson()));
            result["products"] = arr;
            return result;
        }


        public   bool RemovePlayer(int playerId)
        {

            return Players.Remove(playerId.ToString());
        }

        public   decimal GetPlayerBalance(int playerId)
        {
            decimal result = 0;
            try
            {
                result = GetPlayerById(playerId).GetCurrentBalance();

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"GetPlayerBalance: player_id={playerId} ",ex);
            }

            return result;

        }

        public   IPlayer PlayerLogin(int playerId)
        {
            if (playerId == 0)
                throw new AuthenticateException();

            Players.TryGetValue(playerId.ToString(), out var pl);
            if (pl != null) return pl;
            var info = PlayerRepository.GetPlayerInfo(playerId);

            info.Achievements = BaseApplication.Current.AchievementManagerInstance.AchievementRepositoryInstance.UpdatePlayerAchievementsList(playerId);
            info.OrderedProducts = BaseApplication.Current.BaseShopManagerInstance.ShopDBManager.GetOrderedProducts(playerId);

            pl = new Player() { Info = info };

            if (pl.Info.IsBlocked)
                throw new BlockedException();

            return pl;
        }


        public   void PlayerJoin(IPlayerInfo pl)
        {
            var id = pl.PlayerId;
            if (!PlayerExists(id))
            {
                Players.Add(id.ToString(), new Player { Info = pl });

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.ServerPlayersCountChange();
            }

        }
        public   IPlayer PlayerLogin(string tkn)
        {
            return PlayerLogin(GetPlayerIDByToken(tkn));
        }



        public   int GetPlayerIDByToken(string tkn)
        {
            var playerId = 0;
            try
            {
                var token = Guid.Parse(tkn);
                playerId = PlayerRepository.GetPlayerId(token, false);

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"GetPlayerIDByToken: token={tkn} ", ex);
            }
            return playerId;

        }
        public   int GetConnectedPlayerIdByToken(string tokenString)
        {
            var playerId = 0;
            try
            {
                var token = Guid.Parse(tokenString);
                playerId = PlayerRepository.GetPlayerId(token, true);

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"GetConnectedPlayerIdByToken: {tokenString}" ,ex);
            }
            return playerId;

        }
        public   void ChangeAvatar(int playerId, int avatarId)
        {

            var player = GetPlayerById(playerId);
            player.Info.AvatarId = avatarId;
            PlayerRepository.ChangeAvatar(playerId, avatarId);
        }
        public   void ChangeStone(int playerId, int stoneColor)
        {
            var player = GetPlayerById(playerId);
            player.Info.StoneColor = stoneColor;
            PlayerRepository.ChangeStoneColor(playerId, stoneColor);
        }


        //merchant

        public IMerchant GetMerchantById(int id)
        {
            return Merchants.FirstOrDefault(mrchnt => mrchnt.MerchantId == id);
        }
        public bool IsSameNetwork(int firstMerchantId, int secondMerchantId, bool network)
        {
            try
            {

                if (!network)
                {
                    return firstMerchantId == secondMerchantId;
                }

                var firstMerchant = GetMerchantById(firstMerchantId);

                return firstMerchant.IsNetwork;
            }
            catch (Exception)
            {

                // logg error
            }
            return false;
        }


    }
}
