﻿using InterfaceLibrary.Achievements;
using InterfaceLibrary.Database;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using InterfaceLibrary.Manager;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
namespace BLL.Managers
{
    public class AchievementManager : IAchievementManager
    {

        public   List<IAchievement> Achievements { get; set; }

        public IAchievementRepository AchievementRepositoryInstance { get; set; }

        public AchievementManager(IAchievementRepository repository)
        {
            AchievementRepositoryInstance = repository;
        }

        public   void Initialize() {
           
            UpdateAchievements();
        }

        public   void UpdateAchievements() {   
            Achievements = AchievementRepositoryInstance.UpdateAchievementsList(); 

        }

        public   void  CalculateAchievement(int playerId,int roomId,AchievementEvents startEvent ,JObject data)
        {
            try
            {

                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                var player = playerManager.GetPlayerInfoById(playerId);
                var list = Achievements.Where(e => e.IsValid(player.Rank,startEvent)).ToList();

                list.ForEach(e => e.Execute(player, roomId, data));
            }
            catch (Exception ex)
            {
               ApplicationManager.Logger.Error($"CalculateAchievement: player_id={playerId} , room_id={roomId} , data={data.ToString()}", ex);
            }
        }

        public   bool ActivateAchievement(int achievementId)
        {
            var result = true;
            try
            {
                Achievements.RemoveAll(e => e.AchievementId ==achievementId);
                var achievement = AchievementRepositoryInstance.GetAchievementById(achievementId);
                Achievements.Add(achievement);
             
            }
            catch (Exception ex) {
                result = false;
                ApplicationManager.Logger.Error($"ActivateAchievement: achievement_id={achievementId}", ex);
            }
            return result;
        }

        public   bool DeactivateAchievement(int achievementId)
        {
            var result = true;
            try
            {
               
                Achievements.RemoveAll(e => e.AchievementId == achievementId);
                AchievementRepositoryInstance.DeactivateAchievement(achievementId);
            }
            catch (Exception ex)
            {
                result = false;
                ApplicationManager.Logger.Error($"DeactivateAchievement: achievement_id={achievementId}", ex);
            }
            return result;
        }

        public bool PlayerAchievementIncrease(IPlayerInfo info, int achievementId, int count, int maxCount)
        {

          return   AchievementRepositoryInstance.PlayerAchievementIncrease(info,achievementId,count,maxCount);
        }

        public   JArray AchievementsToJson(int playerRank)
        {
            var array = new JArray();
            Achievements = AchievementRepositoryInstance.UpdateAchievementsList();
            Achievements.ForEach(e =>
            {
                if (playerRank<=e.MinRank)
                {
                    array.Add(e.ToJson());
                } 
            });
            return array;
        }
    }
}
