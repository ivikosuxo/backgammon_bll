﻿using BLL.Classes;
using EntityModels.Config;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Classes.GameClass;
using BLL.Classes.Entities;
using EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Achievements;
using InterfaceLibrary.Models;
using EntityModels.Exceptions;
using BLL.Achivments;
using log4net;

namespace BLL.Managers
{
    public class Factory : IFactory
    {
        public ILog Logger { get; set; }
        public Factory() {

              Logger =  LogManager.GetLogger(typeof(Factory));
        }
        public static IGameRule GetGame(int type)
        {

            switch (type)
            {
                case GameType.European:
                    return new EuropianRule();

                case GameType.Georgian:
                    return new GeorgianRule();

                case GameType.Long:
                    return new LongRule();

                case GameType.Hyper:
                    return new HyperRule();

                case GameType.Khachapuri:
                    return new KhachapuriRule();

                case GameType.Eureka:
                    return new EurekaRule();

                case GameType.Blitz:
                    return new NewEurekaRule();


                default:
                    return new EuropianRule();

            }

        }

        public ITournament GetTournamentById(int tournamentType)
        {
            var type = (TournamentType)tournamentType;
            switch (type)
            {
                case TournamentType.SitAndGo:
                    return new SitAndGo();

                case TournamentType.FreeRoll:
                    return new Tournament();


            }

            return new SitAndGo();
        }

        public IAchievement GetAchievementById(AchievementType id)
        {
            switch (id)
            {
                case AchievementType.Roll:
                    return new RollAchievement();

                case AchievementType.RoyalDeffence:
                    return new DadianiAchievement();

                case AchievementType.WinRound:
                    return new WinRoundAchievement();
                case AchievementType.LoseRound:
                    return new LoseRoundAchievement();

                case AchievementType.LoseGame:
                    return new LoseGameAchievement();
                case AchievementType.WinGame:
                    return new WinGameAchievement();

                case AchievementType.TournamentWin:
                    return new TournamentWinAchievement();

                case AchievementType.TournamentPlay:
                    return new TournamentPlayAchievement();

                case AchievementType.TournamentTopPlace:
                    return new TournamentTopPlaceAchievement();

                case AchievementType.Mars:
                    return new MarsAchievement();

                case AchievementType.DoubleMars:
                    return new DoubleMarsAchievement();

                case AchievementType.PlayGame:
                    return new PlayGameAchievement();

                case AchievementType.StoneKiller:
                    return new KillerAchievement();

            }
            return new RollAchievement();
        }

        public IGameException GetExceptionById(int errorId)
        {
            switch (errorId)
            {
                case ErrorCodes.CantAuthenticate:
                    return new AuthenticateException();

                case ErrorCodes.AccountIsBlocked:
                    return new BlockedException();

                case ErrorCodes.InvalidMove:
                    return new InvalidMoveException();

                case ErrorCodes.NotCorrectPassword:
                    return new IncorrectPasswordException();

                case ErrorCodes.ShortPassword:
                    return new PasswordSizeException();

                case ErrorCodes.PasswordSizeError:
                    return new PasswordSizeException();

                case ErrorCodes.NotEnoughMoney:
                    return new NotEnoughBalanceException();

                case ErrorCodes.NotEnoughPlayers:
                    return new NotEnoughPlayerException();

                case ErrorCodes.RoomIsNotActive:
                    return new RoomNotExistsException();

                case ErrorCodes.TournamentAlreadyContains:
                    return new TournirAlreadyContainsPlayerException();

                case ErrorCodes.NotValidBet:
                    return new NotValidBetException();

                case ErrorCodes.TournamentIsFull:
                    return new TournamentIsFullException();

                case ErrorCodes.TournamentIsStarted:
                    return new TournamentIsStartedException();

                case ErrorCodes.TournamenCantFind:
                    return new TournamentCantFindException();

                case ErrorCodes.MaxRank:
                    return new MaxRankException();

                case ErrorCodes.ToManyRoomCreated:
                    return new ToManyRoomCreatedException();

                case ErrorCodes.NotEnoughCoins:
                    return new NotEnoghtCoinException();

                case ErrorCodes.NotEnoughItems:
                    return new NotEnoughItemsException();

                case ErrorCodes.RoomIsFull:
                    return new RoomIsFullException();

                case ErrorCodes.LobbySingleInstanse:
                    return new LobbySingleInstanceException();

                case ErrorCodes.ProductIsSold:

                    return new ProductIsSoldException();

                case ErrorCodes.SomethingWentWrong:
                    return new ProblemDetectedException();

                case ErrorCodes.NotEnoughGameCount:
                    return new NotEnoughGameCountException();

                default:
                    return new GameException();
            }


        }
    }
}
