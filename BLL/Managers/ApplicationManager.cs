﻿using System;
using System.Collections.Generic;
 
using log4net;
using BLL.Classes.RequestHandler;
using EntityModels.Types;
using Newtonsoft.Json.Linq;
 
using EntityModels.Entities;
 

namespace BLL.Managers
{
   public   class ApplicationManager
    {
        public static readonly ILog Logger = LogManager.GetLogger(typeof(ApplicationManager));
 
        private  Dictionary<string,IRequestHandler> Handlers =  new Dictionary<string, IRequestHandler>();
         

        
        public  void Initialize()
        {
            //lobby
            Handlers.Add(CommandKeys.login,new LoginHandler());
            Handlers.Add(CommandKeys.join_room, new JoinRoomHandler());
            Handlers.Add(CommandKeys.join_lobby, new JoinLobbyHandler());
            Handlers.Add(CommandKeys.room_info, new RoomInfoHandler());
            Handlers.Add(CommandKeys.avatar_change, new ChangeAvatarHandler());
            Handlers.Add(CommandKeys.change_stone, new ChangeStoneColor());
            //Handlers.Add(CommandKeys.raise, new RaiseHandler());
            Handlers.Add(CommandKeys.create_room, new CreateRoomHandler());
            Handlers.Add(CommandKeys.roll, new RollHandler());           
            Handlers.Add(CommandKeys.pre_join_room, new PreJoinRoomHandler());
            Handlers.Add(CommandKeys.play, new PlayRequestHandler());
            Handlers.Add(CommandKeys.undo, new UndoRequestHandler());
            Handlers.Add(CommandKeys.submit, new SubmitHandler());
            Handlers.Add(CommandKeys.give_up, new GiveUpHandler());
            Handlers.Add(CommandKeys.raise, new RaisePressHandler());
            Handlers.Add(CommandKeys.raise_answer, new RaiseHandler());
            Handlers.Add(CommandKeys.play_again, new PlayAgainHandler());
            Handlers.Add(CommandKeys.play_several, new PlaySeveralMoves());
            Handlers.Add(CommandKeys.rank_increase, new RankIncreaseHandler());
            //tournament
            Handlers.Add(CommandKeys.tournaments_info, new TournamentInfoHandler());
            Handlers.Add(CommandKeys.tournaments_register, new TournamentRegisterHandler());
            Handlers.Add(CommandKeys.tournaments_unregister, new TournamentUnregisterHandler());
            Handlers.Add(CommandKeys.tournament_pre_join, new TournamentPreJoinHandler());
            Handlers.Add(CommandKeys.tournament_join, new TournamentJoinHandler());

            Handlers.Add(CommandKeys.ping, new PingRequestHandler());
            Handlers.Add(CommandKeys.ping_latency, new PingLatencyHandler());
            Handlers.Add(CommandKeys.achievement_info, new AchievementInfoHandler());
            Handlers.Add(CommandKeys.active_connections, new ActiveConnectionsHandler());
            Handlers.Add(CommandKeys.chat, new ChatRequestHandler());
            Handlers.Add(CommandKeys.buy_product, new BuyProductHandler());
            Handlers.Add(CommandKeys.shop_info, new GetShopInfoHandler());
            Handlers.Add(CommandKeys.order_products, new GetOrderedProductsHandler());
            
            Handlers.Add(CommandKeys.round_data, new RoundInfoHandler());
            Handlers.Add(CommandKeys.game_history, new GameInfoHandler());
            Handlers.Add(CommandKeys.game_lobby_history, new GameHistoryRequestHandler());

             Handlers.Add(CommandKeys.waiting_add, new AddWaitingListHandler());
             Handlers.Add(CommandKeys.waiting_accept, new WaitingAcceptHandler());
             Handlers.Add(CommandKeys.tournament_total_info, new TournamentTotalnfoHandler());
            Handlers.Add(CommandKeys.problem_report, new ProblemReportHandler());
            Handlers.Add(CommandKeys.opponent_info, new GetOpponentInfoHandler());
            
            BaseApplication.Current.PlayerManagerInstance.PlayerRepository.UpdateCreateRoomProperty();
        }

        //connection management
        public  void OnMessage(string session, string data)
        {
            try
            {
                var jdata = JObject.Parse(data);
                IRequestHandler handler = null;  
                Handlers.TryGetValue(jdata[CommandKeys.command].Value<string>(), out handler);
                handler?.HandlerRequest(session, jdata);
            }
            catch (Exception ex)
            {
                Logger.Error($"OnMessage: session={session},data={data.ToString()}", ex);

            }
        }

        public static JObject GetBlockedRoomMessage()
        {

            return BlockCreateRoomModel.ToJson();
        }
  
    }
}
