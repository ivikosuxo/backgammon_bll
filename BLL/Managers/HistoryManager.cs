﻿
using InterfaceLibrary.Database;
using InterfaceLibrary.Manager;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Managers
{
  public   class HistoryManager :IHistoryManager
    {

        public IHistoryRepository HistoryDbInstance { get; set; }

        public HistoryManager(IHistoryRepository repository)
        {
            HistoryDbInstance = repository;
        }

        public   JObject GetGameInfo(string token , int gameId) {

            var playerManager = BaseApplication.Current.PlayerManagerInstance;

            var result = new JObject();
            result["rounds"] = HistoryDbInstance.GetRounds(gameId);
            result["game_info"] = HistoryDbInstance.GetGameInfo(gameId);

           var id = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetPlayerId(Guid.Parse(token), true);
            
            result["my_id"] = id;
            var arr = HistoryDbInstance.GetGamePlayers(gameId, id);
            result["players"] = arr;

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].Value<JObject>()["me"].Value<bool>())
                {
                    result["user_name"] = playerManager.GetPlayerInfoById(arr[i].Value<JObject>()["player_id"].Value<int>()).Username;

                }
                else {
                    result["opponent_name"] = playerManager.GetPlayerInfoById(arr[i].Value<JObject>()["player_id"].Value<int>()).Username;

                }

            }
            return result;

        }

        public   JObject GetRoundInfo( int roundId)
        {
            var result = new JObject();
            result["moves"] = HistoryDbInstance.GetMoves(roundId);
            result["round_info"]= HistoryDbInstance.GetRoundInfo(roundId);
            return result;
        }
    }
}
