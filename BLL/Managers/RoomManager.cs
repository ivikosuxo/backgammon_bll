﻿using BLL.Classes;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Classes.GameClass;
using  EntityModels.Config;
using BLL.Interfaces;
using Newtonsoft.Json.Linq;
using BLL.Classes.Entities;

using  EntityModels.Exceptions;
using EntityModels.Types;
using EntityModels.Entities;
using InterfaceLibrary.Enums;
using BLL.Extensions;
using InterfaceLibrary.Database;
using InterfaceLibrary.Manager;
using InterfaceLibrary.Game;
using InterfaceLibrary.Models;
using InterfaceLibrary.Factory;

namespace BLL.Managers
{
    public class RoomManager :IRoomManager
    { 
        public Dictionary<string, IRoom> Rooms { get; set; } = new Dictionary<string, IRoom>();
        public ILobby LobbyRoom { get; set; }
        private  object locker = new object();

        public IRoomRepository RoomDbManager { get; set; }

        public RoomManager(IRoomRepository repository)
        {
            RoomDbManager = repository;
        }

        public   IRoom GetRoomById(int roomId)
        {

            IRoom room;
            Rooms.TryGetValue(roomId.ToString(), out room);
            return room;
        }

        public   void RemoveRoom(int roomId)
        {
            lock (locker)
            {
                try
                { 
                    Rooms.Remove(roomId.ToString()); 
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"RemoveRoom: room_id={roomId}", ex);

                }
            } 
        }
        public   void AddRoom(int roomId, IRoom room)
        {           
          Rooms.Add(roomId.ToString(), room);             
        }

        public   bool IsExistsRoom(int roomId)
        {
            return GetRoomById(roomId) != null;
        }

        public   void Initialize()
        {
            LobbyRoom = new Lobby();
            // var rooms = RoomRepository.GetActiveRooms(0);
            // rooms.ForEach(e => Rooms.Add(e.RoomId.ToString(), e));            
        }

        public   JArray GetRoomsInfo()
        {
            var roomsArray = new JArray();
            Rooms.Values.Where(e => e.RoomId > 0 && e.TournamentId == 0).ToList().ForEach(rm => roomsArray.Add(rm.GetRoomInfo()));

            return roomsArray;
        }

        public   JObject CreateRoom(ICreateRoomModel data)
        {

            var roomId = 0;
            var result = new JObject();


            if (!BlockCreateRoomModel.CanCreateRoom)
            {
                throw new CantCreateRoomException();
            }
            var playerManager = BaseApplication.Current.PlayerManagerInstance;

            data.Validate();
            data.CreatorId = playerManager.GetPlayerIDByToken(data.Token);
            var player = playerManager.GetPlayerInfoById(data.CreatorId);
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            

            if (gameSocketManager.GetUserConnectionCount(data.CreatorId) >= BlockCreateRoomModel.RoomLimitPerUser)
            {
                throw new ToManyRoomCreatedException();
            }
            if (player.IsBlocked)
            {
                throw new BlockedException();
            }

            data.Roomname = player.Username;
            try
            {
                data.Percent = GlobalConfig.RakePercent;
                roomId = RoomDbManager.CreateRoom(data); 

                if (roomId > 0)
                {
                    var config = new RoomConfig()
                    {
                        MinBet = data.MinBet,
                        MaxPoint = data.MaxPoint,
                        RaiseCount = data.RaiseCount,
                        BeaverRule = data.BeaverRule,
                        Speed = data.Speed,
                        Type = data.RuleType,
                        CreatorId = data.CreatorId,
                        MaxBet = data.MaxBet,
                        CrawfordRule = data.CrawfordRule,
                        DoubleRule = data.DoubleRule,
                        JacobRule = data.JacobRule,
                        PlayersCount = 2,
                        RoomId = roomId,
                        Roomname = player.Username,
                        IsProtected = data.IsProtected,
                        Password = data.Password,
                        ChatEnabled = data.ChatEnabled,
                        WithDouble = data.WithDouble
                    };

                    var room = new Room(roomId, config);
                    RoomDbManager.BlockBalance(player, room.RoomId,  config.MinBet, config.MaxBet); 



                    player.GetCurrentBalance();

 
                    LobbyRoom.UpdateBalance(player.PlayerId, player.Balance); 
                    room.AddRoomPlayer(player,  "",  roomId );  
                    this.AddRoom(roomId, room);

              
                    gameSocketManager.Connections.Add(new GameSocket() { Active = true, Connection = "", ConnType = (int)ConnectionType.Game, PlayerId = player.PlayerId, RoomId = roomId });
                    room.StartLifeTimer();

                    var roomInfo = room.GetRoomInfo();
                    roomInfo["rooms_count"] = Rooms.Count;
                    LobbyRoom.SendMessageAll(CommandKeys.create_room, roomInfo);
                    LobbyRoom.SendMessage(player.PlayerId, CommandKeys.sit_on_room, new JObject() { ["room_id"] = roomId });
                }

                result["room_id"] = roomId;

            }
            catch (GameException ex)
            {
                result = ex.ToJson();
                RoomDbManager.RoomDeactivate(roomId);
            }

            return result;
        }

        public   void SaveChatMessage(int roomId, int playerId, int  gameId, string message)
        {
            try {
                RoomDbManager.SaveChatMessage(roomId, playerId, gameId, message);  
            }
            catch(Exception ex)
            {
                ApplicationManager.Logger.Error($"SaveChatMessage : room_id={roomId} , player_id{playerId} , message={message}" , ex);
            }

        }

    }
}
