﻿using BLL.Classes.Entities;

using EntityModels.Entities;
using  EntityModels.Exceptions;
using EntityModels.Types;
using InterfaceLibrary.Database;
using InterfaceLibrary.Factory;
using InterfaceLibrary.Game;
using InterfaceLibrary.Manager;
using InterfaceLibrary.Models;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Managers
{
    public class ShopManager :IShopManager
    {
        public List<IShopProduct> Products = new List<IShopProduct>();
        public List<ICategoryTranslate> CategoryTranslate = new List<ICategoryTranslate>();

         
        private object _lockObject = new object();
        
        public IShopRepository ShopDBManager { get; set; }

        private IFactory FactoryInstance { get; set; }

        public ShopManager(IShopRepository repository,IFactory factory) {
             ShopDBManager = repository;
            FactoryInstance = factory;
        }

        public void Initialize()
        {
            Products = ShopDBManager.GetShopProducts();
            CategoryTranslate = ShopDBManager.GetCategories();
        }

        public bool GetShopProduct(int productId)
        {
            lock (_lockObject)
            {
                try
                {
                    var product = Products.FirstOrDefault(e => e.Id == productId);
                    if (product != null)
                    {
                        Products.Remove(product);
                    }
                    product = ShopDBManager.GetShopProduct(productId);
                    if (product == null)
                    {
                        throw new Exception();
                    }
                    Products.Add(product);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }

        }

        public bool DeactivateShopProduct(int productId)
        {
            lock (_lockObject)
            {
                try
                {
                    var product = Products.FirstOrDefault(e => e.Id == productId);
                    if (product != null)
                    {
                        Products.Remove(product);

                        var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                        gameSocketManager.SendMessageToAllLobby(CommandKeys.remove_product, new JObject() { ["product_id"] = productId });

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"DeactivateShopProduct: product_id={productId}", ex);
                }
                return false;
            }

        }

        public IShopProduct GetProductById(int productId)
        {
            var prod = Products.FirstOrDefault(e => e.Id == productId);
            if (prod == null)
            {
                var exception = FactoryInstance.GetExceptionById(ErrorCodes.CantFindProduct);
                exception.Throw();
            }
            return prod;
        }

        public void BuyProduct(string session, int playerId, int count, int productId)
        {
            lock (_lockObject)
            {

                var product = GetProductById(productId);
                if (product.StockCount >= count)
                {

                    var playerManager = BaseApplication.Current.PlayerManagerInstance;
                    var info = playerManager.GetPlayerInfoById(playerId);


                    ShopDBManager.BuyProduct(info, product, count);
                    product.StockCount -= count;


                    var roomManager = BaseApplication.Current.RoomManagerInstance;
                    roomManager.LobbyRoom.UpdateCoins(playerId, info.Coin);

                    var jsn = new JObject()
                    {
                        ["product_id"] = productId,
                        ["stock_count"] = product.StockCount
                    };

                    var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                    gameSocketManager.SendMessage(session, CommandKeys.buy_product, jsn);
                }
                else
                {
                    var exception = FactoryInstance.GetExceptionById(ErrorCodes.ProductIsSold);
                    exception.Throw();
                  
                } 


            }
        }

        public JObject ToJson()
        {

            var productJArray = new JArray();

            Products = ShopDBManager.GetShopProducts();
            Products.ForEach(e =>
            {
                if (e.IsActive && !e.IsDeleted)
                {
                    productJArray.Add(e.ToJson());
                }
            });

            var categoryJArray = new JArray();
            CategoryTranslate = ShopDBManager.GetCategories();
            CategoryTranslate.ForEach(e =>
            {
                categoryJArray.Add(e.ToJson());
            });

            var data = new JObject()
            {
                ["items"] = productJArray,
                ["category"] = categoryJArray

            };
            return data;
        } 
    }
}
