﻿using System;
using System.Collections.Generic;
using BLL.Classes.GamePlay;
using BLL.Interfaces;
using Newtonsoft.Json.Linq;
using  EntityModels.Types;
using  EntityModels.Config;
using BLL.Managers;
using System.Linq;
using  EntityModels.Exceptions;
using System.Threading.Tasks;

using BLL.Classes.StateCalculator;
using EntityModels.StateCalculator;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class GeorgianRule : IGameRule
    {

        public int StoneCount { get; set; } = 15;
        public GeorgianRule()
        {
            Initialize();
        }

        public List<BoardPlace> FirstBoard
        {
            get; set;
        }
        public List<BoardPlace> SecondBoard
        {
            get; set;
        }
        public bool CheckMoveValues { get; set; } = true;
        public virtual void Initialize()
        {

            FirstBoard = new List<BoardPlace>();
            SecondBoard = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place1 = new BoardPlace();
                FirstBoard.Add(place1);

                place1 = new BoardPlace();
                SecondBoard.Add(place1);
            }



              FirstBoard[0].Count = 2;
              FirstBoard[11].Count = 5;
              FirstBoard[16].Count = 3;
              FirstBoard[18].Count = 5;

              SecondBoard[0].Count = 2;
              SecondBoard[11].Count = 5;
              SecondBoard[16].Count = 3;
              SecondBoard[18].Count = 5;
             
        }
 

        public virtual bool IsBlocked(int color, int startIndex)
        {
            if (startIndex < 0 || startIndex > 23)
            {
                return false;
            }
            var brd = GetBoardByColor(color);
            return brd[startIndex].IsBlocked;

        }
        public void PlayerRoll(int color, int freezeCount)
        {


        }

        public virtual bool FirstRollDicePlay()
        {

            return false;
        }


        public virtual PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {
            var MaxPlayCount = new PlayModel(false, -1);
 
                try
                {
                    var board = new BoardState(null,GetBoardByColor(roundPlayer.Color), GetBoardByColor(0 - roundPlayer.Color), roundPlayer.MovesList, roundPlayer.dice.GetMaxPoint(), 0, false);
                    board.discardCount = roundPlayer.DiscardCount;
                    board.killedCount = roundPlayer.KilledCount;
                    board.playerstate = roundPlayer.PlayerState;
                    board.Calculate();

                    board.FindSecond(); 

                    MaxPlayCount = new PlayModel(false, board.GetMaxPoint());
                }
                catch (MaxPlayedException ex)
                {
                    MaxPlayCount = ex.Model;
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"CheckMaxPlayCount GeorgianRule, round_player_id={roundPlayer.PlayerId}", ex);
                    MaxPlayCount = new PlayModel(false, -1);
                }
           
            return MaxPlayCount;
        }
        public List<BoardPlace> GetBoard(int color)
        {
            var first = GetBoardByColor(color);
            var second = GetBoardByColor(0 - color);
            var result = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place = new BoardPlace()
                {
                    Color = 0,
                    Count = 0
                };
                if (first[i].Count > second[23 - i].Count)
                {
                    place.Count = first[i].Count;
                    place.Color = 1;
                    place.IsBlocked = first[i].IsBlocked;
                }
                else if (first[i].Count < second[23 - i].Count)
                {
                    place.Count = second[23 - i].Count;
                    place.Color = 2;
                    place.IsBlocked = second[23 - i].IsBlocked;
                }
                result.Add(place);
            }

            return result;
        }

        public bool EndRound(RoundPlayer gu, bool calcMars, Game game)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                ApplicationManager.Logger.Debug($"{nameof(EndRound)} starting game_id={game.GameId}");
                var go = gu.Opponent;
                var currentBet = GetCurrentBet(go, calcMars, game);

                
                bool result = winRound(gu, calcMars, game);


                ApplicationManager.Logger.Debug($"{nameof(EndRound)} game_id={game.GameId} , result={result}");
                go.MovesList.Clear();
                go.UndoList.Clear();


                gu.MovesList.Clear();
                gu.UndoList.Clear();

                var coins = 0m;
                var coins1 = 0m;

                ApplicationManager.Logger.Debug($"{nameof(EndRound)} game_id={game.GameId} , result={result}");
                if (result)
                {
                    var playerManager = BaseApplication.Current.PlayerManagerInstance;
                    var roomManager = BaseApplication.Current.RoomManagerInstance;
                    var gameRepository = BaseApplication.Current.BaseGameRepository;


                    var player = playerManager.GetPlayerInfoById(gu.PlayerId);
                    coins = gameRepository.GameFinishCoins(player, game.GameId, currentBet, 1, game.GameRoom.RoomId);

                    var secondPlayer = playerManager.GetPlayerInfoById(go.PlayerId);
                    coins1 = gameRepository.GameFinishCoins(secondPlayer, game.GameId, currentBet, 2, game.GameRoom.RoomId);

                    roomManager.LobbyRoom.UpdateCoins(player.PlayerId, player.Coin);
                    roomManager.LobbyRoom.UpdateCoins(secondPlayer.PlayerId, secondPlayer.Coin);
                }
                ApplicationManager.Logger.Debug($"{nameof(EndRound)} game_id={game.GameId} , result={result}");

                var giveUp = new JObject()
                {
                    ["win"] = false,
                    ["last"] = result,
                    ["point"] = game.GetPlayerById(gu.PlayerId).GamePoint,
                    ["money"] = currentBet,
                    ["timer_size"] = TimerConfig.NextGameSeconds,
                    ["coin_count"] = coins1
                };
                game.RoundWinner = gu.PlayerId;
                gameSocketManager.SendMessage(go.PlayerId, game.Config.RoomId, CommandKeys.end_game, giveUp);
                giveUp["coin_count"] = coins;
                giveUp["win"] = true;
                gameSocketManager.SendMessage(gu.PlayerId, game.Config.RoomId, CommandKeys.end_game, giveUp);

                ApplicationManager.Logger.Debug($"{nameof(EndRound)} game_id={game.GameId} , result={result}");

                if (!result)
                {
                    game.StartGame(2000);
                    ApplicationManager.Logger.Debug($"{nameof(EndRound)} -> next round started -> game_id={game.GameId} , result={result}");
                }
                else
                {
                    ApplicationManager.Logger.Debug($"{nameof(EndRound)} -> start end game -> game_id={game.GameId} , result={result}");
                    game.GameRoom.EndGame(gu.PlayerId);
                    ApplicationManager.Logger.Debug($"{nameof(EndRound)} -> finish end game -> game_id={game.GameId} , result={result}");
                }

                return result;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"EndRound game_id={game.GameId}", ex);
            }
            return true;
        }

        public Dice RollDice(RoundPlayer pl, bool withDouble)
        {
            var dice = new Dice();
            var opponentDice = pl.Opponent.dice;
            if (opponentDice != null)
            {
                dice.First = (opponentDice.First + dice.First) % 6 + 1;
                dice.Second = (opponentDice.Second + dice.Second) % 6 + 1;
                dice.IsDouble = dice.First == dice.Second;
            }

            if (withDouble)
            {  
                return dice;
            }
            else
            {
                return Dice.GetFirstDice();
            }


        }
        public int GetCorrectPosition(int position)
        {

            return 23 - position;
        }
        public decimal GetCurrentBet(RoundPlayer pl, bool bl, Game game)
        {
            if (game.Config.MatchType() < 2)
            {
                return game.Config.MinBet;
            }
            else
            {
                decimal res = game.Config.MinBet * game.RaiseCount * IsMars(pl, bl, game.Config, game.RaiseCount);
                if (res > game.Config.MaxBet)
                {
                    return game.Config.MaxBet;
                }
                else
                {
                    return res;
                }

            }
        }

        public virtual bool winRound(RoundPlayer rp, bool calcMars, Game game)
        {
            try
            {
                var cfg = game.Config;
                var gp = game.Players.First(e => e.PlayerId == rp.PlayerId);


                int marsRaise = IsMars(rp.Opponent, calcMars, game.Config, game.RaiseCount);
                int currPoint = game.RaiseCount * marsRaise;
                rp.RoundPoint = currPoint;
                gp.GamePoint += currPoint;   


                if (gp.GamePoint > cfg.MaxPoint)
                {
                    gp.GamePoint = cfg.MaxPoint;
                }


                if (game.GameRoom.TournamentId == 0 && cfg.MinBet >= GlobalConfig.AchievementMinBet)
                {

                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;

                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };

                    achievementManager.CalculateAchievement(rp.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson);
                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };

                    achievementManager.CalculateAchievement(rp.Opponent.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson1);
                }

                game.RoundInstance.CloseRound(rp.PlayerId);

                if (gp.GamePoint >= cfg.MaxPoint)
                {
                    game.RaiseCount = game.RaiseCount * marsRaise > game.Config.RaiseCount ? game.Config.RaiseCount : game.RaiseCount * marsRaise;

                    game.CloseGame(gp);

                }
                return gp.GamePoint >= cfg.MaxPoint;

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"winRound: game_id={game.GameId}", ex);
            }
            return true;
        }

        public virtual bool CanRaiseX2(Game game)
        {
            return false;
        }

        public virtual bool CanRaise(Game game)
        {
            var config = game.Config;
            if (config.MatchType() == 0 || !config.DoubleRule)
            {
                return false;
            }

            if (config.MatchType() == 1)
            {
                bool canRs;
                var pl = game.Players[0];
                var pl1 = pl.Opponent;
                canRs = (pl.GamePoint + game.RaiseCount) < config.MaxPoint || (pl1.GamePoint + game.RaiseCount) < config.MaxPoint;

                return (canRs);
            }
            else
            {
                return (game.RaiseCount < game.Config.RaiseCount);
            }

        }

        public virtual bool ValidateMove(RoundPlayer pl, int action, int move, int startPos, int endPos, Game game)
        {
            if (action == RoundPlayerState.KilledState && pl.PlayerState != RoundPlayerState.KilledState)
            {
                throw new InvalidMoveException();
            }
            if (action == RoundPlayerState.DiscardState && pl.PlayerState != RoundPlayerState.DiscardState)
            {
                throw new InvalidMoveException();
            }

            if (pl.PlayerState == RoundPlayerState.KilledState)
            {
                if (action != RoundPlayerState.KilledState || pl.KilledCount == 0 || endPos > 23 || endPos < 0 || (startPos < 24 && startPos > -1))
                {
                    if (pl.PlayerState == RoundPlayerState.KilledState && pl.KilledCount == 0)
                    {
                        if (IsDiscardState(pl))
                        {
                            pl.PlayerState = RoundPlayerState.DiscardState;
                        }
                        else
                        {
                            pl.PlayerState = RoundPlayerState.MoveState;
                        }

                    }
                    throw new InvalidMoveException();
                }
                else
                {
                    return canRevive(pl.Color, endPos);
                }

            }

            if (pl.PlayerState == RoundPlayerState.MoveState)
            {


                return canMove(startPos, endPos, pl.Color);


            }
            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                if (action == RoundPlayerState.KilledState)
                {
                    throw new InvalidMoveException();
                }
                else
                {
                    if (action == RoundPlayerState.MoveState)
                    {

                        return canMove(startPos, endPos, pl.Color);
                    }
                    else
                    {
                        var brd = GetBoardByColor(pl.Color);
                        if (brd[startPos].Count == 0)
                        {
                            throw new InvalidMoveException();
                        }

                        if (brd[startPos].Count == 1 && brd[startPos].IsBlocked)
                        {
                            if (pl.dice.IsDouble)
                            {
                                if (CanPlayDiscardState(pl, true))
                                {
                                    throw new InvalidMoveException();
                                }
                            }
                            else
                            {
                                if (!game.MaxPlayCount.KillDiscard)//CanPlayDiscardState(pl,true))
                                {
                                    throw new InvalidMoveException();
                                }

                            }




                        }

                        return canDiscard(pl);
                    }
                }

            }

            throw new InvalidMoveException();


        }


        public bool CanPlayDiscardState(RoundPlayer pl, bool blocked)
        {
            var result = false;
            for (int i = 18; i < 24; i++)
            {
                var dest = i + pl.MovesList[0];
                if (CanMoveBeforeDiscard(i, dest, pl.Color))
                {
                    return true;
                }

            }
            return result;
        }



        public int discardIndex(RoundPlayer activePl)
        {

            var brd = GetBoardByColor(activePl.Color);
            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[i];

                if (brd[posit].Count > 0)
                {

                    return posit;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    return i;
                }
            }


            return -1;
        }

        public List<BoardPlace> GetBoardByColor(int color)
        {
            if (color > 0)
            {
                return FirstBoard;
            }
            return SecondBoard;
        }

        public int getDiscardIndex(int val)
        {

            return 24 - val;

        }

        public virtual bool canDiscardWithoutMatch(int color, int startPos, int value)
        {

            int index = getDiscardIndex(startPos);
            var myboard = GetBoardByColor(color);


            if (value == 6)
            {

                return true;
            }


            if (index > value)
            {

                return false;
            }

            if (index == value)
            {

                return true;
            }

            for (int j = value; j < 7; j++)
            {

                if (myboard[startPos - j].Count > 0)
                {

                    return false;
                }

            }



            return true;
        }


        public bool canDiscard(RoundPlayer pl)
        {
            return discardIndex(pl) > -1;
        }

        public bool canRevive(int color, int destination)
        {
            var opponentBoard = GetBoardByColor(0 - color);
            var result = false;
            if (destination < 24 && destination >= 0)
            {
                result = opponentBoard[23 - destination].Count < 2;
            }

            if (!result)
            {
                throw new InvalidMoveException();
            }
            return true;
        }
        public virtual bool canMove(int startPos, int endPos, int color)
        {
            var opponentBoard = GetBoardByColor(0 - color);
            var myBoard = GetBoardByColor(color);


            if (endPos > 23 || endPos < 0 || startPos > 23 || startPos < 0)
            {

                throw new InvalidMoveException();
            }
            else
            {
                if (myBoard[startPos].Count == 0 || opponentBoard[23 - endPos].Count > 1)
                {
                    throw new InvalidMoveException();
                }

                if (myBoard[endPos].Count > 0 && myBoard[startPos].IsBlocked && myBoard[startPos].Count == 1)
                {
                    throw new InvalidMoveException();
                }

                return true;
            }
        }
        public virtual bool CanMoveBeforeDiscard(int startPos, int endPos, int color)
        {
            var opponentBoard = GetBoardByColor(0 - color);
            var myBoard = GetBoardByColor(color);


            if (endPos > 23 || endPos < 0 || startPos > 23 || startPos < 0)
            {

                return false;
            }
            else
            {
                if (myBoard[startPos].Count == 0 || opponentBoard[23 - endPos].Count > 1)
                {
                    return false;
                }

                if (myBoard[endPos].Count > 0 && myBoard[startPos].IsBlocked)
                {
                    return false;
                }

                return true;
            }
        }


        public bool IsDiscardState(RoundPlayer pl)
        {
            var opponentBoard = GetBoardByColor(0 - pl.Color);
            var myBoard = GetBoardByColor(pl.Color);

            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                return true;
            }

            int count = 0;
            for (int i = 0; i < 6; i++)
            {
                count += myBoard[23 - i].Count;
            }

            return count + pl.DiscardCount == StoneCount;
        }

        public virtual int IncreaseRaise(int raiseCount, int maxRaiseCount)
        {
            return raiseCount + 1 > maxRaiseCount ? maxRaiseCount : raiseCount + 1;
        }

        public virtual bool canPlaySomething(RoundPlayer activePl, int maxPlay, bool check)
        {
            if (activePl.MovesList.Count == 0)
            {
                return false;
            }

            if ( check)
            {
                return !(maxPlay == activePl.UndoList.Sum(e => e.Value));
            }


            int color = activePl.Color;
            int startingPosition = -1;

            if (activePl.PlayerState == RoundPlayerState.MoveState)
            {

                return canPlayMoveState(activePl);

            }
            else if (activePl.PlayerState == RoundPlayerState.KilledState)
            {

                var result = canMove(color, startingPosition, activePl.MovesList);
                return result.result;
            }
            else if (activePl.PlayerState == RoundPlayerState.DiscardState)
            {

                var candisc = canPlayDiscard(activePl);
                if (candisc.result)
                {

                    return true;
                }


                var res = canPlayMoveState(activePl);



                return res;

            }

            return false;
        }

        public MoveState canPlayDiscard(RoundPlayer activePl)
        {
            var res = new MoveState();


            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);


            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[(i)];
                if (myBoard[posit].Count > 0)
                {
                    res.result = true;
                    res.index = i;
                    return res;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    res.result = true;
                    res.index = i;
                    return res;
                }
            }

            res.result = false;
            res.index = -1;
            return res;
        }


        public bool canPlayMoveState(RoundPlayer activePl)
        {


            var myBoard = GetBoardByColor(activePl.Color);
            for (int i = 0; i < myBoard.Count; i++)
            {
                if (myBoard[i].Count > 0)
                {
                    var result = canMove(activePl.Color, i, activePl.MovesList);

                    if (result.result)
                    {

                        return true;
                    }
                }
            }

            return false;

        }

        public virtual MoveState canMove(int color, int startpos, List<int> moves)
        {
            var obj = new MoveState();

            obj.result = false;
            obj.index = -1;


            var opponentBoard = GetBoardByColor(0 - color);
            var playerBoard = GetBoardByColor(color);
            for (int i = 0; i < moves.Count; i++)
            {
                int pos = startpos + moves[i];

                if (pos > -1 && pos < 24)
                {
                    bool isBlocked = false;
                    if (startpos > -1 && startpos < 24 && startpos > 17)
                    {
                        isBlocked = playerBoard[startpos].IsBlocked;
                    }

                    if (!isBlocked && opponentBoard[23 - pos].Count < 2)
                    {
                        obj.result = true;
                        obj.index = i;
                        obj.pos = pos;
                    }
                }
            }
            return obj;

        }

        public void PlayUndo(RoundPlayer activePl, IMove move)
        {

            var opp = activePl.Opponent;
            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);

            int startPosition = move.StartPosition;
            int endPosition = move.EndPosition;

            if (move.Action == 0)
            {

                myBoard[endPosition].Count--;
                myBoard[startPosition].Count++;
                myBoard[startPosition].IsBlocked = move.Blocked;


            }
            else if (move.Action == 1)
            {
                myBoard[endPosition].Count -= 1;
                activePl.KillStone();

            }
            else if (move.Action == 2)
            {

                activePl.DiscardCount--;
                myBoard[startPosition].Count++;
                myBoard[startPosition].IsBlocked = move.Blocked;
            }


            if (move.Killer)
            {
                opp.ReviveStone();
                opponentBoard[23 - endPosition].Count++;
                myBoard[endPosition].IsBlocked = false;


            }



            opp.PlayerState = move.OpponentState;
            activePl.PlayerState = move.State;


            activePl.MovesList.Add(move.Value);
            activePl.MovesList.Sort();

        }
        public void UnblockBoard()
        {
            FirstBoard.ForEach(e => e.IsBlocked = false);
            SecondBoard.ForEach(e => e.IsBlocked = false);
        }
        public virtual bool getFinalMoves(RoundPlayer pl, Dice dice, int start, int end, int action)
        {

            if (dice.IsDouble || pl.MovesList.Count == 0)
            {
                return false;
            }
            var color = pl.Color;
            var opponentBoard = GetBoardByColor(0 - color);
            var playerBoard = GetBoardByColor(color);

            int secondDice = (pl.MovesList[0] - dice.First) == 0 ? dice.Second : dice.First;

            if (action == 2)
            {
                return false;
            }
            if (action == 1)
            {
                bool firstCheck = canPlayMoveOnBoard(pl.Color, pl.MovesList[0] - secondDice, start);
                if (firstCheck && pl.MovesList[0] > secondDice)
                {
                    return true;
                }
                for (int i = 0; i < 24; i++)
                {
                    if (i != start || playerBoard[i].Count > 1)
                    {
                        bool secondCheck = canPlayMoveOnBoard(color, secondDice, i);

                        if (secondCheck && firstCheck)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            if (pl.PlayerState == RoundPlayerState.MoveState || pl.PlayerState == RoundPlayerState.DiscardState)
            {

                bool firstCheck = canPlayMoveOnBoard(pl.Color, pl.MovesList[0] - secondDice, start);
                if (firstCheck && pl.MovesList[0] > secondDice)
                {
                    return true;
                }

                for (int i = 0; i < 24; i++)
                {
                    if (i != start || playerBoard[i].Count > 1)
                    {
                        bool secondCheck = canPlayMoveOnBoard(color, secondDice, i);
                        bool thirdCheck = canPlayMoveOnBoard(color, pl.MovesList[0] + secondDice, i);

                        if (secondCheck && thirdCheck && NotKillingInGate(pl.Color, secondDice, i))
                        {
                            return true;
                        }
                        if (secondCheck && firstCheck)
                        {

                            return true;
                        }
                    }
                }

                return false;

            }
            else if (pl.PlayerState == RoundPlayerState.KilledState)
            {
                return false;

            }
            else
            {
                return false;
            }
        }


        public virtual int IsMars(RoundPlayer pl, bool calc, RoomConfig cfg, int raiseCount)
        { 

            if (!calc)
            {
                return 1;
            } 

            if (pl.DiscardCount > 0)
            {
                return 1;
            }   

            if (pl.KilledCount > 0)
            {
                return 3;
            }

            return 2;  
        }

        public bool Play(RoundPlayer activePl, int action, int startPos, int endPos, int moveIndex)
        {


            if (action == 0)
            {
                return Move(activePl, startPos, endPos, moveIndex);

            }
            else if (action == 1)
            {
                return Revive(activePl, endPos, moveIndex);
            }
            else
            {
                return Discard(activePl, startPos, endPos, moveIndex);

            }

        }

        public bool Move(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        {
            bool result = false;
            var board = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);
            board[startPos].Count -= 1;


            if (board[startPos].Count == 0)
            {
                board[endPos].IsBlocked = board[startPos].IsBlocked;
            }


            if (opponentBoard[23 - endPos].Count == 1)
            {
                if (endPos > 17)
                {
                    board[endPos].IsBlocked = true;
                }
                opponentBoard[23 - endPos].Count = 0;
                activePl.Opponent.KillStone();
                result = true;
            }

            board[endPos].Count += 1;


            return result;
        }
        public bool Discard(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        {
            bool result = false;
            var board = GetBoardByColor(activePl.Color);

            board[startPos].Count -= 1;

            activePl.AddDiscard();


            return result;
        }


        public bool Revive(RoundPlayer activePl, int endPos, int moveIndex)
        {

            bool result = false;

            var board = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(activePl.Opponent.Color);
            activePl.ReviveStone();
            if (opponentBoard[23 - endPos].Count == 1)
            {
                opponentBoard[23 - endPos].Count = 0;
                activePl.Opponent.KillStone();
                result = true;
            }

            board[endPos].Count++;
            return result;
        }


        public virtual bool canDiscardOnBoard(int color, int move, int startPos)
        {

            var playerBoard = GetBoardByColor(color);
            if (startPos > -1 && startPos < 24 && playerBoard[startPos].IsBlocked)
            {
                return false;
            }

            int dest = startPos + move;
            if (dest == 24)
            {
                return true;
            }

            return canPlayMoveOnBoard(color, move, startPos);

        }

        public virtual bool canPlayMoveOnBoard(int color, int move, int startPos)
        {
            int dest = startPos + move;
            var playerBoard = GetBoardByColor(color);
            var opponentBoard = GetBoardByColor(0 - color);
            if (startPos > -1 && startPos < 24)
            {
                if (playerBoard[startPos].Count == 1 && playerBoard[startPos].IsBlocked)
                {
                    return false;
                }
                if (playerBoard[startPos].Count == 0)
                {
                    return false;
                }
            }

            if (dest > -1 && dest < 24 && opponentBoard[23 - dest].Count < 2)
            {
                return true;
            }
            return false;

        }
        public virtual bool NotKillingInGate(int color, int move, int startPos)
        {
            int dest = startPos + move;
            var playerBoard = GetBoardByColor(color);
            var opponentBoard = GetBoardByColor(0 - color);
            if (startPos > -1 && startPos < 24)
            {
                if (dest > 17 && opponentBoard[23 - startPos].Count == 1)
                {
                    return false;
                }
            }


            return true;

        }
        public virtual bool RollReset(bool reset)
        {
            return reset;
        }
    }

}
