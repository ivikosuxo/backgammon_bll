﻿using System.Collections.Generic;
using  EntityModels.Config;

using Newtonsoft.Json.Linq;
using BLL.Managers;
using  EntityModels.Types;
using System.Linq;
using  EntityModels.Exceptions;
using BLL.Classes.GamePlay;
using System;
using System.Timers;
using BLL.Classes.StateCalculator;
using System.Threading.Tasks;
using EntityModels.StateCalculator;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{

    public class Game
    {

        public int GameId
        {
            get; private set;
        }

        private object Locker { get; set; } = new object();
        public List<GamePlayer> Players { get; set; } = new List<GamePlayer>();
        public Round RoundInstance
        {
            get; private set;
        }
        public RoomConfig Config
        {
            get; private set;
        }
        public Room GameRoom
        {
            get; set;
        }



        public int RoundWinner { get; set; } = 0;
        public GamePlayer Raiser { get; set; } = null;
        public long StartDate
        {
            get; set;
        }
        public bool CrawfordActive = false;
        public int RaiseCount { get; set; } = 1;

        public int WinType { get; set; } = WinTypes.Normal;

        public Timer TurnTimer { get; set; } = new Timer();
        public Timer ReserverTimer { get; set; } = new Timer();
        public Timer RaiseTimer { get; set; } = new Timer();
        public Timer TurnChangeTimer { get; set; } = new Timer();

        public PlayModel MaxPlayCount { get; set; } = new PlayModel(false, -1);
        public decimal CurrentBet => GetCurrentBet();

        public bool Raising
        {
            get; set;
        } = false;
        public Game(RoomConfig cfg, List<RoomPlayer> pls, Room room)
        {
            GameRoom = room;
            pls.ForEach(e =>
            {
                var pl = new GamePlayer(e.GetPlayerId(), e.Info);
                pl.Device = e.Device;
                pl.ConnectionIP = e.ConnectedIp;
                Players.Add(pl);
            }
        );
            Players[0].Color = 1;
            Players[1].Color = -1;
            Config = cfg;

            TurnTimer.Elapsed += TimerOut;
            ReserverTimer.Elapsed += ReserveTimeOut;
            RaiseTimer.Elapsed += RaiseTimeOut;
            TurnChangeTimer.Elapsed += ChangeTimeOut;
            CreateGame();
        }

        public void CreateGame()
        {
            lock (Locker)
            {

                GameId = BaseApplication.Current.BaseGameRepository.CreateGame(GameRoom.RoomId);
                RegisterPlayers();
                SetOpponents();
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                Players.ForEach(e => gameSocketManager.SendMessage(e.PlayerId, Config.RoomId, CommandKeys.game_initialize, e.Opponent.ToJson()));
                StartGame(1000);
            }
        }

        public GamePlayer GetPlayerById(int playerId)
        {
            return Players.First(e => e.PlayerId == playerId);
        }


        public void StartGame(double interval)
        {
            var aTimer = new Timer(interval);
            aTimer.Elapsed += new ElapsedEventHandler((object source, ElapsedEventArgs e) =>
            {
                CreateRound();
            });

            // Only raise the event the first time Interval elapses.
            aTimer.AutoReset = false;
            aTimer.Enabled = true;

        }

        public void DisconnectPlayer(int playerId, bool connected)
        {

            var pl = GetPlayerById(playerId);
            pl.IsConnected = false;
            var data = new JObject();
            data["connect"] = connected;
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(pl.Opponent.PlayerId, GameRoom.RoomId, CommandKeys.disconnect_opponent, data);
        }
        public void CreateRound()
        {
            RoundInstance = new Round(this, Players, Config.Type);
            Raiser = null;
            RaiseCount = 1;

            Raising = false;
            var pl = Players[0];
            pl.RaiseTurn = true;
            pl.Opponent.RaiseTurn = true;
            var roundPlayer = RoundInstance.RoundPlayers[0];
            if (RoundWinner > 0)
            {
                roundPlayer = RoundInstance.GetPlayerById(RoundWinner);
            }

            var activePl = roundPlayer;

            pl.CanRaise = CanRaise(pl.PlayerId);
            pl.Opponent.CanRaise = CanRaise(pl.Opponent.PlayerId);

            var dice = Dice.GetFirstDice();

            int numb1 = dice.First;
            int numb2 = dice.Second;

            if ((!RoundInstance.Rule.RollReset(RoundWinner == 0)) || numb1 > numb2)
            {
                roundPlayer.Turn = true;
            }
            else if (numb1 < numb2)
            {
                roundPlayer.Opponent.Turn = true;
                activePl = roundPlayer.Opponent;
            }

            if (RoundInstance.Rule.RollReset(RoundWinner == 0))
            {

                SavePlayerRoll(activePl, dice, true);
            }

            var dc = dice;

            if (!RoundInstance.Rule.FirstRollDicePlay())
            {
                dc = RoundInstance.Rule.RollDice(activePl, Config.WithDouble);
            }






            int number1 = dc.First;
            int number2 = dc.Second;

            activePl.MovesList.Add(dc.First);
            activePl.MovesList.Add(dc.Second);

            if (dc.IsDouble)
            {
                activePl.MovesList.Add(dc.First);
                activePl.MovesList.Add(dc.First);
            }

            activePl.dice = dc;
            activePl.MovesList.Sort();

            var sfs = new JObject()
            {
                ["game_id"] = GameId,
                ["round_id"] = RoundInstance.RoundId,
                ["first_game"] = RoundInstance.Rule.RollReset(RoundWinner == 0),
                ["turn"] = activePl.Turn,
                ["dice1"] = numb1,
                ["dice2"] = numb2,
                ["r1"] = number1,
                ["r2"] = number2,
                ["can_raise"] = pl.CanRaise,
                ["raise"] = RaiseCount
            };
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

            gameSocketManager.SendMessage(roundPlayer.PlayerId, Config.RoomId, CommandKeys.start_bg, sfs);



            sfs["turn"] = !activePl.Turn;
            sfs["dice1"] = numb2;
            sfs["dice2"] = numb1;
            sfs["r1"] = number1;
            sfs["r2"] = number2;

            gameSocketManager.SendMessage(roundPlayer.Opponent.PlayerId, Config.RoomId, CommandKeys.start_bg, sfs);

            if (RoundInstance.Rule.RollReset(RoundWinner == 0))
            {
                StartChangeTimer();
            }
            else
            {
                StartTimer(activePl, RoundWinner == 0);
            }


        }


        public void Roll(int playerId)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            lock (Locker)
            {

                var roundPlayer = RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);


                if (roundPlayer.Turn)
                {
                    var data = roundPlayer.dice.ToJson();

                    roundPlayer.Roll = true;
                    RoundInstance.Rule.UnblockBoard();

                    RoundInstance.Rule.PlayerRoll(roundPlayer.Color, roundPlayer.dice.getFreezeCounter());

                    SavePlayerRoll(roundPlayer, roundPlayer.dice, false);

                    var jsn = new JObject()
                    {
                        ["can_play"] = roundPlayer.CanPlay,
                        ["can_play_1"] = true,
                        ["first"] = roundPlayer.dice.First,
                        ["second"] = roundPlayer.dice.Second,

                    };

                    MaxPlayCount = RoundInstance.Rule.CheckMaxPlayCount(roundPlayer);
                    roundPlayer.CanPlay = MaxPlayCount.PlayCount > 0;

                    if (!roundPlayer.CanPlay)
                    {

                        CancelTimer();
                        roundPlayer.Turn = false;
                        roundPlayer.Submit = false;
                        var opponent = roundPlayer.Opponent;
                        var activepl = opponent;
                        if (CheckBlockedState(opponent))
                        {
                            activepl = roundPlayer;

                        }

                        opponent.MovesList.Clear();
                        roundPlayer.MovesList.Clear();

                        var dice = RoundInstance.Rule.RollDice(activepl, Config.WithDouble);
                        activepl.MovesList.Add(dice.First);
                        activepl.MovesList.Add(dice.Second);
                        if (dice.IsDouble)
                        {
                            activepl.MovesList.Add(dice.First);
                            activepl.MovesList.Add(dice.First);
                        }

                        activepl.MovesList.Sort();
                        activepl.Roll = false;
                        activepl.dice = dice;

                        jsn["r1"] = dice.First;
                        jsn["r2"] = dice.Second;
                        activepl.Submit = false;
                        bool canPlay = RoundInstance.Rule.canPlaySomething(activepl, -1, false);
                        activepl.CanPlay = canPlay;
                        jsn["can_play_1"] = canPlay;
                        activepl.Turn = true;

                        StartChangeTimer();
                        gameSocketManager.SendMessage(roundPlayer.PlayerId, GameRoom.RoomId, "roll_you", jsn);
                    }
                    else
                    {

                        jsn["can_discard_kill"] = MaxPlayCount.KillDiscard;
                    }

                    gameSocketManager.SendMessage(roundPlayer.Opponent.PlayerId, GameRoom.RoomId, "roll", jsn);
                    gameSocketManager.SendMessage(roundPlayer.PlayerId, GameRoom.RoomId, "roll_you", jsn);
                    if (GameRoom.TournamentId == 0 && Config.MinBet >= GlobalConfig.AchievementMinBet)
                    {
                        var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                        achievementManager.CalculateAchievement(playerId, GameRoom.RoomId, AchievementEvents.Roll, data);
                    }

                }
            }
        }


        public void Submit(int playerId)
        {
            lock (Locker)
            {
                var gu = RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);
                if (gu.Turn && GameRoom.State == (RoomState.Playing) && gu.Submit)
                {
                    var go = gu.Opponent;
                    //save state
                    SaveMoves(gu.UndoList, gu, gu.dice);

                    var result = false;
                    var killing = 0;

                    gu.UndoList.ForEach(e =>
                    {
                        result = (result || e.IsRoyalDefense);
                        if (e.Killer)
                        {
                            killing++;
                        }
                    });
                    gu.UndoList.Clear();


                    CancelTimer();
                    gu.Turn = false;

                    var blocked = false;
                    var activePl = go;
                    if (CheckBlockedState(activePl))
                    {
                        activePl = gu;
                        blocked = true;
                    }

                    RoundInstance.Rule.UnblockBoard();

                    var dice = RoundInstance.Rule.RollDice(activePl, Config.WithDouble);
                    gu.MovesList.Clear();
                    go.MovesList.Clear();
                    activePl.MovesList.Add(dice.First);
                    activePl.MovesList.Add(dice.Second);
                    if (dice.IsDouble)
                    {
                        activePl.MovesList.Add(dice.First);
                        activePl.MovesList.Add(dice.First);
                    }

                    activePl.MovesList.Sort();

                    activePl.Roll = false;
                    activePl.dice = dice;
                    var sfs = new JObject()
                    {
                        ["win"] = false,
                        ["r1"] = dice.First,
                        ["r2"] = dice.Second,
                        ["blocked"] = blocked

                    };
                    bool canP = RoundInstance.Rule.canPlaySomething(activePl, -1, false);
                    activePl.CanPlay = canP;
                    sfs["can_play"] = canP;
                    activePl.Turn = true;

                    var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

                    gameSocketManager.SendMessage(gu.PlayerId, GameRoom.RoomId, CommandKeys.submit_you, sfs);
                    gameSocketManager.SendMessage(go.Opponent.PlayerId, GameRoom.RoomId, CommandKeys.submit_enemy, sfs);


                    var data = RoundInstance.SubmitState(gu.PlayerId, false);
                    gameSocketManager.SendMessage(gu.PlayerId, GameRoom.RoomId, CommandKeys.incorrect_move, data);

                    var data1 = RoundInstance.SubmitState(go.PlayerId, false);
                    gameSocketManager.SendMessage(go.PlayerId, GameRoom.RoomId, CommandKeys.incorrect_move, data1);

                    StartTimer(activePl, false);


                    data = new JObject()
                    {
                        ["royal"] = result,
                        ["killer"] = killing != 0,
                        ["count"] = killing
                    };
                    if (GameRoom.TournamentId == 0 && Config.MinBet >= GlobalConfig.AchievementMinBet)
                    {
                        var achievementManager = BaseApplication.Current.AchievementManagerInstance;

                        achievementManager.CalculateAchievement(playerId, GameRoom.RoomId, AchievementEvents.MoveSubmit, data);
                    }


                }
            }
        }

        public bool CheckBlockedState(RoundPlayer pl)
        {


            if (pl.KilledCount > 0)
            {
                var board = RoundInstance.Rule.GetBoardByColor(pl.Opponent.Color);
                for (int i = 18; i < 24; i++)
                {
                    if (board[i].Count < 2)
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {

                return false;

            }



        }


        public void SaveMoves(List<IMove> list, RoundPlayer roundPlayer, Dice dice)
        {
            BaseApplication.Current.BaseGameRepository.SaveGameState(list, roundPlayer, dice);
        }


        public void Undo(int playerId)
        {
            lock (Locker)
            {
                var pl = RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);
                if (pl.UndoList.Count > 0)
                {
                    int last = pl.UndoList.Count - 1;
                    var move = pl.UndoList[last];


                    pl.UndoList.Remove(move);

                    RoundInstance.Rule.PlayUndo(pl, move);
                    pl.MovesList.Sort();
                    pl.Submit = false;
                    var undoJsn = RoundInstance.SubmitState(pl.PlayerId, true); // = move.ToJson();
                    undoJsn["you"] = true;
                    undoJsn["undo_sound"] = true;


                    var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

                    gameSocketManager.SendMessage(pl.PlayerId, GameRoom.RoomId, CommandKeys.incorrect_move, undoJsn);

                    undoJsn = RoundInstance.SubmitState(pl.Opponent.PlayerId, true);
                    undoJsn["you"] = false;
                    undoJsn["undo_sound"] = true;
                    gameSocketManager.SendMessage(pl.Opponent.PlayerId, GameRoom.RoomId, CommandKeys.incorrect_move, undoJsn);


                }

            }
        }

        public void RegisterPlayers()
        {
            Players.ForEach(pl => pl.RegisterGamePlayer(GameId, pl.Color));
        }

        public void SetOpponents()
        {
            var pl = Players[0];
            var pl1 = Players[1];
            pl.Opponent = pl1;
            pl1.Opponent = pl;
        }


        #region Game Timers
        public void StartTimer(RoundPlayer pl, bool isDelay)
        {

            lock (Locker)
            {
                if (GameRoom.State == RoomState.Playing)
                {

                    StartDate = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    TurnTimer.Interval = (pl.TurnTimeSize + 2) * 1000;
                    TurnTimer.Enabled = false;
                    TurnTimer.Start();
                    var gp = GetPlayerById(pl.PlayerId);

                    var timerData = new JObject()
                    {
                        ["player_id"] = pl.PlayerId,
                        ["timer_size"] = GetTurnTimerSize(),
                        ["left_size"] = pl.TurnTimeSize,
                        ["r1"] = pl.dice.First,
                        ["r2"] = pl.dice.Second,
                        ["can_discard_kill"] = MaxPlayCount.KillDiscard,
                        ["freeze"] = pl.dice.getFreezeCounter(),
                        ["moves"] = pl.getMoves(),
                        ["can_raise"] = gp.CanRaise
                    };

                    GameRoom.SendMessageAll(CommandKeys.turn_timer, timerData);
                }
            }
        }

        public void StartRaiseTimer(GamePlayer pl)
        {
            lock (Locker)
            {
                RaiseTimer.Interval = TimerConfig.RaiseTimerSize * 1000;
                TurnTimer.Enabled = false;

                pl.Opponent.RaiseTurn = false;
                pl.RaiseTurn = true;

                RaiseTimer.Start();


                var raise = new JObject();
                raise["beaver"] = RoundInstance.Rule.CanRaiseX2(this);
                raise["raise"] = RaiseCount;
                raise["you"] = false;
                raise["raise_timer"] = TimerConfig.RaiseTimerSize;
                GameRoom.SendMessage(pl.PlayerId, CommandKeys.raise, raise);
                raise["you"] = true;
                GameRoom.SendMessage(pl.Opponent.PlayerId, CommandKeys.raise, raise);

            }

        }

        public void RaisePress(int plId, bool raisex4)
        {


            lock (Locker)
            {

                var gamePlayer = Players.First(e => e.PlayerId == plId);
                var player = RoundInstance.RoundPlayers.First(e => e.PlayerId == plId);
                if (GameRoom.State == RoomState.Playing && ((player.Turn && !player.Roll) || raisex4) && gamePlayer.RaiseTurn && Config.DoubleRule)
                {

                    if (CanRaise(plId))
                    {
                        Raising = true;
                        Raiser = gamePlayer;
                        gamePlayer.Opponent.RaiseTurn = true;
                        gamePlayer.RaiseTurn = false;

                        gamePlayer.CanRaise = false;
                        gamePlayer.Opponent.CanRaise = CanRaise(gamePlayer.Opponent.PlayerId);
                        StartRaiseTimer(gamePlayer.Opponent);


                    }

                }
                else
                {
                    if (player.Turn && !player.Roll)
                    {
                        throw new CantRaiseException();
                    }


                }
            }
        }

        public void RaiseAccept(int plId, JObject jsn)
        {
            lock (Locker)
            {
                if (GameRoom.State != (RoomState.Playing))
                {
                    return;
                }
                Raising = false;
                var gamePlayer = Players.Find(e => e.PlayerId == plId);
                var RoundPlayer = RoundInstance.RoundPlayers.First(e => e.PlayerId == plId);
                CancelRaiseTimer();
                int raiseAns = jsn["answer"].Value<int>();
                if (raiseAns == 0)
                {
                    WinType = WinTypes.RaiseDecline;
                    GiveUp(RoundPlayer, false, false);
                    return;
                }
                else if (raiseAns == 1)
                {
                    var raise = new JObject();

                    if (GameRoom.State == RoomState.Playing && gamePlayer.CanRaise && Config.DoubleRule && RoundInstance.Rule.CanRaise(this))
                    {

                        RaiseCount = RoundInstance.Rule.IncreaseRaise(RaiseCount, Config.RaiseCount);
                        raise["point_raise"] = false;
                        raise["raise_count"] = RaiseCount;
                        raise["current_bet"] = GetCurrentBet();

                        gamePlayer.CanRaise = RoundInstance.Rule.CanRaise(this);


                        raise["can_raise"] = gamePlayer.Opponent.CanRaise && RoundInstance.Rule.CanRaise(this);
                        raise["you"] = false;
                        GameRoom.SendMessage(gamePlayer.Opponent.PlayerId, CommandKeys.raise_answer, raise);

                        raise["can_raise"] = gamePlayer.CanRaise && RoundInstance.Rule.CanRaise(this);
                        raise["you"] = true;
                        GameRoom.SendMessage(gamePlayer.PlayerId, CommandKeys.raise_answer, raise);

                        var player = RoundInstance.RoundPlayers.First(e => e.Turn);
                        StartTimer(player, false);

                    }
                    else
                    {
                        if (RoundPlayer.Turn && !RoundPlayer.Roll)
                        {
                            throw new CantRaiseException();
                        }
                        else
                        {
                            throw new GameException();
                        }


                    }

                }
                else if (raiseAns == 2)
                {

                    if (RoundInstance.Rule.CanRaise(this))
                    {
                        RaiseCount = RoundInstance.Rule.IncreaseRaise(RaiseCount, Config.RaiseCount);
                    }


                    RaisePress(gamePlayer.PlayerId, true);

                }
            }

        }

        public bool CanRaise(int playerId)
        {
            return RoundInstance.Rule.CanRaise(this) && (Raiser == null || Raiser.PlayerId != playerId);
        }

        public decimal GetCurrentBet()
        {
            var matchType = Config.MatchType();
            if (RaiseCount == 1 || matchType < 2)
            {
                return Config.MinBet;
            }
            return RaiseCount * Config.MinBet;
        }
        public void StartReserveTimer()
        {
            lock (Locker)
            {
                if (GameRoom.State == RoomState.Playing)
                {
                    var pl = RoundInstance.GetActivePlayer();

                    ReserverTimer.Interval = (pl.ReserverTimeSize + TimerConfig.DeltaSeconds) * 1000;
                    ReserverTimer.Enabled = false;
                    ReserverTimer.Start();

                    var timerData = new JObject()
                    {
                        ["player_id"] = pl.PlayerId,
                        ["timer_size"] = GetReserveTimerSize(),
                        ["left_size"] = pl.ReserverTimeSize,
                    };

                    GameRoom.SendMessageAll(CommandKeys.reserve_timer, timerData);
                }
            }
        }

        public void StartChangeTimer()
        {
            TurnChangeTimer.Interval = (TimerConfig.TimerChangeDelay) * 1000;
            TurnChangeTimer.Enabled = false;
            TurnChangeTimer.Start();
        }

        public void ChangeTimeOut(Object source, ElapsedEventArgs e)
        {
            StopTimers();
            var pl = RoundInstance.GetActivePlayer();
            if (pl != null)
            {
                StartTimer(pl, false);
            }
        }

        public int GetReserveTimerSize()

        {

            return TimerConfig.ReserveSeconds + Config.Speed * TimerConfig.ReserveDeltaSeconds;
        }
        public int GetTurnTimerSize()
        {

            return TimerConfig.TurnSeconds + Config.Speed * TimerConfig.TurnDeltaSeconds;
        }

        public void CancelTimer()
        {
            lock (Locker)
            {
                StopTimers();
                CalculateDifference();
            }
        }

        public GamePlayer GetActiveRaisePlayer()
        {

            return Players.First(e => e.RaiseTurn);
        }

        public void StopTimers()
        {
            TurnTimer.Stop();
            ReserverTimer.Stop();
            ReserverTimer.Stop();
            TurnChangeTimer.Stop();
            RaiseTimer.Stop();
        }

        public void CalculateDifference()
        {
            var currentTime = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - StartDate;
            if (currentTime > 1000 * (GetTurnTimerSize() + TimerConfig.DeltaSeconds))
            {
                var pl = RoundInstance.GetActivePlayer();
                pl.ReserverTimeSize = pl.ReserverTimeSize - (int)(currentTime) / 1000 + GetTurnTimerSize() + TimerConfig.DeltaSeconds;
                if (pl.ReserverTimeSize < 0)
                {
                    EndGameWithTimeOut();
                }
            }
        }

        public void TimerOut(Object source, ElapsedEventArgs e)
        {
            StopTimers();
            StartReserveTimer();
        }

        public void ReserveTimeOut(Object source, ElapsedEventArgs e)
        {

            StopTimers();
            EndGameWithTimeOut();
        }

        public void CancelRaiseTimer()
        {
            StopTimers();

        }
        public void RaiseTimeOut(Object source, ElapsedEventArgs e)
        {
            lock (Locker)
            {
                try
                {

                    RaiseTimer.Stop();
                    WinType = WinTypes.RaiseTimeOut;
                    var pl = RoundInstance.GetPlayerById(Raiser.Opponent.PlayerId);

                    GiveUp(pl, false, false);
                }
                catch (Exception)
                {
                    RaiseTimer.Stop();
                }
            }
        }

        public void EndGameWithTimeOut()
        {
            lock (Locker)
            {
                StopTimers();
                var pl = RoundInstance.GetActivePlayer();
                RoundInstance.WinType = WinTypes.TimeOut;
                WinType = WinTypes.TimeOut;
                GiveUp(pl, true, true);
            }
        }

        #endregion


        public void GiveUp(RoundPlayer pl, bool fullGame, bool decline)
        {
            lock (Locker)
            {
                if (GameRoom.State == RoomState.Playing)
                {
                    var opponent = pl.Opponent;
                    StopTimers();
                    pl.Turn = false;
                    opponent.Turn = false;

                    var gp = Players.First(e => e.PlayerId == opponent.PlayerId);
                    if (fullGame)
                    {
                        gp.GamePoint = Config.MaxPoint;
                    }
                    RoundInstance.Rule.EndRound(opponent, decline, this);

                }
            }

        }


        public int IncreaseRaise(int raiseCount)
        {
            return RoundInstance.Rule.IncreaseRaise(raiseCount, Config.RaiseCount);
        }

        public void PlayMoves(int playerId, JObject data)
        {
            lock (Locker)
            {
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var pl = RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);
                if (!pl.Turn)
                {
                    throw new GameException();
                }

                var positions = data["end_positions"].Value<JArray>();
                var startPosition = data["start"].Value<int>();


                try
                {
                    var action = 0;
                    if (startPosition == -1)
                    {
                        action = 1;
                    }

                    for (int i = 0; i < positions.Count; i++)
                    {

                        var moveValue = positions[i].Value<int>() - startPosition;
                        var moveSize = pl.MovesList.IndexOf(moveValue);

                        PlaySeveralMoves(pl, action, startPosition, moveSize, startPosition + moveValue);
                        startPosition = startPosition + moveValue;
                        action = 0;
                    }

                    var lastPlay = !RoundInstance.Rule.canPlaySomething(pl, MaxPlayCount.PlayCount, true);
                    pl.Submit = lastPlay;

                    var arr = new JArray();
                    pl.UndoList.ForEach(e =>
                    {
                        if (e.Killer)
                        {
                            arr.Add(e.EndPosition);
                        }
                    });

                    var sfs = new JObject();
                    sfs["state"] = pl.PlayerState;
                    sfs["submit"] = pl.Submit;
                    sfs["blocked"] = pl.UndoList.Last().Blocked;
                    sfs["killer_arr"] = arr;
                    sfs["end_position"] = startPosition;
                    sfs["kill_en"] = pl.Opponent.KilledCount;
                    sfs["kill"] = pl.KilledCount;

                    gameSocketManager.SendMessage(pl.PlayerId, Config.RoomId, CommandKeys.play_several, sfs);

                    sfs["kill_en"] = pl.KilledCount;
                    sfs["kill"] = pl.Opponent.KilledCount;
                    gameSocketManager.SendMessage(pl.Opponent.PlayerId, Config.RoomId, CommandKeys.play_en_several, sfs);

                }
                catch (InvalidMoveException)
                {

                    var roomState = this.ToJson(playerId);

                    gameSocketManager.SendMessage(playerId, GameRoom.RoomId, CommandKeys.invalid_move, roomState);
                }
                catch (GameException)
                {
                    var jsn = RoundInstance.SubmitState(playerId, true);
                    jsn["you"] = true;
                    gameSocketManager.SendMessage(playerId, Config.RoomId, CommandKeys.incorrect_move, jsn);
                }
            }
        }
        public void PlaySeveralMoves(RoundPlayer pl, int action, int startPosition, int moveSize, int endPosition)
        {

            lock (Locker)
            {


                if (moveSize < 0)
                {
                    throw new InvalidMoveException();
                }

                if (!RoundInstance.ValidateMove(pl, action, moveSize, startPosition, endPosition, this))
                {
                    throw new InvalidMoveException();
                }


                var plState = pl.PlayerState;
                var opState = pl.Opponent.PlayerState;

                var colorPlayer = RoundInstance.GetPlayerByColor(1);
                var blocked = RoundInstance.Rule.IsBlocked(pl.Color, startPosition);
                var move = new Move()
                {
                    Killer = RoundInstance.Play(pl, action, moveSize, startPosition, endPosition),
                    Action = action,
                    StartPosition = startPosition,
                    EndPosition = endPosition,
                    State = plState,
                    OpponentState = opState,
                    RaiseX = RaiseCount,
                    Value = pl.MovesList[moveSize],
                    MyKill = colorPlayer.KilledCount,
                    MyDiscard = colorPlayer.DiscardCount,
                    OpKill = colorPlayer.Opponent.KilledCount,
                    OpDiscard = colorPlayer.Opponent.DiscardCount,
                    BoardState = RoundInstance.GetBoardStateString(),
                    IsRoyalDefense = CheckRoyalDefense(RoundInstance.Rule.GetBoardByColor(pl.Color)),
                };
                move.Blocked = blocked;
                pl.MovesList.RemoveAt(moveSize);
                if (RoundInstance.Rule.IsDiscardState(pl))
                {
                    pl.PlayerState = RoundPlayerState.DiscardState;
                }

                pl.UndoList.Add(move);

            }
        }


        public void Play(int playerId, JObject data)
        {
            lock (Locker)
            {
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var pl = RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);
                if (!pl.Turn)
                {
                    throw new GameException();
                }

                try
                {

                    int action = data["action"].Value<int>();
                    int moveSize = data["index"].Value<int>();
                    int startPosition = data["start"].Value<int>();
                    int endPosition = data["end"].Value<int>();
                    var mute = data["mute"].Value<bool>();
                    if (startPosition == -1)
                    {
                        action = 1;
                    }
                    if (action == 1 && startPosition != -1)
                    {
                        throw new InvalidMoveException();
                    }
                    if (action < 2)
                    {
                        var val = Math.Abs(startPosition - endPosition);
                        moveSize = pl.MovesList.IndexOf(val);
                        if (moveSize < 0)
                        {
                            throw new InvalidMoveException();
                        }
                    }

                     

                    if (!RoundInstance.ValidateMove(pl, action, moveSize, startPosition, endPosition, this))
                    {
                        throw new InvalidMoveException();
                    }
                      

                    var plState = pl.PlayerState;
                    var opState = pl.Opponent.PlayerState;

                    var colorPlayer = RoundInstance.GetPlayerByColor(1);
                    var blocked = RoundInstance.Rule.IsBlocked(pl.Color, startPosition);
                    var move = new Move()
                    {
                        Killer = RoundInstance.Play(pl, action, moveSize, startPosition, endPosition),
                        Action = action,
                        StartPosition = startPosition,
                        EndPosition = endPosition,
                        State = plState,
                        OpponentState = opState,
                        RaiseX = RaiseCount,
                        Value = pl.MovesList[moveSize],
                        MyKill = colorPlayer.KilledCount,
                        MyDiscard = colorPlayer.DiscardCount,
                        OpKill = colorPlayer.Opponent.KilledCount,
                        OpDiscard = colorPlayer.Opponent.DiscardCount,
                        BoardState = RoundInstance.GetBoardStateString(),
                        IsRoyalDefense = CheckRoyalDefense(RoundInstance.Rule.GetBoardByColor(pl.Color)),
                    };
                    move.Blocked = blocked;
                    pl.MovesList.RemoveAt(moveSize);
                    if (RoundInstance.Rule.IsDiscardState(pl))
                    {
                        pl.PlayerState = RoundPlayerState.DiscardState;
                    }

                    pl.UndoList.Add(move);


                    var sfs = new JObject()
                    {
                        ["turn"] = false,
                        ["action"] = action,
                        ["index"] = moveSize,
                        ["start"] = RoundInstance.Rule.GetCorrectPosition(startPosition),
                        ["end"] = RoundInstance.Rule.GetCorrectPosition(endPosition),
                        ["kill_en"] = pl.Opponent.KilledCount,
                        ["moves"] = pl.getMoves(),
                        ["kill_you"] = pl.KilledCount,
                        ["disc_you"] = pl.DiscardCount,
                        ["mute"] = mute
                    };


                    var list = new List<IMove>(pl.UndoList);
                    bool res = roundWinner(pl, sfs, move);




                    if (res)
                    {
                        WinType = WinTypes.Normal;
                        StopTimers();
                        SaveMoves(list, pl, pl.dice);

                        RoundInstance.Rule.EndRound(pl, true, this);
                    }
                    else
                    {
                        var lastPlay = !RoundInstance.Rule.canPlaySomething(pl, MaxPlayCount.PlayCount, true);


                        //finalResult= RoundInstance.Rule.getFinalMoves(pl, pl.dice, move.EndPosition, move.StartPosition, move.Action);

                        pl.Submit = lastPlay;
                        sfs["submit"] = pl.Submit;

                      

                        gameSocketManager.SendMessage(pl.Opponent.PlayerId, Config.RoomId, CommandKeys.play_en, sfs);

                        sfs["kill_en"] = pl.Opponent.KilledCount;
                        sfs["disc_en"] = pl.Opponent.DiscardCount;

                        gameSocketManager.SendMessage(pl.PlayerId, Config.RoomId, CommandKeys.play_you, sfs);
                    }



                }
                catch (InvalidMoveException)
                {

                    var roomState = this.ToJson(playerId);
          
                    gameSocketManager.SendMessage(playerId, GameRoom.RoomId, CommandKeys.invalid_move, roomState);
                }
                catch (GameException)
                {
                    var jsn = RoundInstance.SubmitState(playerId, true);
                    jsn["you"] = true;
                    gameSocketManager.SendMessage(playerId, Config.RoomId, CommandKeys.incorrect_move, jsn);
                }
                catch (Exception ex) {
                    ApplicationManager.Logger.Error($"play_move game_id={GameId} , player_id={playerId} , data={data}", ex);

                }
            }


        }

        public void SavePlayerRoll(RoundPlayer pl, Dice dice, bool first)
        {
            try
            {


                var plState = pl.PlayerState;
                var opState = pl.Opponent.PlayerState;
                var colorPlayer = RoundInstance.GetPlayerByColor(1);
                var move = new Move()
                {

                    Killer = false,
                    Action = 0,
                    StartPosition = 0,
                    EndPosition = 0,
                    State = plState,
                    OpponentState = opState,
                    RaiseX = RaiseCount,
                    Value = 0,
                    MyKill = colorPlayer.KilledCount,
                    MyDiscard = colorPlayer.DiscardCount,
                    OpKill = colorPlayer.Opponent.KilledCount,
                    OpDiscard = colorPlayer.Opponent.DiscardCount,
                    BoardState = RoundInstance.GetBoardStateString(),
                    IsRoyalDefense = false,
                    FirstRoll = first,
                    Roll = true
                };
                var list = new List<IMove>();
                list.Add(move);

                BaseApplication.Current.BaseGameRepository.SaveGameState(list, pl, dice);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SavePlayerRoll: game_id={GameId}", ex);
            }

        }
        public bool CheckRoyalDefense(List<BoardPlace> board)
        {
            var result = true;
            try
            {
                result = result && board[18].Count == 3;
                result = result && board[19].Count == 3;
                result = result && board[20].Count == 3;
                result = result && board[21].Count == 2;
                result = result && board[22].Count == 2;
                result = result && board[23].Count == 2;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        public bool roundWinner(RoundPlayer gu, JObject sfs, Move mv)
        {

            var pl = Players.First(e => e.PlayerId == gu.PlayerId);
            bool result = false;

            if (gu.DiscardCount == RoundInstance.Rule.StoneCount)
            {

                gu.Turn = false;
                gu.Opponent.Turn = false;
                sfs["turn"] = true;
                sfs["last"] = true;
                mv.Winner = true;
                //  this.room.saveMoves(gu);
                gu.UndoList.Clear();

                if (gu.RoundPoint > Config.MaxPoint)
                {
                    gu.RoundPoint = Config.MaxPoint;
                }

                sfs["win"] = true;
                result = true;

                sfs["point"] = pl.GamePoint;


            }

            return result;
        }


        public void CloseGame(GamePlayer pl)
        {
     
                try
                {
                    pl.IsWinner = true;
                    Players.ForEach(
                        gp =>
                        {
                            BaseApplication.Current.BaseGameRepository.SaveGamePlayer(gp);
                            if (CurrentBet>=GlobalConfig.AchievementMinBet)
                            {
                                gp.Info.GameCount++;
                            }
                        }
                        );
                BaseApplication.Current.BaseGameRepository.GameBet(Config.RoomId, GameId, pl.Info, CurrentBet, 1);
                     BaseApplication.Current.BaseGameRepository.GameBet(Config.RoomId, GameId, pl.Opponent.Info, CurrentBet, 2);
                BaseApplication.Current.BaseGameRepository.EndGame(GameId, pl.PlayerId, pl.Opponent.PlayerId, 2 * CurrentBet, WinType);  
                   
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"CloseGame game_id={GameId}", ex);

                }
                GameRoom.StartNextGameTimer();
                Destroy();
     
        }



        public void CancelGame()
        {
            StopTimers();
            WinType = WinTypes.CancelGame;
            BaseApplication.Current.BaseGameRepository.CancelGame(GameId);
            Destroy();
        }


        public void Destroy()
        {
            try
            {
                Players.ForEach(pl => pl.Destroy(Config.RoomId));
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"destroy game_id={GameId}", ex);
            }
        }
        public JObject ToJson(int playerId)
        {
            var pls = new JArray();
            Players.ForEach(e => pls.Add(e.ToJson()));

            var json = new JObject()
            {
                ["game_id"] = GameId,
                ["state"] = (int)GameRoom.State,
                ["raise_count"] = RaiseCount,
                ["can_discard_kill"] = MaxPlayCount.KillDiscard,
                ["players"] = pls,
                ["bet"] = GetCurrentBet(),
                ["time_passed"] = ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - StartDate) / 1000,
                ["full_reserve_size"] = GetReserveTimerSize(),
                ["full_timer_size"] = GetTurnTimerSize()
            };
            if (RoundInstance != null)
            {
                json["round"] = RoundInstance.ToJson(playerId);
            }

            return json;
        }




    }
}
