﻿using  EntityModels.Config;

using BLL.Managers;
using Newtonsoft.Json.Linq;
using System;

using BLL.Extensions;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class Player :IPlayer
    {

        public IPlayerInfo Info { get; set; }
        public long LastActiveDate { get; set; } = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        public decimal GetCurrentBalance()
        {
            Info.Balance = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetPlayerBalance(GetPlayerId());
            return Info.Balance;
        }
     
        public int GetPlayerId() {
           return Info.PlayerId;
       }

        public void Ping() {
            LastActiveDate = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond; 
        }

        public Player()
        {
            Info = new PlayerInfo();
        }

        public JObject ToJson()
        {
            var info = Info.GetInfo();
            info["balance"] = Info.GetCurrentBalance();   
            return info;
        }
        
    }
}
