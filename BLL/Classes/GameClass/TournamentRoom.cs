﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  EntityModels.Config;
using System.Timers;
using SuperWebSocket;
using Newtonsoft.Json.Linq;
using BLL.Managers;
using  EntityModels.Types;
using  EntityModels.Exceptions;
using BLL.Classes.Entities;
using BLL.Extensions;

using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class TournamentRoom : Room
    {
       
        public Timer  TournamentTimer { get; set; } = new Timer();
        public int TournamentLevel { get; set; } = 0;
        public bool IsFinal { get; set; } = false;
         
        public TournamentRoom(int roomId, RoomConfig conf,int tournamentId,int lvl) : base(roomId, conf)
        {
            TournamentId = tournamentId;
            TournamentLevel = lvl;
            StartTournamentTimer();
        }

        public void StartTournamentTimer() {
            this.State = RoomState.Playing;
            TournamentTimer = new Timer();
            TournamentTimer.Elapsed += EndTournamentTimer;
            TournamentTimer.Interval = TimerConfig.TournamentWaitTime*1000;
            TournamentTimer.Enabled = false;
            TournamentTimer.Start();

        }

        public void CancelTournamentTimer()
        {
            TournamentTimer.Stop();


        }
        public void  EndTournamentTimer(Object source, ElapsedEventArgs e)
        {
            State = RoomState.Playing;
            TournamentTimer.Stop();            
            SendOpponentInfo();         
            StartGame();
        }

        public override void EndGame(int winnerId) {
            try
            {
                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
                 
                var tournament = tournamentManager.GetTournamentById(TournamentId);
                tournament.TournamentGameWin(winnerId);
            }
            catch (Exception ex)
            {

                ApplicationManager.Logger.Error($" TournamentGameWin: tournament_id={TournamentId} ,player_id={winnerId}", ex);
                throw;
            }

        }
        public override void AddUser(IPlayerInfo user, string session, JObject jdata)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            if (RoomPlayers.Exists(e => e.GetPlayerId() == user.PlayerId))
            {
                var rp = RoomPlayers.First(e => e.GetPlayerId() == user.PlayerId);
                rp.SetDevice(session, jdata);
                if(RoomPlayers.Count==2)
                {
                   // var rp2=   RoomPlayers.First(e => e.GetPlayerId() != rp.GetPlayerId());
                // rp2.CheckIpRestriction(rp);
                }
                Reconnect(user, session);
 
                session.SetTournamentId(TournamentId);

                playerManager.PlayerJoin(user);
                gameSocketManager.SendMessage(session, CommandKeys.join_room, new JObject());
                return;
            }


            RoomPlayers.Add(new RoomPlayer() { Info = user, Connection = session, RoomId = RoomId });
            if (session!=null)
            {
                session.SetUserId( user.PlayerId);
                session.SetRoomId(RoomId);
                session.SetConnectionType(ConnectionType.Tournament);
            }


            gameSocketManager.Connections.Add(new GameSocket() { Active = true, Connection = session, ConnType = (int)ConnectionType.Game,TournamentId=TournamentId, PlayerId = user.PlayerId, RoomId = RoomId });
            gameSocketManager.SendMessage(session, CommandKeys.join_room, new JObject());
            playerManager.PlayerJoin(user);
 
        }
        public override JObject GetRoomInfo()
        {
            var info = new JObject
            {
                ["room_id"] = RoomId,
                ["tournament_id"]=TournamentId,
                ["is_final"]=IsFinal,
                ["room_name"] = Config.Roomname,
                ["player_count"] = RoomPlayers.Count,
                ["min_bet"] = Config.MinBet,
                ["max_bet"] = Config.RaiseCount * Config.MinBet,
                ["points"] = Config.MaxPoint,
                ["speed"] = Config.Speed,
                ["type"] = Config.Type,
                ["protected"] = Config.IsProtected,
                ["raise_count"] = Config.RaiseCount,
                ["beaver"] = Config.BeaverRule,
                ["double"] = Config.DoubleRule,
                ["crawford"] = Config.CrawfordRule,
                ["jacob"] = Config.JacobRule,
                ["chat_enabled"] = Config.ChatEnabled
            };
            return info;

        }

        public override void StartNextGameTimer()
        {
            lock (locker)
            {
                RoomPlayers.ForEach(e => e.IsAgree = false);
                NextGameTimer.Interval = (TimerConfig.TurnDeltaSeconds) * 1000;
                NextGameTimer.Enabled = false;
                NextGameTimer.Start();
            }

        }
        public   void Disconnect1(int playerId)
        {
            lock (locker)
            { 
                try
                {  
                     var player = RoomPlayers.First(e => e.GetPlayerId() == playerId);
                     LogPlayerSatusChange(playerId, false); 

                }
                catch (Exception ex)
                {

                    ApplicationManager.Logger.Error($"Disconnect1: tournament_id={TournamentId} ,player_id={playerId} ", ex);
                    throw;
                }
            }
        }

    }
}
