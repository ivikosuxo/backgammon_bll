﻿using BLL.Classes.GamePlay;
using BLL.Classes.StateCalculator;
using  EntityModels.Exceptions;
using BLL.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.StateCalculator;

namespace BLL.Classes.GameClass
{
   public class HyperRule :EuropianRule
    {
        public HyperRule()
        {
            StoneCount = 3;
            Initialize();
        }

        public override void Initialize() {


            FirstBoard = new List<BoardPlace>();
            SecondBoard = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place1 = new BoardPlace();
                FirstBoard.Add(place1);

                place1 = new BoardPlace();
                SecondBoard.Add(place1);
            }


            FirstBoard[0].Count = 1;
            FirstBoard[1].Count = 1;
            FirstBoard[2].Count = 1;
 

            SecondBoard[0].Count = 1;
            SecondBoard[1].Count = 1;
            SecondBoard[2].Count = 1;
 
        }

        public override PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {

            var MaxPlayCount = new PlayModel(false, -1);

            try
            {
                var board = new BoardStateCalculatorEuropean(null,GetBoardByColor(roundPlayer.Color), GetBoardByColor(0 - roundPlayer.Color), roundPlayer.MovesList, roundPlayer.dice.GetMaxPoint(), 0);
                board.discardCount = roundPlayer.DiscardCount;
                board.killedCount = roundPlayer.KilledCount;
                board.playerstate = roundPlayer.PlayerState;
                board.Calculate();


                MaxPlayCount = new PlayModel(false, board.GetMaxPoint());
            }
            catch (MaxPlayedException ex)
            {
                MaxPlayCount = ex.Model;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"CheckMaxPlayCount HyperRule, round_player_id={roundPlayer.PlayerId}", ex);
         
                MaxPlayCount = new PlayModel(false, -1);
            } 

            return MaxPlayCount;
        }
    }
}
