﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Classes.GameClass;
using BLL.Classes.GamePlay;
using Newtonsoft.Json.Linq;
using BLL.Interfaces;
using BLL.Managers;
using  EntityModels.Types;
using  EntityModels.Config;
using InterfaceLibrary.Enums;

namespace BLL.Classes
{
    public class Round
    {

#region Round start
        public int GameId{ get; private set; }
        public int RoundId { get; set; }
        public List<RoundPlayer> RoundPlayers { get;  set; } = new List<RoundPlayer>();
        public RoundState State { get; set; } = RoundState.Playing;
        public int WinType { get; set; } = WinTypes.Normal;
        public IGameRule Rule { get; set; }
        public Round(Game game ,List<GamePlayer> players,int gameType)
        {
            Rule =  Factory.GetGame(gameType);
            GameId = game.GameId;
            RoundId = BaseApplication.Current.BaseGameRepository.NewRound(GameId);
        
          
            players.ForEach(p => RoundPlayers.Add(new RoundPlayer(p.PlayerId)));
            RoundPlayers[0].Opponent = RoundPlayers[1];
            RoundPlayers[1].Opponent = RoundPlayers[0];

            RoundPlayers[0].Color = game.GetPlayerById(RoundPlayers[0].PlayerId).Color;
            RoundPlayers[1].Color = game.GetPlayerById(RoundPlayers[1].PlayerId).Color;
             
           
            RoundPlayers.ForEach(rp => {
                    rp.ReserverTimeSize = game.GetReserveTimerSize();
                    rp.TurnTimeSize = game.GetTurnTimerSize();
                
             
            });
            RegisterRoundPlayers();
        }

        public void RegisterRoundPlayers()
        {
            RoundPlayers.ForEach(pl => pl.RegisterRoundPlayer(RoundId));
        }
        public RoundPlayer GetPlayerByColor(int color)
        {
            return RoundPlayers.First(pl => pl.Color==color);
        }
        #endregion

        #region game play


        public bool Play(RoundPlayer pl ,int action, int move , int startPost, int endPos )
        {
             
             return Rule.Play(pl, action,  startPost, endPos, move);
        }

        public bool ValidateMove(RoundPlayer pl, int action, int move, int startPost, int endPos,Game game)
        {
         
            return Rule.ValidateMove(pl,action,move,startPost,endPos,game);
        }


        public RoundPlayer GetActivePlayer() {

           return RoundPlayers.First(e => e.Turn);

        }
        public RoundPlayer GetPlayerById(int id)
        {

            return RoundPlayers.First(e => e.PlayerId==id);

        }






        #endregion






        //end round and round info
        #region round destroy

        public void CloseRound(int winnerId)
        {
            State = RoundState.EndRound;
            BaseApplication.Current.BaseGameRepository.EndRound(RoundId, winnerId,WinType );
            Destroy();
        }

        public void Destroy()
        {
            try
            {
                RoundPlayers.ForEach(e => e.Destroy());
            }
            catch (Exception)
            {
                throw;
            }

        }

         
        public JObject ToJson(int playerId)
        {
            var pls = new JArray();
            RoundPlayers.ForEach(e => pls.Add(e.ToJson()));
            var pl = RoundPlayers.First(e => e.PlayerId == playerId);
            var jsn = new JObject() {
                ["game_id"] = GameId,
                ["round_id"] = RoundId,
                ["players"] = pls,
                ["board_state"] = GetBoardState(pl.Color)
            };

            return jsn;
        }

      
        public JObject SubmitState(int playerId,bool showUndo) {
            var pl = RoundPlayers.First(e => e.PlayerId == playerId);
            var arr = new JArray();
            if (showUndo)
            {
                arr = pl.GetUndoList();
            }
            
            var jsn = new JObject()
            {
                ["undo"] = arr,
                ["kills"] = pl.KilledCount,
                ["op_kills"] = pl.Opponent.KilledCount,
                ["discards"] = pl.DiscardCount,
                ["op_discards"] = pl.Opponent.DiscardCount,
                ["board_state"] = GetBoardState(pl.Color),
                ["moves_list"] = pl.getMoves(),
                ["state"]=pl.PlayerState,
                ["roll"] = pl.Roll

            };
             

            return jsn;

        }



        public JArray GetBoardState(int color)
        {
            var arr = new JArray();
            Rule.GetBoard(color).ForEach(e => arr.Add(e.ToJson()));         
            return arr;
        }

        public string GetBoardStateString()
        {
            var arr ="";
            Rule.GetBoard(1).ForEach(e => arr+=e.SaveState());
            return arr;
        }

#endregion 
    }
}
