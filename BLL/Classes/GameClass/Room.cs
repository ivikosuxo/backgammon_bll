﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Classes.Entities;
using  EntityModels.Config;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using  EntityModels.Exceptions;
using BLL.Extensions;
using System.Timers;
using EntityModels.Entities;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class Room :IRoom
    {
        protected object locker;

        public int RoomId { get; set; }



        public RoomConfig Config { get; set; }

        public List<RoomPlayer> RoomPlayers { get; set; }

        public RoomState State { get; set; }

        public RoomPlayer WaitingPlayer { get; set; } = null;
        public Game GammonGame { get; private set; }
        public int TournamentId { get; set; } = 0;
        public Timer NextGameTimer { get; set; } = new Timer();
        public Timer LifeTimer { get; set; } = new Timer();

        public string GetRoomName() {

            return Config.Roomname;
        }


        public bool Visible
        {
            get; set;
        } = false;

        public bool IsReconnect(int playerId) {

            return RoomPlayers.Exists(e => e.GetPlayerId() == playerId);
        }

        public bool IsGameState(bool tournamentGames) {
            if (tournamentGames)
            {
                return TournamentId >0 && RoomPlayers.Count == 2;
            }
            return TournamentId == 0 && RoomPlayers.Count == 2;
        }

        public Room(int roomId, RoomConfig conf)
        {
            locker = new object();
            State = RoomState.Waiting;
            RoomId = roomId;
            Config = conf;
            conf.RoomId = roomId;
            RoomPlayers = new List<RoomPlayer>();
            CalculateRaiseCount();

            NextGameTimer.Elapsed += TimeOut;
            LifeTimer.Elapsed += LifeTimeOut;
        }

        public void AddRoomPlayer(IPlayerInfo info,string connection , int roomId)
        {
            RoomPlayers.Add(new RoomPlayer { Info = info, Connection = connection, RoomId = roomId });
        }

        public void RaisePress(int playerId, bool accept) {
            GammonGame.RaisePress(playerId, accept); 
        }
        public void RaiseAccept(int playerId, JObject data)
        {
            GammonGame.RaiseAccept(playerId, data);
        }

        public int GetGameId()
        {
            if (GammonGame!=null)
            {
                return GammonGame.GameId;
            };
            return 0;
        }

        public void CalculateRaiseCount()
        {
            Config.RaiseCount = Config.MaxPoint;
            if (Config.MaxBet > Config.MinBet)
            {
                Config.RaiseCount = (int)(Config.MaxBet / Config.MinBet);
            }
        }

        public void PlayAgain(int playerId)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            lock (locker)
            {
                try
                {
                    var player = RoomPlayers.First(e => e.GetPlayerId() == playerId);
                    roomManager.RoomDbManager.BlockBalance(player.Info, this.RoomId,Config.MinBet, Config.MaxBet);
                    player.IsAgree = true;



                    player.Info.GetCurrentBalance();
                    roomManager.LobbyRoom.UpdateBalance(player.Info.PlayerId, player.Info.Balance);

                }
                catch (GameException ex)
                {
                    SendMessage(playerId, CommandKeys.error, ex.ToJson());
                }
            }
        }

        public virtual void EndGame(int winnerId)
        {
            lock (locker)
            {
                var rp = RoomPlayers.First(e => e.GetPlayerId() == winnerId);

                if (TournamentId == 0 && Config.MinBet >= GlobalConfig.AchievementMinBet)
                {

                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                    };
                    achievementManager.CalculateAchievement(rp.GetPlayerId(), RoomId, AchievementEvents.EndGame, achievementJson);
                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                    };
                    achievementManager.CalculateAchievement(rp.Opponent.GetPlayerId(), RoomId, AchievementEvents.EndGame, achievementJson1);
                }
            }
        }
        public virtual void StartNextGameTimer()
        {
            lock (locker)
            {
                RoomPlayers.ForEach(e => e.IsAgree = false);
                NextGameTimer.Interval = TimerConfig.NextGameSeconds * 1000;
                NextGameTimer.Enabled = false;
                NextGameTimer.Start();

                var data = new JObject()
                {
                    ["timer_size"] = TimerConfig.NextGameSeconds
                };

                SendMessageAll(CommandKeys.next_game, data);
            }

        }

        public void StartLifeTimer()
        {
            lock (locker)
            {
                LifeTimer.Interval = TimerConfig.LifeTimeSeconds * 1000;
                LifeTimer.Enabled = false;
                LifeTimer.Start();
            }

        }
        public void LifeTimeOut(Object source, ElapsedEventArgs e)
        {
            lock (locker)
            {
                LifeTimer.Stop();
                State = RoomState.Destroy;
                Destroy();

            }
        }

        public void TimeOut(Object source, ElapsedEventArgs e)
        {



            try
            {
                lock (locker)
                {
                    NextGameTimer.Stop();
                    var result = true;
                    RoomPlayers.ForEach(pl => result = result && pl.IsAgree);
                    if (result && BlockCreateRoomModel.CanCreateRoom)
                    {
                        StartGame();
                    }
                    else
                    {

                        var data = new JObject()
                        {
                            ["id"] = ErrorCodes.OpponentDisagree,
                            ["fatal"] = true
                        };
                        SendMessageAll(CommandKeys.error, data);
                        this.Destroy();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"nextGameTimerOut Exception room_id={RoomId}", ex);
                var data = new JObject()
                {
                    ["id"] = ErrorCodes.OpponentDisagree,
                    ["fatal"] = true
                };
                SendMessageAll(CommandKeys.error, data);
                this.Destroy();

            }



        }



        public bool Reconnect(IPlayerInfo pl, string session)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            lock (locker)
            {
                var gameSocket = gameSocketManager.Connections.First(e => e.RoomId == RoomId && e.PlayerId == pl.PlayerId);
                var wsc = gameSocket.Connection;
                gameSocket.Connection = session;
                wsc.CloseConnection(ErrorCodes.LobbySingleInstanse);
                gameSocketManager.Connections.RemoveAll(e => e.RoomId == RoomId && e.PlayerId == pl.PlayerId);
                gameSocketManager.Connections.Add(gameSocket);
                gameSocket.Active = true;
                session.SetRoomId(RoomId);
                session.SetConnectionType(ConnectionType.Game);
                LogPlayerSatusChange(pl.PlayerId, true);
                var rp = RoomPlayers.First(e => e.GetPlayerId() == pl.PlayerId);
                rp.Connection = session;

                //send game info
                if (GammonGame != null)
                {
                    GammonGame.GetPlayerById(pl.PlayerId).IsConnected = true;
                    var info = GammonGame.ToJson(pl.PlayerId);
                    gameSocketManager.SendMessage(session, CommandKeys.reconnect, info);
                    UpdateGamePlayerDevice(rp);
                }

                return true;
            }

        }
        public virtual void UpdateGamePlayerDevice(RoomPlayer pl)
        {
            try
            {
                if (GammonGame != null)
                {
                    var gamePlayer = GammonGame.GetPlayerById(pl.GetPlayerId());
                    gamePlayer.Device = pl.Device;
                    gamePlayer.ConnectionIP = pl.ConnectedIp;
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"UpdateGamePlayerDevice: player_id={pl.GetPlayerId()}", ex);
            }

        }

        public virtual bool IsSameNetwork(int merchantId)
        {

            var merchantManager = BaseApplication.Current.PlayerManagerInstance;

            return merchantManager.IsSameNetwork(merchantId, Config.MerchantId, Config.IsNetwork);
        }
        public virtual void AddUserWaitingList(IPlayerInfo user, string session, JObject jdata)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                lock (locker)
                {
                    if (State == RoomState.Waiting && RoomPlayers.Count == 1 && WaitingPlayer == null)
                    {
                        var balance = user.GetCurrentBalance();
                        if (balance < Config.MaxBet)
                        {
                            throw new NotEnoughBalanceException();
                        }

                        var roomPlayer = new RoomPlayer();
                        roomPlayer.Info = user;
                        roomPlayer.Connection = session;
                        WaitingPlayer = roomPlayer;
                        session.SetConnectionType(ConnectionType.Waiting);
                        session.SetRoomId(RoomId);
                        gameSocketManager.SendMessage(session, CommandKeys.waiter, new JObject());
                        SendMessage(RoomPlayers[0].GetPlayerId(), CommandKeys.waiting_add, user.GetInfo());
                    }
                    else
                    {

                        throw new RoomIsFullException();
                    }
                }
            }
            catch (GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());
            }
            catch (Exception)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, new JObject() { ["fatal"] = true, ["id"] = ErrorCodes.RoomIsFull });
            }

        }

        public virtual void DisconnectWaiter(int playerId)
        {
            lock (locker)
            {
                if (State == RoomState.Waiting && RoomPlayers.Count == 1 && WaitingPlayer != null)
                {
                    if (WaitingPlayer.GetPlayerId() == playerId)
                    {
                        WaitingPlayer = null;
                        SendMessage(RoomPlayers[0].GetPlayerId(), CommandKeys.waiting_remove, new JObject());
                    }

                }
            }

        }
        public virtual void AcceptOpponent(string session, JObject jdata)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                lock (locker)
                {
                    if (State == RoomState.Waiting && WaitingPlayer != null)
                    {
                        var opp = WaitingPlayer;
                        var data = new JObject();
                        data["password"] = "";
                        this.AddUser(opp.Info, opp.Connection, data);
                    }
                }
            }
            catch (GameException ex)
            {

                gameSocketManager.SendMessage(WaitingPlayer.Connection, CommandKeys.error, ex.ToJson());
                WaitingPlayer = null;
            }
            catch (Exception)
            {
                var data = new JObject()
                {
                    ["id"] = ErrorCodes.UnexpectedToken,
                    ["fatal"] = true
                };

                gameSocketManager.SendMessage(WaitingPlayer.Connection, CommandKeys.error, data);
                WaitingPlayer = null;

            }
        }

        public virtual void DeclineOpponent(string session, JObject jdata)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                lock (locker)
                {
                    if (State == RoomState.Waiting && WaitingPlayer != null)
                    {
                        var opp = WaitingPlayer;
                        WaitingPlayer = null;
                        var error = new JObject();
                        error["fatal"] = true;
                        error["id"] = ErrorCodes.OpponentDeclined;
                        gameSocketManager.SendMessage(opp.Connection, CommandKeys.error, error);

                    }
                }
            }
            catch (Exception)
            {
                WaitingPlayer = null;
            }
        }

        public virtual void AddUser(IPlayerInfo user, string session, JObject jdata)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            lock (locker)
            {
                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                if (RoomPlayers.Exists(e => e.GetPlayerId() == user.PlayerId))
                {
                    var rp = RoomPlayers.First(e => e.GetPlayerId() == user.PlayerId);
                    rp.SetDevice(session, jdata);
                    Reconnect(user, session);
                  
                    playerManager.PlayerJoin(user);

                    gameSocketManager.SendMessage(session, CommandKeys.join_room, new JObject());
                    if (this.Config.CreatorId == user.PlayerId)
                    {
                        var info = new JObject();
                        info["room_id"] = RoomId;
                        this.Visible = true;
                        gameSocketManager.SendMessageToAllLobby(CommandKeys.creator_join, info);
                        LifeTimer.Stop();
                    }
                    return;
                }
                var password = jdata["password"].Value<string>();
                if (Config.IsProtected && !password.Equals(Config.Password))
                {
                    throw new IncorrectPasswordException();
                }

                if (State != RoomState.Waiting)
                {
                    throw new RoomIsFullException();
                }


                roomManager.RoomDbManager.BlockBalance(user, this.RoomId, Config.MinBet, Config.MaxBet); 
                user.GetCurrentBalance();  
                roomManager.LobbyRoom.UpdateBalance(user.PlayerId, user.Balance);

                var roomPlayer = new RoomPlayer() { Info = user, Connection = session, RoomId = RoomId };
                roomPlayer.SetDevice(session, jdata);
                RoomPlayers.Add(roomPlayer);

                session.SetUserId(user.PlayerId);
                session.SetRoomId(RoomId);
                session.SetConnectionType(ConnectionType.Game);
                LifeTimer.Stop();

                gameSocketManager.Connections.Add(new GameSocket() { Active = true, Connection = session, ConnType = (int)ConnectionType.Game, PlayerId = user.PlayerId, RoomId = RoomId });
                gameSocketManager.SendMessage(session, CommandKeys.join_room, new JObject());

               
                playerManager.PlayerJoin(user);
                roomManager.LobbyRoom.SendMessage(user.PlayerId, CommandKeys.sit_on_room, new JObject() { ["room_id"] = RoomId });
                if (RoomPlayers.Count == Config.PlayersCount)
                {
                    State = RoomState.Playing;
                    SendOpponentInfo();
                    StartGame();
                }
                CountChange();
            }

        }


        public void SendOpponentInfo()
        {

            RoomPlayers[0].Opponent = RoomPlayers[1];
            RoomPlayers[1].Opponent = RoomPlayers[0];
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            RoomPlayers.ForEach(e => gameSocketManager.SendMessage(e.GetPlayerId(), RoomId, CommandKeys.opponent_info, e.Opponent.ToJson()));
        }


        public void Undo(int playerId, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                GammonGame.Undo(playerId);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"Undo: room_id={RoomId} , player_id={playerId}", ex);
                gameSocketManager.SendMessage(playerId, RoomId, "error", new JObject());
            }

        }
        public void Submit(int playerId, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                GammonGame.Submit(playerId);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"Submit: room_id={RoomId} , player_id={playerId}", ex);
                gameSocketManager.SendMessage(playerId, RoomId, CommandKeys.error, new JObject());
            }

        }

        public void GiveUp(int playerId, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                var fullGame = data["game"].Value<bool>();
                var decline = true;// data["decline"].Value<bool>();
                var pl = GammonGame.RoundInstance.RoundPlayers.First(e => e.PlayerId == playerId);
                if (fullGame)
                {
                    GammonGame.WinType = WinTypes.GiveUpGame;
                    GammonGame.RoundInstance.WinType = WinTypes.GiveUpGame;
                }
                else
                {
                    GammonGame.RoundInstance.WinType = WinTypes.GiveUpRound;
                    GammonGame.WinType = WinTypes.GiveUpRound;

                }

                GammonGame.GiveUp(pl, fullGame, decline);
            }
            catch (Exception ex)
            {

                ApplicationManager.Logger.Error($"GiveUp: room_id={RoomId} , player_id={playerId}", ex);

                gameSocketManager.SendMessage(playerId, RoomId, CommandKeys.error, new JObject());
            }

        }

        public void Play(int playerId, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                if (!GammonGame.Raising)
                {
                    GammonGame.Play(playerId, data);
                }

            }
            catch (InvalidMoveException)
            {

                var roomState = GammonGame.ToJson(playerId);

                gameSocketManager.SendMessage(playerId, RoomId, CommandKeys.invalid_move, roomState);
            }
            catch (Exception)
            {

                gameSocketManager.SendMessage(playerId, RoomId, "error", new JObject());
            }

        }

        public void PlaySeveralMoves(int playerId, JObject data)
        {
            lock (locker)
            {
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                try
                {
                    if (!GammonGame.Raising)
                    {
                        GammonGame.PlayMoves(playerId, data);
                    }

                }
                catch (InvalidMoveException)
                {
                    var roomState = GammonGame.ToJson(playerId);

                    gameSocketManager.SendMessage(playerId, RoomId, CommandKeys.invalid_move, roomState);
                }
                catch (Exception)
                {

                    gameSocketManager.SendMessage(playerId, RoomId, "error", new JObject());
                }
            }

        }

        public void Roll(int playerId)
        {
            try
            {


                if (this.State == RoomState.Playing && !GammonGame.Raising)
                {
                    GammonGame.Roll(playerId);
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"Roll: room_id={RoomId} , player_id={playerId}", ex);
                throw;
            }

        }
        public void StartGame()
        {
            GammonGame = new Game(Config, RoomPlayers, this);

        }


        public void SendMessage(int userId, string command, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(userId, RoomId, command, message);
        }

        public void SendMessageAll(string command, JObject message)
        {
            message[CommandKeys.command] = command;
            var socketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

            RoomPlayers.ForEach(e => socketManager.SendMessage(e.Connection, message.ToString()));
        }

        public void CountChange()
        {
            var data = new JObject()
            {
                ["room_id"] = RoomId,
                ["count"] = RoomPlayers.Count,
                ["size"] = Config.PlayersCount
            };
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessageToAllLobby(CommandKeys.count_change, data);
        }


        public bool RemoveUser(int userId)
        {
            lock (locker)
            {
                var pl = RoomPlayers.First(e => e.GetPlayerId() == userId);
                return RoomPlayers.Remove(pl);
            }
        }



        public void Disconnect(int playerId)
        {

            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {

                switch (State)
                {
                    case RoomState.Waiting:
                        State = RoomState.Destroy;
                        //Todo unblock money  
                        var amount= roomManager.RoomDbManager.UnblockAmount(playerId, RoomId);
                        roomManager.LobbyRoom.UpdateBalance(playerId, amount);

                        Destroy();
                        break;

                    case RoomState.Playing:
                        var player = RoomPlayers.First(e => e.GetPlayerId() == playerId);
                        LogPlayerSatusChange(playerId, false);
                        // Todo start disconnect Timer
                        break;

                    default:
                        State = RoomState.Destroy;
                        Destroy();
                        break;
                }

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"Disconnect: player_id={playerId} , room_id={RoomId.ToString()}", ex);
            }

        }

        public void LogPlayerSatusChange(int playerId, bool connected)
        {
            try
            {
                var gameId = 0;
                if (GammonGame != null)
                {
                    gameId = GammonGame.GameId;

                    GammonGame.DisconnectPlayer(playerId, connected);


                }

                BaseApplication.Current.BaseGameRepository.PlayerConnectionChanged(RoomId, gameId, playerId, connected);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"LogPlayerSatusChange: player_id={playerId} , room_id={RoomId}", ex);
            }
        }

        public void Destroy()
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {

                RoomPlayers.ForEach(e => e.Destroy());
                gameSocketManager.Connections.Where(gs => gs.RoomId == RoomId).ToList().ForEach(gs => gs.Destroy());
                var obj = new JObject { ["room_id"] = RoomId, ["tournament_id"] = TournamentId };
                gameSocketManager.SendMessageToAllLobby(CommandKeys.remove_room, obj);

                GammonGame?.Destroy();

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"Destroy: room_id={RoomId}", ex);
            }
            finally
            {
                roomManager.RoomDbManager.RoomDeactivate(RoomId);
                roomManager.RemoveRoom(RoomId);
            }
        }

        public JObject GetOpponentInfo(int playerId) {
            var pl = RoomPlayers.FirstOrDefault(e => e.GetPlayerId() != playerId);

            return pl.ToJson();
        }

        public virtual JObject GetRoomInfo()
        {
            var info = new JObject
            {
                ["visible"] = Visible,
                ["room_id"] = RoomId,
                ["room_name"] = Config.Roomname,
                ["player_count"] = RoomPlayers.Count,
                ["min_bet"] = Config.MinBet,
                ["max_bet"] = Config.MaxBet,
                ["points"] = Config.MaxPoint,
                ["speed"] = Config.Speed,
                ["type"] = Config.Type,
                ["protected"] = Config.IsProtected,
                ["raise_count"] = Config.RaiseCount,
                ["beaver"] = Config.BeaverRule,
                ["double"] = Config.DoubleRule,
                ["crawford"] = Config.CrawfordRule,
                ["jacob"] = Config.JacobRule,
                ["creator_id"] = Config.CreatorId,
                ["chat_enabled"] = Config.ChatEnabled,
                ["with_doubles"] = Config.WithDouble
            };
            return info;

        }


    }
}
