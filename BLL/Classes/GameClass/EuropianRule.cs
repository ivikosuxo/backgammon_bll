﻿using System;
using System.Collections.Generic;
using BLL.Classes.GamePlay;
using BLL.Interfaces;
using Newtonsoft.Json.Linq;
using  EntityModels.Types;
using  EntityModels.Exceptions;
using  EntityModels.Config;
using BLL.Classes.StateCalculator;
using System.Linq;
using BLL.Managers;
using EntityModels.StateCalculator;
using InterfaceLibrary.Enums;

namespace BLL.Classes.GameClass
{
    public class EuropianRule :  GeorgianRule
    { 
        public override int IncreaseRaise(int raiseCount, int maxRaiseCount)
        {
            CheckMoveValues = false;
            return raiseCount * 2 > maxRaiseCount ? maxRaiseCount : raiseCount * 2;
        }
        public EuropianRule()
        {
            Initialize();
        } 
        public override bool canMove(int startPos, int endPos, int color)
        {

            var opponentBoard = GetBoardByColor(0 - color);
            var myBoard = GetBoardByColor(color);
             
            if(endPos > 23 || endPos < 0 || startPos > 23 || startPos < 0)
            {

                throw new InvalidMoveException();
            }
            else
            {
                if(myBoard[startPos].Count == 0 || opponentBoard[23 - endPos].Count > 1)
                {
                    throw new InvalidMoveException();
                }
      

                return true;
            }
        }
        public override bool ValidateMove(RoundPlayer pl, int action, int move, int startPos, int endPos,Game game)
        {

            if (pl.PlayerState == RoundPlayerState.KilledState)
            {
                if (action != RoundPlayerState.KilledState || pl.KilledCount == 0 || endPos > 23 || endPos < 0 || (startPos < 24 && startPos > -1))
                {
                    throw new InvalidMoveException();
                }
                else
                {
                    return canRevive(pl.Color, endPos);
                }

            }

            if (pl.PlayerState == RoundPlayerState.MoveState)
            {


                return canMove(startPos, endPos, pl.Color);


            }
            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                if (action == RoundPlayerState.KilledState)
                {
                    throw new InvalidMoveException();
                }
                else
                {
                    if (action == RoundPlayerState.MoveState)
                    {

                        return canMove(startPos, endPos, pl.Color);
                    }
                    else
                    {
                        var brd = GetBoardByColor(pl.Color);
                        if (brd[startPos].Count == 0 )
                        {
                            throw new InvalidMoveException();
                        }

                        return canDiscard(pl);
                    }
                }

            }

            throw new InvalidMoveException();


        }
        public override MoveState canMove(int color, int startpos, List<int> moves)
        {
            var obj = new MoveState();

            obj.result = false;
            obj.index = -1;


            var opponentBoard = GetBoardByColor(0 - color);
            var playerBoard = GetBoardByColor(color);
            for(int i = 0; i < moves.Count; i++)
            {
                int pos = startpos + moves[i];

                if(pos > -1 && pos < 24)
                {
                    bool isBlocked = false;
             
                    if(!isBlocked && opponentBoard[23 - pos].Count < 2)
                    {
                        obj.result = true;
                        obj.index = i;
                        obj.pos = pos;
                    }
                }
            }
            return obj;

        }
        public override PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {
            var MaxPlayCount = new PlayModel(false, -1);
 
                try
                {
                    var board = new BoardStateCalculatorEuropean(null,GetBoardByColor(roundPlayer.Color), GetBoardByColor(0 - roundPlayer.Color), roundPlayer.MovesList,  roundPlayer.dice.GetMaxPoint(), 0);
                    board.discardCount = roundPlayer.DiscardCount;
                    board.killedCount = roundPlayer.KilledCount;
                    board.playerstate = roundPlayer.PlayerState;
                    board.Calculate(); 
                 
                    MaxPlayCount = new PlayModel(false, board.GetMaxPoint());
                }
                catch (MaxPlayedException ex)
                {
                    MaxPlayCount = ex.Model;
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error($"CheckMaxPlayCount EuropeanRule, round_player_id={roundPlayer.PlayerId}",ex);
                    MaxPlayCount = new PlayModel(false, -1);
                }

     

            return MaxPlayCount;
        }
        public override bool IsBlocked(int color, int startIndex)
        {
           return  false;

        }

        public override   bool getFinalMoves(RoundPlayer pl, Dice dice, int start, int end, int action)
        {
            return false;  
        }

        public override bool FirstRollDicePlay()
        { 
            return true;
        }


        public override bool canPlaySomething(RoundPlayer activePl, int maxPlay, bool check)
        {
              
      
            if (activePl.MovesList.Count == 0)
            {
                return false;
            }

            if (check)
            {
                return !(maxPlay == activePl.UndoList.Sum(e => e.Value));
            }


            int color = activePl.Color;
            int startingPosition = -1;

            if (activePl.PlayerState == RoundPlayerState.MoveState)
            {

                return canPlayMoveState(activePl);

            }
            else if (activePl.PlayerState == RoundPlayerState.KilledState)
            {

                var result = canMove(color, startingPosition, activePl.MovesList);
                return result.result;
            }
            else if (activePl.PlayerState == RoundPlayerState.DiscardState)
            {

                var candisc = canPlayDiscard(activePl);
                if (candisc.result)
                {

                    return true;
                }


                var res = canPlayMoveState(activePl);



                return res;

            }

            return false;
        }

        public override bool canDiscardOnBoard(int color,   int move, int startPos)
        {

            var playerBoard = GetBoardByColor(color);
         

            int dest = startPos + move;
            if(dest == 24)
            {
                return true;
            }
           

            return canPlayMoveOnBoard(color,  move, startPos);

        }

        public override bool canPlayMoveOnBoard(int color,  int move, int startPos)
        {
            int dest = startPos +   move;
            var playerBoard = GetBoardByColor(color);
            var opponentBoard = GetBoardByColor(0 - color);
            if (dest > -1 && dest < 24)
            {
                if (playerBoard[startPos].Count == 0)
                {
                    return false;
                }
         
                if( opponentBoard[23 - dest].Count < 2)
                {
                    return true;
                }
            }
            return false;

        }

        
        public override int IsMars(RoundPlayer pl, bool calc, RoomConfig cfg, int raiseCount)
        {

            if (!calc)
            {
                return 1;
            }

            if (cfg.JacobRule && raiseCount == 1 && cfg.MatchType() == 2) //europian 
            {
                return 1;
            }

            if (pl.DiscardCount > 0 )
            {
                return 1;
            }

            if (pl.Opponent.DiscardCount > 0)
            {
                var playerBoard = GetBoardByColor(pl.Color);

                if (pl.KilledCount>0)
                {
                    return 3;
                }

                for (int i = 0; i < 6; i++)
                { 
                    if (playerBoard[i].Count > 0)
                    { 
                        return 3;
                    }
                }
            }

            return 2; 

        }

        public override bool winRound(RoundPlayer rp, bool calcMars, Game game)
        {
            try
            {
                var cfg = game.Config;
                var gp = game.Players.First(e => e.PlayerId == rp.PlayerId);

                int marsRaise = IsMars(rp.Opponent, calcMars, game.Config, game.RaiseCount);
                int currPoint = game.RaiseCount * marsRaise;
                rp.RoundPoint = currPoint;
                gp.GamePoint += currPoint;



                if (gp.GamePoint > cfg.MaxPoint)
                {
                    gp.GamePoint = cfg.MaxPoint;
                }


                if (gp.GamePoint == cfg.MaxPoint - 1 && cfg.CrawfordRule)
                {
                    game.CrawfordActive = true;

                }
                else if (cfg.CrawfordRule)
                {
                    game.CrawfordActive = false;
                }


                if (game.GameRoom.TournamentId == 0 && cfg.MinBet >= GlobalConfig.AchievementMinBet)
                {
                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };

                    achievementManager.CalculateAchievement(rp.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson);

                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };


                    achievementManager.CalculateAchievement(rp.Opponent.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson1);
                }

                game.RoundInstance.CloseRound(rp.PlayerId);

                if (gp.GamePoint >= cfg.MaxPoint)
                {
                    game.RaiseCount =    game.RaiseCount * marsRaise> game.Config.RaiseCount ? game.Config.RaiseCount : game.RaiseCount * marsRaise;
                    game.CloseGame(gp);

                }
                return gp.GamePoint >= cfg.MaxPoint;

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"winRound: game_id={game.GameId} , player_id={rp.PlayerId}", ex);
            }
            return false;
        }

        public override bool CanRaiseX2(Game game)
        {
            if (game.Config.BeaverRule && game.RaiseCount*4 <=game.Config.RaiseCount)
            {
                return true;
            }

            return false;  
        }

        public override bool CanRaise(Game game)
        {
            var config = game.Config;
            if(config.MatchType() == 0 || !config.DoubleRule)
            {
                return false;
            }

            if(config.MatchType() == 1)
            {

                bool canRs;
                var pl = game.Players[0];
                var pl1 = pl.Opponent;
                canRs = (pl.GamePoint + game.RaiseCount) < config.MaxPoint || (pl1.GamePoint + game.RaiseCount) < config.MaxPoint;
                if(config.CrawfordRule)
                {
                    return (!game.CrawfordActive) && (canRs);
                }
                else
                {
                    return canRs;
                }
            }
            else
            {
                return (game.RaiseCount < game.Config.RaiseCount);
            }


        }

        public override bool canDiscardWithoutMatch(int color, int startPos, int value)
        {

            int index = getDiscardIndex(startPos);
            var myboard = GetBoardByColor(color);

       

            if(value == 6)
            {

                return true;
            }


            if(index > value)
            {

                return false;
            }

            if(index == value)
            {

                return true;
            }

            for(int j = value; j < 7; j++)
            {

                if(myboard[startPos - j].Count > 0)
                {

                    return false;
                }

            }



            return true;
        }
        public override bool RollReset(bool reset)
        {
            return true;
        }

    }
}
