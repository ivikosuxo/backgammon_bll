﻿using  EntityModels.Config;

using  EntityModels.Exceptions;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Managers;

using BLL.Extensions;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes
{
    public class RoomPlayer
    {
        public int RoomId { get; set; }
        public IPlayerInfo Info { get; set; }
        public string Connection { get; set; }

        public RoomPlayer Opponent {get;set;}
        public bool IsAgree { get; set; } = false;

        public DeviceType Device { get; set; } = DeviceType.Desktop;

        public string ConnectedIp { get; set; } = "";

        public int GetPlayerId() {
            return Info.PlayerId;
        }

        public decimal GetBalance()
        {
            return Info.Balance;
        }

        public decimal GetCurrentBalance()
        {
            return Info.GetCurrentBalance();
        }

        public JObject ToJson()
        {
            var data = new JObject() {
                ["username"] = Info.Username,
                ["avatar"] = Info.AvatarId,
                ["player_id"] = Info.PlayerId,
                 
            };
            return data;
        }

        public void Destroy()
        {
            Opponent = null;
            var roomManager = BaseApplication.Current.RoomManagerInstance; 
            roomManager.RoomDbManager.UnblockAmount(Info.PlayerId, RoomId);

            Info.GetCurrentBalance();
            roomManager.LobbyRoom.UpdateBalance(Info.PlayerId, Info.Balance);

        }

        public void SetDevice(string session, JObject jdata)
        {

            try
            {
                var obj = jdata["mobile"];
                if(obj!=null)
                {
                    Device = DeviceType.Mobile;
                }            
              //  ConnectedIp = session.RemoteEndPoint.ToString().Split(':')[0];     

            } catch(Exception ex) {
                ApplicationManager.Logger.Error($"SetDevice: session={session} , data={jdata.ToString()}", ex);
            } 
        
        }
        public void CheckIpRestriction(RoomPlayer op)
        {                        
            try
            {
                if(!op.ConnectedIp.Equals("")&& op.ConnectedIp.Equals(ConnectedIp))
                {
                    op.ConnectedIp = "";
                   throw new SameIPRestrinction();
                }
               
            }
            catch(Exception ex)
            {
                ApplicationManager.Logger.Error($"CheckIpRestriction: player_id={op.GetPlayerId()}", ex);
            }

        }
    }
}
