﻿using  EntityModels.Config;

using BLL.Managers;
using Newtonsoft.Json.Linq;

using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class GamePlayer : IGamePlayer
    {
        public int GameId { get; set; }

        public int PlayerId { get; set; }

        public int GamePoint { get; set; }

        public bool RaiseTurn { get; set; } = true;

        public bool CanRaise { get; set; }

        public IPlayerInfo Info {get;set;}

        public GamePlayer Opponent { get; set; }
        public int Color { get; set; } = 1;
        public bool IsWinner
        {
            get; set;
        } = false;

        public DeviceType Device
        {
            get; set;
        }

        public string ConnectionIP
        {
            get; set;
        }

        public bool IsConnected { get; set; } = true;
        public bool IsAgree { get; set; }

        public GamePlayer(int plId, IPlayerInfo cfg)
        {
            PlayerId = plId;
            GamePoint = 0;
            RaiseTurn = true;
            Info = cfg;
        }

        public void RegisterGamePlayer(int gameId, int color)
        {
            GameId = gameId;
            BaseApplication.Current.BaseGameRepository.RegisterGamePlayer(gameId, PlayerId, color);
        }

        public void Destroy(int roomId)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var amount = roomManager.RoomDbManager.UnblockAmount(PlayerId, roomId);
            roomManager.LobbyRoom.UpdateBalance(PlayerId, amount);

            Info = null;
        }
         
        public JObject ToJson()
        {
            var info = Info.GetInfo();       
            info["turn"] = RaiseTurn;
            info["point"] = GamePoint;
            info["can_raise"] = CanRaise;
            info["game_id"] = GameId;
            info["is_connected"] = IsConnected;
            
            return info;
        }


    }
}
