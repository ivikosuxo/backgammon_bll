﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Classes.GamePlay;
using BLL.Classes.StateCalculator;
using  EntityModels.Config;
using  EntityModels.Types;
using  EntityModels.Exceptions;
using BLL.Managers;
using Newtonsoft.Json.Linq;

using EntityModels.StateCalculator;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class EurekaRule : IGameRule
    {
        public int StoneCount { get; set; } = 15;
        public EurekaRule()
        {
            Initialize();
        }

        public List<BoardPlace> FirstBoard
        {
            get; set;
        }
        public List<BoardPlace> SecondBoard
        {
            get; set;
        }
        public bool CheckMoveValues { get; set; } = true;
        public virtual void Initialize()
        {

            FirstBoard = new List<BoardPlace>();
            SecondBoard = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place1 = new BoardPlace();
                FirstBoard.Add(place1);

                place1 = new BoardPlace();
                SecondBoard.Add(place1);
            }



            FirstBoard[23].Count = 3;
            FirstBoard[22].Count = 3;
            FirstBoard[21].Count = 3;
            FirstBoard[20].Count =2;
            FirstBoard[19].Count =2;
            FirstBoard[18].Count =2;

            SecondBoard[23].Count = 3;
            SecondBoard[22].Count = 3;
            SecondBoard[21].Count = 3;
            SecondBoard[20].Count = 2;
            SecondBoard[19].Count = 2;
            SecondBoard[18].Count = 2; 

        }


        public virtual bool IsBlocked(int color, int startIndex)
        {
             
            return false;

        }
        public void PlayerRoll(int color, int freezeCount)
        {


        }

        public virtual bool FirstRollDicePlay()
        {

            return false;
        }


        public virtual PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {
            var MaxPlayCount = new PlayModel(false, -1);
            roundPlayer.PlayerState = RoundPlayerState.DiscardState;
 
            try
            {
                var board = new EurekaBoardStateCalculator(null, GetBoardByColor(roundPlayer.Color), GetBoardByColor(0 - roundPlayer.Color), roundPlayer.MovesList, roundPlayer.dice.GetMaxPoint(), 0);
                board.discardCount = roundPlayer.DiscardCount;  
                board.Calculate(); 
            
                MaxPlayCount = new PlayModel(false, board.GetMaxPoint());
            }
            catch (MaxPlayedException ex)
            {
                MaxPlayCount = ex.Model;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"CheckMaxPlayCount GeorgianRule, round_player_id={roundPlayer.PlayerId}", ex);
                MaxPlayCount = new PlayModel(false, -1);
            }

            return MaxPlayCount;
        }
        public List<BoardPlace> GetBoard(int color)
        {
            var first = GetBoardByColor(color);
            var second = GetBoardByColor(0 - color);
            var result = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place = new BoardPlace()
                {
                    Color = 0,
                    Count = 0
                };
                if (first[i].Count > second[23 - i].Count)
                {
                    place.Count = first[i].Count;
                    place.Color = 1;
                
                }
                else if (first[i].Count < second[23 - i].Count)
                {
                    place.Count = second[23 - i].Count;
                    place.Color = 2;
                   
                }
                result.Add(place);
            }

            return result;
        }

        public bool EndRound(RoundPlayer gu, bool calcMars, Game game)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

            var go = gu.Opponent;
            var currentBet = GetCurrentBet(go, calcMars, game);
            bool result = winRound(gu, calcMars, game);


            go.MovesList.Clear();
            go.UndoList.Clear();


            gu.MovesList.Clear();
            gu.UndoList.Clear();

            var coins = 0m;
            var coins1 = 0m;
            if (result)
            {
                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                var roomManager = BaseApplication.Current.RoomManagerInstance;
                var gameRepository = BaseApplication.Current.BaseGameRepository;


                var player = playerManager.GetPlayerInfoById(gu.PlayerId);  
                coins = gameRepository.GameFinishCoins(player, game.GameId, currentBet, 1, game.GameRoom.RoomId); 

                var secondPlayer = playerManager.GetPlayerInfoById(go.PlayerId); 
                coins1 = gameRepository.GameFinishCoins(secondPlayer, game.GameId, currentBet, 2, game.GameRoom.RoomId);

                roomManager.LobbyRoom.UpdateCoins(player.PlayerId, player.Coin);
                roomManager.LobbyRoom.UpdateCoins(secondPlayer.PlayerId, secondPlayer.Coin); 

         
            }
            var giveUp = new JObject()
            {
                ["win"] = false,
                ["last"] = result,
                ["point"] = game.GetPlayerById(gu.PlayerId).GamePoint,
                ["money"] = currentBet,
                ["timer_size"] = TimerConfig.NextGameSeconds,
                ["coin_count"] = coins1
            };
            game.RoundWinner = gu.PlayerId;

            gameSocketManager.SendMessage(go.PlayerId, game.Config.RoomId, CommandKeys.end_game, giveUp);

            giveUp["coin_count"] = coins;
            giveUp["win"] = true;

            gameSocketManager.SendMessage(gu.PlayerId, game.Config.RoomId, CommandKeys.end_game, giveUp);

            if (!result)
            {
                game.StartGame(2000);
            }
            else
            {

                game.GameRoom.EndGame(gu.PlayerId);
            }

            return result;
        }

        public Dice RollDice(RoundPlayer pl, bool withDouble)
        {
            pl.PlayerState = RoundPlayerState.DiscardState;
            pl.Opponent.PlayerState = RoundPlayerState.DiscardState;

            var dice = new Dice();

            if (pl.dice!=null)
            {
                dice.First = (pl.dice.First + dice.First)%6+1;
                dice.Second = (pl.dice.Second + dice.Second) % 6 + 1;
                dice.IsDouble = dice.First == dice.Second;
            }

            if (withDouble)
            {
                return dice;
            }
            else
            {
                return Dice.GetFirstDice();
            }


        }
        public int GetCorrectPosition(int position)
        {

            return 23 - position;
        }
        public decimal GetCurrentBet(RoundPlayer pl, bool bl, Game game)
        {
            if (game.Config.MatchType() < 2)
            {
                return game.Config.MinBet;
            }
            else
            {
                decimal res = game.Config.MinBet * game.RaiseCount * IsMars(pl, bl, game.Config, game.RaiseCount);
                if (res > game.Config.MaxBet)
                {
                    return game.Config.MaxBet;
                }
                else
                {
                    return res;
                }

            }
        }

        public virtual bool winRound(RoundPlayer rp, bool calcMars, Game game)
        {
            try
            {
                var cfg = game.Config;
                var gp = game.Players.First(e => e.PlayerId == rp.PlayerId);

                int marsRaise = IsMars(rp.Opponent, calcMars, game.Config, game.RaiseCount);
                int currPoint = game.RaiseCount * marsRaise;
                rp.RoundPoint = currPoint;
                gp.GamePoint += currPoint;




                if (gp.GamePoint > cfg.MaxPoint)
                {
                    gp.GamePoint = cfg.MaxPoint;
                }



                if (game.GameRoom.TournamentId == 0 && cfg.MinBet >= GlobalConfig.AchievementMinBet)
                {
                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;

                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };

                    achievementManager.CalculateAchievement(rp.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson);
                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                        ["mars"] = (marsRaise == 2),
                        ["double_mars"] = (marsRaise == 3)
                    };

                    achievementManager.CalculateAchievement(rp.Opponent.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson1);
                }

                game.RoundInstance.CloseRound(rp.PlayerId);

                if (gp.GamePoint >= cfg.MaxPoint)
                {
                    game.RaiseCount = game.RaiseCount * marsRaise > game.Config.RaiseCount ? game.Config.RaiseCount : game.RaiseCount * marsRaise;

                    game.CloseGame(gp);

                }
                return gp.GamePoint >= cfg.MaxPoint;

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"winRound: game_id={game.GameId}", ex);
            }
            return false;
        }

        public virtual bool CanRaiseX2(Game game)
        {
            return false;
        }

        public virtual bool CanRaise(Game game)
        {
            var config = game.Config;
            if (config.MatchType() == 0 || !config.DoubleRule)
            {
                return false;
            }

            if (config.MatchType() == 1)
            {
                bool canRs;
                var pl = game.Players[0];
                var pl1 = pl.Opponent;
                canRs = (pl.GamePoint + game.RaiseCount) < config.MaxPoint || (pl1.GamePoint + game.RaiseCount) < config.MaxPoint;

                return (canRs);
            }
            else
            {
                return (game.RaiseCount < game.Config.RaiseCount);
            }

        }

        public virtual bool ValidateMove(RoundPlayer pl, int action, int move, int startPos, int endPos, Game game)
        {
 
            if (action != RoundPlayerState.DiscardState)
            {
                throw new InvalidMoveException();
            }

          
 
            if (pl.PlayerState == RoundPlayerState.DiscardState)
            { 
                        var brd = GetBoardByColor(pl.Color);
                        if (brd[startPos].Count == 0)
                        {
                            throw new InvalidMoveException();
                        } 

                        return canDiscard(pl);  
            }

            throw new InvalidMoveException();


        }


        public bool CanPlayDiscardState(RoundPlayer pl, bool blocked)
        {
            var result = false;
            for (int i = 18; i < 24; i++)
            {
                var dest = i + pl.MovesList[0];
                if (CanMoveBeforeDiscard(i, dest, pl.Color))
                {
                    return true;
                }

            }
            return result;
        }



        public int discardIndex(RoundPlayer activePl)
        {

            var brd = GetBoardByColor(activePl.Color);
            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[i];

                if (brd[posit].Count > 0)
                {

                    return posit;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    return i;
                }
            }


            return -1;
        }

        public List<BoardPlace> GetBoardByColor(int color)
        {
            if (color > 0)
            {
                return FirstBoard;
            }
            return SecondBoard;
        }

        public int getDiscardIndex(int val)
        {

            return 24 - val;

        }

        public virtual bool canDiscardWithoutMatch(int color, int startPos, int value)
        {
 
            return false;
        }


        public bool canDiscard(RoundPlayer pl)
        {
            return discardIndex(pl) > -1;
        }

        public bool canRevive(int color, int destination)
        {
            return false;
        }

        public virtual bool canMove(int startPos, int endPos, int color)
        { 
            return false;
        }
        public virtual bool CanMoveBeforeDiscard(int startPos, int endPos, int color)
        {
      
                return false;
         
        }


        public bool IsDiscardState(RoundPlayer pl)
        {
            var opponentBoard = GetBoardByColor(0 - pl.Color);
            var myBoard = GetBoardByColor(pl.Color);

            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                return true;
            }

            int count = 0;
            for (int i = 0; i < 6; i++)
            {
                count += myBoard[23 - i].Count;
            }

            return count + pl.DiscardCount == StoneCount;
        }

        public virtual int IncreaseRaise(int raiseCount, int maxRaiseCount)
        {
            return raiseCount + 1 > maxRaiseCount ? maxRaiseCount : raiseCount + 1;
        }

        public virtual bool canPlaySomething(RoundPlayer activePl, int maxPlay, bool check)
        {
            if (activePl.MovesList.Count == 0)
            {
                return false;
            }
 
           return !(maxPlay == activePl.UndoList.Sum(e => e.Value));
           

 
        }

        public MoveState canPlayDiscard(RoundPlayer activePl)
        {
            var res = new MoveState();


            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);


            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[(i)];
                if (myBoard[posit].Count > 0)
                {
                    res.result = true;
                    res.index = i;
                    return res;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    res.result = true;
                    res.index = i;
                    return res;
                }
            }

            res.result = false;
            res.index = -1;
            return res;
        }


        public bool canPlayMoveState(RoundPlayer activePl)
        { 
         
            return false;

        }

        public virtual MoveState canMove(int color, int startpos, List<int> moves)
        {
            var move =new MoveState();
            move.result = false;
            return move;

        }

        public void PlayUndo(RoundPlayer activePl, IMove move)
        {

            var opp = activePl.Opponent;
            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);

            int startPosition = move.StartPosition;
            int endPosition = move.EndPosition;
             

                activePl.DiscardCount--;
                myBoard[startPosition].Count++; 
 


            activePl.MovesList.Add(move.Value);
            activePl.MovesList.Sort();

        }
        public void UnblockBoard()
        {
  
        }
        public virtual bool getFinalMoves(RoundPlayer pl, Dice dice, int start, int end, int action)
        {

            return false;
        }


        public virtual int IsMars(RoundPlayer pl, bool calc, RoomConfig cfg, int raiseCount)
        {


            if (!calc)
            {
                return 1;
            }
             

            if (pl.DiscardCount > 0)
            {
                return 1;
            }


            if (pl.KilledCount > 0)
            {
                return 3;
            }
            return 2;


        }

        public bool Play(RoundPlayer activePl, int action, int startPos, int endPos, int moveIndex)
        { 
             
                return Discard(activePl, startPos, endPos, moveIndex);  
        }

        public bool Move(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        { 
            return false;
        }
        public bool Discard(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        {
            bool result = false;
            var board = GetBoardByColor(activePl.Color);

            board[startPos].Count -= 1;

            activePl.AddDiscard();


            return result;
        }


        public bool Revive(RoundPlayer activePl, int endPos, int moveIndex)
        {

            return false;
        }


        public virtual bool canDiscardOnBoard(int color, int move, int startPos)
        {

            var playerBoard = GetBoardByColor(color);
            if (startPos > -1 && startPos < 24)
            {
                return false;
            }

            int dest = startPos + move;
            if (dest == 24)
            {
                return true;
            }

            return canPlayMoveOnBoard(color, move, startPos);

        }

        public virtual bool canPlayMoveOnBoard(int color, int move, int startPos)
        {
            return  false;

        }
   
        public virtual bool RollReset(bool reset)
        {
            return reset;
        }
    }
}
