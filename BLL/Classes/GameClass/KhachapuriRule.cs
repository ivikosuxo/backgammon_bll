﻿using BLL.Classes.GamePlay;
using  EntityModels.Config;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfaceLibrary.Enums;

namespace BLL.Classes.GameClass
{
   public class KhachapuriRule : LongRule
    {


        public KhachapuriRule() :base(){
            
        }

        public override void UnblockBoard()
        {
            FirstBoard.ForEach(e => { e.IsBlocked = false; e.FreezeCount = 4; });
            SecondBoard.ForEach(e => { e.IsBlocked = false; e.FreezeCount = 4; }); 
        }

        public override void PlayerRoll(int color, int freezeCount)
        {
            var board = GetBoardByColor(color);
            board[0].FreezeCount = 4;

        }
        protected override void FreezeFirstStones(int color, Dice dice)
        {

            var board = GetBoardByColor(color);
            if (board[0].Count == StoneCount)
            {
                board[0].FreezeCount = 4;
            }
        }
        public override bool winRound(RoundPlayer rp, bool calcMars, Game game)
        {
            try
            {
                var cfg = game.Config;
                var gp = game.Players.First(e => e.PlayerId == rp.PlayerId);

                int marsRaise = IsMars(rp,calcMars,game.Config,1);
                int currPoint = game.RaiseCount * marsRaise * (15-rp.Opponent.DiscardCount);
                rp.RoundPoint = currPoint;
                gp.GamePoint += currPoint;


                if (gp.GamePoint > cfg.MaxPoint)
                {
                    gp.GamePoint = cfg.MaxPoint;
                }



                if (gp.GamePoint == cfg.MaxPoint - 1 && cfg.CrawfordRule)
                {
                    game.CrawfordActive = true;

                }
                else if (cfg.CrawfordRule)
                {
                    game.CrawfordActive = false;
                }



                if (game.GameRoom.TournamentId == 0 && cfg.MinBet >= GlobalConfig.AchievementMinBet)
                {
                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                        ["mars"] = false,
                        ["double_mars"] = false
                    };

                    achievementManager.CalculateAchievement(rp.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson);
                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                        ["mars"] = false,
                        ["double_mars"] = false
                    };
                    achievementManager.CalculateAchievement(rp.Opponent.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson1);
                }
                game.RoundInstance.CloseRound(rp.PlayerId);

                if (gp.GamePoint >= cfg.MaxPoint)
                {
                    game.CloseGame(gp);
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SaveGameState: game_id={game.GameId} , player_id={rp.PlayerId} ", ex);
            }
            return false;
        }


    }
}
