﻿using BLL.Classes.GamePlay;
using BLL.Managers;
using  EntityModels.Types;
using InterfaceLibrary.Game;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
 

namespace BLL.Classes
{
    public class RoundPlayer : IRoundPlayer
    {
        public int PlayerId { get; set; }
        public int RoundId { get; set; }

        public RoundPlayer Opponent { get; set; }

        public int TurnTimeSize { get; set; } = 15;
        public int ReserverTimeSize { get; set; } = 60;
        public bool Roll { get; set; }
        public bool Submit { get; set; }
        public int Color { get; set; } = 1;
        public int PlayerState { get; set; } = RoundPlayerState.MoveState;
        public int KilledCount { get; set; } = 0;

        public bool CanPlay { get; set; } = true;

        public int DiscardCount { get; set; } = 0;

        public Dice dice { get; set; }   
        
        public bool Turn { get; set; } 
        
        public int Discard { get; set; } 

        public int RoundPoint { get; set; }

        public List<int>  MovesList { get; set; } = new List<int>();
        public List<IMove> UndoList { get; set; } = new List<IMove>();

        public RoundPlayer(int playerId)
        {
            PlayerId = playerId;
            Discard = 0;
            RoundPoint = 0;
            TurnTimeSize = 15;
            ReserverTimeSize = 60;
        }

 
        public void Destroy()
        {
            BaseApplication.Current.BaseGameRepository.SaveRoundPlayer(this);
            Opponent = null;
        }


 

        public void AddDiscard()
        {
            DiscardCount++;
        }
        public bool ReviveStone()
        {
            if (KilledCount<=0)
            {
                KilledCount = 1;
            }
            KilledCount--;
            if (KilledCount == 0)
            {
                PlayerState = RoundPlayerState.MoveState;
                return true;
            }

            return false;

        }

        public bool KillStone()
        {
            KilledCount++;
            PlayerState = RoundPlayerState.KilledState;
            return true;
        }

        public void RegisterRoundPlayer(int roundId)
        {
            RoundId = roundId;
            BaseApplication.Current.BaseGameRepository.RegisterRoundPlayer(RoundId, PlayerId);

        }
        public JObject ToJson()
        {
            var info = new JObject()
            {
                ["player_id"]=PlayerId,
                ["roll"] = Roll,
                ["submit"] = Submit,             
                ["color"] = Color,
                ["state"] = PlayerState,
                ["kills"] = KilledCount,
                ["discards"] = DiscardCount,
                ["moves"] = getMoves(),
                ["undo_list"] = GetUndoList(),
                ["turn_timer_size"]=TurnTimeSize,
                ["reserve_timer_size"]=ReserverTimeSize,
                ["turn"]=Turn
            
            };

            if (dice!=null)
            {
                info["dice"] = dice.ToJson();
            }
            return info;
        }
        public JArray getMoves()
        {
            var  arr = new JArray();
            MovesList.Sort();
            MovesList.ForEach(e=> arr.Add(e));              
            
            return arr;
        }

        public JArray GetUndoList()
        {
            var arr = new JArray();
            UndoList.ForEach(e => arr.Add(e.ToJson()));

            return arr;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }



}

