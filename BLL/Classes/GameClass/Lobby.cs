﻿using System.Collections.Generic;
using System.Linq;
using BLL.Classes.Entities;
using BLL.Extensions;
using BLL.Interfaces;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;

using System.Timers;
using System;
using  EntityModels.Config;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;
using InterfaceLibrary.Factory;

namespace BLL.Classes.GameClass
{
    public class Lobby :  ILobby
    {
        public int RoomId { get; set; } = 0;
        public List<int> StatusPoints { get; set; } = new List<int>();
        public List<int> StatusGameCounts { get; set; } = new List<int>();

        public Timer PlayerCountTimer { get; set; } = new Timer();
        public Timer TournamentRemoveTimer { get; set; } = new Timer();

        public object locker = new object();

        public Lobby()
        {

            UpdateStatuses();

            PlayerCountTimer.Elapsed += TimeElapsed;
            PlayerCountTimer.Interval = TimerConfig.LobbyPlayerClearTimeMinutes * 1000 * 60;
            PlayerCountTimer.Enabled = false;
            PlayerCountTimer.Start();


            TournamentRemoveTimer.Elapsed += TournamentCleaner;
            TournamentRemoveTimer.Interval = TimerConfig.LobbyTournamentClearTimeMinutes * 1000 * 60 * 60;
            TournamentRemoveTimer.Enabled = false;
            TournamentRemoveTimer.Start();
        }


        public void UpdateStatuses()
        {

            try
            {
                BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetStatusPoints(this);

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("UpdateStatuses : lobby ", ex);
            }

        }
        public void TimeElapsed(Object source, ElapsedEventArgs e)
        {
            PlayerCountTimer.Stop();
            try
            {
                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                playerManager.ClearPlayers();

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("ClearPlayers ", ex);
            }
            finally
            {

                PlayerCountTimer.Start();
                CountChange();
            }

        }

        public void TournamentCleaner(Object source, ElapsedEventArgs e)
        {
            TournamentRemoveTimer.Stop();

            try
            {
                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
                tournamentManager.CleanTournament();
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error("TournamentCleaner: ", ex);
            }
            finally
            {
                TournamentRemoveTimer.Start();
            }

        }


        public bool AddUser(IPlayer user, string session, JObject jdata)
        {
            GameSocket gameSocket;
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerId = user.GetPlayerId();
            if (ContainsPlayer(playerId))
            {
                gameSocket = gameSocketManager.Connections.First(e => e.RoomId == 0 && e.PlayerId == playerId);
                var wsc = gameSocket.Connection;
                gameSocket.Connection = session;
                wsc.CloseConnection(ErrorCodes.LobbySingleInstanse);

            }
            else
            {
                gameSocket = new GameSocket()
                {
                    Active = true,
                    Connection = session,
                    ConnType = (int)ConnectionType.Lobby,
                    PlayerId = playerId,
                    RoomId = RoomId
                };
                session.SetConnectionType(ConnectionType.Lobby);
            }
            gameSocketManager.Connections.RemoveAll(e => e.RoomId == 0 && e.PlayerId == playerId);
            gameSocketManager.Connections.Add(gameSocket);

            var data = GetRoomInfo();
            data["rank"] = user.Info.Rank;
            gameSocketManager.SendMessage(session, CommandKeys.lobby_info, data);

            var createRoomModel = ApplicationManager.GetBlockedRoomMessage();
            gameSocketManager.SendMessage(session, CommandKeys.can_create_room, createRoomModel);

            return true;
        }

        public bool ContainsPlayer(int playerId)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            return gameSocketManager.Connections.Exists(rp => rp.PlayerId == playerId && rp.RoomId == 0);
        }


        public bool RemoveUser(int userId)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var gameSocket = gameSocketManager.Connections.First(e => e.RoomId == 0 && e.PlayerId == userId);
            gameSocket.Connection.CloseConnection(ErrorCodes.LobbySingleInstanse, ConnectionType.Close);

            gameSocketManager.Connections.RemoveAll(e => e.PlayerId == userId && e.RoomId == RoomId);

            return true;
        }




        public void SendMessageAll(string command, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessageToAllLobby(command, data);
        }

        public void CountChange()
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var data = new JObject
            {
                ["players_count"] = playerManager.PlayersCount(),
                ["rooms_count"] = roomManager.Rooms.Count, //Where(e=>e.Value.RoomPlayers.Count==2).ToList().Count,
                ["room_id"] = RoomId
            };
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessageToAllLobby(CommandKeys.lobby_data, data);
        }


        public JObject GetRoomInfo()
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var data = new JObject
            {
                ["players_count"] = playerManager.PlayersCount(),
                ["rooms_count"] = roomManager.Rooms.Count, //Where(e => e.Value.RoomPlayers.Count == 2).ToList().Count,
                ["rooms_info"] = roomManager.GetRoomsInfo(),
                ["status_points"] = StatusPoints.ToJArray(),
                ["status_game_count"] = StatusGameCounts.ToJArray()
            };

            return data;
        }

        public void ServerPlayersCountChange()
        {

            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;

            var data = new JObject
            {

                ["players_count"] = playerManager.PlayersCount(),

                ["rooms_count"] = roomManager.Rooms.Count
            };
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessageToAllLobby(CommandKeys.lobby_data, data);
        }

        public void Destroy()
        {

        }

        public void Disconnect(int playerId)
        {
            RemoveUser(playerId);
        }


        public void UpdateBalance(int playerId, decimal balance)
        {
          
            var data = new JObject()
            {
                ["balance"] = balance
            };

            SendMessage(playerId, CommandKeys.update_balance, data);
        }

        public void UpdateCoins(int playerId, decimal coin)
        {

            var data = new JObject()
            {
                ["coins"] = coin
            };

            SendMessage(playerId, CommandKeys.update_coin_count, data);
        }

        public void UpdateBalance(int playerId)
        {
            var balance = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetPlayerBalance(playerId);

            var data = new JObject()
            {
                ["balance"] = balance
            };

            SendMessage(playerId, CommandKeys.update_balance, data);
        }
        public void SendMessage(int userId, string command, JObject data)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var session = gameSocketManager.GetConnectionOnRoom(userId, RoomId);
            data[CommandKeys.command] = command;
            gameSocketManager.SendMessage(session, data.ToString());
        }
    }
}
