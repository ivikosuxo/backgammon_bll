﻿using System;
using System.Collections.Generic;
using BLL.Classes.GamePlay;
using BLL.Interfaces;
using Newtonsoft.Json.Linq;
using  EntityModels.Types;
using  EntityModels.Exceptions;
using BLL.Managers;
using System.Linq;
using  EntityModels.Config;

using BLL.Classes.StateCalculator;
using EntityModels.StateCalculator;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Game;

namespace BLL.Classes.GameClass
{
    public class LongRule : IGameRule
    {
        public int StoneCount { get; set; } = 15;
        public LongRule()
        {
            Initialize();
        }
        public List<BoardPlace> FirstBoard { get; set; }
        public List<BoardPlace> SecondBoard { get; set; }

        public void Initialize()
        {

            FirstBoard = new List<BoardPlace>();
            SecondBoard = new List<BoardPlace>();

            for (int i = 0; i < 24; i++)
            {
                var place = new BoardPlace();
                FirstBoard.Add(place);
                place = new BoardPlace();
                SecondBoard.Add(place);
            }
            FirstBoard[0].Count = 15;
            SecondBoard[0].Count = 15;

        }

        public Dice RollDice(RoundPlayer pl, bool withDouble)
        {
            var dice = new Dice();

            if (pl.dice != null)
            {
                dice.First = (pl.dice.First + dice.First) % 6 + 1;
                dice.Second = (pl.dice.Second + dice.Second) % 6 + 1;
                dice.IsDouble = dice.First == dice.Second;
            }

            if (withDouble)
            {
              
                FreezeFirstStones(pl.Color, dice);
                return dice;
            }
            else
            {
                return Dice.GetFirstDice();
            }
        }

        protected virtual void FreezeFirstStones(int color, Dice dice)
        {

            var board = GetBoardByColor(color);
            if (board[0].Count == StoneCount)
            {
                board[0].FreezeCount = dice.getFreezeCounter();
            }
            else
            {
                board[0].FreezeCount = 1;
            }
        }


        public virtual void UnblockBoard()
        {
            FirstBoard.ForEach(e => { e.IsBlocked = false; e.FreezeCount = 1; });
            SecondBoard.ForEach(e => { e.IsBlocked = false; e.FreezeCount = 1; });

        }

        public virtual int IsMars(RoundPlayer pl, bool calc, RoomConfig cfg, int raiseCount)
        {
            if (!calc)
            {
               return 1;
            }   

            if (pl.DiscardCount > 0)
            {
                return 1;
            }

            return 2;

        }

        public bool FirstRollDicePlay()
        {

            return false;
        }

        public virtual PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {
            var MaxPlayCount = new PlayModel(false, -1);

            try
            {
                var board = new LongBoardStateCalculator(null, GetBoardByColor(roundPlayer.Color), GetBoardByColor(0 - roundPlayer.Color), roundPlayer.MovesList, roundPlayer.dice.GetMaxPoint(), 0);
                board.discardCount = roundPlayer.DiscardCount;
                board.playerstate = roundPlayer.PlayerState;
                board.Calculate();

                MaxPlayCount = new PlayModel(false, board.GetMaxPoint());
            }
            catch (MaxPlayedException ex)
            {
                MaxPlayCount = ex.Model;
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"CheckMaxPlayCount LongRule, round_player_id={roundPlayer.PlayerId}", ex);
                MaxPlayCount = new PlayModel(false, -1);
            }
            return MaxPlayCount;
        }

        public bool canPlaySomething(RoundPlayer activePl, int maxValue, bool check)
        {


            if (SixRowIsBlocked(activePl.Color))
            {
                return true;
            }

            if (check)
            {
                return !(maxValue == activePl.UndoList.Sum(e => e.Value));
            }

            if (activePl.PlayerState == RoundPlayerState.MoveState)
            { 
                return canPlayMoveState(activePl); 
            }

            else if (activePl.PlayerState == RoundPlayerState.DiscardState)
            { 
                var candisc = canPlayDiscard(activePl);
                if (candisc.result)
                {
                    return true;
                }

                var res = canPlayMoveState(activePl);

                return res;
            }

            return false;
        }

        private bool SixRowIsBlocked(int color)
        {
            var opLockedGate = OpponentTookGate(1 - color);

            if (opLockedGate)
            {
                return false;
            }

            var board = GetBoardByColor(color);
            var count = 0;
            for (int i = 0; i < 12; i++)
            {
                if (board[i].Count > 0)
                {
                    count++;
                    if (count >= 6)
                    {
                        return true;
                    }

                }
                else
                {
                    count = 0;
                }
            }
            return count >= 6;
        }

        private bool OpponentTookGate(int color)
        {
            var board = GetBoardByColor(color);

            for (int i = 18; i < 24; i++)
            {
                if (board[i].Count > 0)
                {
                    return true;
                } 

            }
            return false;
        }

        public virtual bool IsBlocked(int color, int startIndex)
        {
            if (startIndex == 0)
            {
                var brd = GetBoardByColor(color);
                return brd[startIndex].IsBlocked;
            }
            return false;

        }
        public MoveState canPlayDiscard(RoundPlayer activePl)
        {
            var res = new MoveState();


            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);


            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[(i)];
                if (myBoard[posit].Count > 0)
                {
                    res.result = true;
                    res.index = i;
                    return res;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    res.result = true;
                    res.index = i;
                    return res;
                }
            }

            res.result = false;
            res.index = -1;
            return res;
        }

        public bool Revive(RoundPlayer activePl, int endPos, int moveIndex)
        {


            return false;
        }

        public int getDiscardIndex(int val)
        {

            return 24 - val;

        }
        public bool canDiscardWithoutMatch(int color, int startPos, int value)
        {

            int index = getDiscardIndex(startPos);
            var myboard = GetBoardByColor(color);



            if (value == 6)
            {

                return true;
            }


            if (index > value)
            {

                return false;
            }

            if (index == value)
            {

                return true;
            }

            for (int j = value; j < 7; j++)
            {
                if (myboard[startPos - j].Count > 0)
                {
                    return false;
                }

            }



            return true;
        }

        public void PlayUndo(RoundPlayer activePl, IMove move)
        {

            var opp = activePl.Opponent;
            var myBoard = GetBoardByColor(activePl.Color);
            var opponentBoard = GetBoardByColor(0 - activePl.Color);

            int startPosition = move.StartPosition;
            int endPosition = move.EndPosition;

            if (move.Action == 0)
            {

                myBoard[endPosition].Count--;
                myBoard[startPosition].Count++;

                if (startPosition == 0)
                {
                    myBoard[0].FreezeCount++;
                    myBoard[0].IsBlocked = false;
                }

            }

            else if (move.Action == 2)
            {

                activePl.DiscardCount--;
                myBoard[startPosition].Count++;
            }



            opp.PlayerState = move.OpponentState;
            activePl.PlayerState = move.State;


            activePl.MovesList.Add(move.Value);
            activePl.MovesList.Sort();

        }


        public bool getFinalMoves(RoundPlayer pl, Dice dice, int start, int end, int action)
        {

            return false;
  

        }

        public bool canDiscardOnBoard(int color, int move, int startPos)
        {

            var playerBoard = GetBoardByColor(color);
            if (startPos > -1 && startPos < 24 && playerBoard[startPos].IsBlocked)
            {
                return false;
            }

            int dest = startPos + move;
            if (dest == 24)
            {
                return true;
            }

            return canPlayMoveOnBoard(color, move, startPos);

        }
        public bool canPlayMoveOnBoard(int color, int move, int startPos)
        {
            int dest = startPos + move;
            var playerBoard = GetBoardByColor(color);
            var opponentBoard = GetBoardByColor(0 - color);
            if (startPos > -1 && startPos < 24 && playerBoard[startPos].IsBlocked)
            {
                return false;
            }

            if (dest > -1 && dest < 24 && opponentBoard[(12 + dest) % 24].Count == 0)
            {
                return true;
            }
            return false;

        }
        public virtual bool winRound(RoundPlayer rp, bool calcMars, Game game)
        {
            try
            {
                var cfg = game.Config;
                var gp = game.Players.First(e => e.PlayerId == rp.PlayerId);

                int marsRaise = IsMars(rp,calcMars,game.Config,1);
                int currPoint = game.RaiseCount * marsRaise;
                rp.RoundPoint = currPoint;
                gp.GamePoint += currPoint; 


                if (gp.GamePoint > cfg.MaxPoint)
                {
                    gp.GamePoint = cfg.MaxPoint;
                } 


                if (gp.GamePoint == cfg.MaxPoint - 1 && cfg.CrawfordRule)
                {
                    game.CrawfordActive = true;

                }
                else if (cfg.CrawfordRule)
                {
                    game.CrawfordActive = false;
                }



                if (game.GameRoom.TournamentId == 0 && cfg.MinBet >= GlobalConfig.AchievementMinBet)
                {
                    var achievementManager = BaseApplication.Current.AchievementManagerInstance;
                    var achievementJson = new JObject()
                    {
                        ["win"] = true,
                        ["mars"] = false,
                        ["double_mars"] = false
                    };

                    achievementManager.CalculateAchievement(rp.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson);
                    var achievementJson1 = new JObject()
                    {
                        ["win"] = false,
                        ["mars"] = false,
                        ["double_mars"] = false
                    };
                    achievementManager.CalculateAchievement(rp.Opponent.PlayerId, game.GameRoom.RoomId, AchievementEvents.EndRound, achievementJson1);
                }
                game.RoundInstance.CloseRound(rp.PlayerId);

                if (gp.GamePoint >= cfg.MaxPoint)
                {
                    game.CloseGame(gp);
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"SaveGameState: game_id={game.GameId} , player_id={rp.PlayerId} ", ex);
            }
            return false;
        }

   

        public int GetCorrectPosition(int position)
        {

            return (position + 12) % 24;
        }

        public bool Play(RoundPlayer activePl, int action, int startPos, int endPos, int moveIndex)
        {


            if (action == 0)
            {
                return Move(activePl, startPos, endPos, moveIndex);

            }
            else
            {
                return Discard(activePl, startPos, endPos, moveIndex);

            }

        }

        public virtual void PlayerRoll(int color, int freezeCount)
        {
            var board = GetBoardByColor(color); 
            if (board[0].Count == StoneCount)
            {
                board[0].FreezeCount = freezeCount;
            }
            else
            {
                board[0].FreezeCount = 1;
            }

        

        }

        public bool Move(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        {
            bool result = false;
            var board = GetBoardByColor(activePl.Color);
            board[startPos].Count -= 1;
            if (startPos == 0)
            {
                board[0].FreezeCount--;
                if (board[0].FreezeCount == 0)
                {
                    board[0].IsBlocked = true;
                }
            }

            board[endPos].Count += 1;


            return result;
        }

        public bool Discard(RoundPlayer activePl, int startPos, int endPos, int moveIndex)
        {
            bool result = false;
            var board = GetBoardByColor(activePl.Color);

            board[startPos].Count -= 1;

            activePl.AddDiscard();


            return result;
        }

        public bool EndRound(RoundPlayer gu, bool calcMars, Game game)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            bool result = true;
            try
            {
                var go = gu.Opponent;
                var currentBet = GetCurrentBet(go, calcMars, game);


                result = winRound(gu, calcMars, game);

                go.MovesList.Clear();
                go.UndoList.Clear();


                gu.MovesList.Clear();
                gu.UndoList.Clear();

                var coins = 0m;
                var coins1 = 0m;
                if (result)
                {
                    var playerManager = BaseApplication.Current.PlayerManagerInstance;
                    var roomManager = BaseApplication.Current.RoomManagerInstance;
                    var gameRepository = BaseApplication.Current.BaseGameRepository;


                    var player = playerManager.GetPlayerInfoById(gu.PlayerId);
                    coins = gameRepository.GameFinishCoins(player, game.GameId, currentBet, 1, game.GameRoom.RoomId);

                    var secondPlayer = playerManager.GetPlayerInfoById(go.PlayerId);
                    coins1 = gameRepository.GameFinishCoins(secondPlayer, game.GameId, currentBet, 2, game.GameRoom.RoomId);

                    roomManager.LobbyRoom.UpdateCoins(player.PlayerId, player.Coin);
                    roomManager.LobbyRoom.UpdateCoins(secondPlayer.PlayerId, secondPlayer.Coin);
                }
                game.RoundWinner = gu.PlayerId;
                var giveUp = new JObject()
                {

                    ["win"] = false,
                    ["last"] = result,
                    ["point"] = game.GetPlayerById(gu.PlayerId).GamePoint,
                    ["money"] = currentBet,
                    ["timer_size"] = TimerConfig.NextGameSeconds,
                    ["coin_count"] = coins1

                };
                gameSocketManager.SendMessage(go.PlayerId, game.Config.RoomId, "end_game", giveUp);
                giveUp["coin_count"] = coins;
                giveUp["win"] = true;
                gameSocketManager.SendMessage(gu.PlayerId, game.Config.RoomId, "end_game", giveUp);
            }
            catch (Exception)
            {
                //log error
            }

            if (!result)
            {

                game.StartGame(2000);
            }
            else
            {

                game.GameRoom.EndGame(gu.PlayerId);
            }


            return result;
        }
        public decimal GetCurrentBet(RoundPlayer pl, bool bl, Game game)
        {
            if (game.Config.MatchType() < 2)
            {
                return game.Config.MinBet;
            }
            else
            {
                decimal res = game.Config.MinBet * game.RaiseCount;
                if (res > game.Config.MaxBet)
                {
                    return game.Config.MaxBet;
                }
                else
                {
                    return res;
                }

            }
        }


        public bool canPlayMoveState(RoundPlayer activePl)
        {


            var myaBoard = GetBoardByColor(activePl.Color);
            for (int i = 0; i < myaBoard.Count; i++)
            {
                if (myaBoard[i].Count > 0)
                {
                    var result = canMove(activePl.Color, i, activePl.MovesList);

                    if (result.result)
                    {

                        return true;
                    }
                }
            }

            return false;

        }



        public MoveState canMove(int color, int startpos, List<int> moves)
        {
            var obj = new MoveState();

            obj.result = false;
            obj.index = -1;


            var opponentBoard = GetBoardByColor(0 - color);
            var playerBoard = GetBoardByColor(color);
            for (int i = 0; i < moves.Count; i++)
            {
                int pos = startpos + moves[i];

                if (pos > -1 && pos < 24)
                {
                    bool isBlocked = false;
                    if (startpos > -1 && startpos < 24)
                    {
                        isBlocked = startpos == 0 && playerBoard[startpos].FreezeCount == 0;
                    }

                    if (!isBlocked && opponentBoard[(12 + pos) % 24].Count == 0)
                    {
                        obj.result = true;
                        obj.index = i;
                        obj.pos = pos;
                    }
                }
            }
            return obj;

        }

        public bool CanRaiseX2(Game game)
        {
            /*  if (game.Config.BeaverRule && game.RaiseCount + 2 <= game.Config.RaiseCount)
               {
                   return true;
               } */

            return false;
        }

        public bool CanRaise(Game game)
        {
            var config = game.Config;
            if (config.MatchType() == 0 || !config.DoubleRule)
            {
                return false;
            }

            if (config.MatchType() == 1)
            {

                bool canRs;
                var pl = game.Players[0];
                var pl1 = pl.Opponent;
                canRs = (pl.GamePoint + game.RaiseCount) < config.MaxPoint || (pl1.GamePoint + game.RaiseCount) < config.MaxPoint;
                if (config.CrawfordRule)
                {
                    return (!game.CrawfordActive) && (canRs);
                }
                else
                {
                    return canRs;
                }
            }
            else
            {
                return (game.RaiseCount < ((int)config.MaxBet / config.MinBet));
            }


        }




        public List<BoardPlace> GetBoard(int color)
        {
            var first = GetBoardByColor(color);
            var second = GetBoardByColor(0 - color);
            var result = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place = new BoardPlace()
                {
                    Color = 0,
                    Count = 0
                };
                if (first[i].Count > second[GetCorrectPosition(i)].Count)
                {
                    place.Count = first[i].Count;
                    place.Color = 1;
                    place.IsBlocked = first[i].IsBlocked;
                    place.FreezeCount = first[i].FreezeCount;
                }
                else if (first[i].Count < second[GetCorrectPosition(i)].Count)
                {
                    place.Count = second[GetCorrectPosition(i)].Count;
                    place.Color = 2;
                    place.IsBlocked = second[GetCorrectPosition(i)].IsBlocked;

                }
                result.Add(place);
            }
            return result;
        }


        public int IncreaseRaise(int raiseCount, int maxRaiseCount)
        {
            return raiseCount + 1 > maxRaiseCount ? maxRaiseCount : raiseCount + 1;
        }

        public bool ValidateMove(RoundPlayer pl, int action, int move, int startPos, int endPos, Game game)
        {

            if (pl.PlayerState == RoundPlayerState.MoveState || action == RoundPlayerState.MoveState)
            {

                return canMove(startPos, endPos, pl.Color);

            }
            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                var brd = GetBoardByColor(pl.Color);
                if (brd[startPos].Count == 0)
                {
                    throw new InvalidMoveException();
                }


                if (canDiscard(pl))
                {
                    return true;
                }

            }

            throw new InvalidMoveException();


        }

        public bool canDiscard(RoundPlayer pl)
        {
            return discardIndex(pl) > -1;
        }
        public int discardIndex(RoundPlayer activePl)
        {
            var brd = GetBoardByColor(activePl.Color);

            for (int i = 0; i < activePl.MovesList.Count; i++)
            {
                int posit = 24 - activePl.MovesList[i];

                if (brd[posit].Count > 0)
                {

                    return posit;
                }

                if (canDiscardWithoutMatch(activePl.Color, 24, activePl.MovesList[i]))
                {

                    return i;
                }
            }


            return -1;
        }
        public bool canDiscardWithoutMatch(int color, int startPos, int desc, int value)
        {
            var yourList = this.GetBoardByColor(color);
            var enemyList = this.GetBoardByColor(0 - color);

            int index = 24 - startPos;

            if (startPos < 0 || startPos > 23)
            {

                return false;

            }


            if (value == 6)
            {

                return true;
            }


            if (index > value)
            {

                return false;
            }

            if (index == value)
            {

                return true;
            }

            for (int j = value; j < 7; j++)
            {

                if (yourList[24 - j].Count > 0)
                {

                    return false;
                }

            }



            return true;
        }


        public List<BoardPlace> GetBoardByColor(int color)
        {
            if (color > 0)
            {
                return FirstBoard;
            }
            return SecondBoard;
        }






        public bool canMove(int startPos, int endPos, int color)
        {


            var yourList = this.GetBoardByColor(color);
            var enemyList = this.GetBoardByColor(0 - color);


            if (endPos > 23 || endPos < 0 || startPos > 23 || startPos < 0)
            {

                throw new InvalidMoveException();
            }
            else
            {


                if (startPos == 0 && yourList[0].FreezeCount == 0)
                {
                    throw new InvalidMoveException();
                }

                if (yourList[startPos].Count == 0)
                {
                    throw new InvalidMoveException();
                }

                if (enemyList[(12 + endPos) % 24].Count > 0)
                {
                    throw new InvalidMoveException();
                }


                return canPlaySixCards(endPos, startPos, color);
            }
        }



        public bool canPlaySixCards(int position, int startPos, int color)
        {

            return true;

            
        }





        public bool IsDiscardState(RoundPlayer pl)
        {
            if (pl.PlayerState == RoundPlayerState.DiscardState)
            {
                return true;
            }


            var yourList = this.GetBoardByColor(pl.Color);
            var enemyList = this.GetBoardByColor(0 - pl.Color);


            for (int i = 0; i < 18; i++)
            {
                if (yourList[i].Count > 0)
                {
                    return false;
                }

            }

            return true;
        }

        public bool IsDiscardState(int color)
        {
            var yourList = this.GetBoardByColor(color);

            for (int i = 0; i < 18; i++)
            {
                if (yourList[i].Count > 0)
                {
                    return false;
                }
            }

            return true;
        }

        public bool RollReset(bool reset)
        {
            return true;
        }
    }
}
