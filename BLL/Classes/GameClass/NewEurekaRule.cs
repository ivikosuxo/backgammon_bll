﻿using BLL.Classes.StateCalculator;
using  EntityModels.Exceptions;
using BLL.Managers;
using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.StateCalculator;
using BLL.Classes.GamePlay;

namespace BLL.Classes.GameClass
{
   public class NewEurekaRule : EuropianRule
    {


        public override void Initialize()
        {

            FirstBoard = new List<BoardPlace>();
            SecondBoard = new List<BoardPlace>();
            for (int i = 0; i < 24; i++)
            {
                var place1 = new BoardPlace();
                FirstBoard.Add(place1);

                place1 = new BoardPlace();
                SecondBoard.Add(place1);
            }

            FirstBoard[23].Count = 3;
            FirstBoard[22].Count = 3;
            FirstBoard[21].Count = 3;
            FirstBoard[20].Count = 2;
            FirstBoard[19].Count = 2;
            FirstBoard[18].Count = 2;

            SecondBoard[23].Count = 3;
            SecondBoard[22].Count = 3;
            SecondBoard[21].Count = 3;
            SecondBoard[20].Count = 2;
            SecondBoard[19].Count = 2;
            SecondBoard[18].Count = 2;

        }

        public override PlayModel CheckMaxPlayCount(RoundPlayer roundPlayer)
        {
            roundPlayer.PlayerState = RoundPlayerState.DiscardState;
            roundPlayer.Opponent.PlayerState = RoundPlayerState.DiscardState;
            return base.CheckMaxPlayCount(roundPlayer);
        }



    }
}
