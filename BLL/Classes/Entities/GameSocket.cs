﻿using  EntityModels.Types;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Extensions;
using BLL.Managers;
using InterfaceLibrary.Enums;

namespace BLL.Classes.Entities
{
    public class GameSocket
    {
        public string Connection { get; set; }
        public int PlayerId { get; set; }
        public int RoomId { get; set; }
        public int ConnType { get; set; }
        public bool Active { get; set; }

        public int TournamentId { get; set; } = 0;
        public void Destroy()
        {

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;


            Active = false;
            Connection.CloseConnection(ConnectionType.Close);
            gameSocketManager.RemoveAllSameConnection(this);
            var plId = PlayerId;
            if (!gameSocketManager.Connections.Exists(e=>e.PlayerId==plId))
            {
                playerManager.RemovePlayer(plId);

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.CountChange();  
            }
        }

        
    }
}
