﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  EntityModels.Types;

using BLL.Classes.GameClass;
using Newtonsoft.Json.Linq;
using BLL.Managers;
using  EntityModels.Exceptions;
using  EntityModels.Config;
using System.Timers;
using InterfaceLibrary.Game;

namespace BLL.Classes.Entities
{
    public class SitAndGo : Tournament
    {

        public SitAndGo()
        {
            Initialize();
        }

        public override void Activate() {
            StartDate = DateTime.Now; 
            MinimalCount = MaxCount; 
            IsActive = true;
            GetRegisteredPlayers();
            var jsn = ToJson(0);

            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournament_add, jsn);
        
        }

        public override void AddPlayerToTournament(IPlayerInfo player)
        {
            base.AddPlayerToTournament(player);
            if (TournamentPlayers.Count == MaxCount)
            {
                StartDate = DateTime.Now;
                StartTournament();
            }
        }

        protected override void StartTournament()
        {
            base.StartTournament();
            var tournamentDBManager = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
            tournamentDBManager.OpenTournament(TournamentId);
        } 

    }
}
