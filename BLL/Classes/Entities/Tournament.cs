﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

using BLL.Classes.GameClass;
using Newtonsoft.Json.Linq;
using BLL.Managers;
using  EntityModels.Exceptions;
using  EntityModels.Config;
using System.Timers;
using EntityModels.Types;
using InterfaceLibrary.Enums;
using InterfaceLibrary.Models;
using InterfaceLibrary.Game;
using BLL.Extensions;

namespace BLL.Classes.Entities
{
    public class Tournament :ITournament
    {
        public int MerchantId { get; set; }
        public bool IsNetwork { get; set; }
        public string TournamentName
        {
            get; set;
        }
        public int TournamentId
        {
            get; set;
        }

        public string TournamentNameRus
        {
            get; set;
        }
        public string TournamentNameGeo
        {
            get; set;
        }

        public int FinalPoint { get; set; } = 2;
        public int MaxCount { get; set; } = 1024;
        public int MinimalCount { get; set; } = 8;
        public decimal TournamentFee { get; set; } = 0.0m;
        public decimal TournamentPrize { get; set; } = 0.0m;
        public int TournamentDepth { get; set; } = 1;
        public bool IsActive
        {
            get; set;
        }
        private object locker { get; set; } = new object();
        public DateTime FinishTime
        {
            get; set;
        }
        public DateTime StartDate
        {
            get; set;
        }
        public RoomConfig Config { get; set; } = new RoomConfig();
        public Timer TournamentTimer
        {
            get; set;
        }

        public Timer TournamentStartTimer { get; set; } = new Timer();

        public List<decimal> PrizeList { get; set; } = new List<decimal>();

        public TournamentType TournamentType { get; set; } = TournamentType.FreeRoll;
        public TournamentState State { get; set; } = TournamentState.Waiting;

        public List<ITournamentPlayer> TopPlayers { get; set; } = new List<ITournamentPlayer>();

        public List<List<ITournamentPlayer>> TournamentLevelPlayers { get; set; } = new List<List<ITournamentPlayer>>();

        public List<ITournamentPlayer> TournamentPlayers { get; set; } = new List<ITournamentPlayer>();


        public Tournament()
        {
            Initialize();
            TournamentTimer = new Timer()
            {
                Enabled = false,
                Interval = TimerConfig.MatchmackingDelay * 1000
            };

            TournamentTimer.Elapsed += TournamentNextState;
        }

        public void FillList(List<ITournamentPlayer> list, int size)
        {
            for (int i = 0; i < size; i++)
            {
                list.Add(null);
            }

        }

        public virtual void Initialize()
        {


            Config = new RoomConfig()
            {
                BeaverRule = false,
                ChatEnabled = false,
                CrawfordRule = false,
                CreatorId = 0,
                DoubleRule = false,
                IsProtected = false,
                JacobRule = false,
                MaxBet = 0,
                MinBet = 0,
                MaxPoint = 1,
                Password = "",
                PlayersCount = 2,
                RaiseCount = 1,
                RoomId = 0,
                Roomname = TournamentName,
                Speed = GameSpeed.Fast,
                Type = GameType.European
            };

        }

        public RoomConfig GetConfig()
        {

            return new RoomConfig()
            {
                BeaverRule = Config.BeaverRule,
                ChatEnabled = Config.ChatEnabled,
                CrawfordRule = Config.CrawfordRule,
                CreatorId = Config.CreatorId,
                DoubleRule = Config.DoubleRule,
                IsProtected = Config.IsProtected,
                JacobRule = Config.JacobRule,
                MaxBet = Config.MaxBet,
                MinBet = Config.MinBet,
                MaxPoint = Config.MaxPoint,
                Password = Config.Password,
                PlayersCount = Config.PlayersCount,
                RaiseCount = Config.RaiseCount,
                Roomname = TournamentName,
                Speed = Config.Speed,
                Type = Config.Type,
            };

        }

 

        private void TournamentNextState(Object source, ElapsedEventArgs e)
        {
            lock (locker)
            {
                TournamentTimer.Stop();
                if (State == TournamentState.OnGoing)
                {
                    ///FirstMatchMacking();
                  //  TournamentTimer.Start();
                }
            }
        }

        private void InitializeTournamentPlayers()
        {

            var maxCount = ReturnMaxPlayersCount(1);
            var difference = maxCount - 2 * (maxCount - TournamentPlayers.Count);



            int size = 1;
            for (int i = 0; i <= TournamentDepth; i++)
            {
                var list = new List<ITournamentPlayer>();
                FillList(list, maxCount / size);
                TournamentLevelPlayers.Add(list);
                size = size * 2;
            }
            TournamentPlayers = TournamentPlayers.OrderBy(tourPlayer => tourPlayer.RandomIndex).ToList();
            var firstList = TournamentLevelPlayers[0];
            var index = 0;
            for (int i = 0; i < TournamentPlayers.Count; i++)
            {

                if (i < (difference))
                {
                    firstList[i] = TournamentPlayers[i];
                    firstList[i].Index = i;
                    index = i;
                }
                else
                {
                    index++;
                    firstList[index] = TournamentPlayers[i];
                    firstList[index].Index = index;
                    index++;
                }

            }
        }
        public virtual void AddPlayerToTournament(IPlayerInfo playerInfo)
        {
            lock (locker)
            {

                if (this.State != TournamentState.Waiting)
                {
                    throw new TournamentIsStartedException();
                }

                if (TournamentPlayers.Exists(e => e.PlayerId == playerInfo.PlayerId))
                {
                    throw new TournirAlreadyContainsPlayerException();
                }
                if (TournamentPlayers.Count >= MaxCount)
                {
                    throw new TournamentIsFullException();
                }
                if (!playerInfo.IsVerified)
                {
                    playerInfo.IsVerified = BaseApplication.Current.PlayerManagerInstance.PlayerRepository.GetPlayerInfo(playerInfo.PlayerId).IsVerified;
                    if (!playerInfo.IsVerified)
                    {
                        throw new TournamentVerifiedException();
                    }
                }
                var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
                tournamentDbInstance.BlockTournamentFee(this, playerInfo.PlayerId);
           

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.UpdateBalance(playerInfo.PlayerId, playerInfo.Balance);

               
                //   TournamentRepository.RegisterTournamentPlayer(this, player.GetPlayerId());
                TournamentPlayers.Add(new TournamentPlayer(playerInfo.PlayerId, playerInfo.Username, TournamentId));

                var data = new JObject()
                {
                    ["id"] = TournamentId,
                    ["players_count"] = TournamentPlayers.Count,
                    ["max_count"] = MaxCount
                };

             
                roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournaments_players_count, data);
            }
        }

        public void RemovePlayerFromTournament(int playerId, bool isPlayerRequest)
        {
            lock (locker)
            {
                if (isPlayerRequest && State != TournamentState.Waiting)
                {
                    throw new TournamentIsFullException();
                }

                if (!TournamentPlayers.Exists(e => e.PlayerId == playerId))
                {
                    throw new GameException();
                }

                var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
                tournamentDbInstance.UnBlockTournamentFee(this, playerId);
                tournamentDbInstance.RemovePlayerFromTournament(playerId, TournamentId);
                TournamentPlayers.RemoveAll(e => e.PlayerId == playerId);

                var data = new JObject()
                {
                    ["id"] = TournamentId,
                    ["players_count"] = TournamentPlayers.Count,
                    ["max_count"] = MaxCount
                };

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournaments_players_count, data);
            }
        }


        protected void UpdateWinAmount() {

            var roomManager = BaseApplication.Current.RoomManagerInstance;


            this.TopPlayers.ForEach(e =>
            { 
                roomManager.LobbyRoom.UpdateBalance(e.PlayerId);
            });
           
        }

        public void TournamentGameWin(int playerId)
        {


            lock (locker)
            {

                var tournamentPlayer = TournamentPlayers.First(e => e.PlayerId == playerId);
                tournamentPlayer.TournamentLevel++;
                tournamentPlayer.IsActive = true;
                if (tournamentPlayer.TournamentLevel == this.TournamentDepth)
                {
                    Finish(playerId);

                    var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
                    tournamentDbInstance.CloseTournament(this.TournamentId);
                    tournamentDbInstance.TournamentBalanceReset(this);
                    tournamentDbInstance.GivePlayerWinAmount(this); 

                    UpdateWinAmount();

                }
                else
                {
                    tournamentPlayer.Index = tournamentPlayer.Index / 2;
                    var sameLevelPlayers = TournamentLevelPlayers[tournamentPlayer.TournamentLevel];
                    sameLevelPlayers[tournamentPlayer.Index] = tournamentPlayer;
                    var opponent = sameLevelPlayers[opponentIndex(tournamentPlayer.Index)];
                    if (opponent != null && opponent.IsActive)
                    {
                        CreateTournamentRoom(tournamentPlayer, opponent);
                    }
                    else
                    {
                        opponent = sameLevelPlayers[opponentIndex(tournamentPlayer.Index)];

                        var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                        gameSocketManager.SendMessageToAllLobby("run guys", new JObject());
                    }

                }
            }

        }

        private bool IsEven(int index)
        {
            return index % 2 == 0;
        }

        private int opponentIndex(int index)
        {
            if (IsEven(index))
            {
                return index + 1;
            }
            else
            {
                return index - 1;
            }

        }

        public virtual void Activate()
        {

            // TournamentRepository.CreateTournament(this);

            var difference = (StartDate - DateTime.Now).TotalMilliseconds;
            if (difference < 0)
            {
                throw new GameException();
            }
            GetRegisteredPlayers();

            TournamentStartTimer.Interval = difference;
            TournamentStartTimer.Elapsed += TournamentStart;
            TournamentStartTimer.Enabled = false;
            TournamentStartTimer.Start();
            IsActive = true;
            var jsn = ToJson(0);
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournament_add, jsn); 
        }

        public void SetGameType(int type)
        {
            Config.Type = type;
        }

        protected void GetRegisteredPlayers()
        {
            try
            {
                var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;

                var playersIds = tournamentDbInstance.GetRegisteredPlayers(this);
                var players = tournamentDbInstance.GetRegisteredTournamentPlayers(playersIds, this.TournamentId);
                this.TournamentPlayers.AddRange(players);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"GetRegisteredPlayers: tournament_id={TournamentId}", ex);
            }

        }

        public void TournamentStart(Object source, ElapsedEventArgs e)
        {
            TournamentStartTimer.Stop();
            StartTournament();
        }

        public void CancelTournament(int reason)
        {
            TournamentTimer.Stop();
            State = TournamentState.Declined;
            FinishTime = DateTime.Now;
            IsActive = false;

            var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
            tournamentDbInstance.UnblockAmountForAllPlayers(this);
            TournamentStatusChange();


            var roomManager = BaseApplication.Current.RoomManagerInstance;
            if (TournamentFee>0)
            {
                TournamentPlayers.ForEach(e => {
                 
                    roomManager.LobbyRoom.UpdateBalance(e.PlayerId);
                });
            }
          
        }

        public void DeleteTournament()
        {
            TournamentTimer.Stop();
            State = TournamentState.Declined;
            IsActive = false;

            var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
            tournamentDbInstance.UnblockAmountForAllPlayers(this);
            TournamentStatusChange();
            var jsn = new JObject()
            {
                ["tournament_id"] = TournamentId
            };
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournament_remove, jsn);

           
            if (TournamentFee > 0)
            {
                TournamentPlayers.ForEach(e => {

                    roomManager.LobbyRoom.UpdateBalance(e.PlayerId);
                });
            }

        }
        public void Finish(int winnerId)
        {
            FinishTime = DateTime.Now;
            State = TournamentState.Finished;
            IsActive = false;
            TournamentStatusChange();
            var data = new JObject()
            {
                ["winner_id"] = winnerId,
                ["tournament_id"] = TournamentId,
                ["fee"] = TournamentFee
            };
            var achievementManager = BaseApplication.Current.AchievementManagerInstance;
            TournamentPlayers.ForEach(pl =>
            {
                achievementManager.CalculateAchievement(pl.PlayerId, 0, AchievementEvents.TournamentFinish, data);
            });
            TopPlayers.ForEach(pl =>
            {
                achievementManager.CalculateAchievement(pl.PlayerId, 0, AchievementEvents.TournamentTopPlace, data);
            });
        }

        public JObject ToJson(int plId)
        {
            var start = StartDate;
            if (TournamentType == TournamentType.SitAndGo && !IsActive)
            {
                start = DateTime.Now;
            }
            var startDate = start.ToString("dd-MM HH:mm");

            var startDateFull = start.ToString("dd-MM-yyyy HH:mm:ss");

            var ticksDifference = (start.Ticks - DateTime.Now.Ticks);

            var daysDifference = (int)(ticksDifference / TimeSpan.TicksPerDay);
            var hoursDifference = (ticksDifference % TimeSpan.TicksPerDay) / TimeSpan.TicksPerSecond;


            if (TournamentType == TournamentType.SitAndGo && State == TournamentState.Waiting)
            {
                startDate = "Sit&Go";
                startDateFull = "Sit&Go";
            }

            var json = new JObject()
            {
                ["name"] = TournamentName,
                ["name_ru"] = TournamentNameRus,
                ["name_ge"] = TournamentNameGeo,
                ["id"] = TournamentId,
                ["fee"] = TournamentFee,
                ["min_count"] = MinimalCount,
                ["max_count"] = MaxCount,
                ["tournament_type"] = TournamentType.ToString(),
                ["tournament_id"] = (int)TournamentType,
                ["type"] = Config.Type,
                ["state"] = (int)State,
                ["prize"] = TournamentPrize,
                ["start_date"] = startDate,
                ["full_date"] = startDateFull,
                ["players_count"] = TournamentPlayers.Count,
                ["days_difference"] = daysDifference,
                ["hours_difference"] = hoursDifference,
                ["player_sit"] = TournamentPlayers.Exists(e => e.PlayerId == plId)
            };
            return json;
        }


        public List<int> RemoveExtraPlayers()
        {

            var extraPlayersList = new List<int>();
            for (int i = TournamentDepth; i < TournamentPlayers.Count; i++)
            {
                var id = TournamentPlayers[TournamentDepth].PlayerId;
                extraPlayersList.Add(id);
                TournamentPlayers.RemoveAt(TournamentDepth);
            }

            return extraPlayersList;
        }




        protected virtual void StartTournament()
        {
            try
            {
                lock (locker)
                {
                    State = TournamentState.OnGoing;
                    var playersCount = TournamentPlayers.Count;
                    if (playersCount < MinimalCount)
                    {
                        CancelTournament(ErrorCodes.NotEnoughPlayers);
                        return;
                    }
                    TournamentStatusChange();
                    var count = ReturnMaxPlayersCount(1);
                    TournamentDepth = GetTournamentDepth(1, count);
                    var difference = (count - playersCount);
                    InitializeTournamentPlayers();

                    // TournamentTimer.Start();

                    var timer = new Timer(TimerConfig.StartTournamentDelay * 1000);
                    timer.Elapsed += (sender, e) => FirstMatchMacking();
                    timer.AutoReset = false;
                    timer.Start();
                }
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"StartTournament: tournament_id={TournamentId}", ex);
            }

        }

        public void TournamentStatusChange()
        {

            var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;
            tournamentDbInstance.UpdateTournamentStatus(TournamentId, (int)State);
            var data = new JObject()
            {
                ["id"] = TournamentId,
                ["state"] = (int)State
            };
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournament_status_change, data);

        }

        public void FirstMatchMacking()
        {

            try
            {
                lock (locker)
                {
                    var players = TournamentLevelPlayers[0];

                    for (int i = 0; i < players.Count - 1; i++)
                    {
                        var player1 = players[i];
                        var player2 = players[i + 1];
                        if (player2 != null)
                        {
                            CreateTournamentRoom(player1, player2);

                        }
                        else
                        {
                            TournamentGameWin(player1.PlayerId);
                        }
                        i++;
                    }

                }
            }
            catch (Exception ex)
            {

                ApplicationManager.Logger.Error($"FirstMatchMacking:", ex);

                throw;
            }
        }

        public void MatchMacking()
        {
            lock (locker)
            {

                var players = TournamentPlayers.Where(e => e.IsActive).ToList();
                players.OrderBy(e => e.TournamentLevel);
                for (int i = 0; i < TournamentPlayers.Count - 1; i++)
                {
                    var player1 = TournamentPlayers[i];
                    var player2 = TournamentPlayers[i + 1];
                    if (player1.IsActive && player2.IsActive && player1.TournamentLevel == player2.TournamentLevel)
                    {
                        CreateTournamentRoom(player1, player2);
                        i++;
                    }
                }

            }
        }

        public void GetTournamentDepth()
        {

        }

        public void RemoveTournament()
        {
            try
            {

                var data = new JObject();
                data["id"] = TournamentId;
                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.SendMessageAll(CommandKeys.tournament_remove, data);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"RemoveTournament: tournament_id={TournamentId}" + ex.Message, ex);
            }

        }


        protected void CreateTournamentRoom(ITournamentPlayer player1, ITournamentPlayer player2)
        {
            try
            {
                var playerManager = BaseApplication.Current.PlayerManagerInstance;


                lock (locker)
                {
                    player1.IsActive = false;
                    player2.IsActive = false;

                    var tournamentDbInstance = BaseApplication.Current.TournamentManagerInstance.TournamentDbInstance;

                    var room = new TournamentRoom(tournamentDbInstance.CreateTournamentRoom(this,this.Config.Speed,this.Config.Type), GetConfig(), TournamentId, player1.TournamentLevel);
                    room.IsFinal = (player1.TournamentLevel == TournamentDepth - 1);
                    if (room.IsFinal)
                    {
                        room.Config.MaxPoint = FinalPoint;
                    }
                    room.AddUser(playerManager.GetPlayerInfoById(player1.PlayerId), null, new JObject());
                    room.AddUser(playerManager.GetPlayerInfoById(player2.PlayerId), null, new JObject());
                    var roomManager = BaseApplication.Current.RoomManagerInstance;
                    roomManager.AddRoom(room.RoomId, room);

                    var data = new JObject()
                    {
                        ["room_id"] = room.RoomId,
                        ["tournament_id"] = TournamentId,
                        ["name"] = TournamentName
                    };

                    roomManager.LobbyRoom.SendMessage(player1.PlayerId, CommandKeys.tournament_room_create, data);
                    roomManager.LobbyRoom.SendMessage(player2.PlayerId, CommandKeys.tournament_room_create, data);
                }
            }
            catch (Exception ex)
            {

                ApplicationManager.Logger.Error($"CreateTournamentRoom: tournament_id={TournamentId} ", ex);
                throw;
            }
        }


        public virtual JObject GetFlow()
        {

            var result = new JObject();
            result["players"] = GetPlayers();
            result["prizes"] = GetPrizeList();
            result["matches"] = GetMatches();

            return result;

        }

        protected virtual JArray GetPlayers()
        {
            var arr = new JArray();
            TournamentPlayers.OrderByDescending(e => e.Prize).ToList().ForEach(e => arr.Add(e.ToJson()));
            return arr;

        }
        protected virtual JArray GetPrizeList()
        {
            var arr = new JArray();
            PrizeList.ForEach(e => arr.Add(e));
            return arr;

        }

        protected virtual JArray GetMatches()
        {
            var globalArray = new JArray();

            TournamentLevelPlayers.ForEach(e =>
            {
                var array = new JArray();
                if (e.Count > 1)
                {


                    for (int i = 0; i < e.Count - 1; i++)
                    {
                        var obj = new JObject();

                        if (e[i] != null)
                        {
                            obj["player_id"] = e[i].PlayerId;
                            obj["username"] = e[i].Username;
                            obj["level"] = e[i].TournamentLevel;
                        }
                        else
                        {
                            obj["player_id"] = 0;
                            obj["username"] = "---";
                            obj["level"] = -1;
                        }

                        if (e[i + 1] != null)
                        {
                            obj["opponent_id"] = e[i + 1].PlayerId;
                            obj["opponent_name"] = e[i + 1].Username;
                            obj["op_level"] = e[i + 1].TournamentLevel;

                        }
                        else
                        {
                            obj["opponent_id"] = 0;
                            obj["opponent_name"] = "---";
                            obj["op_level"] = -1;

                        }




                        array.Add(obj);
                        i++;
                    }
                    globalArray.Add(array);
                }

            });

            return globalArray; 
        }

        protected int ReturnMaxPlayersCount(int count)
        {
            if (count >= TournamentPlayers.Count)
            {
                return count;
            }
            return ReturnMaxPlayersCount(count * 2);
        }

        protected int GetTournamentDepth(int first, int count)
        {
            if (Math.Pow(2, first) == count)
            {
                return first;
            }

            return GetTournamentDepth(first + 1, count);

        }
    }
}
