﻿using BLL.Classes.GamePlay;
using  EntityModels.Exceptions;
using BLL.Extensions;
using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.StateCalculator;

namespace BLL.Classes.StateCalculator
{
   public class LongBoardStateCalculator
    {
        List<BoardPlace> playerBoard;
        List<BoardPlace> opponentBoard;
        private PlayModel model;

        public int playerstate = RoundPlayerState.MoveState;

        List<int> playerMoves;
        public int playedMove; 
        public int maxCount = 0;
        public int discardCount = 0;
        public bool IsValidState = true;

        LongBoardStateCalculator Base;

        List<LongBoardStateCalculator> possibleMoves = new List<LongBoardStateCalculator>();

        public PlayModel Model { get => model; set => model = value; }

        public int GetMaxPoint()
        {
            if (possibleMoves.Count > 0)
            {
                return possibleMoves.Where(e=>e.IsValidState).Max(e=>e.playedMove);
            }
            return 0;
        }

        public LongBoardStateCalculator(LongBoardStateCalculator mainState ,List<BoardPlace> myboard, List<BoardPlace> opBoard, List<int> moves, int max, int move)
        {

            Base = mainState;
            if (mainState == null)
            {
                Base = this;
            }

            maxCount = max;
            playedMove = move; 
            playerMoves = moves;
            playerBoard = myboard.Clone();
            opponentBoard = opBoard.Clone(); 

            IsValidState = IsValidBoardState();
           

            if (IsValidState && move == maxCount)
            {
                Model = new PlayModel(false, max);
                throw new MaxPlayedException(Model);
            }

       
        }

        public void Calculate()
        {
            if (discardCount == GetTotalCount())
            {
                Model = new PlayModel(false, maxCount);
                throw new MaxPlayedException(Model);
            }

           if (playerstate == RoundPlayerState.MoveState)
            {
                for (int i = 0; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        PlayMove(i, j);

                    }
                }
            }
            else if (playerstate == RoundPlayerState.DiscardState)
            {
                for (int i = 18; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        if (CanMove(i, playerMoves[j]))
                        {
                            PlayMove(i, j);
                        }
                        else
                        {
                            if (CanDiscard(i, playerMoves[j]))
                            {
                                var boardState = Discard(i, playerMoves[j]);
                                boardState.discardCount = discardCount + 1;
                                boardState.playerstate = playerstate;
                                Base.possibleMoves.Add(boardState);
                                boardState.Calculate();
                            }
                        }

                    }
                }
            }

 
        }
        public bool CanMove(int index, int move)
        {
            var dest = index + move;

            if (index==0 && playerBoard[0].FreezeCount==0)
            {
                return false;
            }

            if (dest < 24 && opponentBoard[GetOpponentsPosition(dest)].Count == 0 && playerBoard[index].Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool CanBlockOpponent() { 
            for (int i = 18; i < 24; i++)
            {
                if (opponentBoard[i].Count > 0)
                {
                    return true;
                } 

            }
            return  false;
        }

        public bool IsValidBoardState() {

            return CanBlockOpponent() || !IsSixRowBlocked();

        }

        public bool IsSixRowBlocked() {  

                        
            var rowCount = 0;
            for (int i = 0; i < 12; i++)
            {
                if (playerBoard[i].Count > 0)
                {
                    rowCount++;
                    if (rowCount==6)
                    {
                        return true;
                    }
                }
                else {
                    rowCount=0; 
                } 

            }
            return rowCount == 6;
        }




        public bool CanDiscard(int start, int move)
        {

            if (24 - start > move || playerBoard[start].Count == 0)
            {
                return false;
            }

            if (move == 24 - start)
            {
                return true;
            }

            for (int i = 18; i < start; i++)
            {
                if (playerBoard[i].Count > 0)
                {
                    return false;
                }
            }
            return true;
        }

        public void PlayMove(int i, int j)
        {

            var dest = i + playerMoves[j];
            if (dest < 24 && CanMove(i, playerMoves[j]))
            {
                var boardState = Move(i, playerMoves[j]);
                boardState.playerBoard[i].Count--;
                if (boardState.playerBoard[i].Count == 0)
                {
                    boardState.playerBoard[i].IsBlocked = true;
                }
                Base.possibleMoves.Add(boardState);
                if (dest > 17)
                {
                    boardState.playerstate = GetPlayerState(boardState);
                }
                boardState.Calculate();

            }

         
        }

        public int GetPlayerState(LongBoardStateCalculator state)
        {
            
            if (state.playerstate == RoundPlayerState.DiscardState)
            {
                return RoundPlayerState.DiscardState;
            }
            if (state.playerstate == RoundPlayerState.MoveState)
            {
                var count = state.discardCount;
                for (int i = 18; i < 24; i++)
                {
                    count += state.playerBoard[i].Count;
                }
                if (count == GetTotalCount())
                {
                    return RoundPlayerState.DiscardState;
                }
            }

            return RoundPlayerState.MoveState;
        }


        public LongBoardStateCalculator Move(int index, int move)
        {
            var dest = index + move;
            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move);

            if (index==0)
            {
                playerBoard[0].FreezeCount--;
            }
            var state = new LongBoardStateCalculator(Base,playerBoard, opponentBoard, newMoves, maxCount, playedMove + move);
            state.discardCount = discardCount;

            state.playerBoard[index + move].Count++;
             

            return state;
        }

        public LongBoardStateCalculator Discard(int start, int move)
        {

            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move); 
            var state = new LongBoardStateCalculator(Base, playerBoard, opponentBoard, newMoves, maxCount, playedMove + move);

            state.playerBoard[start].Count--;

            return state;
        }


        public int GetOpponentsPosition(int dest) { 

            return (12 + dest) % 24;
        }
        public int GetTotalCount()
        {

            return 15;
        }
    }
}
