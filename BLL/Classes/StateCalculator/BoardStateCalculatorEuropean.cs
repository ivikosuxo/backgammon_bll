﻿using BLL.Classes.GamePlay;
using  EntityModels.Exceptions;
using BLL.Extensions;
using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.StateCalculator;

namespace BLL.Classes.StateCalculator
{
    public class BoardStateCalculatorEuropean
    {
        protected int maxCount = 0;
        public int playerstate = RoundPlayerState.MoveState;
        public int killedCount = 0;
        public int discardCount = 0;
        protected List<BoardPlace> playerBoard;
        protected List<BoardPlace> opponentBoard;

        protected List<BoardStateCalculatorEuropean> possibleMoves = new List<BoardStateCalculatorEuropean>();
        protected bool KillDiscard = false;
        protected PlayModel Model;
        protected List<int> playerMoves;
        protected  int playedMove;

        BoardStateCalculatorEuropean Base;

        public int GetMaxPoint()
        {

            if (possibleMoves.Count > 0)
            {
                return possibleMoves.Max(e=>e.playedMove);
            }
            return 0;
        }

    


        public BoardStateCalculatorEuropean(BoardStateCalculatorEuropean mainState,List<BoardPlace> myboard, List<BoardPlace> opBoard, List<int> moves, int max, int move)
        {

            Base = mainState;
            if (mainState==null)
            {
                Base = this;
            }

            maxCount = max;
            playedMove = move;
            Model = new PlayModel(false, playedMove);

            if (move == maxCount)
            {
                throw new MaxPlayedException(Model);
            }

            playerMoves = moves;
            playerBoard = myboard.Clone();
            opponentBoard = opBoard.Clone();

        }


        public virtual int GetTotalCount() {

            return 15;
        }


        public void Calculate()
        {
            if (discardCount == GetTotalCount())
            {
                Model = new PlayModel(false, maxCount);
                throw new MaxPlayedException(Model);
            }

            if (playerstate == RoundPlayerState.KilledState)
            {

                for (int j = 0; j < playerMoves.Count; j++)
                {

                    if (CanRevive(-1, playerMoves[j]))
                    {
                        var boardState = Move(-1, playerMoves[j]);
                        boardState.killedCount = killedCount - 1;
                        if (boardState.killedCount == 0)
                        {
                            boardState.playerstate = RoundPlayerState.MoveState; 
                        }
                        if (boardState.killedCount > 0)
                        {
                            boardState.playerstate = RoundPlayerState.KilledState; 
                        }
                        Base.possibleMoves.Add(boardState);
                        boardState.Calculate();
                    }
                }


            }
            else if (playerstate == RoundPlayerState.MoveState)
            {
                for (int i = 0; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        PlayMove(i, j);

                    }
                }
            }
            else if (playerstate == RoundPlayerState.DiscardState)
            {
                for (int i = 18; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        if (CanMove(i, playerMoves[j]))
                        {
                            PlayMove(i, j);
                        }
                        else
                        {
                            if (CanDiscard(i, playerMoves[j]))
                            {
                                var boardState = Discard(i, playerMoves[j]);
                                boardState.discardCount = discardCount + 1;
                                boardState.playerstate = playerstate;
                                Base.possibleMoves.Add(boardState);
                                boardState.Calculate();
                            }
                        }

                    }
                }
            }
        }

        public bool CanDiscard(int start, int move)
        {

            if (24 - start > move || playerBoard[start].Count == 0)
            {
                return false;
            }

            if (move == 24 - start)
            {
                return true;
            }

            for (int i = 18; i < start; i++)
            {
                if (playerBoard[i].Count > 0)
                {
                    return false;
                }
            }
            return true;
        }
        public void PlayMove(int i, int j)
        {

            var dest = i + playerMoves[j];
            if (dest < 24 && CanMove(i, playerMoves[j]))
            {
                var boardState = Move(i, playerMoves[j]);
                boardState.playerBoard[i].Count--;

                Base.possibleMoves.Add(boardState);
                if (dest > 17)
                {
                    boardState.playerstate = GetPlayerState(boardState);
                }
                boardState.Calculate();

            }
        }

        public int GetPlayerState(BoardStateCalculatorEuropean state)
        {
            if (state.killedCount > 0)
            {
                return RoundPlayerState.KilledState;
            }
            if (state.playerstate == RoundPlayerState.DiscardState)
            {
                return RoundPlayerState.DiscardState;
            }
            if (state.playerstate == RoundPlayerState.MoveState)
            {
                var count = state.discardCount;
                for (int i = 18; i < 24; i++)
                {
                    count += state.playerBoard[i].Count;
                }
                if (count == GetTotalCount())
                {
                    return RoundPlayerState.DiscardState;
                }
            }

            return RoundPlayerState.MoveState;
        }
        public bool CanRevive(int index, int move)
        {
            var dest = index + move;
            if (opponentBoard[23 - dest].Count < 2)
            {
                return true;
            }

            return false;
        }
        public bool CanMove(int index, int move)
        {
            var dest = index + move;

            if (dest < 24 && opponentBoard[23 - dest].Count < 2 && playerBoard[index].Count > 0  )
            {
                return true;
            }

            return false;
        }
        public BoardStateCalculatorEuropean Move(int index, int move)
        {
            var dest = index + move;
            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move);
            var state = new BoardStateCalculatorEuropean(Base,playerBoard, opponentBoard, newMoves, maxCount, playedMove + move);
            state.discardCount = discardCount;

            state.playerBoard[index + move].Count++;

            if (state.opponentBoard[23 - dest].Count == 1)
            {
                state.opponentBoard[23 - dest].Count = 0; 
            }
             

            return state;
        }

        public BoardStateCalculatorEuropean Discard(int start, int move)
        {

            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move); 

            var state = new BoardStateCalculatorEuropean(Base,playerBoard, opponentBoard, newMoves, maxCount, playedMove + move); 
            state.playerBoard[start].Count--;

            return state;
        }

    }
}
