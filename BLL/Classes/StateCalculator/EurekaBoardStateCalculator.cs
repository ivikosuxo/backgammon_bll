﻿using BLL.Classes.GamePlay;
using  EntityModels.Exceptions;
using BLL.Extensions;
using  EntityModels.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModels.StateCalculator;

namespace BLL.Classes.StateCalculator
{
  public  class EurekaBoardStateCalculator
    {
        protected int maxCount = 0;
        public int playerstate = RoundPlayerState.DiscardState;
        public int killedCount = 0;
        public int discardCount = 0;
        protected List<BoardPlace> playerBoard;
        protected List<BoardPlace> opponentBoard;

        protected List<EurekaBoardStateCalculator> possibleMoves = new List<EurekaBoardStateCalculator>();
        protected bool KillDiscard = false;
        protected PlayModel Model;
        protected List<int> playerMoves;
        protected int playedMove;

        EurekaBoardStateCalculator Base;

        public int GetMaxPoint()
        {

            if (possibleMoves.Count > 0)
            {
                return possibleMoves.Max(e => e.playedMove);
            }
            return 0;
        }




        public EurekaBoardStateCalculator(EurekaBoardStateCalculator mainState, List<BoardPlace> myboard, List<BoardPlace> opBoard, List<int> moves, int max, int move)
        {

            Base = mainState;
            if (mainState == null)
            {
                Base = this;
            }

            maxCount = max;
            playedMove = move;
            Model = new PlayModel(false, playedMove);

            if (move == maxCount)
            {
                throw new MaxPlayedException(Model);
            }

            playerMoves = moves;
            playerBoard = myboard.Clone();
            opponentBoard = opBoard.Clone();

        }


        public virtual int GetTotalCount()
        {

            return 15;
        }


        public void Calculate()
        {
            if (discardCount == GetTotalCount())
            {
                Model = new PlayModel(false, maxCount);
                throw new MaxPlayedException(Model);
            }

  
            for (int i = 18; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                          if (CanDiscard(i, playerMoves[j]))
                            {
                                var boardState = Discard(i, playerMoves[j]);
                                boardState.discardCount = discardCount + 1;
                                boardState.playerstate = playerstate;
                                Base.possibleMoves.Add(boardState);
                                boardState.Calculate();
                            }
                      } 
                }
            
        }

        public bool CanDiscard(int start, int move)
        {
             

            return (24 - start == move) && playerBoard[start].Count >0; 
             
        }
   
 

        public EurekaBoardStateCalculator Discard(int start, int move)
        {

            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move);

            var state = new EurekaBoardStateCalculator(Base, playerBoard, opponentBoard, newMoves, maxCount, playedMove + move);
            state.playerBoard[start].Count--;

            return state;
        }

    }
}
