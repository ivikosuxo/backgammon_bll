﻿using BLL.Classes.GamePlay;
using  EntityModels.Exceptions;
using BLL.Extensions;
using  EntityModels.Types;
using EntityModels.StateCalculator;
 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.StateCalculator
{
    public class BoardState
    {
        public int maxCount = 0;
        public int playerstate = RoundPlayerState.MoveState;
        public int killedCount = 0;
        public int discardCount = 0;
        List<BoardPlace> playerBoard;
        List<BoardPlace> opponentBoard;

        BoardState Base;

        List<BoardState> possibleMoves = new List<BoardState>();
        public bool KillDiscard = false;
        public PlayModel Model;

        public int GetMaxPoint() {
             
            if (possibleMoves.Count>0)
            {
                return possibleMoves.Where(e=>!e.KillDiscard).Max(e => e.playedMove);
            }
            return 0;
        }

        List<int> playerMoves;
        public int playedMove;
        
        public BoardState(BoardState mainState, List<BoardPlace> myboard, List<BoardPlace> opBoard,   List<int> moves, int max, int move,bool killDiscard )
        {
        
            maxCount = max;
            playedMove = move;
            Model = new PlayModel(killDiscard,playedMove);
            KillDiscard = killDiscard;
            if (move == maxCount)
            {
                if(!killDiscard)
                {
                    throw new MaxPlayedException(Model);
                }
            }
            playerMoves = moves;
            playerBoard = myboard.Clone();
            opponentBoard = opBoard.Clone();

            Base = mainState;

            if (mainState == null)
            {
                Base = this;
            }
        }

        public void FindSecond()
        {
            if(Model.KillDiscard)
            {
                Model = new PlayModel(true, maxCount);
                throw new MaxPlayedException(Model);
            }
            possibleMoves.ForEach(e => e.FindSecond());
        
        }

  

        public void Calculate()
        {
            if (discardCount==15)
            {
                Model = new PlayModel(true, maxCount);
                throw new MaxPlayedException(Model);
            }

            if (playerstate == RoundPlayerState.KilledState)
            {
                 
                    for (int j = 0; j < playerMoves.Count; j++)
                    {

                        if (CanRevive(-1, playerMoves[j]))
                        {
                            var boardState = Move(-1, playerMoves[j]);
                            boardState.killedCount=killedCount-1;
                            if (boardState.killedCount == 0)
                            {
                                boardState.playerstate = RoundPlayerState.MoveState;
                             
                            }
                            if (boardState.killedCount > 0)
                            {
                                boardState.playerstate = RoundPlayerState.KilledState;

                            }
                        Base.possibleMoves.Add(boardState);
                            boardState.Calculate();
                        }
                    }

 
            }
            else if (playerstate == RoundPlayerState.MoveState)
            {
                for (int i = 0; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        PlayMove(i,j);
                        
                    }
                }
            }
                else if (playerstate == RoundPlayerState.DiscardState)
                {
                for (int i = 18; i < 24; i++)
                {
                    for (int j = 0; j < playerMoves.Count; j++)
                    {
                        if ( CanMove(i, playerMoves[j]))
                        {
                             PlayMove(i, j);
                        }
                        else
                        {
                           if (CanDiscard(i, playerMoves[j]))
                            {
                                var boardState = Discard(i, playerMoves[j]);
                                boardState.discardCount= discardCount+1;
                                boardState.playerstate = playerstate;
                                Base.possibleMoves.Add(boardState);
                                boardState.Calculate();
                            }  
                        }
                       
                    }
                }
            }
            }

        public bool CanDiscard(int start , int move) {
            
            if (24-start > move || playerBoard[start].Count==0)
            {
                return false;
            }

            if (move == 24 - start)
            {
                return true;
            }

            for (int i = 18 ; i < start; i++)
            {
                if (playerBoard[i].Count>0)
                {
                    return false;
                }
            }
             return true;
        }
        public void PlayMove(int i, int j) {

            var dest = i + playerMoves[j];
            if (dest < 24 && CanMove(i, playerMoves[j]))
            {
                var boardState = Move(i, playerMoves[j]);
                boardState.playerBoard[i].Count--;  


                Base.possibleMoves.Add(boardState);
                if (dest > 17)
                {
                    boardState.playerstate = GetPlayerState(boardState);
                }
                boardState.Calculate();
              
            }
        }

        public int GetPlayerState(BoardState state) {
            if (state .killedCount> 0)
            {
                return RoundPlayerState.KilledState;
            }
            if (state.playerstate ==RoundPlayerState.DiscardState )
            {
                return RoundPlayerState.DiscardState;
            }
            if (state.playerstate == RoundPlayerState.MoveState)
            {
                var count = state.discardCount;
                for (int i =  18; i < 24; i++)
                {
                    count += state.playerBoard[i].Count;
                }
                if (count ==15)
                {
                    return RoundPlayerState.DiscardState;
                }
            }

            return RoundPlayerState.MoveState;
        }
        public bool CanRevive(int index,int move)
        {
            var dest = index + move;
            if (opponentBoard[23 - dest].Count < 2)
            {
                return true;
            }
             
            return false;
        }
        public bool CanMove(int index, int move)
        {
            var dest = index + move;
         
            if (dest <24 &&opponentBoard[23 - dest].Count < 2 && playerBoard[index].Count > 0 && (!playerBoard[index].IsBlocked || playerBoard[index + move].Count == 0))
            {
                return true;
            }

            return false;
        }
        public BoardState Move(int index, int move)
        {
            var dest = index + move;
            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move);
            var state = new BoardState(Base,playerBoard, opponentBoard, newMoves, maxCount, playedMove + move,false);
            state.discardCount = discardCount;

            state.playerBoard[index + move].Count++;

            if (state.opponentBoard[23 - dest].Count == 1)
            {
                state.opponentBoard[23 - dest].Count = 0;
                if (dest > 17)
                {
                    state.playerBoard[dest].IsBlocked = true;
                }
            }
    
            if (dest > 17 && state.playerBoard[index].Count == 0)
            {
                state.playerBoard[dest].IsBlocked  =  state.playerBoard[dest].IsBlocked || state.playerBoard[index].IsBlocked;
                state.playerBoard[index].IsBlocked = false;
            }

            return state;
        }

        public BoardState Discard(int start, int move) {
           
            var newMoves = new List<int>(playerMoves);
            newMoves.Remove(move);
            var killed = playerBoard[start].Count == 1 && playerBoard[start].IsBlocked;
            var state = new BoardState(Base,playerBoard, opponentBoard, newMoves, maxCount, playedMove + move, killed);
                
            state.playerBoard[start].Count--;
             
            return state;
        }
    }
}
