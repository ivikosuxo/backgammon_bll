﻿using BLL.Classes.GamePlay;
using  EntityModels.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.StateCalculator
{
    public class HyperBoardStateCalculator : BoardStateCalculatorEuropean
    {
        public HyperBoardStateCalculator(HyperBoardStateCalculator mainState,List<BoardPlace> myboard, List<BoardPlace> opBoard, List<int> moves, int max, int move):base(mainState,myboard, opBoard, moves, max,move) 
        {
   
        }

        public override int GetTotalCount()
        {
            return 3;
        }
    }
}
