﻿using  EntityModels.Exceptions;
using BLL.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.GamePlay
{
    public class RandomGenerator
    {
        private static readonly Random getrandom = new Random();
        private static readonly RNGCryptoServiceProvider CprytoRNG = new RNGCryptoServiceProvider();
        private static readonly Quantis quantis = new Quantis(QuantisDeviceType.QUANTIS_DEVICE_USB, 0);
        private static object synclock = new object();
        private static int minCount = 4000;


        private static ConcurrentQueue<int> queue = new ConcurrentQueue<int>();


        public static void Initialize()
        {

           // FillQueue(5000);
            //  FillQueue(1000);
        }





        public static int GetRandomNumber(int min, int max)
        {
            lock (synclock)
            {
                try
                {
                    try
                    {
                        return quantis.ReadInt(min, max - 1);
                       /* try
                        {
                            return GetQueueInt();
                        }
                        catch (Exception ex)
                        {
                            ApplicationManager.Logger.Error("RandomORG error : " + ex.Message, ex);
                          
                        }*/

                    }

                    catch (Exception ex)
                    {
                        ApplicationManager.Logger.Error("IDQ error : " + ex.Message, ex);
                        return RandomIntFromRNG(min, max);
                    }
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error("RNG error :" + ex.Message, ex);
                    return getrandom.Next(min, max);
                }

            }

        }

        private static int RandomIntFromRNG(int min, int max)
        {
            // Generate four random bytes
            var four_bytes = new byte[4];
            CprytoRNG.GetBytes(four_bytes);

            // Convert the bytes to a UInt32
            var scale = BitConverter.ToUInt32(four_bytes, 0);

            // And use that to pick a random number >= min and < max
            return (int)(min + (max - min) * (scale / (uint.MaxValue + 1.0)));
        }


        private static int GetQueueInt()
        {


            var queueCheck = queue.TryDequeue(out var queueResult);


            if (queue.Count < minCount)
            {
                minCount = 3500; 
                FillQueue(5000); 
            }

            if (!queueCheck)
            {
                throw new QueueIsEmptyException();
            }


            return queueResult;

        }

        private static void FillQueue(int count)
        {
            Task.Factory.StartNew(() =>
            {
                ApplicationManager.Logger.Error($"FillQueue Random ORG: ");
                try
                {
                    using (var client = new WebClient())
                    {
                        var requestParams = new JObject();
                        requestParams["n"] = count;
                        requestParams["min"] = 1;
                        requestParams["max"] = 6;
                        requestParams["replacement"] = true;
                        requestParams["base"] = 10;
                        requestParams["apiKey"] = "5c3e0244-0a37-43fc-9d25-c3f05fec30d7";

                        var data = new JObject();
                        data["jsonrpc"] = "2.0";
                        data["method"] = "generateIntegers";
                        data["id"] = "9380";
                        data["params"] = requestParams;

                        var response = client.UploadString("https://api.random.org/json-rpc/1/invoke", data.ToString());


                        var randomModel = JsonConvert.DeserializeObject<RandomOrgResponseModel>(response);

                        var arr = randomModel.result.random.data;
                        foreach (var item in arr)
                        {
                            queue.Enqueue(item);
                        }

                        minCount = 4000;

                    }
                }
                catch (Exception ex)
                {
                    ApplicationManager.Logger.Error("RandomORG error : ", ex);
                }
            });
        }

    }
}
