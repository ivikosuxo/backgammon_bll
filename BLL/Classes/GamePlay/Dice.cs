﻿using BLL.Managers;
using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.GamePlay
{
    public class Dice :IDice
    {
        public int First { get; set; }
        public int Second { get; set; }
        public bool IsDouble { get; set; } = false;


        public Dice()
        {

            First =  RandomGenerator.GetRandomNumber(1, 7);
            Second =   RandomGenerator.GetRandomNumber(1, 7);
            IsDouble = First == Second;

        }

        public static Dice GetFirstDice() {

            var dice = new Dice();
            if (dice.IsDouble)
            {
         
                return GetFirstDice();
            }
            return dice;

        }

     
     

        public JObject ToJson()
        {
            var jsn = new JObject()
            {
                ["first"] = First,
                ["second"] = Second,
                ["is_double"] = IsDouble,
            };


            return jsn;
        }

        public int getFreezeCounter()
        {
      
            if (IsDouble && (First == 3 || First == 4 || First == 6))
            {
                return 2;
            }

            return 1;

        }
        public int GetMaxPoint()
        {

            var result = First + Second;
            if (IsDouble)
            {
                result = result * 2;
            }
            return result;
        }


        // Return a random integer between a min and max value.

    }
}
