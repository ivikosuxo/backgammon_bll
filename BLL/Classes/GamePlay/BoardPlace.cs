﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.GamePlay
{
    public class BoardPlace
    {
        public int Color { get; set; } = 0;
        public int Count { get; set; } = 0;
        public bool IsBlocked { get; set; } = false;

        public int FreezeCount { get; set; } = 0;
        public JObject ToJson()
        {
            var sfs = new JObject()
            {
                ["color"] = Color,
                ["number"] = Count,
                ["block"] = IsBlocked,
                ["freeze_count"] = FreezeCount
            };
            return sfs;
        }

        public string SaveState()
        {
            return Color + "_" + Count + ",";
        }
    }


}
