﻿using InterfaceLibrary.Game;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.GamePlay
{
   public class Move : IMove
    {
        public bool Roll
        {
            get; set;
        } = false;
        public int StartPosition { get; set; } = 0;
        public int EndPosition { get; set; } = 0;
        public int OpKill { get; set; } = 0;
        public int MyKill { get; set; } = 0;

        public int OpDiscard { get; set; } = 0;
        public int MyDiscard { get; set; } = 0;
        public bool Killer { get; set; } = false;
        public int OpponentState { get; set; } = 0;
        public int State { get; set; } = 0;
        public int Value { get; set; } = 0;
        public int Action { get; set; } = 0;
        public bool Winner { get; set; } = false;
        public string BoardState { get; set; } = "";
        public int FreezeCount { get; set; } = 0;
        public bool IsRoyalDefense { get; set; } = false;
        public bool Blocked { get; set; } = false;

        public bool FirstRoll { get; set; } = false;

        public int RaiseX
        {
            get; set;
        } = 1;

        public JObject ToJson()
        {
            var jsn = new JObject();
            jsn["roll"] = Roll;
            jsn["start"]= StartPosition;
            jsn["end"] =EndPosition;
            jsn["kill"] =Killer;
            jsn["state"]= State;
            jsn["op_state"]= OpponentState;
            jsn["value"] =Value;
            jsn["action"]= Action;
            jsn["freeze"]= FreezeCount;
            return jsn;
        }
    }
}
