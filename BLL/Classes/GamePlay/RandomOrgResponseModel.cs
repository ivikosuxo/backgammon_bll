﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.GamePlay
{


    public class RandomOrgResponseModel
    {
        public string jsonrpc { get; set; }
        public Result result { get; set; }
        public int id { get; set; }
    }

    public class Result
    {
        public RandomModel random { get; set; }
        public int bitsUsed { get; set; }
        public int bitsLeft { get; set; }
        public int requestsLeft { get; set; }
        public int advisoryDelay { get; set; }
    }

    public class RandomModel
    {
        public int[] data { get; set; }
        public string completionTime { get; set; }
    }


}
