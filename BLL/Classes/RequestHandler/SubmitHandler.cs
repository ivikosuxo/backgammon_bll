﻿using BLL.Classes.GameClass;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
  public  class SubmitHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var id = session.GetRoomId();
            var plId = session.GetUserId();
            var room = roomManager.GetRoomById(id);
 
            room.Submit(plId, message);

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }
    }
}
