﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class TournamentTotalnfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var tourId = message["tournament_id"].Value<int>();
                var token = message["token"].Value<string>();
                token = Guid.Parse(token).ToString();
                var plId = playerManager.GetPlayerIDByToken(token);
                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
                var tournament = tournamentManager.GetTournamentById(tourId);
                var result = tournament.ToJson(plId);
                result["flow"] = tournament.GetFlow();
                gameSocketManager.SendMessage(session, CommandKeys.tournament_total_info, result);
                //get info about game
            }
            catch (Exception)
            {

                gameSocketManager.SendMessage(session, CommandKeys.error, new JObject() { ["id"] = ErrorCodes.UnexpectedToken, ["fatal"] = true });
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(
         () =>
         {
             Execute(session, message);
         });

        }
    }
}
