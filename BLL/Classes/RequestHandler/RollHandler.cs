﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Exceptions;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using BLL.Classes.GameClass;

namespace BLL.Classes.RequestHandler
{
   public class RollHandler :IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
     
          Execute(session, message);
          
        }

        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var data = new JObject();
            try
            {
                var id = session.GetRoomId();
                var plId = session.GetUserId();
                var room = roomManager.GetRoomById(id);
              
                room.Roll(plId);          
               
            }
            catch (GameException ex)
            {


                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());

            }
            catch (Exception)
            {

                gameSocketManager.SendMessage(session, CommandKeys.error, new UnknownException().ToJson());
            }

           

        }
    }
}
