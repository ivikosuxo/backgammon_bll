﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GetShopInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var shopManager = BaseApplication.Current.BaseShopManagerInstance;
            var data = shopManager.ToJson();
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(session, CommandKeys.shop_info, data);

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }
    }
}
