﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class RoundInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var roundId = message["round_id"].Value<int>();
            var historyManager = BaseApplication.Current.BaseHistoryManagerInstance;

            var result = historyManager.GetRoundInfo( roundId);

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(session, CommandKeys.round_data, result);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(
         () =>
         {
             Execute(session, message);
         });
        }
    }
}
