﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Exceptions;

namespace BLL.Classes.RequestHandler
{
    public class JoinRoomHandler: IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
               Execute(session,message);

            });
        }

        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
     
                var id = message["room_id"].Value<int>();
                var plId = session.GetUserId();
                var room = roomManager.GetRoomById(id);
                var player = playerManager.PlayerLogin(plId);
                room.AddUser(player.Info, session, message);
            }
            catch (IncorrectPasswordException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.incorrect_password, ex.ToJson());
            }
            catch (GameException ex)
            {

                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());             
            }
            catch (Exception ex)
            {
                var data = new JObject()
                {
                    ["id"] =ErrorCodes.UnknownError,
                    ["fatal"] = true
                };

                gameSocketManager.SendMessage(session, CommandKeys.error, data);
                ApplicationManager.Logger.Error($"JoinRoomHandler: session={session} , message={message}", ex);

            }
        }
    }
}
