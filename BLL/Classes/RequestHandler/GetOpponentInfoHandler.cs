﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GetOpponentInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var plId = 0;
            var roomId = 0;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {
                  plId = session.GetUserId();
                  roomId = session.GetRoomId();


                var room = roomManager.GetRoomById(roomId);
                var pl = room.GetOpponentInfo(plId);

               
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session,CommandKeys.opponent_info, pl);
             

            }
            catch (Exception ex) {

                ApplicationManager.Logger.Error($"GetOpponentInfoHandler : player_id={plId} , room_id={roomId}",ex);
            }
         

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
