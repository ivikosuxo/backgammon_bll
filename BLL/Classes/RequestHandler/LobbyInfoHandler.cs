﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Managers;
using  EntityModels.Types;
using SuperWebSocket;
using Newtonsoft.Json.Linq;

namespace BLL.Classes.RequestHandler
{
  public  class LobbyInfoHandler :IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

        public void Execute(string session, JObject message)
        {
            try
            {
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                var roomManager = BaseApplication.Current.RoomManagerInstance;
                var info= roomManager.LobbyRoom.GetRoomInfo();  
                info[CommandKeys.command] = CommandKeys.lobby_info;
                gameSocketManager.SendMessage(session, info.ToString());

                var createRoomData =ApplicationManager.GetBlockedRoomMessage();
                gameSocketManager.SendMessage(session,CommandKeys.can_create_room, info);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"LobbyInfoHandler: session={session} , data={message.ToString()}", ex);
            }


        }
    }
}
