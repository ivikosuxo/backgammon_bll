﻿using BLL.Classes.RequestHandler;
using  EntityModels.Exceptions;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
   public class TournamentUnregisterHandler :IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                var tournamentId = message["id"].Value<int>();
                var plId = session.GetUserId();
                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
         
                    var tournament = tournamentManager.GetTournamentById(tournamentId);
              
                tournament.RemovePlayerFromTournament(plId,true);
                var result = new JObject()
                {
                    ["id"] = tournamentId,
                    ["result"] = true

                };
                gameSocketManager.SendMessage(session, CommandKeys.tournaments_unregister, result);
            }
            catch (GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());

            }


        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
