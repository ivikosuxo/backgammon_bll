﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using  EntityModels.Types;
using BLL.Managers;

namespace BLL.Classes.RequestHandler
{
    public class AddWaitingListHandler : IRequestHandler
    {
    
        public void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;

            var id = message["room_id"].Value<int>();
            var plId = session.GetUserId();
            var room = roomManager.GetRoomById(id);
            var player = playerManager.PlayerLogin(plId);
            room.AddUserWaitingList(player.Info, session, message);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
