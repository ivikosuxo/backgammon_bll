﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Types;
using BLL.Extensions;
using BLL.Managers;
using BLL.Classes.GameClass;

namespace BLL.Classes.RequestHandler
{
    public class UndoRequestHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var id = session.GetRoomId();
            var plId = session.GetUserId();
            var room = roomManager.GetRoomById(id);     
            room.Undo(plId, message); 
        }

        public void HandlerRequest(string session, JObject message)
        {
            
                Execute(session, message);
     
        }
    }
}
