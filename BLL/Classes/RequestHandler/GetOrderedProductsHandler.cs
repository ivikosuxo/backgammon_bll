﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using BLL.Extensions;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GetOrderedProductsHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
             var id = session.GetUserId();
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var jsn = playerManager.GetPlayerOrderProducts(id);
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(session,CommandKeys.order_products,jsn);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }
    }
}
