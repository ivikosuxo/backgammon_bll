﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
 
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class AchievementInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {

            var achievementManager = BaseApplication.Current.AchievementManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            var plId = session.GetUserId();          
            var player = playerManager.PlayerLogin(plId);
            var data = new JObject();
            data["rank"] = player.Info.Rank;
            data["achievements"] = achievementManager.AchievementsToJson(player.Info.Rank);
            data["my_achievements"]=player.Info.GetAchievementsToJson();

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(session, CommandKeys.achievement_info, data);

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
