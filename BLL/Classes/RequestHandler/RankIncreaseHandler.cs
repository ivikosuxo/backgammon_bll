﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using  EntityModels.Exceptions;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using InterfaceLibrary.Game;

namespace BLL.Classes.RequestHandler
{
    public class RankIncreaseHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            IPlayerInfo info=null;

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var plId = session.GetUserId();
                  info = playerManager.GetPlayerInfoById(plId);


                BaseApplication.Current.PlayerManagerInstance.PlayerRepository.RankIncrease(info);

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.UpdateCoins(info.PlayerId, info.Coin);
                gameSocketManager.SendMessage(session,CommandKeys.rank_increase,new JObject() {["rank"]=info.Rank });
            }
            catch (NotEnoughGameCountException ex)
            {
                ex.Fatal = false;


                var json = ex.ToJson();
                json["game_count"] = info.GameCount;
                gameSocketManager.SendMessage(session, CommandKeys.rank_increase_error, json);
            }
            catch (GameException ex)
            {
                ex.Fatal = false;
                var json = ex.ToJson();

                gameSocketManager.SendMessage(session, json.ToString());
            }
         
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
