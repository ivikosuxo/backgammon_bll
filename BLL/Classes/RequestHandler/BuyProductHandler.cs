﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Exceptions;
using BLL.Managers;
using  EntityModels.Types;
using BLL.Extensions;

namespace BLL.Classes.RequestHandler
{
    public class BuyProductHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            try
            {
                var playerId = session.GetUserId();
                var count = Math.Abs(message["count"].Value<int>());
                var productId = message["product_id"].Value<int>();
                if (count==0)
                {
                    count = 1;
                }

                var shopManager = BaseApplication.Current.BaseShopManagerInstance;
                shopManager.BuyProduct(session , playerId, count, productId);


            }
            catch(GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.buy_product, ex.ToJson());
            }
            catch(Exception ex)
            {
                ApplicationManager.Logger.Error($"BuyProductHandler  data-{message}",ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
