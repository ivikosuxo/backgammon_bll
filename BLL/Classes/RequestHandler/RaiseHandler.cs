﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using BLL.Managers;
using BLL.Classes.GameClass;
using  EntityModels.Types;
using  EntityModels.Exceptions;

namespace BLL.Classes.RequestHandler
{
    public class RaiseHandler :IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

       public   void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;

            var plId = session.GetUserId();
            var roomId = session.GetRoomId();
            var answer = message["answer"].Value<int>();
            var room = roomManager.GetRoomById(roomId);
         
            try
            {
                room.RaiseAccept(plId, message);
            }
            catch(CantRaiseException)
            {
                gameSocketManager.SendMessage(session, CommandKeys.cant_raise, new JObject());
            }
            catch(Exception ex)
            {
                ApplicationManager.Logger.Error($"RaiseHandler message={message}", ex);
            }
          
        
        } 
     
    }
}
