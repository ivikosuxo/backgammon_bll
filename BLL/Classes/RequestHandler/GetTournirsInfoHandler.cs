﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using  EntityModels.Types;
using BLL.Managers;

namespace BLL.Classes.RequestHandler
{
    public class TournamentInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {

            var plId = session.GetUserId();
            var tournamentManager = BaseApplication.Current.TournamentManagerInstance;

            var data = tournamentManager.GetTournamentsInfo(plId);

            var roomManager = BaseApplication.Current.RoomManagerInstance;
            roomManager.LobbyRoom.SendMessage(plId, CommandKeys.tournaments_info, data);

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }
    }
}
