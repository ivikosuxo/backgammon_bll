﻿using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
 public   class JoinLobbyHandler :IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }

        public void Execute(string session, JObject message)
        {
            try
            {
                // var id = message["room_id"].Value<int>();
                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                var plId = session.GetUserId();             
                var player = playerManager.PlayerLogin(plId);

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                roomManager.LobbyRoom.AddUser(player, session,message);
                playerManager.PlayerJoin(player.Info);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"JoinLobbyHandler: session={session} , data={message.ToString()}", ex);

            }
        }
    }
}
