﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using BLL.Extensions;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    class PlaySeveralMoves : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {
                var id = session.GetRoomId();
                var plId = session.GetUserId();
                var room = roomManager.GetRoomById(id);

                room.PlaySeveralMoves(plId, message);
                //get info about game
            }
            catch(Exception)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, new JObject() { ["id"] = ErrorCodes.UnexpectedToken, ["fatal"] = true });
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
