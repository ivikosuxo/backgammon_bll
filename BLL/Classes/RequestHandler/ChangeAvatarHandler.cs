﻿using BLL.Classes.Entities;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
  public  class ChangeAvatarHandler:IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

        public void Execute(string session, JObject message)
        {
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var avatarId = Math.Abs(message["avatar_id"].Value<int>());
                avatarId = avatarId > 0 ? avatarId : 1; 
                var socket = session.GetGameSocket();
                playerManager.ChangeAvatar( socket.PlayerId, avatarId);
                var result = new JObject() {
                    ["success"]=true
                };
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session, CommandKeys.avatar_change, result);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"ChangeAvatarHandler: session={session} , data={message.ToString()}", ex);
            }

        }
    }
}
