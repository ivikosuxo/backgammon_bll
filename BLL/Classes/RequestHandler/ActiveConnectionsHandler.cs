﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Types;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Exceptions;

namespace BLL.Classes.RequestHandler
{
    public class ActiveConnectionsHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {
                var plId = session.GetUserId();
               
                var connections = gameSocketManager.Connections.Where(e => e.PlayerId == plId &&e.RoomId!=0).ToList();

                var result = new JObject();
                var arr = new JArray();

                connections.ForEach(e=>
                {
                    var roomName = roomManager.GetRoomById(e.RoomId).GetRoomName();
                    var data = new JObject()
                    {
                        ["room_id"] = e.RoomId,
                        ["tournament_id"] = e.TournamentId,
                        ["name"] = roomName
                    };
                    arr.Add(data); 
                });
                result["rooms"] = arr;
 
                gameSocketManager.SendMessage(session, CommandKeys.active_connections, result);
            }
            catch(GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson()); 
            }   
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
