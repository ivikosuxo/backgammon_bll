﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Types;
using BLL.Extensions;
using  EntityModels.Exceptions;
using BLL.Managers;

namespace BLL.Classes.RequestHandler
{
    public class TournamentPreJoinHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var data = new JObject();
            try
            {
                var id = message["room_id"].Value<int>();
               
                var plId = session.GetUserId();

                var roomManager = BaseApplication.Current.RoomManagerInstance;
                var room = roomManager.GetRoomById(id);
                if (room==null)
                {
                    throw new RoomNotExistsException();
                }

                data = room.GetRoomInfo();
                data["reconnect"] = room.IsReconnect(plId);
                data["command"] = CommandKeys.pre_join_room;
                gameSocketManager.SendMessage(session, data.ToString());
            }
            catch (GameException ex)
            {
                gameSocketManager.SendMessage(session,CommandKeys.error,ex.ToJson());

            }
            catch (Exception)
            {

                gameSocketManager.SendMessage(session, CommandKeys.error, new UnknownException().ToJson());
            }

        
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }
    }
}
