﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Exceptions;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GameHistoryRequestHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try {
                var token = message["token"].Value<string>();
                var plId = playerManager.GetPlayerIDByToken(token);
                var result = new JObject();

                var historyRepository = BaseApplication.Current.BaseHistoryManagerInstance.HistoryDbInstance;
                result["rows"] = historyRepository.GetAllGamesInfo(plId);
                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session,CommandKeys.game_history,result);
            }
            catch (GameException ex)
            {
                ApplicationManager.Logger.Error($"GameHistoryRequestHandler message={message}",ex);
            }
            catch (Exception ex) {
                ApplicationManager.Logger.Error($"GameHistoryRequestHandler message={message}", ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
