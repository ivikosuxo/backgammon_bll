﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Classes.Entities;
using Ganss.XSS;
using BLL.Managers;
using  EntityModels.Exceptions;
using  EntityModels.Types;
using BLL.Extensions;
using EntityModels.Entities;

namespace BLL.Classes.RequestHandler
{
    public class CreateRoomHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var result = new JObject();
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {
            
                 
                var maxBet = message["max_bet"].Value<int>();
                var raiseCount = maxBet;
                var model = new CreateRoomModel();

                model.MinBet = message["min_bet"].Value<decimal>();
                model.MaxPoint = message["max_point"].Value<int>();
                model.RaiseCount = raiseCount;
                model.BeaverRule = message["beaver"].Value<bool>();
                model.CrawfordRule = message["crawford"].Value<bool>();
                model.DoubleRule = message["double"].Value<bool>();
                model.JacobRule = message["jacob"].Value<bool>();
                model.RuleType = message["type"].Value<int>();
                model.Password = message["password"].Value<string>().Replace(" ", "");
                model.WithDouble = message["with_double"].Value<bool>();


                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedCssClasses?.Clear();
                sanitizer.AllowedAtRules?.Clear();
                sanitizer.AllowedAttributes?.Clear();
                sanitizer.AllowedCssProperties?.Clear();
                sanitizer.AllowedSchemes?.Clear();
                sanitizer.AllowedTags?.Clear();
                //sanitize password
                model.Password = sanitizer.Sanitize(model.Password);


                model.Speed = message["speed"].Value<int>();

                model.CreatorId = session.GetUserId();
                model.ChatEnabled = message["chat_enabled"].Value<bool>();


                result = roomManager.CreateRoom(model);
            }
            catch (GameException ex)
            {
                result["id"] = ex.ErrorId;
                result["room_id"] = 0;
            }

            catch (Exception)
            {
                result["id"] = ErrorCodes.UnexpectedToken;
                result["room_id"] = 0;
            }

            result["token"] = message["token"].Value<string>();

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            gameSocketManager.SendMessage(session, CommandKeys.create_room, result);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
