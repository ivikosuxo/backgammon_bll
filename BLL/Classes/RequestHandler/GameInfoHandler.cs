﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GameInfoHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
             
            try {
                var gameId = message["game_id"].Value<int>();
                var token = message["token"].Value<string>();
                token = Guid.Parse(token).ToString();

                var historyManager = BaseApplication.Current.BaseHistoryManagerInstance;
                var result = historyManager.GetGameInfo(token,gameId);

                gameSocketManager.SendMessage(session, CommandKeys.game_history, result);
                //get info about game
            }
            catch (Exception)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, new JObject() { ["id"] = ErrorCodes.UnexpectedToken, ["fatal"] = true });
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(
         () =>
         {
             Execute(session, message);
         });
        }
    }
}
