﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using BLL.Extensions;
using BLL.Classes.GameClass;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class GiveUpHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var plId = session.GetUserId();
            var roomId = session.GetRoomId();
            var   room = roomManager.GetRoomById(roomId);
 
            room.GiveUp(plId, message);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(
           () =>
           {
               Execute(session, message);
           });
        }
    }
}
