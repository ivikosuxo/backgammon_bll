﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using BLL.Extensions;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    class PingLatencyHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var plId = session.GetUserId();
                var pl = playerManager.GetPlayerById(plId);
                if(pl != null)
                {
                    pl.Ping();  
                }
                var sendTime = message["date"].Value<long>();
                var startTime = new DateTime(1970, 1, 1);
                var now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - startTime.Ticks / TimeSpan.TicksPerMillisecond;
                var data = new JObject(){ 
                    ["server_time"]=now,
                    ["send_time"] = sendTime,
                    ["ping"] = (now- sendTime)/20000
                };
                gameSocketManager.SendMessage(session,CommandKeys.ping, data);
            }
            catch(Exception ex)
            {
                ApplicationManager.Logger.Error($"PingLatencyHandler: session={session} , data={message.ToString()}", ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Execute(session, message);

        }
    }
}
