﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class ChangeStoneColor : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var stoneColor = Math.Abs(message["color"].Value<int>());
                stoneColor = stoneColor >0 ? 1 : 0;
                var socket = session.GetGameSocket();
              
               
                var pl = playerManager.GetPlayerById(socket.PlayerId);
                pl.Info.StoneColor = stoneColor;
                playerManager.ChangeStone(socket.PlayerId, stoneColor);
                var result = new JObject()
                {
                    ["success"] = true
                };

                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session, CommandKeys.change_stone, result);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"ChangeStoneColor: session={session} , data={message.ToString()}", ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
