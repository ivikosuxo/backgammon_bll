﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using Ganss.XSS;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class ProblemReportHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomDBManager = BaseApplication.Current.RoomManagerInstance.RoomDbManager;
            var result = new JObject();
            try
            { 

                var token = message["token"].Value<string>();
                var problem = message["text"].Value<string>();
                var length = problem.Length;
                length = length > 512 ? 512 : length;
                problem = problem.Substring(0, length);
                var roomId = message["room_id"].Value<int>();
                var gameId = message["game_id"].Value<int>();
                var roundId = message["round_id"].Value<int>();

                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedCssClasses?.Clear();
                sanitizer.AllowedAtRules?.Clear();
                sanitizer.AllowedAttributes?.Clear();
                sanitizer.AllowedCssProperties?.Clear();
                sanitizer.AllowedSchemes?.Clear();
                sanitizer.AllowedTags?.Clear();
                var problemSanitized = sanitizer.Sanitize(problem);

                result["success"] = roomDBManager.ProblemReport(roomId, gameId, roundId, token, problemSanitized);
            }
            catch (Exception)
            { 
                result["success"] = false;
            }
            gameSocketManager.SendMessage(session, CommandKeys.problem_report, result);
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
