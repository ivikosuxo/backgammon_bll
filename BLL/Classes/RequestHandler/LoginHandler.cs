﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Managers;
using Newtonsoft.Json.Linq;
using SuperWebSocket;

namespace BLL.Classes.RequestHandler
{
    public class LoginHandler : IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(
                () =>
                {
                    Execute(session, message);
                }
        );
        }

        public void Execute(string session, JObject message)
        {
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            playerManager.Login(session, message);
        }
    }
}
