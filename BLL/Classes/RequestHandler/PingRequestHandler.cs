﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Managers;
using BLL.Extensions;

namespace BLL.Classes.RequestHandler
{
    public class PingRequestHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            try
            {
                var playerManager = BaseApplication.Current.PlayerManagerInstance;
                var plId = session.GetUserId() ;
                var pl = playerManager.GetPlayerById(plId);
                if (pl!=null)
                {
                    pl.Ping();
                }               
            } catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"PingRequestHandler: session={session} , data={message}", ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Execute(session,  message);

        }
    }
}
