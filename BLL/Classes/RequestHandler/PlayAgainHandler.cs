﻿using BLL.Extensions;
using BLL.Managers;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
   public class PlayAgainHandler :IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

        public void Execute(string session, JObject message)
        {
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var plId = session.GetUserId();
            var roomId = session.GetRoomId();         
            var room = roomManager.GetRoomById(roomId);
            room.PlayAgain(plId);

        }

    }
}
