﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Classes.Entities;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;

namespace BLL.Classes.RequestHandler
{
   public class RoomInfoHandler:IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

        public void Execute(string session, JObject message)
        {
            try
            {
                var roomManager = BaseApplication.Current.RoomManagerInstance;
                var roomId = message["room_id"].Value<int>();
                var room = roomManager.GetRoomById(roomId);
                var info =  room.GetRoomInfo();
                info[CommandKeys.command] = CommandKeys.room_info;

                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session, info.ToString());

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"RoomInfoHandler: session={session} , data={message.ToString()}", ex);
            }

             
        }
    }
}
