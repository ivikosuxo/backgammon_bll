﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;

namespace BLL.Classes.RequestHandler
{
    public class WaitingAcceptHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            try
            {
                var roomManager = BaseApplication.Current.RoomManagerInstance;
                var playerManager = BaseApplication.Current.PlayerManagerInstance;

                var room_id = session.GetRoomId();
                var plId = session.GetUserId();
                var room = roomManager.GetRoomById(room_id);
                var player = playerManager.PlayerLogin(plId);
                var accept = message["accept"].Value<bool>();

                if(accept)
                {
                    room.AcceptOpponent(session, message);
                }
                else
                {
                    room.DeclineOpponent(session, message);
                }
              
            } catch(Exception)
             {

             }
    
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
