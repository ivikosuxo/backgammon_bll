﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Ganss.XSS;
using  EntityModels.Exceptions;
using InterfaceLibrary.Game;

namespace BLL.Classes.RequestHandler
{
    public class ChatRequestHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            IPlayerInfo info = null;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var gs = session.GetGameSocket();
                var room = roomManager.GetRoomById(gs.RoomId);
                var player = playerManager.GetPlayerById(gs.PlayerId);
                info = player.Info;
                if (player.Info.ChatBan)
                {
                    throw new PlayerIsBannedException();
                }
                var returnMessage = new JObject();
                var chatMessage = message["text"].Value<string>();
                returnMessage["player_id"] = gs.PlayerId;
                returnMessage["user_name"] = player.Info.Username;

 

 


                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedCssClasses?.Clear();
                sanitizer.AllowedAtRules?.Clear();
                sanitizer.AllowedAttributes?.Clear();
                sanitizer.AllowedCssProperties?.Clear();
                sanitizer.AllowedSchemes?.Clear();
                sanitizer.AllowedTags?.Clear();
                var problemSanitized = sanitizer.Sanitize(chatMessage);
                problemSanitized= problemSanitized.Substring(0, 512);
                returnMessage["text"] = problemSanitized;
                room.SendMessageAll(CommandKeys.message, returnMessage);


                var game_id = room.GetGameId();
                

                roomManager.SaveChatMessage(gs.RoomId, gs.PlayerId, game_id, problemSanitized);
            }
            catch (PlayerIsBannedException) {
                var returnMessage = new JObject();  

                returnMessage["player_id"] = 0;
                returnMessage["user_name"] = "system";
                returnMessage["text"] = "თქვენი ჩატი დაბლოკილია";

                var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
                gameSocketManager.SendMessage(session,CommandKeys.message, returnMessage);
            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"ChatRequestHandler: message={message.ToString()}", ex);
            }
        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
