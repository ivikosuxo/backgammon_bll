﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using BLL.Extensions;
using  EntityModels.Types;
using BLL.Managers;
using  EntityModels.Exceptions;

namespace BLL.Classes.RequestHandler
{
    public class PreJoinRoomHandler : IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);

            });
        }

        public void Execute(string session, JObject message)
        {
            var data = new JObject();

            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;
            try
            {
                var id = message["room_id"].Value<int>();
                var plId = session.GetUserId();
                var room = roomManager.GetRoomById(id);

                if(room==null)
                {
                    throw new RoomNotExistsException();
                }

                  data = room.GetRoomInfo();
                  data["reconnect"] = room.IsReconnect(plId);
                  data["command"] = CommandKeys.pre_join_room;
                gameSocketManager.SendMessage(session, data.ToString());
            }
            catch (GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());
            }
            catch (Exception)
            {

                gameSocketManager.SendMessage(session, CommandKeys.error, new UnknownException().ToJson());
            }

           
        }
    }
}
