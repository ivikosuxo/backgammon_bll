﻿using Newtonsoft.Json.Linq;
using SuperWebSocket;

namespace BLL.Classes.RequestHandler
{
  public  interface IRequestHandler
    {
          void HandlerRequest(string session, JObject message);
          void Execute(string session, JObject message);
    }
}
