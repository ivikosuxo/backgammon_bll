﻿using BLL.Classes.GameClass;
using  EntityModels.Exceptions;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Types;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Classes.RequestHandler
{
    public class RaisePressHandler : IRequestHandler
    {
        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }

        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var roomManager = BaseApplication.Current.RoomManagerInstance;

            try
            {
                var plId = session.GetUserId();
                var roomId = session.GetRoomId();
                var room = roomManager.GetRoomById(roomId);

                room.RaisePress(plId, false);
            }
            catch (CantRaiseException)
            {
                gameSocketManager.SendMessage(session, CommandKeys.cant_raise, new JObject());

            }
            catch (Exception ex)
            {
                ApplicationManager.Logger.Error($"RaisePressHandler: session={session} , data={message.ToString()}", ex);
            }

        }



    }
}
