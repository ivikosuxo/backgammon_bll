﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SuperWebSocket;
using  EntityModels.Types;
using BLL.Extensions;
using BLL.Managers;
using  EntityModels.Exceptions;

namespace BLL.Classes.RequestHandler
{
    public class TournamentRegisterHandler : IRequestHandler
    {
        public void Execute(string session, JObject message)
        {
            var gameSocketManager = BaseApplication.Current.BaseGameSocketManagerInstance;
            var playerManager = BaseApplication.Current.PlayerManagerInstance;
            try
            {
                var tournamentId = message["id"].Value<int>();
                var plId = session.GetUserId();

                var tournamentManager = BaseApplication.Current.TournamentManagerInstance;
                var tournament = tournamentManager.GetTournamentById(tournamentId);
                var player = playerManager.GetPlayerById(plId);
                tournament.AddPlayerToTournament(player.Info);
                var result = new JObject()
                {
                    ["id"] = tournamentId,
                    ["state"]= (int)tournament.State,
                    ["result"] = true

                };
                gameSocketManager.SendMessage(session, CommandKeys.tournaments_register, result);
            }
            catch (GameException ex)
            {
                gameSocketManager.SendMessage(session, CommandKeys.error, ex.ToJson());
                
            }
    

        }

        public void HandlerRequest(string session, JObject message)
        {
            Task.Factory.StartNew(() =>
            {
                Execute(session, message);
            });
        }
    }
}
